\score {
  \new GrandStaff \with { instrumentName = #(*instrument-name*) } <<
    \new Staff <<
      \global \keepWithTag #'violino1 \includeNotes "violini"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'violino2 \includeNotes "violini"
    >>
  >>
  \layout {
    #(define indent (if (markup? (*instrument-name*))
                        largeindent
                        smallindent))
    \context {
      \Staff
      \override StaffSymbol.staff-space = #(magstep -1)
      fontSize = #-1
    }
  }
}