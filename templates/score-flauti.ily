\score {
  \new GrandStaff \with { instrumentName = #(*instrument-name*) } <<
    \new Staff <<
      \global \keepWithTag #'flauto1 \includeNotes "flauti"
    >>
    \new Staff <<
      \global \keepWithTag #'flauto2 \includeNotes "flauti"
    >>
  >>
  \layout {
    #(define indent (if (markup? (*instrument-name*))
                        largeindent
                        smallindent))
    \context {
      \Staff
      \override StaffSymbol.staff-space = #(magstep -1)
      fontSize = #-1
    }
  }
}