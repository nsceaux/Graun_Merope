\score {
  \new GrandStaff \with { instrumentName = #(*instrument-name*) } <<
    \new Staff <<
      \global \includeNotes "violino1"
    >>
    \new Staff <<
      \global \includeNotes "violino2"
    >>
  >>
  \layout {
    #(define indent (if (markup? (*instrument-name*))
                        largeindent
                        smallindent))
    \context {
      \Staff
      \override StaffSymbol.staff-space = #(magstep -1)
      fontSize = #-1
    }
  }
}