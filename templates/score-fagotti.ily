\score {
  \new GrandStaff \with { instrumentName = "Fagotti" } <<
    \new Staff <<
      \global \keepWithTag #'fagotto1 \includeNotes "bassi"
    >>
    \new Staff <<
      \global \keepWithTag #'fagotto2 \includeNotes "bassi"
    >>
  >>
  \layout { indent = \largeindent }
}
