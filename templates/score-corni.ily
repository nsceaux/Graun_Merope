\score {
  \new GrandStaff \with { instrumentName = #(*instrument-name*) }
  <<
    \new Staff <<
      \keepWithTag #'corni \global
      \keepWithTag #'corno1 \includeNotes "corni"
    >>
    \new Staff <<
      \keepWithTag #'corni \global
      \keepWithTag #'corno2 \includeNotes "corni"
    >>
  >>
  \layout { indent = \largeindent }
}