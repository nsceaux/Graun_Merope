\include "common.ily"

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Merope" }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markup\null\pageBreak
  \markuplist
  \abs-fontsize-lines #8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
%% Musique

%% 0-1
\ouverture "Sinfonia"
\includeScore "AAAsinfonia"
\includeScore "AABlarghetto"
\includeScore "AACallegro"

%%%
%%% ATTO PRIMO
%%%
\newBookPart #'(bassi corni fagotti flauti viola violino1 violino2)
\act "ATTO PRIMO"
\scene "Scena Prima" "Scena I"
%% 1-1
\pieceToc\markup\wordwrap {
  Aria. Polifonte: \italic { Di questo aciaro il lampo }
}
\includeScore "AAGpolifonte"

\scene "Scena Terza" "Scena III"
%% 1-2
\pieceToc\markup\wordwrap {
  Aria. Egisto: \italic { Bel desio m’accese il core }
}
\includeScore "ACBegisto"

\scene "Scena Ottava" "Scena VIII"
%% 1-3
\pieceToc\markup\wordwrap {
  Recit. Merope: \italic { Quanto è barbaro oh stelle! il mio destino! }
}
\includeScore "AHAmerope"
%% 1-4
\pieceToc\markup\wordwrap {
  Aria. Merope: \italic { Quando a fronte di un periglio }
}
\includeScore "AHBmerope"

\scene "Scena Nona" "Scena IX"
%% 1-5
\pieceToc\markup\wordwrap {
  Aria. Narbace: \italic { Ah che dovunque movo }
}
\includeScore "AIBnarbace"

\scene "Scena Duodecima" "Scena XII"
%% 1-6
\pieceToc\markup\wordwrap {
  Arioso. Egisto: \italic { Sì, ti lascio, o Padre mio! }
}
\includeScore "ALBegisto"

\scene "Scena Decimaquarta" "Scena XIV"
%% 1-7
\pieceToc\markup\wordwrap {
  Aria. Polifonte: \italic { A consolar quel duolo }
}
\includeScore "ANBpolifonte"

%%%
%%% ATTO SECONDO
%%%
\newBookPart #'()
\act "ATTO SECONDO"
\scene "Scena Seconda" "Scena II"
%% 2-1
\pieceToc\markup\wordwrap {
  Aria. Egisto, Merope: \italic { Cessa omai di quel Tiranno }
}
\includeScore "BBBduo"

%% 2-2
\pieceToc\markup\wordwrap {
  Aria. Polifonte: \italic { Arbitra della sorte }
}
\includeScore "BBDpolifonte"

\scene "Scena Quarta" "Scena IV"
%% 2-3
\pieceToc\markup\wordwrap {
  Aria. Narbace: \italic { Alle vendetti i Numi }
}
\includeScore "BDBnarbace"

\scene "Scena Quinta" "Scena V"
%% 2-4
\pieceToc\markup\wordwrap {
  Recit. Merope: \italic { Ebben: sì dagli stessi }
}
\includeScore "BEArecit"
\newBookPart #'(bassi)
%% 2-5
\pieceToc\markup\wordwrap {
  Aria. Merope: \italic { M’opprime un mostro indegno }
}
\includeScore "BEBmerope"

%%%
%%% ATTO TERZO
%%%
\newBookPart #'(bassi corni fagotti flauti oboi viola)
\act "ATTO TERZO"
\scene "Scena Quarta" "Scena IV"
%% 3-1
\pieceToc\markup\wordwrap {
  Aria. Merope: \italic { Par che lo scorga il Cielo }
}
\includeScore "CDBmerope"

\scene "Scena Quinta" "Scena V"
%% 3-2
\pieceToc\markup\wordwrap {
  Aria. Narbace: \italic { Mille ogetti di terrore }
}
\includeScore "CEBnarbace"

\scene "Scena Sesta" "Scena VI"
%% 3-3
\pieceToc\markup\wordwrap {
  Coro: \italic { Del mortal, tu, che sei l’arbitra }
}
\includeScore "CFBcoro"

\scene "Scena Settima" "Scena VII"
%% 3-4
\pieceToc\markup\wordwrap {
  Recit. Egisto, Popolo: \italic { Amici ancora dubitar potrete }
}
\includeScore "CGArecit"
%% 3-5
\pieceToc\markup\wordwrap {
  Aria. Egisto: \italic { Se alle tempeste in seno }
}
\includeScore "CGBegisto"

%% 3-6
\pieceToc\markup\wordwrap {
  Coro: \italic { Felice può dirsi, può dirsi beato }
}
\includeScore "CGCcoro"
