\livretAct ATTO PRIMO
\livretScene Scena Prima
\livretDescAtt\column {
  \wordwrap-center { Atrio del Palazzo Reale. }
  \wordwrap-center { Polifonte, Eroce. }
}
\livretPers Polifonte
\livretRef #'AAGpolifonte
%#~ Di questo aciaro il lampo
%#~ Mi puo guidare al soglio
%#~ Ma sorpassare io voglio
%#~ L’arte d’un vicitor.

\livretScene Scena Terza
\livretDescAtt\wordwrap-center {
  Egisto incatenato fra le Guardie, e suddetti.
}
\livretPers Egisto
\livretRef #'ACBegisto
%#~ Bel desio m’accese il core
%#~ Di tentar le tue vendette;
%#~ Fra perigli il mio valore
%#~ Sol mostrar volea per te.
%#~ Il languire in ozio vile
%#~ Di mia età mi parve indegno.
%#~ Ruppe il Cielo il mio disegno,
%#~ E colpevole mi fe.

\livretScene Scena Ottava
\livretDescAtt\wordwrap-center {
  Merope, Ismenia, Ericle.
}
\livretPers Merope
\livretRef #'AHAmerope
%# Quanto è barbaro oh stelle! il mio destino!
%# Osa un tiranno indegno
%# Oltraggiarmi a tal segno?
%# Non sarà mai compito
%# Un nodo sì abborrito…
%# Sì, l’infame uccisor del caro figlio
%# Pria svenerà questa mia destra ardita,
%# Quindi a me stessa troncherà la vita.
%# Ogni Nume del Ciel pur mi si mostri
%# A voglia sua cruccioso;
%# Posso ancor vendicare e figlio, e sposo.
%# Ch’io alle funeree faci
%# Le faci d’Imeneo volessi unire?
%# Ch’io potessi levare il mesto ciglio
%# Verso quel Ciel, che più non vede il figlio?
%# Gettata ò la mia sorte. Sì, quest alma
%# Tranquilla omai, poichè ai disastri è avezza,
%# Li prevede, li insulta, e li disprezza.
\livretRef #'AHBmerope
%#~ Quando a fronte di un periglio
%#~ Ogni speme è alfin smarrita
%#~ È viltà bramar la vita;
%#~ E virtù saper morir.
%#~ Or che giace estinto il figlio,
%#~ Or che vedo al soglio eletto
%#~ Un indegno, un vil soggetto,
%#~ Che mi resta da soffrir?

\livretScene Scena Nona
\livretDescAtt\column {
  \wordwrap-center {
    Piazza con Tempio accanto all’ ingresso del quale si vedrà il Mausoleo
    di Cresfonte.
  }
  \wordwrap-center { Narbace solo. }
}
\livretPers Narbace
\livretRef #'AIBnarbace
%#~ Ah che dovunque movo,
%#~ Misero! il piè tremante,
%#~ Sento, che ad ogni instante
%#~ Raddoppia in me l'orror!
%#~ Se fia, che al suo tiranno
%#~ Il mio signor soccomba,
%#~ Chiudami omai la tomba;
%#~ Vissi purtroppo ancor!

\livretScene Scena Duodecima
\livretDescAtt\wordwrap-center {
  Merope, Ismenia, Ericle, Egisto incatenato,
  Guardie, Sacrificatori, poi Narbace.
}
\livretPers Egisto
\livretRef #'ALBegisto
%#~ Sì, ti lascio, o Padre mio!
%#~ Prendi omai l’estremo addio:
%#~ Del destin tal’ è il rigor.
%#~ Fuggi o Dio! lochi sì orrendi,
%#~ Ma giustifica, difendi
%#~ L’innocenza del mio cor.

\livretScene Scena Decimaquarta
\livretDescAtt\column {
  \wordwrap-center { Polifonte col suo Seguito. }
  \wordwrap-center { Merope, Ismenia, Erick. }
}
\livretPers Polifonte
\livretRef #'ANBpolifonte
%#~ A consolar quel duolo,
%#~ A vendicarti intesa
%#~ D’amor quest’ alma accesa
%#~ Tutto per te farà.
%#~ Del mio potere a parte
%#~ Vieni sul trono, e regna;
%#~ E di giurar ti degna
%#~ La mia felicità.
\sep
\column-break
