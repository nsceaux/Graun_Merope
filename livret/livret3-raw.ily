\livretAct ATTO TERZO
\livretScene Scena Quarta
\livretDescAtt\wordwrap-center {
  Merope, Egisto, Narbace, Ericle.
}
\livretPers Merope
\livretRef #'CDBmerope
%#~ Par, che lo scorga il Cielo,
%#~ Che fausto alfin gli arride.
%#~ Ecco il figliuol d’Alcide:
%#~ No, più mortal non è.
%#~ Vieni, mio dolce Figlio,
%#~ Corriamo uniti al Tempio.
%#~ Desta sì grand’ esempio
%#~ Tutto il coraggio in me.
\livretDidasP\wordwrap {
  (partono insieme seguiti dalle Guardie.)
}

\livretScene Scena Quinta
\livretDescAtt\wordwrap-center {
  Narbace, Ericle.
}
\livretPers Narbace
\livretRef #'CEBnarbace
%#~ Mille ogetti di terrore
%#~ O presenti al mesto sguardo:
%#~ Fra la speme, e fra il timore
%#~ Son costretto a palpitar.
%#~ Sol conforta i sense miei
%#~ Quel fidar con fermo core,
%#~ Che vorran gli eterni Dei
%#~ L’innocenza sollevar.
\livretDidasP\wordwrap { (Partono.) }

\livretScene Scena Sesta
\livretDescAtt\column {
  \wordwrap-center {
    Interno di un Tempio con Ara in mezzo.
  }
  \wordwrap-center {
    Merope, Polifonte, Ismenia, Eroce.
    Sacerdoti, Popolo, Guardie, poi Egisto.
  }
}
\livretPers Coro
\livretRef #'CFBcoro
%#~ Del mortal, tu, che sei l’arbitra,
%#~ O divina Providenza,
%#~ Compi omai la tua grand opera
%#~ L’innocenza – in sostener.
\livretDidasP\wordwrap {
  (Il Coro non è ripetuto. Tutti partono, e
  siegue frattanto una strepitose Sinfonia.)
}

\livretScene Scena Settima
\livretDescAtt\column {
  \wordwrap-center {
    Gran Piazza di Messene, con veduta di buona parte della Città.
  }
  \wordwrap-center {
    Merope, Ismenia indi Egisto, e poi Narbace.
  }
  \wordwrap-center {
    Sacerdoti, Popolo, Soldati. Si vede in lontano strascinato
    il corpo di Polifonte coperto però di un tapeto.
  }
}
\livretPers Egisto
\livretRef #'CGArecit
%# Amici ancora dubitar potrete
%# Del core d’una Madre,
%# D’un figlio, che da lei viene difeso,
%# E che del Padre è alle vendette inteso?
\livretPers Popolo
%# Nella tua gloria, e più nel nostro amore
%# Vieni il frutto a gustar del tuo valore.
\livretPers Egisto
%# Non è mia questa gloria, ella è dei Numi.
%# Ogni destin felice
%# Parte sol di lor mano,
%# E virtù ne discende in petto umano.
\livretDidasP\wordwrap { (a Narb.) }
\livretRef #'CGBegisto
%#~ Se alle tempeste in seno
%#~ Fosti mio Padre amante,
%#~ Ora ch’è il ciel sereno
%#~ Sarò tuo figlio ancor.
\livretDidasP\wordwrap { (a Merope.) }
%#~ Vieni a divider meco,
%#~ Madre adorata, il soglio;
%#~ Io meritar non voglio
%#~ Che il tuo materno amor.
\livretPers Coro
\livretRef #'CGCcoro
%#~ Felice può dirsi, può dirsi beato
%#~ Quel Popol soggeto d’un giusto Sovrano
%#~ Fra mille disastri dal Cielo provato.
%#~ È questa la scola migliore dei Re.
%#~ Dell’ira de Numi non è che l’effetto
%#~ Se giusto castigo riceve il delitto.
%#~ O prence ben degno d’ossequio, d’affetto!
%#~ Il trono degli empi l’afilo non è.
\livretFinAct Fine.
