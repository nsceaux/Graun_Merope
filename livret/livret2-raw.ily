\livretAct ATTO SECONDO
\livretScene Scena Seconda
\livretDescAtt\wordwrap-center {
  Polifonte, Merope, Ericle, Ismenia, Egisto, Eroce.
}
\livretPers Egisto
\livretRef #'BBBduo
%#~ Cessa omai di quel Tiranno
%#~ Di calmare il reo furor
\livretPers Merope a Polifonte
%#~ Puoi veder, se madre io sono
%#~ Dall’ acerbo mio dolor.
\livretPers Egisto
%#~ Deh m’affermi per tuo figlio
%#~ La grandezza del tuo cor.
\livretPers Merope a Egisto
%#~ Ahi! che obblio nel tuo periglio
%#~ Del mio grado ogni splendor!
\livretPers Merope, Egisto, a. 2.
%#~ Qual orror! qual giorno è questo!
%#~ E respiro, o Numi, ancor!
\livretPers Merope a Polifonte
%#~ De’ tuoi Re tu vedi in lui
%#~ Il sol germe, che ancor resta;
%#~ E il destin de’ giorni sui
%#~ De te sol dipenderà
\livretPers Egisto
%#~ Se del sangue io son d’Alcide
%#~ I perigli non pavento.
%#~ Il valor, che in me risento,
%#~ Scintillare ancor saprà.
\livretPers Merope, Egisto, a. 2.
%#~ Giusti Numi difendete,
%#~ Vendicate, proteggete
%#~ L’innocenza, e la pietà!

\livretPers Polifonte
\livretRef #'BBDpolifonte
%#~ Arbitra della sorte
%#~ Del figlio tuo ti rendo:
%#~ Il viver suo, la morte
%#~ Dipenderà da te.
%#~ Se il tuo rigor secondi
%#~ Egli cadrà svenato;
%#~ Se all’ amor mio rispondi
%#~ Egli avrà un Padre in me.

\livretScene Scena Quarta
\livretDescAtt\wordwrap-center {
  Merope, Narbace.
}
\livretPers Narbace
\livretRef #'BDBnarbace
%#~ Alle vendetti i Numi
%#~ Possino armar la mano,
%#~ E nel suo fallo insano
%#~ Quell’ empio fulminar.

\livretScene Scena Quinta
\livretDescAtt\wordwrap-center {
  Ismenia, Merope, Narbace.
}
\livretPers Merope
\livretRef #'BEArecit
%# Ebben: sì dagli stessi
%# Miei pensier disperati
%# Render tutto mi sento il mio corraggio.
%# Nel Tempio, ove m’attende il grand oltraggio,
%# Omai tutti corriamo,
%# E al Popolo mostriamo – il Figlio mio.
%# Fra la Madre, e l’Altare
%# Agli occhi suoi s’esponga in guardia ai Numi.
%# Nato è del sangue loro:
%# La sua innocenza assai
%# Devono aver tradita in fin ad ora:
%# Sì, prenderan la sua difesa ancora.
%# Del traditore infame
%# Dipingerò il furore,
%# E al cor d’ognuno inspirerò vendetta.
%# Sì, Tiranni, temete i gridi, i pianti
%# Di Madre disperata… Ma… chi viene?
\livretDidasP\wordwrap { (Si vedono da lontano venire i Sacrificatori.) }
%# Ah mi s’agghiaccia il sangue!.. chi m’appella?
%# Già il Figlio mi sovra la tomba à il piede,
%# E a un batter d’occhio il barbaro Tiranno,
%# Precipitar vel può. Stelle! che affanno!
\livretDidasP\wordwrap {
  (Entrano i Sacrificatori, e alcune Guardie di Polifonte.)
}
%# Ah ministri crudeli
%# Di mostro più crudel, sì, voi venite
%# A strascinar la vittima all’ Altare.
%# O Natura! o dover! Vendetta! Amore!
%# Che tutti m’assaltate,
%# Da un disperato cor che più bramate?
\livretRef #'BEBmerope
%#~ M’opprime un mostro indegno
%#~ Col più fatal rigor!
%#~ Numi, se giusti siete,
%#~ Voi, che il mio duol vedete,
%#~ Troncate il reo disegno
%#~ Del perfido suo cor.
\livretDidasP\wordwrap {
  (I Sacrificatori seguono Merope, ed Ismenia du una parte;
  e le Guardie accompagnano Narbace dall’ altra.)
}
\sep
\column-break
