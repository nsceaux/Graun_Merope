\notesSection "Libretto"
\markuplist\page-columns-title \act\line { LIBRETTO } {
\livretAct ATTO PRIMO
\livretScene Scena Prima
\livretDescAtt\column {
  \wordwrap-center { Atrio del Palazzo Reale. }
  \wordwrap-center { Polifonte, Eroce. }
}
\livretPers Polifonte
\livretRef #'AAGpolifonte
\livretVerse#8 { Di questo aciaro il lampo }
\livretVerse#8 { Mi puo guidare al soglio }
\livretVerse#8 { Ma sorpassare io voglio }
\livretVerse#8 { L’arte d’un vicitor. }

\livretScene Scena Terza
\livretDescAtt\wordwrap-center {
  Egisto incatenato fra le Guardie, e suddetti.
}
\livretPers Egisto
\livretRef #'ACBegisto
\livretVerse#8 { Bel desio m’accese il core }
\livretVerse#8 { Di tentar le tue vendette; }
\livretVerse#8 { Fra perigli il mio valore }
\livretVerse#8 { Sol mostrar volea per te. }
\livretVerse#8 { Il languire in ozio vile }
\livretVerse#8 { Di mia età mi parve indegno. }
\livretVerse#8 { Ruppe il Cielo il mio disegno, }
\livretVerse#8 { E colpevole mi fe. }

\livretScene Scena Ottava
\livretDescAtt\wordwrap-center {
  Merope, Ismenia, Ericle.
}
\livretPers Merope
\livretRef #'AHAmerope
\livretVerse#12 { Quanto è barbaro oh stelle! il mio destino! }
\livretVerse#12 { Osa un tiranno indegno }
\livretVerse#12 { Oltraggiarmi a tal segno? }
\livretVerse#12 { Non sarà mai compito }
\livretVerse#12 { Un nodo sì abborrito… }
\livretVerse#12 { Sì, l’infame uccisor del caro figlio }
\livretVerse#12 { Pria svenerà questa mia destra ardita, }
\livretVerse#12 { Quindi a me stessa troncherà la vita. }
\livretVerse#12 { Ogni Nume del Ciel pur mi si mostri }
\livretVerse#12 { A voglia sua cruccioso; }
\livretVerse#12 { Posso ancor vendicare e figlio, e sposo. }
\livretVerse#12 { Ch’io alle funeree faci }
\livretVerse#12 { Le faci d’Imeneo volessi unire? }
\livretVerse#12 { Ch’io potessi levare il mesto ciglio }
\livretVerse#12 { Verso quel Ciel, che più non vede il figlio? }
\livretVerse#12 { Gettata ò la mia sorte. Sì, quest alma }
\livretVerse#12 { Tranquilla omai, poichè ai disastri è avezza, }
\livretVerse#12 { Li prevede, li insulta, e li disprezza. }
\livretRef #'AHBmerope
\livretVerse#8 { Quando a fronte di un periglio }
\livretVerse#8 { Ogni speme è alfin smarrita }
\livretVerse#8 { È viltà bramar la vita; }
\livretVerse#8 { E virtù saper morir. }
\livretVerse#8 { Or che giace estinto il figlio, }
\livretVerse#8 { Or che vedo al soglio eletto }
\livretVerse#8 { Un indegno, un vil soggetto, }
\livretVerse#8 { Che mi resta da soffrir? }

\livretScene Scena Nona
\livretDescAtt\column {
  \wordwrap-center {
    Piazza con Tempio accanto all’ ingresso del quale si vedrà il Mausoleo
    di Cresfonte.
  }
  \wordwrap-center { Narbace solo. }
}
\livretPers Narbace
\livretRef #'AIBnarbace
\livretVerse#8 { Ah che dovunque movo, }
\livretVerse#8 { Misero! il piè tremante, }
\livretVerse#8 { Sento, che ad ogni instante }
\livretVerse#8 { Raddoppia in me l'orror! }
\livretVerse#8 { Se fia, che al suo tiranno }
\livretVerse#8 { Il mio signor soccomba, }
\livretVerse#8 { Chiudami omai la tomba; }
\livretVerse#8 { Vissi purtroppo ancor! }

\livretScene Scena Duodecima
\livretDescAtt\wordwrap-center {
  Merope, Ismenia, Ericle, Egisto incatenato,
  Guardie, Sacrificatori, poi Narbace.
}
\livretPers Egisto
\livretRef #'ALBegisto
\livretVerse#8 { Sì, ti lascio, o Padre mio! }
\livretVerse#8 { Prendi omai l’estremo addio: }
\livretVerse#8 { Del destin tal’ è il rigor. }
\livretVerse#8 { Fuggi o Dio! lochi sì orrendi, }
\livretVerse#8 { Ma giustifica, difendi }
\livretVerse#8 { L’innocenza del mio cor. }

\livretScene Scena Decimaquarta
\livretDescAtt\column {
  \wordwrap-center { Polifonte col suo Seguito. }
  \wordwrap-center { Merope, Ismenia, Erick. }
}
\livretPers Polifonte
\livretRef #'ANBpolifonte
\livretVerse#8 { A consolar quel duolo, }
\livretVerse#8 { A vendicarti intesa }
\livretVerse#8 { D’amor quest’ alma accesa }
\livretVerse#8 { Tutto per te farà. }
\livretVerse#8 { Del mio potere a parte }
\livretVerse#8 { Vieni sul trono, e regna; }
\livretVerse#8 { E di giurar ti degna }
\livretVerse#8 { La mia felicità. }
\sep
\column-break
\livretAct ATTO SECONDO
\livretScene Scena Seconda
\livretDescAtt\wordwrap-center {
  Polifonte, Merope, Ericle, Ismenia, Egisto, Eroce.
}
\livretPers Egisto
\livretRef #'BBBduo
\livretVerse#8 { Cessa omai di quel Tiranno }
\livretVerse#8 { Di calmare il reo furor }
\livretPers Merope a Polifonte
\livretVerse#8 { Puoi veder, se madre io sono }
\livretVerse#8 { Dall’ acerbo mio dolor. }
\livretPers Egisto
\livretVerse#8 { Deh m’affermi per tuo figlio }
\livretVerse#8 { La grandezza del tuo cor. }
\livretPers Merope a Egisto
\livretVerse#8 { Ahi! che obblio nel tuo periglio }
\livretVerse#8 { Del mio grado ogni splendor! }
\livretPers Merope, Egisto, a. 2.
\livretVerse#8 { Qual orror! qual giorno è questo! }
\livretVerse#8 { E respiro, o Numi, ancor! }
\livretPers Merope a Polifonte
\livretVerse#8 { De’ tuoi Re tu vedi in lui }
\livretVerse#8 { Il sol germe, che ancor resta; }
\livretVerse#8 { E il destin de’ giorni sui }
\livretVerse#8 { De te sol dipenderà }
\livretPers Egisto
\livretVerse#8 { Se del sangue io son d’Alcide }
\livretVerse#8 { I perigli non pavento. }
\livretVerse#8 { Il valor, che in me risento, }
\livretVerse#8 { Scintillare ancor saprà. }
\livretPers Merope, Egisto, a. 2.
\livretVerse#8 { Giusti Numi difendete, }
\livretVerse#8 { Vendicate, proteggete }
\livretVerse#8 { L’innocenza, e la pietà! }

\livretPers Polifonte
\livretRef #'BBDpolifonte
\livretVerse#8 { Arbitra della sorte }
\livretVerse#8 { Del figlio tuo ti rendo: }
\livretVerse#8 { Il viver suo, la morte }
\livretVerse#8 { Dipenderà da te. }
\livretVerse#8 { Se il tuo rigor secondi }
\livretVerse#8 { Egli cadrà svenato; }
\livretVerse#8 { Se all’ amor mio rispondi }
\livretVerse#8 { Egli avrà un Padre in me. }

\livretScene Scena Quarta
\livretDescAtt\wordwrap-center {
  Merope, Narbace.
}
\livretPers Narbace
\livretRef #'BDBnarbace
\livretVerse#8 { Alle vendetti i Numi }
\livretVerse#8 { Possino armar la mano, }
\livretVerse#8 { E nel suo fallo insano }
\livretVerse#8 { Quell’ empio fulminar. }

\livretScene Scena Quinta
\livretDescAtt\wordwrap-center {
  Ismenia, Merope, Narbace.
}
\livretPers Merope
\livretRef #'BEArecit
\livretVerse#12 { Ebben: sì dagli stessi }
\livretVerse#12 { Miei pensier disperati }
\livretVerse#12 { Render tutto mi sento il mio corraggio. }
\livretVerse#12 { Nel Tempio, ove m’attende il grand oltraggio, }
\livretVerse#12 { Omai tutti corriamo, }
\livretVerse#12 { E al Popolo mostriamo – il Figlio mio. }
\livretVerse#12 { Fra la Madre, e l’Altare }
\livretVerse#12 { Agli occhi suoi s’esponga in guardia ai Numi. }
\livretVerse#12 { Nato è del sangue loro: }
\livretVerse#12 { La sua innocenza assai }
\livretVerse#12 { Devono aver tradita in fin ad ora: }
\livretVerse#12 { Sì, prenderan la sua difesa ancora. }
\livretVerse#12 { Del traditore infame }
\livretVerse#12 { Dipingerò il furore, }
\livretVerse#12 { E al cor d’ognuno inspirerò vendetta. }
\livretVerse#12 { Sì, Tiranni, temete i gridi, i pianti }
\livretVerse#12 { Di Madre disperata… Ma… chi viene? }
\livretDidasP\wordwrap { (Si vedono da lontano venire i Sacrificatori.) }
\livretVerse#12 { Ah mi s’agghiaccia il sangue!.. chi m’appella? }
\livretVerse#12 { Già il Figlio mi sovra la tomba à il piede, }
\livretVerse#12 { E a un batter d’occhio il barbaro Tiranno, }
\livretVerse#12 { Precipitar vel può. Stelle! che affanno! }
\livretDidasP\wordwrap {
  (Entrano i Sacrificatori, e alcune Guardie di Polifonte.)
}
\livretVerse#12 { Ah ministri crudeli }
\livretVerse#12 { Di mostro più crudel, sì, voi venite }
\livretVerse#12 { A strascinar la vittima all’ Altare. }
\livretVerse#12 { O Natura! o dover! Vendetta! Amore! }
\livretVerse#12 { Che tutti m’assaltate, }
\livretVerse#12 { Da un disperato cor che più bramate? }
\livretRef #'BEBmerope
\livretVerse#8 { M’opprime un mostro indegno }
\livretVerse#8 { Col più fatal rigor! }
\livretVerse#8 { Numi, se giusti siete, }
\livretVerse#8 { Voi, che il mio duol vedete, }
\livretVerse#8 { Troncate il reo disegno }
\livretVerse#8 { Del perfido suo cor. }
\livretDidasP\wordwrap {
  (I Sacrificatori seguono Merope, ed Ismenia du una parte;
  e le Guardie accompagnano Narbace dall’ altra.)
}
\sep
\column-break
\livretAct ATTO TERZO
\livretScene Scena Quarta
\livretDescAtt\wordwrap-center {
  Merope, Egisto, Narbace, Ericle.
}
\livretPers Merope
\livretRef #'CDBmerope
\livretVerse#8 { Par, che lo scorga il Cielo, }
\livretVerse#8 { Che fausto alfin gli arride. }
\livretVerse#8 { Ecco il figliuol d’Alcide: }
\livretVerse#8 { No, più mortal non è. }
\livretVerse#8 { Vieni, mio dolce Figlio, }
\livretVerse#8 { Corriamo uniti al Tempio. }
\livretVerse#8 { Desta sì grand’ esempio }
\livretVerse#8 { Tutto il coraggio in me. }
\livretDidasP\wordwrap {
  (partono insieme seguiti dalle Guardie.)
}

\livretScene Scena Quinta
\livretDescAtt\wordwrap-center {
  Narbace, Ericle.
}
\livretPers Narbace
\livretRef #'CEBnarbace
\livretVerse#8 { Mille ogetti di terrore }
\livretVerse#8 { O presenti al mesto sguardo: }
\livretVerse#8 { Fra la speme, e fra il timore }
\livretVerse#8 { Son costretto a palpitar. }
\livretVerse#8 { Sol conforta i sense miei }
\livretVerse#8 { Quel fidar con fermo core, }
\livretVerse#8 { Che vorran gli eterni Dei }
\livretVerse#8 { L’innocenza sollevar. }
\livretDidasP\wordwrap { (Partono.) }

\livretScene Scena Sesta
\livretDescAtt\column {
  \wordwrap-center {
    Interno di un Tempio con Ara in mezzo.
  }
  \wordwrap-center {
    Merope, Polifonte, Ismenia, Eroce.
    Sacerdoti, Popolo, Guardie, poi Egisto.
  }
}
\livretPers Coro
\livretRef #'CFBcoro
\livretVerse#8 { Del mortal, tu, che sei l’arbitra, }
\livretVerse#8 { O divina Providenza, }
\livretVerse#8 { Compi omai la tua grand opera }
\livretVerse#8 { L’innocenza – in sostener. }
\livretDidasP\wordwrap {
  (Il Coro non è ripetuto. Tutti partono, e
  siegue frattanto una strepitose Sinfonia.)
}

\livretScene Scena Settima
\livretDescAtt\column {
  \wordwrap-center {
    Gran Piazza di Messene, con veduta di buona parte della Città.
  }
  \wordwrap-center {
    Merope, Ismenia indi Egisto, e poi Narbace.
  }
  \wordwrap-center {
    Sacerdoti, Popolo, Soldati. Si vede in lontano strascinato
    il corpo di Polifonte coperto però di un tapeto.
  }
}
\livretPers Egisto
\livretRef #'CGArecit
\livretVerse#12 { Amici ancora dubitar potrete }
\livretVerse#12 { Del core d’una Madre, }
\livretVerse#12 { D’un figlio, che da lei viene difeso, }
\livretVerse#12 { E che del Padre è alle vendette inteso? }
\livretPers Popolo
\livretVerse#12 { Nella tua gloria, e più nel nostro amore }
\livretVerse#12 { Vieni il frutto a gustar del tuo valore. }
\livretPers Egisto
\livretVerse#12 { Non è mia questa gloria, ella è dei Numi. }
\livretVerse#12 { Ogni destin felice }
\livretVerse#12 { Parte sol di lor mano, }
\livretVerse#12 { E virtù ne discende in petto umano. }
\livretDidasP\wordwrap { (a Narb.) }
\livretRef #'CGBegisto
\livretVerse#8 { Se alle tempeste in seno }
\livretVerse#8 { Fosti mio Padre amante, }
\livretVerse#8 { Ora ch’è il ciel sereno }
\livretVerse#8 { Sarò tuo figlio ancor. }
\livretDidasP\wordwrap { (a Merope.) }
\livretVerse#8 { Vieni a divider meco, }
\livretVerse#8 { Madre adorata, il soglio; }
\livretVerse#8 { Io meritar non voglio }
\livretVerse#8 { Che il tuo materno amor. }
\livretPers Coro
\livretRef #'CGCcoro
\livretVerse#8 { Felice può dirsi, può dirsi beato }
\livretVerse#8 { Quel Popol soggeto d’un giusto Sovrano }
\livretVerse#8 { Fra mille disastri dal Cielo provato. }
\livretVerse#8 { È questa la scola migliore dei Re. }
\livretVerse#8 { Dell’ira de Numi non è che l’effetto }
\livretVerse#8 { Se giusto castigo riceve il delitto. }
\livretVerse#8 { O prence ben degno d’ossequio, d’affetto! }
\livretVerse#8 { Il trono degli empi l’afilo non è. }
\livretFinAct Fine.
}
