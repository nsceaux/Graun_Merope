\version "2.19.37"

\header {
  copyrightYear = "2016"
  composer = "Karl Heinrich Graun"
  date = "1756"
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
%% Staff size
#(set-global-staff-size
  (cond ((not (symbol? (ly:get-option 'part))) 16)
        ((memq (ly:get-option 'part) '(basse-continue)) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((flauti "Flauti" ((violini "Violini"))
           (#:notes "flauti" #:instrument "Flauti"
                    #:score-template "score-flauti"))
   (oboi "Oboi" ((violini "Violini"))
         (#:notes "oboi" #:instrument "Oboi"
                  #:score-template "score-flauti"))
   (corni "Corni" ()
          (#:notes "corni" #:score-template "score-corni"))
   (fagotti "Fagotti" ((bassi "Basso"))
            (#:notes "fagotti" #:instrument "Fagotti"
                     #:score-template "score-fagotti"))
   (violini "Violini" ()
            (#:notes "violini" #:score-template "score-violini"))
   (violino1 "Violino I" ()
             (#:notes "violini" #:tag-notes violino1))
   (violino2 "Violino II" ()
             (#:notes "violini" #:tag-notes violino2))
   (viola "Viola" () (#:notes "viola" #:clef "alto"))
   (bassi "Bassi" () (#:notes "bassi" #:clef "bass")))

\opusTitle "Merope"

\header {
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
}

trill = #(make-articulation "trill")

ouverture =
#(define-music-function (parser location title) (string?)
   (let ((rehearsal (rehearsal-number)))
    (add-toc-item parser 'tocActMarkup #{ \markup\sep #})
    (add-toc-item parser 'tocPieceMarkup title rehearsal)
    (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
    (add-odd-page-header-text parser (string-upper-case title) #f)
    (add-toplevel-markup parser (markup #:act (string-upper-case title)))
    (add-no-page-break parser)
    (if (eqv? #t (ly:get-option 'use-rehearsal-numbers))
        (begin
         (add-toplevel-markup parser (markup #:rehearsal-number rehearsal))
         (add-no-page-break parser))))
  (make-music 'Music 'void #t))
