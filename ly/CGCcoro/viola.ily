\clef "alto" mib'4 |
mib'2 mib'4 |
\appoggiatura mib'2 re'2. |
mib'4 r mib'\p |
mib'2 mib'4 |
\appoggiatura mib'2 re'2. |
mib'4 r sib\f |
sib sib sib |
sib sib sol' |
fa'4. sib'8 sib'4 |
sib' sib sib |
mib' mib' mib' |
mib'8.(\trill re'32 mib') fa'4-! sib |
fa' fa' fa' |
fa'8.(\trill mib'32 fa') sol'8.-! re'16 mib'8. sib16 |
do'8. sol16 lab4 sib |
mib2 mib'4 |
mib' mib' mib' |
\appoggiatura mib'2 re'2. |
mib'4 r mib'\p |
mib' mib' mib' |
\appoggiatura mib'2 re'2. |
mib'4 r sib\f |
sib sib sib |
sib4 mib sol' |
fa'4. sib'8 sib'4 |
sib'4 sib sib\p |
sib sib sib |
sib sib sib |
sib4. sib8 sib4 |
sib sib fa\f |
sib sib sib |
sib la fa'\f |
fa' fa' fa' |
fa' fa' fa' |
sol' sol' sol' |
fa'2 fa'4\p |
sol' sol' sol' |
fa'2 fa4\f |
mib'4 mib' mib' |
mib'8*2/3 re' do' re'4 re'\f |
do' do' fa' |
fa' fa' sib |
sib fa' fa' |
fa'2 sib4 |
sib fa' mib' |
re'2 fa'4\ff |
sib' sib' sib' |
sib'8.(\trill la'32 sib') do''4-! fa'4 |
do'' do'' do'' |
do''8.(\trill sib'32 do'') re''8.-! la'16 sib'8. fa'16 |
sol'8. re'16 mib'4 fa' |
sib2 mib'4 |
mib' mib' mib' |
\appoggiatura mib'2 re'2. |
mib'4 r mib'\p |
mib' mib' mib' |
\appoggiatura mib'2 re'2. |
mib'4 r mib'\f |
mib' mib' mib' |
mib' mib' mib' |
mib'8( reb') reb'( do') do'( sib) |
sib4 lab do' |
fa' fa' fa' |
fa' fa' fa' |
fa'8[ mib'] mib'[ re'] re'[ do'] |
do'4 sib sib |
mib' mib' mib' |
mib' re' sib |
sib sib sib |
sib sib sib |
do' do' do' |
sib2 sib4\p |
do' do' do' |
sib2 sib4\f |
lab' lab' lab' |
lab'8*2/3( sol' fa') sol'4 mib' |
fa' fa' re' |
re' mib' mib' |
mib'4 mib' re' |
\appoggiatura re'4 mib'2 sib4 |
do' sib lab |
sol2 sib4\ff |
mib' mib' mib' |
mib'8.\trill re'32 mib' fa'4-! sib |
fa' fa' fa' |
fa'8.\trill mib'32 fa' sol'8.-! re'16 mib'8. sib16 |
do'8. sol16 lab4 sib |
mib mib mib |
mib r r |
