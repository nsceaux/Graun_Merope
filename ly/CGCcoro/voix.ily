<<
  \tag #'canto1 {
    \clef "soprano/treble" r4 |
    R2.*15 |
    r4 r sib' |
    mib'' mib''8([ do'']) do''([ sib']) |
    \appoggiatura sib'2 lab'2. |
    sol'4 r sib'^\p |
    mib''4 mib''8[ do''] do''[ sib'] |
    \appoggiatura sib'2 lab'2. |
    sol'4 r mib'' |
    re''8*2/3([ mib'' fa'']) mib''4 mib'' |
    re''8*2/3([ mib'' fa'']) mib''4 sib' |
    lab''4. lab''8 sol''4 |
    sol'' fa'' mib''^\p |
    re''8*2/3([ mib'' fa'']) mib''4 mib'' |
    re''8*2/3([ mib'' fa'']) mib''4 sib' |
    lab'4. lab'8 solb'4 |
    solb' fa' r |
    R2. |
    r4 r fa'^\f |
    re'' re'' re'' |
    re'' do'' fa'' |
    fa''8([ do'']) do''4 do'' |
    do''2 fa''4^\p |
    fa''8[ do''] do''4 do'' |
    do''2 r4 |
    R2. |
    r4 r fa'^\f |
    mib'' mib'' mib'' |
    mib''8*2/3([ re'' do'']) re''4 fa'' |
    sol''8*2/3[ fa'' mib''] re''4 do'' |
    \appoggiatura do''4 re''2 fa''4 |
    sol''8*2/3[ fa'' mib''] re''4 do'' |
    sib'2 r4 |
    R2.*5 |
    r4 r sib' |
    mib'' mib''8([ do'']) do''([ sib']) |
    \appoggiatura sib'2 lab'2. |
    sol'4 r4 sib'^\p |
    mib'' mib''8[ do''] do''[ sib'] |
    \appoggiatura sib'2 lab'2. |
    sol'4 r sib'^\f |
    sib'8*2/3[ do'' reb''] do''4 do'' |
    sib'8*2/3[ do'' reb''] do''4 do'' |
    do''8([ fa'']) fa''([ mib'']) mib''([ reb'']) |
    reb''4 do'' do'' |
    do''8*2/3[ re''! mib''] re''4 re'' |
    do''8*2/3[ re'' mib''] re''4 re'' |
    re''8[ sol''] sol''[ fa''] fa''[ mib''] |
    mib''4 re'' r |
    R2. |
    r4 r sib' |
    mib'' mib'' mib'' |
    mib'' re'' mib'' |
    mib''8([ fa']) fa'4 fa' |
    fa'2 mib''4^\p |
    mib''8([ fa']) fa'4 fa' |
    fa'2 r4 |
    R2. |
    r4 r sib'^\f |
    lab'' lab'' lab'' |
    lab''8*2/3[ sol'' fa''] sol''4 mib'' |
    do''8*2/3([ re'' mib'']) re''8([ do'']) sib'([ lab']) |
    \appoggiatura lab'4 sol'2 mib''4 |
    do''8*2/3([ sib' lab']) sol'4 fa' |
    mib'2 r4 |
    R2.*7 |
  }
  \tag #'canto2 {
    \clef "soprano/treble" r4 |
    R2.*15 |
    r4 r sol' |
    sol' sol'8([ lab']) lab'([ sol']) |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'^\p |
    sol' sol'8[ lab'] lab'[ sol'] |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'^\f |
    fa'8*2/3([ sol' lab']) sol'4 sol' |
    fa'8*2/3[ sol' lab'] sol'4 sib' |
    sib'4. sib'8 sib'4 |
    sib' sib' solb'^\p |
    fa'8*2/3([ solb' lab']) solb'4 solb' |
    fa'8*2/3[ solb' lab'] solb'4 solb' |
    fa'4. fa'8 mib'4 |
    mib' re' r |
    R2. |
    r4 r fa'^\f |
    sib' sib' sib' |
    sib' la' sib' |
    sib' sib' sib' |
    la'2 sib'4^\p |
    sib' sib' sib' |
    la'2 r4 |
    R2. |
    r4 r fa'^\f |
    sol' do'' la' |
    la' sib' sib' |
    sib' sib' la' |
    \appoggiatura la'4 sib'2 sib'4 |
    sib' sib' la' |
    sib'2 r4 |
    R2.*5 |
    r4 r sol' |
    sol' sol'8([ lab']) lab'([ sol']) |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'^\p |
    sol' sol'8[ lab'] lab'[ sol'] |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'^\f |
    sol'8*2/3[ lab' sib'] lab'4 lab' |
    sol'8*2/3[ lab' sib'] lab'4 lab' |
    fa' fa' sol' |
    sol' lab' la' |
    la'8*2/3[ sib' do''] sib'4 sib' |
    la'8*2/3[ sib' do''] sib'4 sib' |
    sol'4 sol' la' |
    la' sib' r |
    R2. |
    r4 r sib' |
    sol' sol' sol' |
    sol' fa' mib' |
    mib' mib' mib' |
    re'2 mib'4^\p |
    mib' mib' mib' |
    re'2 r4 |
    R2. |
    r4 r sib'^\f |
    lab' sib' sib' |
    sib' sib' sib' |
    lab'4 lab' fa' |
    \appoggiatura fa'4 mib'2 mib'4 |
    mib' mib' re' |
    mib'2 r4 |
    R2.*7 |
  }
  \tag #'tenore {
    \clef "tenor/G_8" r4 |
    R2.*15 |
    r4 r mib' |
    mib' mib' mib' |
    \appoggiatura mib'2 re'2. |
    mib'4 r mib'^\p |
    mib' mib' mib' |
    \appoggiatura mib'2 re'2. |
    mib'4 r sib^\f |
    sib sib sib |
    sib4 sib sib |
    fa'4. fa'8 mib'4 |
    mib' re' sib^\p |
    sib sib sib |
    sib sib sib |
    sib4. sib8 sib4 |
    sib sib fa^\f |
    sib sib sib |
    sib la fa' |
    fa' fa' fa' |
    fa' fa' fa' |
    sol' sol' sol' |
    fa'2 fa'4^\p |
    sol' sol' sol' |
    fa'2 fa4^\f |
    mib'4 mib' mib' |
    mib'8*2/3([ re' do']) re'4 re' |
    do' do' fa' |
    fa' fa' sib |
    sib fa' fa' |
    fa'2 sib4 |
    sib fa' mib' |
    re'2 r4 |
    R2.*5 |
    r4 r mib' |
    mib' mib' mib' |
    \appoggiatura mib'2 re'2. |
    mib'4 r mib'^\p |
    mib' mib' mib' |
    \appoggiatura mib'2 re'2. |
    mib'4 r mib'^\f |
    mib' mib' mib' |
    mib' mib' mib' |
    mib'8([ reb']) reb'([ do']) do'([ sib]) |
    sib4 lab do' |
    fa' fa' fa' |
    fa' fa' fa' |
    fa'8[ mib'] mib'[ re'] re'[ do'] |
    do'4 sib sib |
    mib' mib' mib' |
    mib' re' sib |
    sib sib sib |
    sib sib sib |
    do' do' do' |
    sib2 sib4^\p |
    do' do' do' |
    sib2 sib4^\f |
    lab' lab' lab' |
    lab'8*2/3([ sol' fa']) sol'4 mib' |
    fa' fa' re' |
    re' mib' mib' |
    mib'4 mib' re' |
    \appoggiatura re'4 mib'2 sib4 |
    do' sib lab |
    sol2 r4 |
    R2.*7 |
  }
  \tag #'basso {
    \clef "bass/bass" r4 |
    R2.*15 |
    r4 r mib |
    mib mib lab |
    sib2( sib,4) |
    mib4 r mib^\p |
    mib mib lab |
    sib2( sib,4) |
    mib4 r mib^\f |
    sib mib mib |
    sib mib mib |
    re4. re8 mib4 |
    sib sib, mib^\p |
    sib mib mib |
    sib mib mib |
    re4. re8 mib4 |
    sib sib, fa^\f |
    sib sib sib |
    sib la la |
    sib sib,8[ do] re[ mib] |
    fa4 fa re |
    mib mib mi |
    fa2 re4^\p |
    mib mib mi |
    fa2 fa4^\f |
    mib' mib' mib' |
    mib'8*2/3([ re' do']) re'4 re' |
    do' la fa |
    sib sib, re |
    mib fa fa |
    sib2 re4 |
    mib fa fa |
    sib,2 r4 |
    R2.*5 |
    r4 r mib |
    mib mib lab |
    sib2( sib,4) |
    mib4 r mib^\p |
    mib mib lab |
    sib2( sib,4) |
    mib4 r mib^\f |
    mib lab lab |
    mib lab lab |
    reb reb mib |
    lab lab, fa |
    fa sib sib |
    fa sib sib |
    mib4 mib fa |
    sib sib, sib |
    mib' mib' mib' |
    mib' re' sib |
    mib4 mib8[ fa] sol[ lab] |
    sib4 sib, sol |
    lab lab la |
    sib2 sol4^\p |
    lab lab la |
    sib2 sib,4^\f |
    lab lab lab |
    lab8*2/3([ sol fa]) sol4 sol |
    fa re sib, |
    mib mib sol |
    lab4 lab sib |
    do'2 sol4 |
    lab sib sib, |
    mib2 r4 |
    R2.*7 |
  }
>>
