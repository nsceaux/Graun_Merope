\clef "treble"
<<
  \tag #'violino1 {
    sib' |
    mib'' mib''8( do'') do''( sib') |
    \appoggiatura sib'2 lab'2. |
    sol'4 r sib'\p |
    mib'' mib''8( do'') do''( sib') |
    \appoggiatura sib'2 lab'2. |
    sol'4 r mib''\f |
    re''8*2/3( mib'' fa'') mib''4 mib'' |
    re''8*2/3( mib'' fa'') mib''4 sib' |
    lab''4. lab''8 sol''4 |
    \appoggiatura lab''8 sol''4 fa''
  }
  \tag #'violino2 {
    sol'4 |
    sol' sol'8( lab') lab'( sol') |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'\p |
    sol' sol'8( lab') lab'( sol') |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'\f |
    fa'8*2/3( sol' lab') sol'4 sol' |
    fa'8*2/3( sol' lab') sol'4 sib' |
    fa''4. fa''8 mib''4 |
    \appoggiatura fa''8 mib''4 re''
  }
>> sib4 |
mib' mib' mib' |
mib'8.(\trill re'32 mib') fa'4-! sib |
fa' fa' fa' |
fa'8.(\trill mib'32 fa') sol'8.-! re'16 mib'8. sib16 |
do'8. sol16 lab4 sib |
mib'2
<<
  \tag #'violino1 {
    sib'4 |
    mib'' mib''8( do'') do''( sib') |
    \appoggiatura sib'2 lab'2. |
    sol'4 r sib'\p |
    mib''4 mib''8( do'') do''( sib') |
    \appoggiatura sib'2 lab'2. |
    sol'4 r mib'' |
    re''8*2/3( mib'' fa'') mib''4 mib'' |
    re''8*2/3( mib'' fa'') mib''4 sib' |
    lab''4. lab''8 sol''4 |
    sol'' fa'' mib''\p |
    re''8*2/3( mib'' fa'') mib''4 mib'' |
    re''8*2/3( mib'' fa'') mib''4 sib' |
    lab'4. lab'8 solb'4 |
    solb' fa' r |
    R2. |
    r4 r fa'\f |
    re'' re'' re'' |
    re'' do'' fa'' |
    fa''8( do'') do''4 do'' |
    do''2 fa''4\p |
    fa''8 do'' do''4 do'' |
    do''2 r4 |
    R2. |
    r4 r fa'\f |
    mib'' mib'' mib'' |
    mib''8*2/3( re'' do'') re''4 fa'' |
    sol''8*2/3 fa'' mib'' re''4 do'' |
    \appoggiatura do''4 re''2 fa''4 |
    sol''8*2/3 fa'' mib'' re''4 do'' |
    sib'2
  }
  \tag #'violino2 {
    sol'4 |
    sol' sol'8( lab') lab'( sol') |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'\p |
    sol' sol'8( lab') lab'( sol') |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'\f |
    fa'8*2/3( sol' lab') sol'4 sol' |
    fa'8*2/3 sol' lab' sol'4 sib' |
    fa''4. fa''8 mib''4 |
    mib'' re'' solb'\p |
    fa'8*2/3( solb' lab') solb'4 solb' |
    fa'8*2/3( solb' lab') solb'4 solb' |
    fa'4. fa'8 mib'4 |
    mib' re' r |
    R2. |
    r4 r fa'\f |
    sib' sib' sib' |
    sib' la' sib' |
    sib' sib' sib' |
    la'2 sib'4\p |
    sib' sib' sib' |
    la'2 r4 |
    R2. |
    r4 r fa'\f |
    sol' do'' la' |
    la' sib' sib' |
    sib' sib' la' |
    \appoggiatura la'4 sib'2 sib'4 |
    sib' sib' la' |
    sib'2
  }
>> fa'4\ff |
sib' sib' sib' |
sib'8.(\trill la'32 sib') do''4-! fa' |
do'' do'' do'' |
do''8.(\trill sib'32 do'') re''8.-! la'16 sib'8. fa'16 |
sol'8. re'16 mib'4 fa' |
sib2 <<
  \tag #'violino1 {
    sib'4 |
    mib'' mib''8( do'') do''( sib') |
    \appoggiatura sib'2 lab'2. |
    sol'4 r4 sib'\p |
    mib'' mib''8( do'') do''( sib') |
    \appoggiatura sib'2 lab'2. |
    sol'4 r sib'\f |
    sib'8*2/3( do'' reb'') do''4 do'' |
    sib'8*2/3( do'' reb'') do''4 do'' |
    do''8( fa'') fa''( mib'') mib''( reb'') |
    reb''4 do'' do'' |
    do''8*2/3( re''! mib'') re''4 re'' |
    do''8*2/3( re'' mib'') re''4 re'' |
    re''8[ sol''] sol''[ fa''] fa''[ mib''] |
    mib''4 re'' r |
    R2. |
    r4 r sib' |
    mib'' mib'' mib'' |
    mib'' re'' mib'' |
    mib''8( fa') fa'4 fa' |
    fa'2 mib''4\p |
    mib''8( fa') fa'4 fa' |
    fa'2 r4 |
    R2. |
    r4 r sib'\f |
    lab'' lab'' lab'' |
    lab''8*2/3 sol'' fa'' sol''4 mib'' |
    do''8*2/3( re'' mib'') re''8( do'') sib'( lab') |
    \appoggiatura lab'4 sol'2 mib''4 |
    do''8*2/3( sib' lab') sol'4 fa' |
    mib'2
  }
  \tag #'violino2 {
    sol'4 |
    sol' sol'8( lab') lab'( sol') |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'\p |
    sol' sol'8( lab') lab'( sol') |
    \appoggiatura sol'2 fa'2. |
    mib'4 r sol'\f |
    sol'8*2/3( lab' sib') lab'4 lab' |
    sol'8*2/3( lab' sib') lab'4 lab' |
    fa' fa' sol' |
    sol' lab' la' |
    la'8*2/3( sib' do'') sib'4 sib' |
    la'8*2/3( sib' do'') sib'4 sib' |
    sol'4 sol' la' |
    la' sib' r |
    R2. |
    r4 r sib' |
    sol' sol' sol' |
    sol' fa' mib' |
    mib' mib' mib' |
    re'2 mib'4\p |
    mib' mib' mib' |
    re'2 r4 |
    R2. |
    r4 r sib'\f |
    lab' sib' sib' |
    sib' sib' sib' |
    lab'4 lab' fa' |
    \appoggiatura fa'4 mib'2 mib'4 |
    mib' mib' re' |
    mib'2
  }
>> sib4\ff |
mib' mib' mib' |
mib'8.(\trill re'32 mib') fa'4-! sib |
fa' fa' fa' |
fa'8.(\trill mib'32 fa') sol'8.-! re'16 mib'8. sib16 |
do'8.-! sol16 lab4 sib |
mib' <mib' sib' sol''> q |
q r r |
