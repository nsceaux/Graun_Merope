\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Corni" } <<
        \keepWithTag #'corni \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new GrandStaff \with { instrumentName = "[Violini]" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "[Viola]" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new ChoirStaff \with { instrumentName = "[Coro]" } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'canto1 \includeNotes "voix"
      >> \keepWithTag #'canto1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'canto2 \includeNotes "voix"
      >> \keepWithTag #'canto2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'tenore \includeNotes "voix"
      >> \keepWithTag #'tenore \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'basso \includeNotes "voix"
      >> \keepWithTag #'basso \includeLyrics "paroles"
    >>
    \new Staff \with { instrumentName = "[Bassi]" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s4 s2.*8\pageBreak
        s2.*7\pageBreak s2.*8\pageBreak
        s2.*7\pageBreak s2.*7\pageBreak
        s2.*7\pageBreak s2.*6\pageBreak
        s2.*7\pageBreak s2.*7\pageBreak
        s2.*7\pageBreak s2.*7\pageBreak
        s2.*7\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}