\clef "bass" mib4 |
mib mib lab |
sib2( sib,4) |
mib4 r mib\p |
mib mib lab |
sib2( sib,4) |
mib r mib\f |
sib mib mib |
sib mib mib |
re4. re8 mib4 |
sib sib, sib, |
mib mib mib |
mib8.(\trill re32 mib) fa4-! sib, |
fa fa fa |
fa8.(\trill mib32 fa) sol8.-! re16 mib8. sib,16 |
do8. sol,16 lab,4 sib, |
mib2 mib4 |
mib mib lab |
sib2( sib,4) |
mib4 r mib\p |
mib mib lab |
sib2( sib,4) |
mib4 r mib\f |
sib mib mib |
sib mib mib |
re4. re8 mib4 |
sib sib, mib\p |
sib mib mib |
sib mib mib |
re4. re8 mib4 |
sib sib, fa\f |
sib sib sib |
sib la la |
sib sib,8 do re mib |
fa4 fa re |
mib mib mi |
fa2 re4\p |
mib mib mi |
fa2 fa4\f |
mib' mib' mib' |
mib'8*2/3( re' do') re'4 re' |
do' la fa |
sib sib, re |
mib fa fa |
sib2 re4 |
mib fa fa |
sib,2 fa4\ff |
sib sib sib |
sib8.(\trill la32 sib) do'4-! fa |
do' do' do' |
do'8.(\trill sib32 do') re'8.-! la16 sib8. fa16 |
sol8. re16 mib4 fa |
sib,2 mib4 |
mib mib lab |
sib2( sib,4) |
mib4 r mib\p |
mib mib lab |
sib2( sib,4) |
mib4 r mib\f |
mib lab lab |
mib lab lab |
reb reb mib |
lab lab, fa |
fa sib sib |
fa sib sib |
mib4 mib fa |
sib sib, sib |
mib' mib' mib' |
mib' re' sib |
mib4 mib8 fa sol lab |
sib4 sib, sol |
lab lab la |
sib2 sol4\p |
lab lab la |
sib2 sib,4\f |
lab lab lab |
lab8*2/3( sol fa) sol4 sol |
fa re sib, |
mib mib sol |
lab4 lab sib |
do'2 sol4 |
lab sib sib, |
mib2 sib,4\ff |
mib mib mib |
mib8.\trill re32 mib fa4-! sib, |
fa fa fa |
fa8.\trill mib32 fa sol8.-! re16 mib8. sib,16 |
do8. sol,16 lab,4 sib, |
mib, mib, mib, |
mib, r r |

  