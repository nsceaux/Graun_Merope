\clef "treble" \transposition mib <>
<<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol''8( la'') la''( sol'') |
    \appoggiatura sol''2 fa''2. |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi''4 |
    mi'' mi''8( fa'') fa''( mi'') |
    \appoggiatura mi''2 re''2. |
    do''4
  }
>> r4 <>\p <<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol''8( la'') la''( sol'') |
    \appoggiatura sol''2 fa''2. |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi''4 |
    mi'' mi''8( fa'') fa''( mi'') |
    \appoggiatura mi''2 re''2. |
    do''4
  }
>> r4 <>\f <<
  \tag #'(corno1 corni) {
    mi''4 |
    re'' mi'' mi'' |
    re'' mi'' mi'' |
    re''4. re''8 mi''4 |
    mi'' re''
  }
  \tag #'(corno2 corni) {
    do''4 |
    sol' do'' do'' |
    sol' do'' do'' |
    sol'4. sol'8 do''4 |
    do'' sol'
  }
>> sol'4 |
do'' do'' do'' |
do''( re'') sol' |
re'' re'' re'' |
re''( mi'') r |
R2. |
r4 r <<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol''8( la'') la''( sol'') |
    \appoggiatura sol''2 fa''2. |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi''4 |
    mi'' mi''8( fa'') fa''( mi'') |
    \appoggiatura mi''2 re''2. |
    do''4
  }
>> r4 <>\p <<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol''8( la'') la''( sol'') |
    \appoggiatura sol''2 fa''2. |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi''4 |
    mi'' mi''8( fa'') fa''( mi'') |
    \appoggiatura mi''2 re''2. |
    do''4
  }
>> r4 <>\f <<
  \tag #'(corno1 corni) {
    mi''4 |
    re'' mi'' mi'' |
    re'' mi'' mi'' |
    re''4. re''8 mi''4 |
    mi'' re''
  }
  \tag #'(corno2 corni) {
    do''4 |
    sol' do'' do'' |
    sol' do'' do'' |
    sol'4. sol'8 do''4 |
    do'' sol'
  }
>> sol'4 |
\override Voice.Script.avoid-slur = #'inside
sol'-.( sol'-. sol'-.) |
sol'-.( sol'-. sol'-.) |
sol'4. sol'8 sol'4 |
sol' sol' r |
R2. |
r4 r <>\f re'' |
<<
  \tag #'(corno1 corni) {
    sol'' si'' si'' |
    si'' la'' la'' |
    la''-.( la''-. la''-.) |
    la''2 la''4 |
    la''-.( la''-. la''-.) |
    la''2
  }
  \tag #'(corno2 corni) {
    re''4 sol'' sol'' |
    sol'' fad'' fad'' |
    sol''-.( sol''-. sol''-.) |
    fad''2 fad''4 |
    sol''-.( sol''-. sol''-.) |
    fad''2
  }
  { s2.*3 s2 s4\p }
>> r4 |
\override Voice.Script.avoid-slur = #'outside
R2. |
r4 r re''\f |
re'' re'' re'' |
re'' re'' <<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol'' fad'' |
    \appoggiatura fad''4 sol''2 sol''4 |
    sol'' sol'' fad'' |
    sol''2
  }
  \tag #'(corno2 corni) {
    re''4 |
    mi'' re'' re'' |
    re''2 re''4 |
    mi'' re'' re'' |
    re''2
  }
>> re''4 |
sol'' sol'' sol'' |
sol''(\trill la'') re'' |
la'' la'' la'' |
la''\trill si'' r4 |
R2. |
r4 r <<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol''8( la'') la''( sol'') |
    \appoggiatura sol''2 fa''2. |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi''4 |
    mi'' mi''8( fa'') fa''( mi'') |
    \appoggiatura mi''2 re''2. |
    do''4
  }
>> r4 <>\p <<
  \tag #'(corno1 corni) {
    sol''4 |
    sol'' sol''8( la'') la''( sol'') |
    \appoggiatura sol''2 fa''2. |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi''4 |
    mi'' mi''8( fa'') fa''( mi'') |
    \appoggiatura mi''2 re''2. |
    do''4
  }
>> r4 <>\f do'' |
\override Voice.Script.avoid-slur = #'inside
do''-.( do''-. do''-.) |
do''-.( do''-. do''-.) |
re'' re'' mi'' |
fa'' do'' re'' |
re''-.( re''-. re''-.) |
re''-.( re''-. re''-.) |
mi'' mi'' fad'' |
fad'' re'' r |
R2. |
r4 r <<
  \tag #'(corno1 corni) {
    re''4 |
    mi'' mi'' mi'' |
    mi'' re'' mi'' |
    re''-.( re''-. re''-.) |
    re''2 mi''4 |
    re''-.( re''-. re''-.) |
    re''2
  }
  \tag #'(corno2 corni) {
    sol'4 |
    do'' do'' do'' |
    do'' sol' do'' |
    do''-.( do''-. do''-.) |
    sol'2 do''4 |
    do''-.( do''-. do''-.) |
    sol'2
  }
  { s4 s2.*3 s2 s4\p }
>> r4 |
R2. |
r4 r <>\f <<
  \tag #'(corno1 corni) {
    sol''4 |
    fa'' sol'' sol'' |
    sol'' sol'' sol'' |
    la'' sol'' fa'' |
    \appoggiatura fa''4 mi''2 sol''4 |
  }
  \tag #'(corno2 corni) {
    do''4 |
    re'' re'' re'' |
    re'' do'' do'' |
    do''4 re'' re'' |
    \appoggiatura re''4 do''2 do''4 |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { la''8*2/3[ sol'' fa''] mi''4 re'' | do''2 }
  { do''4 do'' sol' | mi'2 }
>> sol'4\ff |
do'' do'' do'' |
do''( re'') sol' |
re'' re'' re'' |
re''4( mi'') r |
R2. |
r4 <<
  \tag #'(corno1 corni) { sol'4 sol' | sol' }
  \tag #'(corno2 corni) { mi'4 mi' | mi' }
>> r4 r |
