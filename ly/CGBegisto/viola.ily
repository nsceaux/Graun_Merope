\clef "alto" re'4 r8 |
re'8 fa' re' |
fa'4. |
fa' |
mib' |
do' |
r8 sib' mi' |
fa' fa fa' |
mib'!4. |
fa' |
fa'8. mib'16( re' mib') |
re'4 re''8 |
do'' la' do'' |
sib' la' r |
r do'' la' |
sib' la' r |
sib sib fa' |
sib sib sol' |
do' do' sol' |
do'4 r8 |
r sib' sib' |
r sib' sib' |
r sib'\p sib' |
r sib' sib' |
r fa'8\f fa' |
fa' mib' fa' |
sol' \appoggiatura sib la4\trill |
sib4.~ |
sib4 r8 |
R4. |
re'8 fa' re' |
fa'4. |
fa' |
mib' |
fa' |
r8 sib' mi' |
fa' fa fa' |
mib'4. |
fa' |
fa'8. mib'16( re' mib') |
re'4 r8 |
re'4 r8 |
r fa' fa' |
fa' fa fa |
fa4 fa'8 |
mi'8 do' mi' |
fa' do' r |
r mi' do' |
fa' do' r |
la'\pocof fa' la' |
sib4\p do'8 |
do'4 do'8 |
r8 do' sib |
la sol fa |
mi' re' do' |
r fa' sol' |
la' la' la' |
sib' sib' fad' |
sol' sol' sol' |
sol' sol' la' |
sib' sib' si' |
do'' do' re' |
mi' mi' mi' |
fa'4. |
sib |
do'4 r8 |
r fa'\f fa' |
r fa' fa' |
r fa' fa' |
r fa' fa' |
r do' do' |
fa4. |
sib8 do' do |
fa4 r8 |
r fa'\f fa' |
r fa' fa' |
r fa'\p fa' |
r fa' fa' |
r do'\pocof do' |
do'4. |
sib8 do' do |
fa4 r8 |
la\f do' la |
do'4. |
do' |
sib |
sol |
r8 fa' si |
do' do do' |
sib!4. |
do' |
do'8. sib16( la sib) |
la4 r8 |
R4.*2 |
r8 sol' fa' |
mib' re' do' |
sol'4 r8 |
sol'4 r8 |
sol'4 r8 |
sol'4 r8 |
r do'' fad' |
sol' sol fa'! |
mib'4 r8 |
r mi' do' |
fa' fa r |
r la' fa' |
sib' fa' r |
r la' fa' |
sib'4. |
mib'4. |
fa'4 r8 |
R4.*2 |
r8 sib' mi' |
fa' fa fa |
fa r r |
R4. |
r8 sib' mi' |
fa' fa mib'! |
re'4 r8 |
mib8 mib mib |
mib mib mib |
fa fa fa |
fa fa fa |
sol sol sol |
sol sol sol |
la la la |
sib sib sib |
la la la |
sib sib sib |
mib'4. |
fa'8 fa fa' |
sib' sib sib' |
r8 sib'\f sib' |
r sib' sib' |
r sib\p sib |
r sib sib |
fa'4.\pocof |
sib |
mib'8 fa' fa |
sib4 r8 |
r sib'\f sib' |
r sib' sib' |
r sib\p sib |
r sib sib |
r fa'\pocof fa' |
sib re' sib |
mib' fa' fa |
sib\f re' sib |
mib' fa' fa |
sib4 r8 |
re'8\f fa' re' |
fa'4. |
fa'4 r8 |
r sib' sib' |
r sib' sib' |
r sib'\p sib' |
r sib' sib' |
r fa'\ff fa' |
fa' mib' fa' |
sol' \appoggiatura sib8 la4\trill |
sib8 fa' re' |
sib4\trill r8 |
