\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "[Violini]" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "[Viola]" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character [Egisto]
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "[Bassi]" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s4.*10\pageBreak
        s4.*8\break s4.*10\break s4.*9\pageBreak
        s4.*8\break s4.*7\break \grace s8 s4.*8\pageBreak
        s4.*8\break s4.*8\break s4.*9\pageBreak
        s4.*8\break s4.*8\break \grace s4 s4.*9\pageBreak
        s4.*9\break s4.*9\break s4.*9\pageBreak
        s4.*8\break s4.*8\break s4.*8\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}