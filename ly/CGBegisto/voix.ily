\clef "soprano/treble" R4.*29 |
sib'8 re'' fa'' |
sib'' sib'4 |
do''~ do''16.([ re''64 mib'']) |
re''4. |
sol''8 fa''16([ mib'']) re''([ do'']) |
\appoggiatura sib'8 la'4 mib''8 |
\appoggiatura mib''8 re''4. |
do'' |
sol''8 fa''16[ mib''] re''[ do''] |
\appoggiatura sib'8 la'4 mib''8 |
mib''[ re''16( do'' sib' do'')] |
sib'4 r8 |
sib' re'' fa'' |
sib'' re''4 |
re''8.([ mi''16] fa''16*2/3[ mi'' re'']) |
do''4 do''8 |
do''4 sib'16*2/3[ la' sol'] |
la'8 sol' do'' |
do''4 sib'16*2/3[ la' sol'] |
la'8 sol' fa'' |
fa''4 mi''16*2/3[ re'' do''] |
re''8 mi'' fa''~ |
fa'' sib' la' |
\appoggiatura la'8 sol'4 r8 |
do'' do'' do'' |
do'' do''4 |
la''4\melisma sol''16*2/3([ fa'' mi'']) |
fa''4 mi''16*2/3([ re'' dod'']) |
re''4 do''!16*2/3([ sib' la']) |
sib'8[ re'' sol''] |
sib''4 la''16*2/3([ sol'' fad'']) |
sol''4 fa''!16*2/3([ mi'' re'']) |
mi''4 re''16*2/3([ do'' si']) |
do''4 sib'!16*2/3([ la' sol']) |
la'8([ sib' do'']) |
\appoggiatura mib''16 re''16.[ do''32 sib'8 la'] |
\appoggiatura sib'16 la'8\melismaEnd sol' r |
R4. |
r8 r r16 fa'' |
fa''8([ re'']) r16 fa'' |
fa''[ do''] do''8 r |
sib'4. |
la'8([ sib']) do''8 |
re''16*2/3([ do'' sib']) \appoggiatura la'8 sol'4\trill |
\appoggiatura sol'8 la'4 r8 |
R4. |
r8 r r16 fa'' |
fa''8[ re''] r16 fa'' |
fa''[ do''] do''8 r |
sib''4. |
la''8([ mi'']) fa''8 |
re''16*2/3[ do'' sib'] \appoggiatura la'8 sol'4 |
fa' r8 |
R4.*11 |
fa'8 lab' do'' |
fa''[ sol''] lab'' |
\appoggiatura do''4 si'4. |
do''4 r8 |
\appoggiatura fa''16 mib''8 re'' re'' |
re''[ mib''] re'' |
\appoggiatura fa''16 mib''8 re'' re'' |
re''([ mib'']) fa'' |
\appoggiatura fa''4 mib''4. |
re''4 r8 |
sol' do'' mib'' |
sol'' sib'4 |
sib'8 la' fa'' |
fa''4 mib''16*2/3([ re'' do'']) |
re''8 do'' fa'' |
fa''4 mib''16*2/3[ re'' do''] |
re''8([\melisma mib'' fa'']) |
sol''16([ mib'' do''8])\melismaEnd sib' |
\appoggiatura sib'8 la'4 r8 |
fa' fa' fa' |
fa'[ do''] mib'' |
\appoggiatura mib''4 re''4. |
do''8 r r |
fa' fa' fa' |
fa'[ do''] mib'' |
\appoggiatura mib''4 re''4. |
do''4 r8 |
sib' sib' fa' |
sol'4\melisma sib'16*2/3([ la' sol']) |
mib''4 re''16*2/3[ do'' sib'] |
la'4 do''16*2/3[ sib' la'] |
fa''4 mib''16*2/3[ re'' do''] |
sib'4 re''16*2/3[ do'' sib'] |
sol''4 fa''16*2/3[ mib'' re''] |
do''4 sol''16*2/3[ fa'' mib''] |
re''4 fa''16*2/3[ mib'' re''] |
do''4 sol''16*2/3[ fa'' mib''] |
re''4 fa''16*2/3[ mib'' re''] |
do''8[ sol'']\melismaEnd fa'' |
\appoggiatura fa''8 mib''4. |
re''4 r8 |
R4. |
r8 r r16 sib' |
sib'8[ sol'] r16 sib' |
sib'16[ fa'] fa'8 r |
mib''4. |
re''8([ mib'']) fa'' |
sol''16*2/3[ fa'' mib''] \appoggiatura re''8 do''4\trill |
\appoggiatura do''4*1/2 re''4. |
R4. |
r8 r r16 sib' |
sib'8([ sol']) r16 sib' |
sib'16[ fa'] fa'8 r |
mib''4. |
re''8[ fa''] sib'' |
sol''16*2/3[ fa'' mib''] \appoggiatura re''8 do''4 |
re''8[ fa''] sib'' |
sol''16*2/3[ fa'' mib''] \appoggiatura re''8 do''4 |
sib'4 r8 |
R4.*12 |
