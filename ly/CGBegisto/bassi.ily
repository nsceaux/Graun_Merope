\clef "bass" sib4 r8 |
sib re' sib |
fa' fa fa' |
sib sib, sib |
mib4. fa |
r8 sib mi |
fa fa, fa |
mib!4. |
fa |
sib,~ |
sib,8 re' sib |
la fa la |
sib fa r |
r la\p fa |
sib fa r |
re\f re re |
mib mib mib |
mi mi mi |
fa4 r8 |
r mib' mib' |
r re' re' |
r mib'\p mib' |
r re' re' |
r la,\f la, |
sib, do re |
mib fa fa, |
sib,4.~ |
sib,4 r8 |
R4. |
sib8 re' sib |
fa' fa fa' |
sib sib, sib |
mib4. |
fa |
r8 sib mi |
fa fa, fa |
mib4. |
fa |
sib,~ |
sib,8 re fa |
sib4 r8 |
r sib sib, |
fa fa, fa, |
fa, fa, fa |
mi do mi |
fa do r |
r mi do |
fa do r |
la\pocof fa la |
sib4\p la8 |
sol4 fa8 |
do do' sib |
la sol fa |
mi re do |
fa fa sol |
la la la |
sib sib fad |
sol sol sol |
sol sol la |
sib sib si |
do' do re |
mi mi mi |
fa4. |
sib, |
do4 r8 |
r sib\f sib |
r la la |
r sib\p sib |
r la la |
r mi mi |
fa4. |
sib,8 do do, |
fa,4 r8 |
r sib\f sib |
r la la |
r sib\p sib |
r la la |
r mi\pocof mi |
fa sol la |
sib do' do |
fa4 r8 |
fa\f la fa |
do' do do' |
fa fa, fa |
sib,4. |
do |
r8 fa si, |
do do, do |
sib,!4. |
do |
fa,~ |
fa,4 r8 |
R4.*2 |
r8 sol fa |
mib re do |
sol4 r8 |
sol4 r8 |
sol4 r8 |
sol4 r8 |
r do' fad |
sol sol, fa! |
mib4 r8 |
r mi do |
fa fa, r |
r la fa |
sib fa r |
r la fa |
sib4. |
mib |
fa4 r8 |
R4.*2 |
r8 sib mi |
fa fa, fa, |
fa, r r |
R4. |
r8 sib mi |
fa fa, mib! |
re4 r8 |
mib8 mib mib |
mib mib mib |
fa fa fa |
fa fa fa |
sol sol sol |
sol sol sol |
la la la |
sib sib sib |
la la la |
sib sib sib |
mib4. |
fa8 fa, fa |
sib sib, sib |
r8 mib'\f mib' |
r re' re' |
r mib\p mib |
r re re |
r la\pocof la |
sib4. |
mib8 fa fa, |
sib,4 r8 |
r mib'\f mib' |
r re' re' |
r mib\p mib |
r re re |
r la,\pocof la, |
sib, re sib, |
mib fa fa, |
sib,\f re sib, |
mib fa fa, |
sib,4 r8 |
sib,\ff re sib, |
fa fa, fa |
sib, sib sib, |
r mib' mib' |
r re' re' |
r mib'\p mib' |
r re' re' |
r la,\ff la, |
sib, do re |
mib fa fa, |
sib,4.~ |
sib,4 r8 |
