\clef "treble" sib'8 re'' fa'' |
sib'' sib'4 |
<<
  \tag #'violino1 {
    do''4~ do''16.( re''64 mib'') |
    re''4. |
  }
  \tag #'violino2 {
    la'4~ la'16.( sib'64 do'') |
    sib'4. |
  }
>>
sol''8 fa''16( mib'') re''( do'') |
\appoggiatura sib'8 la'4 <<
  \tag #'violino1 {
    mib''8 |
    \appoggiatura mib''4 re''4. |
    do''4
  }
  \tag #'violino2 {
    do''8 |
    \appoggiatura do''4 sib'4. |
    la'4 \startHaraKiri
  }
>> r8 |
sol'' fa''16( mib'') re''( do'') |
\appoggiatura sib'8 la'4 mib''8 |
mib''8 re''16( do'' sib' do'') |
sib'4 fa''8 |
fa''4 mib''16*2/3( re'' do'') |
\appoggiatura { do''16[ mib''] } re''8[ do''] fa''\p |
fa''4 mib''16*2/3 re'' do'' |
\appoggiatura { do''16[ mib''] } re''8[ do''] fa''\f |
fa''4 mib''32( re'' do'' sib') |
sol''4 fa''32( mib'' re'' do'') |
sol''4 fa''32( mi'' re'' do'') |
\appoggiatura sib'8 la'4 r16 sib'' |
sib''8( sol'') r16 sib'' |
sib''8( fa'') r16 sib''\p |
sib''8( sol'') r16 sib'' |
sib''8( fa'') r |
<>\f <<
  \tag #'violino1 { mib''4. | re''16. }
  \tag #'violino2 { \stopHaraKiri do''4. | sib'16. \startHaraKiri }
>> sib''32 la''16. sib''32 fa''16. sol''32 |
do''16. mib''32 \appoggiatura re''8 do''4\trill |
sib'8 fa' re' |
sib4\trill r8 |
sib'8 re'' fa'' |
sib'' sib'4 |
<<
  \tag #'violino1 {
    do''4~ do''16.( re''64 mib'') | re''4. |
  }
  \tag #'violino2 {
    \stopHaraKiri la'4~ la'16.( sib'64 do'') | sib'4 r8 |
  }
>>
sol''8 fa''16( mib'') re''( do'') |
\appoggiatura sib'8 la'4
<<
  \tag #'violino1 {
    mib''8 |
    \appoggiatura mib''4*1/2 re''4. |
    do'' |
  }
  \tag #'violino2 {
    do''8 |
    \appoggiatura do''4*1/2 sib'4. |
    la' | \startHaraKiri
  }
>>
sol''8 fa''16 mib'' re'' do'' |
\appoggiatura sib'8 la'4 mib''8 |
mib'' re''16( do'' sib' do'') |
sib'4 r8 |
sib'8 re'' fa'' |
sib'' <<
  \tag #'violino1 {
    re''4 |
    re''8.([ mi''16] fa''16*2/3[ mi'' re'']) |
    do''4 do''8 |
    do''4 sib'16*2/3 la' sol' |
    la'8 sol' do'' |
    do''4 sib'16*2/3 la' sol' |
    la'8[ sol'] fa''\pocof |
    fa''4 mi''16*2/3 re'' do'' |
    re''8\p mi'' fa''~ |
    fa'' sib' la' |
    \appoggiatura la'8 sol'4 r8 |
    do'' do'' do'' |
    do'' do''4 |
    la'' sol''16*2/3( fa'' mi'') |
    fa''4 mi''16*2/3( re'' dod'') |
    re''4 do''!16*2/3( sib' la') |
    sib'8 re'' sol'' |
    sib''4 la''16*2/3( sol'' fad'') |
    sol''4 fa''!16*2/3( mi'' re'') |
    mi''4 re''16*2/3( do'' si') |
    do''4 sib'!16*2/3( la' sol') |
    la'8( sib' do'') |
    \appoggiatura mib''16 re''16. do''32 sib'8 la' |
    \appoggiatura sib'16 la'8 sol'
  }
  \tag #'violino2 {
    \stopHaraKiri sib'4 |
    sib'8.([ do''16] re''16*2/3[ do'' sib']) |
    la'4 la'8 |
    sol' mi' sol' |
    fa' mi' r |
    r sol' mi' |
    fa' mi' r |
    do''\pocof la' do'' |
    fa'\p sol' la' |
    sib' mi' fa' |
    \appoggiatura fa'8 mi'4 r8 |
    fa' sol' la' |
    sol' fa' mi' |
    fa' la' sib' |
    do'' do'' fa' |
    fa' fa' la' |
    re'4 r8 |
    re' sib' do'' |
    re'' re'' re'' |
    sol' sol' fa' |
    sol' sol' sol' |
    fa' sol' la' |
    \appoggiatura do''16 sib'16. la'32 sol'8 fa' |
    \appoggiatura sol'16 fa'8 mi' \startHaraKiri
  }
>> r16 fa''\f |
fa''8( re'') r16 fa'' |
fa''8( do'') r16 fa''\p |
fa''8( re'') r16 fa'' |
fa''8( do'') r |
<<
  \tag #'violino1 {
    sib'4. |
    la'8( sib') do'' |
    re''16*2/3( do'' sib') \appoggiatura la'8 sol'4\trill |
    \appoggiatura sol'8 la'4
  }
  \tag #'violino2 {
    \stopHaraKiri sol'4. |
    fa'8( sol') la' |
    sib'16*2/3( la' sol') \appoggiatura fa'8 mi'4\trill |
    \appoggiatura mi'8 fa'4 \startHaraKiri
  }
>> r16 fa''\f |
fa''8( re'') r16 fa'' |
fa''8( do'') r16 fa''\p |
fa''8( re'') r16 fa'' |
fa''8( do'') r |
<>\pocof <<
  \tag #'violino1 {
    sib''4. |
    la''8( mi'') fa'' |
    re''16*2/3 do'' sib' \appoggiatura la'8 sol'4 |
  }
  \tag #'violino2 {
    \stopHaraKiri sol''4. |
    do''8( sib') do'' |
    fa'8 \appoggiatura fa'8 mi'4 |
  }
>>
fa'8\f la' do'' |
fa'' fa'4 |
<<
  \tag #'violino1 {
    sol'4~ sol'16.( la'64 sib') |
    la'4. |
  }
  \tag #'violino2 {
    mi'4~ mi'16.( fa'64 sol') |
    fa'4. |
  }
>>
re''8 do''16( sib') la'( sol') |
\appoggiatura fa'8 mi'4 <<
  \tag #'violino1 {
    sib'8 |
    \appoggiatura sib'4 la'4. |
    sol'4
  }
  \tag #'violino2 {
    sol'8 |
    \appoggiatura sol'4 fa'4. |
    mi'4 \startHaraKiri
  }
>> r8 |
re'' do''16( sib') la'( sol') |
\appoggiatura fa'8 mi'4 sib'8 |
sib' la'16( sol' fa' sol') |
fa'4 r8 |
fa'8\p( lab' do'') |
fa''( sol'') lab'' |
\appoggiatura do''4 si'4. |
do''4 r8 |
<<
  \tag #'violino1 {
    \appoggiatura fa''16 mib''8 re'' re'' |
    re'' mib'' re'' |
    \appoggiatura fa''16 mib''8 re'' re'' |
    re''( mib'') fa'' |
    \appoggiatura fa''4 mib''4. |
    re''4 r8 |
    sol'8 do'' mib'' |
    sol'' sib'4 |
    sib'8 la' fa'' |
    fa''4 mib''16*2/3( re'' do'') |
    re''8 do'' fa'' |
    fa''4 mib''16*2/3( re'' do'') |
    re''8( mib'' fa'') |
    sol''16( mib'' do''8) sib' |
    \appoggiatura sib'8 la'4
  }
  \tag #'violino2 {
    \stopHaraKiri \appoggiatura re''16 do''8 si' si' |
    si' do'' si' |
    \appoggiatura re''16 do''8 si' si' |
    si'( do'') re'' |
    \appoggiatura re''4 do''4. |
    si'4 r8 |
    sol' do'' mib'' |
    sol'' sol'4 |
    sol'8 fa' r |
    r do'' la' |
    sib' la' r |
    r do'' la' |
    sib'( do'' re'') |
    mib'' mib' re' |
    \appoggiatura re'8 do'4
  }
>> r8 |
fa' fa' fa' |
<<
  \tag #'violino1 {
    fa'( do'') mib'' |
    \appoggiatura mib''4 re''4. |
    do''4 r8 |
  }
  \tag #'violino2 {
    fa'8( la') do'' |
    \appoggiatura do''4 sib'4. |
    la'4 r8 |
  }
>>
fa'8 fa' fa' |
<<
  \tag #'violino1 {
    fa'( do'') mib'' |
    \appoggiatura mib''4 re''4. |
    do''4 r8 |
    sib'8 sib' fa' |
    sol'4 sib'16*2/3( la' sol') |
    mib''4 re''16*2/3 do'' sib' |
    la'4 do''16*2/3 sib' la' |
    fa''4 mib''16*2/3 re'' do'' |
    sib'4 re''16*2/3 do'' sib' |
    sol''4 fa''16*2/3 mib'' re'' |
    do''4 sol''16*2/3 fa'' mib'' |
    re''4 fa''16*2/3 mib'' re'' |
    do''4 sol''16*2/3 fa'' mib'' |
    re''4 fa''16*2/3 mib'' re'' |
    do''8 sol'' fa'' |
    \appoggiatura fa''4*1/2 mib''4. |
    re''4
  }
  \tag #'violino2 {
    fa'8( la') do'' |
    \appoggiatura do''4 sib'4. |
    la'4 r8 |
    fa'4 r8 |
    sib sib sib |
    do' do' do' |
    do' do' do' |
    re' re' re' |
    re' re' re' |
    mib' mib' mib' |
    fa' fa' fa' |
    fa' fa' fa' |
    fa' fa' fa' |
    fa' fa' fa' |
    sol' mib'' re'' |
    \appoggiatura re''4*1/2 do''4. |
    sib'4
  }
>> r16 sib''\f |
sib''8( sol'') r16 sib'' |
sib''8( fa'') r16 sib'\p |
sib'8( sol') r16 sib' |
sib'8( fa') r |
<>\pocof <<
  \tag #'violino1 {
    mib''4. |
    re''8( mib'') fa'' |
    sol''16*2/3( fa'' mib'') \appoggiatura re''8 do''4\trill |
    \appoggiatura do''8 re''4
  }
  \tag #'violino2 {
    do''4. |
    sib'8( do'') re'' |
    mib''16*2/3( re'' do'') \appoggiatura sib'8 la'4\trill |
    \appoggiatura la'8 sib'4
  }
>> r16 sib''\f |
sib''8( sol'') r16 sib'' |
sib''8( fa'') r16 sib'\p |
sib'8( sol') r16 sib' |
sib'8( fa') r |
<>\pocof <<
  \tag #'violino1 {
    mib''4. |
    re''8( fa'') sib'' |
    sol''16*2/3 fa'' mib'' \appoggiatura re''8 do''4 |
    re''8(\f fa'') sib'' |
    sol''16*2/3( fa'' mib'') \appoggiatura re''8 do''4 |
  }
  \tag #'violino2 {
    do''4. |
    sib'4 re''8 |
    mib''16*2/3( re'' do'') \appoggiatura sib'8 la'4 |
    sib'4\f re''8 |
    mib''16*2/3( re'' do'') \appoggiatura sib'8 la'4\trill |
  }
>>
sib'8\ff re'' fa'' |
sib'' sib'4 |
<<
  \tag #'violino1 {
    do''4~ do''16.( re''64 mib'') |
    re''4
  }
  \tag #'violino2 {
    la'4~ la'16.( sib'64 do'') |
    sib'4
  }
>> r16 sib'' |
sib''8( sol'') r16 sib'' |
sib''8( fa'') r16 sib''\p |
sib''8( sol'') r16 sib'' |
sib''8( fa'') r |
<>\ff <<
  \tag #'violino1 { mib''4. | re''16. }
  \tag #'violino2 { do''4. | sib'16. }
>> sib''32 la''16. sib''32 fa''16. sol''32 |
do''16. mib''32 \appoggiatura re''8 do''4\trill |
sib'8 fa' re' |
sib4\trill r8 |
