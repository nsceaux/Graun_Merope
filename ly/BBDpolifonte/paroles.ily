Ar -- bi -- tra del -- la sor -- te
del fi -- glio tuo __ ti ren -- do:
il vi -- ver suo, la mor -- te
di -- pen -- de -- rà da te
la mor -- te il vi -- ver suo la mor -- te
di -- pen -- de -- rà __ da te
di -- pen -- de -- rà di -- pen -- de -- rà __ da te
di -- pen -- de -- rà da te
di -- pen -- de -- rà da te.

Se il tuo ri -- gor __ se -- con -- di
e -- gli ca -- drà sve -- na -- to;
se all’ a -- mor mio ri -- spon -- di
e -- gli a -- vrà un Pa -- dre in me
un pa -- dre in me
se all’ a -- mor mio ri -- spon -- di
e -- gli a -- vrà un pa -- dre in me
un pa -- dre
e -- gli a -- vrà un pa -- dre in me
un pa -- dre in me un pa -- dre in me.
