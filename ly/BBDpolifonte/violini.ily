\clef "treble" la''4..\trill sold''16 la''4-! |
dod''-! re''-! red''-! |
mi''8.(\trill red''16) mi''4 r |
<<
  \tag #'(violino1 violini) {
    fad''8 si''4 re''' dod'''16( si'') |
    la''4~ la''8 sold''16.( la''32 si''8.) re''16 |
    dod''8.(\trill si'16)
  }
  \tag #'(violino2 violini) {
    la'8 re''4 fad'' mi''16( re'') |
    dod''4~ dod''8 si'16.( dod''32 re''8.) si'16 |
    la'8.(\trill sold'16)
  }
>> la'4 r4 |
la'4..\p\trill sold'16 la'4 |
dod'-! re'-! red'-! |
mi'8.(\trill red'16) mi'4 r |
<<
  \tag #'(violino1 violini) {
    re''8 re'' re'' re'' re'' re'' |
    dod'' dod'' si' si' si' re'' |
    dod''2
  }
  \tag #'(violino2 violini) {
    si'8 si' si' si' si' si' |
    la' la' la' sold' sold' si' |
    la'2
  }
>> r4 |
<<
  \tag #'(violino1 violini) {
    s8 dod''' dod''' dod''' dod''' dod''' |
    s si'' si'' si'' si'' si'' |
    si'' si'' la'' la'' sold'' sold'' |
    s8 fad''16.( sold''32) fad''8 fad''16.( sold''32) fad''8 fad'' |
    s8 sold''16.( la''32) sold''8 sold''16.( la''32) sold''8 sold'' |
    s8 la''16.( si''32) la''8 la''16.( si''32) la''8 la'' |
    s8 sold''16.( la''32) sold''8 sold''16.( la''32) sold''8 si'' |
    dod''' dod''' dod''' dod''' la'' la'' |
    s8 fad'' fad'' fad'' fad'' fad'' |
    s sold'' sold'' sold'' sold'' sold'' |
    s la'' la'' la'' la'' la'' |
    sold''
  }
  \tag #'(violino2 violini) {
    s8 mi'' mi'' mi'' mi'' mi'' |
    s red'' red'' red'' red'' red'' |
    mi'' mi'' red'' red'' mi'' mi'' |
    s8 red''16.( mi''32) red''8 red''16.( mi''32) red''8 red'' |
    s8 mi''16.( fad''32) mi''8 mi''16.( fad''32) mi''8 mi'' |
    s8 fad''16.( sold''32) fad''8 fad''16.( sold''32) fad''8 fad'' |
    s8 mi''16.( fad''32) mi''8 mi''16.( fad''32) mi''8 sold'' |
    la'' la'' la'' la'' fad'' fad'' |
    s red'' red'' red'' red'' red'' |
    s mi'' mi'' mi'' mi'' mi'' |
    s fad'' fad'' fad'' fad'' fad'' |
    mi''8
  }
  { r8 s s2 |
    r8 s s2 |
    s2. |
    r8 s s2 |
    r8 s s2 |
    r8 s s2 |
    r8 s s2 |
    s2. |
    r8 s s2 |
    r8 s s2 |
    r8 s s2 | }
>> r16. mi'32 mi'8.(\trill red'32 mi') mi'8.(\trill red'32 mi') |
si'8\f r16. mi'32\p mi'8.(\trill red'32 mi') mi'8.(\trill red'32 mi') |
<<
  \tag #'(violino1 violini) { dod''4~ dod''8. si'32 la' sold'8. fad'16 | sold'8 }
  \tag #'(violino2 violini) { la'4~ la'8. sold'32 fad' mi'8. red'16 | mi'8 }
>> r16. si'32 mi'8. sold'16 si8. la16 |
sold8 r16. si'32 mi'8. sold'16 si8. la16 |
la4( sold) r |
mi''4..\f\trill red''16 mi''4-! |
sold'-! la'-! lad'-! |
si'8.(\trill lad'16) si'4 r |
<<
  \tag #'(violino1 violini) {
    dod''8 fad''4 la'' sold''16( fad'') |
    mi''4~ mi''8 red''16.( mi''32 fad''8.) la'16 |
    sold'8.(\trill fad'16)
  }
  \tag #'(violino2 violini) {
    mi'8 la'4 dod'' si'16( la') |
    sold'4~ sold'8 fad'16.( sold'32 la'8.) fad'16 |
    mi'8.(\trill red'16)
  }
>> mi'4 r |
mi'4\p mi'8( fad') fad'( sol') |
lad2~ lad8. <<
  \tag #'(violino1 violini) { fad'16 | re'4 dod' }
  \tag #'(violino2 violini) { lad16 | si4 lad }
>> r4 |
<>\pocof <<
  \tag #'(violino1 violini) { si''4-! si''-! lad''-! | si''-! }
  \tag #'(violino2 violini) { fad''4-! sol''-! fad''-! | fad''-! }
>> r4 r |
<<
  \tag #'(violino1 violini) {
    s8 si'16.( dod''32 si'8) si'16.( dod''32 si'8) si' |
    s8 dod''16.( re''32 dod''8) dod''16.( re''32 dod''8) dod'' |
    s8 si'16.( dod''32 si'8) si'16.( dod''32 si'8) si' |
    mi''8 mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' |
    s8 la'16.( si'32 la'8) la'16.( si'32 la'8) si' |
    dod''8 dod'' dod'' dod'' red''? dod'' |
    s8 si'16.( dod''32 si'8) si'16.( dod''32 si'8) dod'' |
    re''2.~ |
    re''8 re' re' re' re' re' |
  }
  \tag #'(violino2 violini) {
    s8 sold'!16.( la'32 sold'8) sold'16.( la'32 sold'8) sold' |
    s8 la'16.( si'32 la'8) la'16.( si'32 la'8) la' |
    s8 sold'16.( la'32 sold'8) sold'16.( la'32 sold'8) sold' |
    la'8 la' la' la' la' la' |
    sol' sol' sol' sol' sol' sol' |
    s8 fad'16.( sol'32 fad'8) fad'16.( sol'32 fad'8) sold' |
    la'8 la' la' la' si' la' |
    s8 sold'16.( la'32 sold'8) sold'16.( la'32 sold'8) la' |
    si'2.~ |
    si'8 si si si si si |
  }
  { r8 s s2 | r8 s s2 | r8 s s2 | s2.*2 | r8 s s2 | s2. | r8 s s2 | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { dod'8( la') sold'( la') mi' sol' | }
  { dod'4 re' mi' | }
>>
<<
  \tag #'(violino1 violini) {
    fad'8.( re''16) \appoggiatura dod''4 si'2\trill |
    s8 dod''16.( re''32 dod''8) dod''16.( re''32 dod''8) dod'' |
    s8 dod''16.( re''32 dod''8) dod''16.( re''32 dod''8) dod'' |
    re''2. |
    dod''8
  }
  \tag #'(violino2 violini) {
    re'8.( si'16) \appoggiatura la'4 sold'2\trill |
    s8 la'16.( si'32 la'8) la'16.( si'32 la'8) la' |
    s8 la'16.( si'32 la'8) la'16.( si'32 la'8) la' |
    si'2. |
    la'8
  }
  { s2. | la'8 s s2 | r8 s s2 | }
>> dod'8 re'4 red'\trill |
mi'8 dod'\pocof re'!4 mi'\trill |
la8 <>\f <<
  \tag #'(violino1 violini) {
    dod''16.( re''32 dod''8) dod''16.( re''32 dod''8) dod'' |
    la''-! dod''16.( re''32 dod''8) dod''16.( re''32 dod''8) dod'' |
    fad''8-! fad''~ fad''16 mi''( re'' mi'') re''( dod'') si'( dod'') |
    si'8
  }
  \tag #'(violino2 violini) {
    la'16.( si'32 la'8) la'16.( si'32 la'8) la' |
    dod''8-! la'16.( si'32 la'8) la'16.( si'32 la'8) la' |
    re''8-! re''~ re''16 dod''( si' dod'') si'( la') sold'( la') |
    sold'8
  }
>> r16. mi''32 mi''8.(\trill red''32 mi'') mi''8.(\trill red''32 mi'') |
la''8-! r16. mi''32 mi''8.(\trill red''32 mi'') mi''8.(\trill red''32 mi'') |
si''8-! r16. mi''32 mi''8.(\trill red''32 mi'') mi''8.(\trill red''32 mi'') |
dod'''16-! dod'''( re''' si'') la''( sold'') la''( si'') la''( sold'') fad''( mi'') |
mi''( re'') dod''( re'') re''2 |
dod''16-!\p dod''( re'' si') la'( sold') la'( si') la'( sold') fad'( mi') |
re'4( dod') r |
