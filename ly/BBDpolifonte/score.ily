\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      %\new GrandStaff \with { instrumentName = "Violini" } <<
      %  \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
      %  \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      %>>
      \new Staff \with { instrumentName = "Violini" } <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Polifonte } \withLyrics <<
      \global \keepWithTag #'merope \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2.*7\break s2.*6\break s2.*5\pageBreak
        \grace s8 s2.*5\break s2.*4\break s2.*5\pageBreak
        s2.*5\break \grace s8 s2.*5\break s2.*5\pageBreak
        s2.*6\break s2.*5\break s2.*5\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}