\clef "soprano/treble" R2.*6 |
la'4..\trill sold'16 la'4 |
dod'( re') red' |
mi'8.[\trill red'16] mi'4 r |
fad' si'4. re''16[ si'] |
la'4~ la'8[ sold'16. la'32 si'8.] re'16 |
dod'8.[ si16] la4 r |
la4 dod'8 mi' la'8. dod''16 |
si'8[ red'] red'4. red'8 |
mi'8. si'16 \appoggiatura si'4 la'4. sold'8 |
\appoggiatura sold'8 fad'4 r r8 si' |
si'4 si r8 si |
si4. red'8 fad' la' |
\appoggiatura la'8 sold'8.[ la'16] si'4 r |
dod' la' \appoggiatura sold'8 fad'4 |
red'8.([\melisma mi'32 fad']) si4~ si16.[ la'32 sold'16.\trill fad'32] |
mi'8.([ fad'32 sold']) si4~ si16.[ si'32 la'16. sold'32] |
fad'8.([ sold'32 la']) si4\melismaEnd la!\trill |
sold8 r16. mi'32 mi'8.[\trill red'32 mi'] mi'8.[\trill red'32 mi'] |
si'8 r16. mi'32 mi'8.[\trill red'32 mi'] mi'8.[\trill red'32 mi'] |
dod''4~ dod''8.[ si'32 la' sold'8.] fad'16 |
sold'8 r16. si'32 mi'8. sold'16 si8. la16 |
sold8 r16. si'32 mi'8. sold'16 si8. la16 |
la4( sold) r |
R2.*6 |
mi'4 mi'8[ fad'] fad'[ sol'] |
lad2~ lad8. fad'16 |
\appoggiatura mi'8 re'4 dod' r |
si'4. la'16 sol' fad'8 mi' |
re'8.[ dod'16] si4 r |
sold'! si'4. re'8 |
dod'4( la'4.) dod'8 |
dod'[ si] si4 r |
mi'4 \appoggiatura re'8 dod'4 \appoggiatura si8 la4 |
sol'~ sol'8[ fad'16. sol'32 la'8] sol' |
sol'[ fad'] r4 r8 sold' |
la'4~ la'8.[\trill sold'32 la' si'8] la' |
la'8([ sold']) r4 r |
mi'8[ sold'] sold' si' si' re'' |
re''[ re'] re'2 |
dod'8[ la'] sold'[ la'] mi' sol' |
fad'8.[ si'16] \appoggiatura la'4 sold'2 |
la' r8 la' |
la'4 la r |
si'4 la'16[ sold'] fad'[ mi'] si'8 re' |
dod' la' re'4 red' |
mi'8 la' re'!4 mi' |
la2 r4 |
R2.*9 |
