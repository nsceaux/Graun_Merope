\clef "bass" la4..\trill sold16 la4-! |
dod-! re-! red-! |
mi-! mi,-! r |
re8 re re re re re |
mi mi mi mi mi mi |
la4-! la,-! r |
la4..\p sold16 la4 |
dod re red |
mi mi,-! r |
re re re |
mi mi mi |
la la, r |
la la la |
la la la |
sold fad mi |
si si si |
si si si |
si si si |
mi mi mi |
la, la, la, |
si, si, si, |
si, si, si, |
si, si, si, |
mi,8 r r4 r |
sold,8\f r r4 r |
r8 la,\p la, la, si, si, |
mi,4 sold, si, |
mi sold, si, |
mi,2 r4 |
mi'4..\f\trill red'16 mi'4-! |
sold-! la-! lad-! |
si-! si,-! r |
la8 la la la la la |
si si si si si, si, |
mi4 mi, r |
R2. |
r8 sol\p fad mi re dod |
si,4 fad r |
re4-!\pocof mi-! fad-! |
si-! si,-! r |
mi4\p mi mi |
mi mi mi |
mi mi re |
dod dod dod |
dod dod dod |
re re re |
red red red |
mi mi mi |
mi mi mi |
sold, sold, sold, |
la, si, dod |
re mi mi, |
la, la sold |
fad fad mi |
re mi mi, |
la,8 dod re4 red |
mi8 dod\pocof re!4 mi |
la,4\f la sold |
fad fad mi |
re re re |
mi8 mi mi mi re re |
dod dod dod dod dod dod |
sold, sold, sold, sold, sold, sold, |
la, re mi4 mi, |
fad r8 fad sold mi |
la\p re mi4 mi, |
la,2 r4 |
