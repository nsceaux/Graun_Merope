\clef "alto" la'4..\trill sold'16 la'4-! |
dod'-! re'-! red'-! |
mi'-! mi-! r |
re'8 re' re' re' re' re' |
mi' mi' mi' mi' sold sold |
la4 la r |
la'4..\p\trill sold'16 la'4 |
dod' re' red' |
mi' mi-! r |
fad'4 fad' fad' |
mi' mi' mi' |
la2 r4 |
r8 la' la' la' la' la' |
r fad' fad' fad' fad' fad' |
sold'4 fad' mi' |
si' si' si' |
si' si' si' |
red' red' red' |
mi' mi' mi' |
la la la |
si si si |
si si si |
si si si |
mi'8 r r4 r |
mi'8\f r r4 r |
r8 la\p la la si si |
mi4 sold si |
mi sold si |
mi2 r4 |
mi'4..\trill\f red'16 mi'4-! |
sold'4-! la'-! lad'-! |
si'-! si-! r4 |
la8 la la la la la |
si si si si si si |
si8.\trill la16 sold4 r |
R2. |
r8 sol!\p fad mi re dod |
fad4 fad r |
re''-!\pocof dod''-! dod''-! |
re'' r r |
mi'\p mi' mi' |
mi' mi' mi' |
mi'4 mi' re' |
dod' dod' dod' |
la8 la la la la la |
re'4 re' re' |
red' red' red' |
mi' mi' mi' |
sold'2. |
mi4 mi mi |
la si dod' |
re' mi' mi |
la la sold |
fad fad' mi' |
re' mi' mi |
la8 dod' re'4 red' |
mi'8 dod'\pocof re'!4 mi' |
la4\f la sold |
fad fad' mi' |
re' re' re' |
mi'8 sold' sold' sold' sold' si' |
mi' la' la' la' la' dod'' |
mi' si' si' si' si' si' |
mi' fad' dod'4 si |
la r8 la' si' sold' |
mi'\p fad' dod'4 si |
si( la) r4 |
