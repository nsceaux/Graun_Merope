<<
  \tag #'(egisto basse) {
    \clef "soprano/treble" <>^\markup\character Egisto r4 r8 fa' |
    sib' sib' r sib' re''4 re'' |
    re''8. re''16 re''8 mib'' do'' do'' r do'' |
    mib'' mib'' mib''8. do''16 la'8 la' r fa' |
    la' la' la'8. sib'16 do''4 mib''8 mib''16 re'' |
    sib'8 sib' r16 re'' do'' sib' mi''!8 mi'' r4 |
    sib'8 sib'16 sib' sib'8 la' do'' do'' r4 |
    <<
      \tag #'basse { s1*3 s2 <> \ffclef "treble" }
      \tag #'egisto { R1*3 | r2 }
    >> <>^\markup\character Egisto
    r4 re''8 mib'' |
    do'' do'' do''8. re''16 sib'8 sib' r4 |
    re''4 dod''8 re'' re'' la' r4 |
    fa'8 fa'16 fa' sib'8. la'16 sib'8 sib' re''8. fa''16 |
    lab'4 lab'8 sib' sol' sol' r4 |
    sib'8 sib' sib' do''16 reb'' do''8 do'' re'' mib'' |
    mib'' sib' r4 r2 |
  }
  \tag #'(popolo1 basse) {
    <<
      \tag #'basse { s2 s1*6 \ffclef "treble" }
      \tag #'popolo1 { \clef "soprano/treble" r2 R1*6 }
    >> \ffclef "soprano/treble" <>^\markup\character Popolo
    r4 do''8 do''16 do'' la'8 la' r do'' |
    mib'' mib'' mib''8. fa''16 re''8 re'' r4 |
    re''8 re'' re'' re''16 mib'' do''8. do''16 do''8. re''16 |
    sib'8 sib' r4
    \tag #'popolo1 { r2 R1*6 }
  }
  \tag #'popolo2 {
    \clef "soprano/treble" r2 R1*6
    \clef "soprano/treble"
    r4 la'8 la'16 la' fa'8 fa' r la' |
    la' la' la'8. la'16 fa'8 fa' r4 |
    fa'8 fa' fa' fa'16 sol' sol'8. sol'16 la'8. sib'16 |
    fa'8 fa' r4 r2 |
    R1*6 |
  }
  \tag #'popolo3 {
    \clef "tenor/G_8" r2 R1*6
    \clef "tenor/G_8"
    r4 do'8 do'16 do' do'8 do' r do' |
    do' do' do'8. do'16 sib8 sib r4 |
    sib8 sib sib sib16 sib do'8. do'16 mib'8. re'16 |
    re'8 re' r4 r2 |
    R1*6 |
  }
  \tag #'popolo4 {
    \clef "bass/bass" r2 R1*6
    \clef "bass/bass"
    r4 fa8 fa16 fa fa8 fa r fa |
    fa fa fa8. fa16 sib,8 sib, r4 |
    sib8 sib sib sib16 sib mib8. mib16 fa8. fa16 |
    sib,8 sib, r4 r2 |
    R1*6 |
  }
>>
