\clef "bass" sib2~ |
sib1~ |
sib2 mib~ |
mib1~ |
mib |
re2 sol~ |
sol fa8. fa,16 fa,8. fa,16 |
fa,4 r fa8. fa,16 fa,8. fa,16 |
fa,4 r sib8. sib,16 sib,8. sib,16 |
sib,4 r mib8. mib16 fa8. fa16 |
sib,8. sib16 sib16. sib32 la16. sol32 fad2~ |
fad sol~ |
sol r4 la |
sib1~ |
sib2 mib~ |
mib lab |
r4 sib mib r |
