\tag #'(egisto basse) {
  A -- mi -- ci an -- co -- ra du -- bi -- tar po -- tre -- te
  Del co -- re d’u -- na Ma -- dre,
  D’un fi -- glio, che da lei vie -- ne di -- fe -- so,
  E che del Pa -- dre è al -- le ven -- det -- te in -- te -- so?
}
\tag #'(popolo basse) {
  Nel -- la tua glo -- ria, e più nel no -- stro a -- mo -- re
  Vie -- ni il frut -- to a gu -- star del tuo va -- lo -- re.
}
\tag #'(egisto basse) {
  Non è mia que -- sta glo -- ri -- a, e sol de Nu -- mi.
  O -- gni de -- stin fe -- li -- ce
  Par -- te sol di lor ma -- no,
  E vir -- tù ne di -- scen -- de in pet -- to u -- ma -- no.
}
