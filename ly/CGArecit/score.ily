\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'popolo1 \includeNotes "voix"
      >> \keepWithTag #'popolo \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'popolo2 \includeNotes "voix"
      >> \keepWithTag #'popolo \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'popolo3 \includeNotes "voix"
      >> \keepWithTag #'popolo \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'popolo4 \includeNotes "voix"
      >> \keepWithTag #'popolo \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'egisto \includeNotes "voix"
    >> \keepWithTag #'egisto \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "bassi"
      \origLayout {
        s2 s1*3\break s1*2 s2\break \bar "" s2 s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3\break
      }
      \modVersion { s2 s1*6\break }
    >>
  >>
  \layout { }
  \midi { }
}