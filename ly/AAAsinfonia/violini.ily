\clef "treble" \tag #'violino2 \startHaraKiri
mib'4 sol'8 mib' sib'4 sib' |
mib'' sol''8 fa'' mib''2~ |
mib''8 sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 sol''8 fa'' mib''2~ |
mib''8 sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 sol''8 lab'' sib''4 sib'' |
sib'' sib'~ sib'8 re'' fa'' lab'' |
sol''4 sol''8 lab'' sib''4 sib'' |
sib'' sib'~ sib'8 re'' fa'' lab'' |
sol''4 <<
  \tag #'(violino1 violini) {
    do'''8( sib'') sib''2 |
    lab''8( sol'') do'''( sib'') sib''2 |
    lab''8( sol'') do'''( sib'') lab''4 sol'' |
    \appoggiatura lab''8 sol''4 fa''
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri lab''8( sol'') sol''2 |
    fa''8( mib'') lab''( sol'') sol''2 |
    fa''8( mib'') lab''( sol'') fa''4 mib'' |
    \appoggiatura fa''8 mib''4 re'' \startHaraKiri
  }
>> r2 |
<>\p <<
  \tag #'(violino1 violini) {
    sol'4 do''8( sib') sib'2 |
    lab'8( sol') do''( sib') sib'2 |
    lab'8( sol') do''( sib') lab'4 sol' |
    \appoggiatura lab'8 sol'4 fa'
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri mib'4 lab'8( sol') sol'2 |
    fa'8( mib') lab'( sol') sol'2 |
    fa'8( mib') lab'( sol') fa'4 mib' |
    \appoggiatura fa'8 mib'4 re' \startHaraKiri
  }
>> r2 |
sib4\f re'8 sib fa'4 fa' |
sib'4 re''8 do'' sib'2~ |
sib'8 sol'' fa'' mib'' re'' do'' sib' la' |
sib'4 re''8 do'' sib'2~ |
sib'8 sib'' la'' sol'' fa'' mib'' re'' do'' |
re'' mib'' fa'' sol'' fa'' sol'' do'' mib'' |
re'' mib'' fa'' sol'' fa'' sol'' do'' mib'' |
re'' mib'' fa'' sol'' fa'' fa'' sol'' la'' |
la''2( sib''4) r |
sib'8 do'' re'' mib'' re'' re'' mib'' fa'' |
fa''2( sol''4) r |
sol'8 la' sib' do'' sib' sib' do'' re'' |
mib'' fa'' sol'' fa'' sol'' mib'' re'' do'' |
do'''4. sib''8 la'' sol'' fa'' mib'' |
re'' mib'' fa'' mib'' fa'' re'' do'' sib' |
sib''4. la''8 sol'' fa'' mib'' re'' |
sol''4. fa''8 mib'' re'' do'' sib' |
la'8 sol' fa'4 r2 |
<>\p <<
  \tag #'(violino1 violini) {
    do''4. re''8 mib''4 mib'' |
    re''8( mib'' fa'' sol'') fa''4 la''~ |
    la'' sib''2 re''4 |
    \appoggiatura mib''8 re''4 do''
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri la'4. sib'8 do''4 do'' |
    sib'8( do'' re'' mib'') re''4 do''~ |
    do'' re''2 sib'4 |
    \appoggiatura do''8 sib'4 la' \startHaraKiri
  }
>> r2 |
<>\p <<
  \tag #'(violino1 violini) {
    do''4. re''8 mib''4 mib'' |
    re''8( mib'' fa'' sol'') fa''4 la''~ |
    la'' sib''2 re''4 |
    \appoggiatura mib''8 re''4 do''
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri la'4. sib'8 do''4 do'' |
    sib'8( do'' re'' mib'') re''4 do''~ |
    do'' re''2 sib'4 |
    \appoggiatura do''8 sib'4 la' \startHaraKiri
  }
>> r2 |
fa''8\f sib' la' sib' do'' sib' la' sib' |
fa'' sib' la' sib' do'' sib' la' sib' |
mi'' sib' la' sib' do'' sib' la' sib' |
sol'' sib' la' sib' do'' sib' la' sib' |
la' mib' re' mib' fa' mib' re' mib' |
do'' mib' re' mib' fa' mib' re' mib' |
re'4 <>\p <<
  \tag #'(violino1 violini) {
    re''8( mib'' fa''4) fa''~ |
    fa'' do''8( re'' mib''4) mib''~ |
    mib'' re''8( mib'' fa''4) fa''~ |
    fa'' do''8( re'' mib''4) mib'' |
    re''8
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri sib'8( do'' re''4) re''~ |
    re'' la'8( sib' do''4) do''~ |
    do'' sib'8( do'' re''4) re''~ |
    re'' la'8( sib' do''4) do'' |
    sib'8 \startHaraKiri
  }
  { s2. s1*3 <>\f }
>>
sib''8 la'' sol'' fa'' sol'' fa'' mib'' |
\appoggiatura mib''8 re''4 do''8 sib' do''2\trill |
sib'4 <>\p <<
  \tag #'(violino1 violini) {
    re''8( mib'' fa''4) re'' |
    s do''8( re'' mib''4) do'' |
    s re''8( mib'' fa''4) re'' |
    s do''8( re'' mib''4) do'' |
    re''8
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri sib'8( do'' re''4) sib' |
    s la'8( sib' do''4) la' |
    s sib'8( do'' re''4) sib' |
    s la'8( sib' do''4) la' |
    sib'8 \startHaraKiri
  }
  { s2. | r4 s2. | r4 s2. | r4 s2. | <>\f }
>> sib''8 sib'' sib'' sib'' sib'' sib'' sib'' |
re''' sib'' sib'' sib'' sib'' sib'' sib'' sib'' |
<<
  \tag #'(violino1 violini) {
    re'''4 do'''8 sib'' la'' sol'' fa'' mib'' |
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri sib''4 la''8 sol'' fa'' mib'' re'' do'' |
    \startHaraKiri
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { \appoggiatura mib''8 re''4 do''8 sib' do''2\trill | }
  { \stopHaraKiri sib'2 la'\trill | \startHaraKiri }
>>
sib'2 r |
mib'4 sol'8 mib' sib'4 sib' |
mib'' sol''8 fa'' mib''2~ |
mib''8 sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 sol''8\p fa'' mib''2~ |
mib''8 sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 sol''8\f lab'' sib''4 sib'' |
sib'' sib'~ sib'8 re'' fa'' lab'' |
sol''4 sol''8\p lab'' sib''4 sib'' |
sib'' sib'~ sib'8 re'' fa'' lab'' |
sol''4 mib''8\f fa'' sol''4 sol'' |
sol'' sol'~ sol'8 si' re'' fa'' |
mib''4 mib''8\p fa'' sol''4 sol'' |
sol'' sol'~ sol'8 si' re'' fa'' |
mib''4 do''8\f re'' mib''4 mib'' |
mib'' mib'~ mib'8 sol' sib' reb'' |
\appoggiatura reb''8 do''4. fa''8 \appoggiatura mib''8 re''4. sol''8 |
\appoggiatura fa''8 mib''4. lab''8 \appoggiatura sol''8 fa''4. sib''8 |
lab''8( sol'') <<
  \tag #'(violino1 violini) {
    do'''8( sib'') sib''2 |
    lab''8( sol'') do'''( sib'') sib''2 |
    lab''8( sol'') do'''( sib'') lab''4 sol'' |
    \appoggiatura lab''8 sol''4 fa''
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri lab''8( sol'') sol''2 |
    fa''8( mib'') lab''( sol'') sol''2 |
    fa''8( mib'') lab''( sol'') fa''4 mib'' |
    \appoggiatura fa''8 mib''4 re'' \startHaraKiri
  }
>> r2 |
<>\p <<
  \tag #'(violino1 violini) {
    sol'4 do''8( sib') sib'2 |
    lab'8( sol') do''( sib') sib'2 |
    lab'8( sol') do''( sib') lab'4 sol' |
    \appoggiatura lab'8 sol'4 fa'
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri mib'4 lab'8( sol') sol'2 |
    fa'8( mib') lab'( sol') sol'2 |
    fa'8( mib') lab'( sol') fa'4 mib' |
    \appoggiatura fa'8 mib'4 re' \startHaraKiri
  }
>> r2 |
sib''8\f reb'' do'' reb'' sib'' reb'' do'' reb'' |
do'' sib'' sib''( lab'') lab''( sol'') sol''( lab'') |
lab'' do'' si' do'' lab'' do'' si' do'' |
si' lab''! lab''( sol'') sol''( fad'') fad''( sol'') |
sol'' sib'! la' sib' sol'' sib' la' sib' |
la' sol'' sol''( fa'') fa''( mi'') mi''( fa'') |
fa'' lab'! sol' lab' fa'' lab' sol' lab' |
sol' sib' do'' re'' mib'' fa'' sol'' lab'' |
sol'' lab'' sib'' do''' sib'' do''' fa'' lab'' |
sol'' lab'' sib'' do''' sib'' do''' fa'' lab'' |
sol'' lab'' sib'' do''' sib'' sib'' do''' re''' |
re'''2( mib'''4) r |
mib''8\p fa'' sol'' lab'' sol'' sol'' lab'' sib'' |
sib''2( do'''4) r |
do''8\f re'' mib'' fa'' mib'' mib'' fa'' sol'' |
lab'' sib'' do''' sib'' do''' lab'' sol'' fa'' |
sol'' lab'' sib'' lab'' sib'' sol'' fa'' mib'' |
re'' do'' sib'4 r2 |
<>\p <<
  \tag #'(violino1 violini) {
    fa''4. sol''8 lab''4 lab'' |
    sol''8( lab'' sib'' do''') sib''4 re'''~ |
    re''' mib'''2 sol''4 |
    \appoggiatura lab''8 sol''4 fa''
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri re''4. mib''8 fa''4 fa'' |
    mib''8( fa'' sol'' lab'') sol''4 fa''~ |
    fa'' sol''2 mib''4 |
    \appoggiatura fa''8 mib''4 re''
  }
>> r2 |
<>\p <<
  \tag #'(violino1 violini) {
    fa'4. sol'8 lab'4 lab' |
    sol'8( lab' sib' do'') sib'4 re''~ |
    re'' mib''2 sol'4 |
    \appoggiatura lab'8 sol'4 fa'
  }
  \tag #'(violino2 violini) {
    re'4. mib'8 fa'4 fa' |
    mib'8( fa' sol' lab') sol'4 fa'~ |
    fa' sol'2 mib'4 |
    \appoggiatura fa'8 mib'4 re' \startHaraKiri
  }
>> r2 |
sib''8\f mib'' re'' mib'' fa'' mib'' re'' mib'' |
sib'' mib'' re'' mib'' fa'' mib'' re'' mib'' |
la'' mib'' re'' mib'' fa'' mib'' re'' mib'' |
do''' mib'' re'' mib'' fa'' mib'' re'' mib'' |
re'' lab'! sol' lab' sib' lab' sol' lab' |
fa'' lab' sol' lab' sib' lab' sol' lab' |
sol'4 <>\p <<
  \tag #'(violino1 violini) {
    sol''8( lab'' sib''4) sib''~ |
    sib'' fa''8( sol'' lab''4) lab''~ |
    lab'' sol''8( lab'' sib''4) sib''~ |
    sib'' fa''8( sol'' lab''4) lab'' |
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri mib''8( fa'' sol''4) sol''~ |
    sol'' re''8( mib'' fa''4) fa''~ |
    fa'' mib''8( fa'' sol''4) sol''~ |
    sol'' re''8( mib'' fa''4) fa'' |
  }
>>
sol''8\f mib'' re'' do'' sib' do'' sib' lab' |
\appoggiatura lab'8 sol'4 fa'8 mib' fa'2\trill |
mib'4 <>\p <<
  \tag #'(violino1 violini) {
    sol''8( lab'' sib''4) sol'' |
    s fa''8( sol'' lab''4) fa'' |
    s sol''8( lab'' sib''4) sol'' |
    s fa''8( sol'' lab''4) fa'' |
  }
  \tag #'(violino2 violini) {
    mib''8( fa'' sol''4) mib'' |
    s re''8( mib'' fa''4) re'' |
    s mib''8( fa'' sol''4) mib'' |
    s re''8( mib'' fa''4) re'' |
  }
  { s2. r4 s2. r4 s2. r4 }
>>
sol''8\f mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
sol'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
<<
  \tag #'(violino1 violini) {
    sol''4 fa''8 mib'' re'' do'' sib' lab' |
  }
  \tag #'(violino2 violini) {
    mib''4 re''8 do'' sib' lab' sol' fa' |
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { \appoggiatura lab'8 sol'4 fa'8 mib' fa'2\trill | }
  { mib'2 re'\trill | \startHaraKiri }
>>
mib'4 sol'8 mib' sib'4 sib' |
mib'' sol''8 fa'' mib''2~ |
mib''8 sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 sol'8\p fa' mib'2~ |
mib'8 sol' fa' mib' re' do' sib lab |
lab1( |
sol2) r |
