\clef "alto" mib'4 sol'8 mib' sib'4 sib' |
mib''2 r4 sib' |
do''2 fa' |
mib' r4 sib' |
do''2 fa' |
mib' r4 sol' |
fa' mib' fa' re' |
mib'2 r4 sol' |
fa' mib' fa' re' |
mib'2 r4 mib |
mib'2 r4 mib |
mib'2 sib'4 sib' |
sib'2 r |
mib'2\p r4 mib |
mib'2 r4 mib |
mib'2 sib4 sib |
sib2 r |
sib4\f re'8 sib fa'4 fa' |
sib'2 r4 fa' |
sol'2 fa' |
re' r4 fa' |
sol'2 la' |
sib'4 re'' do'' la' |
sib' re'' do'' la' |
fa'2 do'' |
r4 sib' sib' sib' |
re'2 la' |
r4 sol' sol' sol' |
sib2 fa' |
sol' r |
fa'4 fa' fa' fa' |
fa'2 r |
fa'4 fa' sib' sib' |
sib sib sol' sol' |
fa'2 r |
fa'4\p fa' fa' fa' |
fa' fa' fa' fa' |
fa' fa' fa' fa' |
fa'2 r |
fa'4 fa' fa' fa' |
fa' fa' fa' fa' |
fa' fa' fa' fa' |
fa'2 r |
fa'1\f |
fa' |
sol' |
do' |
do' |
fa |
fa4 r r2 |
fa'1\p |
fa' |
fa' |
fa'4\f mib' fa' sol' |
sib2 la\trill |
sib4 r r2 |
fa'1\p |
fa' |
fa' |
fa'4 mib' fa' mib' |
re' mib' fa' mib' |
re' mib' fa' sol' |
fa'2 mib'\trill |
re' r |
mib'4 sol'8 mib' sib'4 sib' |
mib''2 r4 sib' |
do''2 fa' |
mib' r4 sib'\p |
do''2 fa' |
mib' r4 sol'\f |
fa' mib' fa' re' |
mib'2 r4 sol'\p |
fa'4 mib' fa' re' |
mib'2 r4 mib'\f |
re' do' re' si |
do'2 r4 mib'\p |
re' do' re' si |
do'2 r4 do''\f |
sib' lab'! sib' sol' |
mib' fa'2 sol'4~ |
sol' lab'2 sib'4 |
sib'2 r4 mib |
mib'2 r4 mib |
mib'2 sib'4 sib' |
sib'2 r |
mib'2\p r4 mib |
mib'2 r4 mib |
mib'2 sib4 sib |
sib2 re'4 fa' |
mib'1\f |
mib'4 r r2 |
fa'1 |
re'4 r r2 |
do'1 |
do'4 r r2 |
sib1 |
sib4 r r2 |
sib4 sol' fa' re' |
sib sol' fa' re' |
mib'2 fa' |
r4 mib' mib' mib' |
sol'2 re' |
r4 do' do' do' |
mib'2\f sib |
do' fa' |
sib sol' |
fa' r |
sib'4\p sib' sib' sib' |
sib' sib' sib' sib' |
sib' sib' sib' sib' |
sib'2 r |
sib4 sib sib sib |
sib sib sib sib |
sib sib sib sib |
sib2 r |
sib1\f |
sib |
do' |
fa' |
fa' |
sib |
sib4 r r2 |
sib'1\p |
sib' |
sib' |
sib'4\f lab' sol'8 lab' sol' fa' |
mib'2 re'\trill |
mib'4 r r2 |
sib'1\p |
sib' |
sib' |
sib'2\f sib'4 lab' |
sol' lab' sib' lab' |
sol' lab' sib' do'' |
sib2 sib |
mib'4 sol'8 mib' sib'4 sib' |
mib''2 r4 sib' |
do''2 fa' |
mib' r4 sib |
do'2 fa |
fa1( |
mib2) r |
