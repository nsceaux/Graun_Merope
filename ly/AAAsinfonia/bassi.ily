\clef "bass" mib4 sol8 mib sib4 sib |
mib'2 r4 sol |
lab2 sib |
mib r4 sol |
lab2 sib |
mib r4 mib' re' do' re' sib |
mib2 r4 mib' |
re' do' re' sib |
mib'2 r4 mib |
mib'2 r4 mib |
mib'2 re'4 mib' |
sib2 r |
mib\p r4 mib, |
mib2 r4 mib, |
mib2 re4 mib |
sib,2 r |
sib,4\f re8 sib, fa4 fa |
sib2 r4 re |
mib2 fa |
sol r4 re |
mib2 fa |
sib,4 sib la fa |
sib, sib la fa |
sib2 la |
r4 sol sol sol |
sol2 fa |
r4 mib mib mib |
mib2 re |
do r |
la4 la la la |
sib2 r |
re4 re re re |
mib mib mib mib |
fa4. la8 do'4 fa' |
fa2\p r |
R1*2 |
fa4.\f la8 do'4 fa' |
fa2\p r |
R1*2 |
fa4.\f la8 do'4 mib' |
re'1 |
reb' |
do' |
mi |
fa |
la, |
sib,4 r r2 |
fa1\p |
sib |
fa' |
sib,4\f do re mib |
fa2 fa, |
sib,4 r r2 |
fa1\p |
sib |
fa' |
sib,4\f do re do |
sib, do re do |
sib, do re mib |
fa2 fa, |
sib,4 sib8 lab! sol4 fa |
mib sol8 mib sib4 sib |
mib'2 r4 sol |
lab2 sib |
mib2 r4 sol\p |
lab2 sib |
mib r4 mib'\f re' do' re' sib |
mib'2 r4 mib'\p |
re' do' re' sib |
mib'2 r4 do'\f |
si la si sol |
do'2 r4 do'\p |
si la si sol |
do'2 r4 lab!\f |
sol fa sol mib |
lab2 sib |
do' re' |
mib' r4 mib |
mib'2 r4 mib |
mib'2 re'4 mib' |
sib2 r |
mib\p r4 mib, |
mib2 r4 mib, |
mib2 re4 mib |
sib,2 sib4\f lab |
sol1 |
lab4 r r2 |
fa1 |
sol4 r r2 |
mi1 |
fa4 r r2 |
re1 |
mib4 r r2 |
mib4 mib' re' sib |
mib mib' re' sib |
mib'2 re' |
r4 do' do' do' |
do'2\p sib |
r4 lab lab lab |
lab2\f sol |
fa re |
mib mib, |
sib,4. re8 fa4 sib |
sib,2\p r |
R1*2 |
sib,4.\f re8 fa4 sib |
sib,2\p r |
R1*2 |
sib,4.\f re8 fa4 lab! |
sol1 |
solb |
fa |
la, |
sib, |
re, |
mib,4 r r2 |
sib,1\p |
mib |
sib |
mib4\f fa sol lab |
sib2 sib, |
mib,4 r r2 |
sib,1\p |
mib |
sib |
mib4\f fa sol fa |
mib fa sol fa |
mib fa sol lab |
sib2 sib, |
mib4 sol8 mib sib4 sib |
mib'2 r4 sol |
lab2 sib |
mib r4 sol,\p |
lab,2 sib, |
mib,1~ |
mib,2 r |
