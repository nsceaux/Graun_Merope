\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Corni" } <<
      \keepWithTag #'corni \global
      \keepWithTag #'corni \includeNotes "corni"
    >>
    \new GrandStaff \with { instrumentName = "Violini" } <<
      \new Staff <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
    >>
    \new Staff \with { instrumentName = "Viola" } <<
      \global \includeNotes "viola"
    >>
    \new Staff \with { instrumentName = "Basso" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*4 s2 \bar "" \break s2 s1*6\break s1*7\pageBreak
        s1*7\break s1*7\break s1*7\pageBreak
        s1*7\break s1*6\break s1*7\pageBreak
        s1*7\break s1*7\break s1*7\pageBreak
        \grace s8 s1*6\break s1*6 s2 \bar "" \break s2 s1*6 \pageBreak
        s1*6\break s1*6 s2 \bar "" \break s2 s1*6\pageBreak
        s1*7\break s1*6\break s1*6\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}