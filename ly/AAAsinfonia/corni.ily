\clef "treble" \transposition mib
do'4 mi'8 do' sol'4 sol' |
do''2 r4 <<
  \tag #'(corno1 corni) {
    mi''4 |
    re''2 re'' |
    mi'' s4 mi'' |
    re''2 re'' |
    mi'' s4 sol'' |
  }
  \tag #'(corno2 corni) {
    do''4 |
    do''2 sol' |
    do'' s4 do'' |
    do''2 sol' |
    do'' s4 mi'' |
  }
  { s4 s1 s2 r4 s s1 s2 r4 s }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''2 fa''4 | }
  { re''4 do'' re'' re'' | }
>>
<<
  \tag #'(corno1 corni) { mi''2 s4 sol'' | }
  \tag #'(corno2 corni) { do''2 s4 mi'' | }
  { s2 r4 s }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''2 fa''4 | }
  { re''4 do'' re'' re'' | }
>>
<<
  \tag #'(corno1 corni) {
    mi''4 la''8( sol'') sol''2 |
    s4 la''8( sol'') sol''2 |
    s4 la''8 sol'' fa''4 mi'' |
    mi'' re''
  }
  \tag #'(corno2 corni) {
    do''4 fa''8( mi'') mi''2 |
    s4 fa''8( mi'') mi''2 |
    s4 fa''8 mi'' re''4 do'' |
    do'' sol'
  }
  { s1 | r4 s2. | r4 s2. }
>> r2 |
R1*4 |
sol'2 re''4 re'' |
sol''2 r4 re'' |
mi''2 fad'' |
sol'' r4 re'' |
mi''2 fad'' |
sol''4 <<
  \tag #'(corno1 corni) {
    si''4 la'' fad'' |
    sol'' si'' la'' fad'' |
    sol''2 la'' |
    s4 sol'' sol'' sol'' |
    sol''2 fad'' |
    s4 mi'' mi'' mi'' |
    mi''2 s |
    mi''
  }
  \tag #'(corno2 corni) {
    re''4 re'' re'' |
    re'' re'' re'' re'' |
    re''2 re'' |
    s4 mi'' mi'' mi'' |
    mi''2 re'' |
    s4 do'' do'' do'' |
    do''2 s |
    do''
  }
  { s2. | s1*2 | r4 s2. | s1 | r4 s2. | s2 re'' }
>> r2 |
re''4 re'' re'' re'' |
<<
  \tag #'(corno1 corni) {
    re''2 s |
    re''4 re'' re'' re'' |
    mi'' mi'' mi'' mi'' |
  }
  \tag #'(corno2 corni) {
    sol'2 s |
    sol'4 sol' sol' sol' |
    do'' do'' do'' do'' |
  }
  { s2 r | }
>>
re''2 r |
R1*3 |
re''4. re''8 re''4 re'' |
re''2 r |
R1*2 |
re''4. re''8 re''4 re'' |
re''1 |
re'' |
mi''1 |
mi'' |
re'' |
re'' |
sol'4 r r2 |
R1*3 |
re''4 do'' re'' mi'' |
re''2 re'' |
sol'4 r r2 |
R1*3 |
re''2 re'' |
re'' re'' |
re''4 do'' re'' mi'' |
re''2 re'' |
sol' r |
do'4 mi'8 do' sol'4 sol' |
do''2 r4 <<
  \tag #'(corno1 corni) {
    mi''4 |
    re''2 re'' |
    mi'' s4 mi'' |
    re''2 re'' |
    mi'' s4 sol'' |
  }
  \tag #'(corno2 corni) {
    do''4 |
    do''2 sol' |
    do'' s4 do'' |
    do''2 sol' |
    do'' s4 mi''
  }
  { s4 s1 s2 r4 s\p s1 s2 r4 s\f }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''2 fa''4 | }
  { re''4 do'' re'' re'' | }
>>
<<
  \tag #'(corno1 corni) { mi''2 s4 sol'' | }
  \tag #'(corno2 corni) { do''2 s4 mi'' | }
  { s2 r4 s\p }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''2 fa''4 | mi''2 }
  { re''4 do'' re'' re'' | do''2 }
  { s1 s2\f }
>> r4 mi'' |
mi'' mi''2 re''4 |
do''2 r4 mi''4\p |
mi'' mi''2 re''4 |
do''2 r4 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { la''4 | sol'' fa'' sol'' mi'' | }
  { do''4 | do''2 do'' | }
>>
do''4 re''2 mi''4~ |
mi'' fa''2 sol''4 |
sol'' <<
  \tag #'(corno1 corni) {
    la''8( sol'') sol''2 |
    s4 la''8( sol'') sol''2 |
    s4 la''8 sol'' fa''4 mi'' |
    mi'' re''
  }
  \tag #'(corno2 corni) {
    fa''8( mi'') mi''2 |
    s4 fa''8( mi'') mi''2 |
    s4 fa''8 mi'' re''4 do'' |
    do'' sol'
  }
  { s2. | r4 s2. | r4 }
>> r2 |
R1*4 |
do''1\f |
do''4 r r2 |
re''1 |
mi''4 r r2 |
mi''1 |
re''4 r r2 |
re''1 |
do''4 r r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''4 sol''2 fa''4 |
    mi''4 sol''2 fa''4 | }
  { do''4 mi'' re'' re'' |
    do'' mi'' re'' re'' | }
>>
<<
  \tag #'(corno1 corni) {
    mi''2 s |
    s4 mi'' mi'' mi'' |
    mi''2 sol'' |
    s4 la'' la'' la'' |
    la''2 sol'' |
    fa'' fa'' |
    mi'' mi'' |
    re''
  }
  \tag #'(corno2 corni) {
    do''2 s |
    s4 do'' do'' do'' |
    do''2 re'' |
    s4 do'' do'' do'' |
    do''2 do'' |
    re'' re'' |
    do'' do'' |
    sol'
  }
  { s2 re'' | r4 s2. | s1\p | r4 s2. | s1\f }
>> r2 |
R1*3 |
sol'4.\f sol'8 sol'4 sol' |
sol'2 r |
R1*2 |
sol'4. sol'8 sol'4 sol' |
sol'1 |
sol' |
re'' |
re'' |
<<
  \tag #'(corno1 corni) { re''1 | re'' | }
  \tag #'(corno2 corni) { sol'1 | sol' | }
>>
do''4 r r2 |
R1*3 |
<<
  \tag #'(corno1 corni) {
    mi''4 fa'' sol'' fa'' |
    mi''2 re'' |
    do''4
  }
  \tag #'(corno2 corni) {
    do''4 re'' mi'' re'' |
    do''2 sol' |
    mi'4
  }
>> r4 r2 |
R1*3 |
<<
  \tag #'(corno1 corni) {
    mi''4 fa'' sol'' fa'' |
    mi'' fa'' sol'' fa'' |
    mi'' fa'' sol'' la'' |
    mi''2 re'' |
    do''4 mi''8 do'' sol''4 sol'' |
  }
  \tag #'(corno2 corni) {
    do''4 re'' mi'' re'' |
    do'' re'' mi'' re'' |
    do'' re'' mi'' re'' |
    do''2 sol' |
    do'4 mi'8 do' sol'4 sol' |
  }
>>
do''2 r4 <<
  \tag #'(corno1 corni) {
    mi''4 |
    re''2 re'' |
    do''
  }
  \tag #'(corno2 corni) {
    do''4 |
    do''2 sol' |
    mi'
  }
>> r2 |
R1*3 |
