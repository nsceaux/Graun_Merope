\clef "alto" mib'8. mib'16 sol'8. sol'16 |
sib'8. sib'16 lab' sol' fa' mib' |
lab'8 lab'16 sib' do''8 re'' |
mib''8. mib'16 mib'4 |
mib'8. mib'16 sol'8. sol'16 |
sib'8. sib'16 lab' sol' fa' mib' |
lab'8 lab'16 sib' do''8 re'' |
mib''8. mib'16 mib'4 |
mib'8 mib' mib' mib' |
mib' mib' mib' mib' |
mib' mib' mib' mib' |
mib' mib' mib' mib' |
sib sib sib sib |
mib' fa' sol' lab' |
fa' fa' mib' mib' |
sib sib sib sib |
sib sib sib sib |
sib mib' mib' mib' |
mib' mib' mib' mib' |
mib' mib' mib' mib' |
do' do' do' do' |
do' do' do' do' |
sib sib sib sib |
sib sib sib mib' |
mib' mib' mib' fa' |
sib sib sib sib |
mib' mib' mib' fa' |
sib sib sib sib |
sib sib sib sib |
sib sib sib sib |
do'-\sug\p do' do' do' |
do' sib sib sib |
fa'-\sug\f fa' fa' fa' |
mib' lab sib do' |
sib sib sib sib |
sol-\sug\ff sol sol sol |
sol4 r |
sib8 sib sib sib |
sib4 r |
mib'8 mib' mib' mib' |
mib'4 r |
sol'8 sib' sib' sib' |
sib' sol' lab' lab' |
sib' sib' sib' sib' |
sib' sol' lab' lab' |
sib' sib' sib' sib' |
mib' mib' mib' mib' |
mib' do' do' do' |
mib' do' do' do' |
do'' do'' do'' do'' |
fa' fa' fa' fa' |
fa' re' re' re' |
fa' re' re' re' |
sib sib sib sib |
sib sib sib sib |
sib sib sib sib |
sib sib sib sib |
re' re' re' re' |
sib sib sib sib |
mib'8. mib'16 sol'8. sol'16 |
sib'8. sib'16 sib'16 lab' sol' fa' |
lab'8 lab'16 sib' do''8 re'' |
mib''4 r8 mib' |
do''4 r8 do'' |
sib'4 r8 sib' |
mib'4 re' |
mib' r |
mib' r |
mib' r |
mib' re' |
mib'8 mib16 mib mib8 mib |
mib4 r |
