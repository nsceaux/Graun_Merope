\clef "treble" \tag #'violino2 \startHaraKiri
mib'8. mib'16 sol'8. sol'16 |
sib'8. sib'16 lab' sol' fa' mib' |
lab'8 lab'16 sib' do''8 re'' |
mib''8. mib'16 mib'4 |
mib'8. mib'16 sol'8. sol'16 |
sib'8. sib'16 lab' sol' fa' mib' |
lab'8 lab'16 sib' do''8 re'' |
mib''16 sol'' mib'' sib' sol' sib' sol' mib' |
<<
  \tag #'violino1 {
    do''16 lab'' lab'' do'' do'' lab'' lab'' do'' |
    do'' sib' sib' sib' sib' sib' sib' sib' |
    do'' lab'' lab'' do'' do'' lab'' lab'' do'' |
    do'' sib' sib' sib' sib' sib' sib' sib' |
    fa'' lab' lab' lab' fa'' lab' lab' lab' |
  }
  \tag #'violino2 {
    \stopHaraKiri lab'16 do'' do'' lab' lab' do'' do'' lab' |
    lab' sol' sol' sol' sol' sol' sol' sol' |
    lab' do'' do'' lab' lab' do'' do'' lab' |
    lab' sol' sol' sol' sol' sol' sol' sol' |
    lab' fa' fa' fa' lab' fa' fa' fa' |
  }
>>
sol'16 mib'' lab' mib'' sib' mib'' do'' mib'' |
re'' lab'' fa'' re'' mib'' sib' mib''
<<
  \tag #'violino1 {
    sol'16 |
    sol' fa' fa' fa' fa' fa' fa' fa' |
    fa'' lab' lab' lab' fa'' lab' lab' lab' |
    sol' mib' sib' sol' mib'' sib' sol'' mib'' |
    sib'' reb'' reb'' reb'' sib'' reb'' reb'' reb'' |
    do'' lab' mib'' do'' lab'' do'' reb'' do'' |
    sol'' sib' sib' sib' sol'' sib' sib' sib' |
    sib' lab' sol' lab' lab' lab' do'' lab' |
    fa'' lab' lab' lab' fa'' lab' lab' lab' |
    lab' sol' fa' sol' sol' sol' sib' sol' |
  }
  \tag #'violino2 {
    mib'16 |
    mib' re' re' re' re' re' re' re' |
    lab' fa' fa' fa' lab' fa' fa' fa' |
    mib' sib sol' mib' sib' sol' mib'' sib' |
    reb'' sib' sib' sib' reb'' sib' sib' sib' |
    lab' mib' do'' lab' do'' lab' sib' lab' |
    sib' sol' sol' sol' sib' sol' sol' sol' |
    sol' fa' mi' fa' fa' fa' lab' fa' |
    lab' fa' fa' fa' lab' fa' fa' fa' |
    fa' mib' re' mib' mib' mib' sol' mib' |
  }
>> 
do''16 lab' sol' lab' do'' lab' re'' sib' |
mib'' sib' lab' sol' fa' mib' re' mib' |
do'' lab' sol' lab' do'' lab' re'' sib' |
mib'' sib' lab' sol' fa' mib' re' mib' |
<<
  \tag #'violino1 {
    lab'16 fa'' fa'' lab' sol' mib'' mib'' sol' |
    sol' fa' fa' fa' fa' fa' fa' fa' |
    la'\p la' la' la' la' mib'' mib'' mib'' |
    mib'' re'' re'' re'' re'' re'' re'' re'' |
    re''\f re'' re'' re'' re'' lab''! lab'' lab'' |
    sol'' sol'' fa'' mib'' re'' do'' sib' lab' |
    sol' sol' sol' sol' fa' fa' fa' fa' |
  }
  \tag #'violino2 {
    fa'16 lab' lab' fa' mib' sol' sol' mib' |
    mib' re' re' re' re' re' re' re' |
    mib'\p mib' mib' mib' mib' la' la' la' |
    la' sib' sib' sib' sib' sib' sib' sib' |
    lab'!\f lab' lab' lab' lab' re'' re'' re'' |
    mib'' mib'' re'' do'' sib' lab' sol' fa' |
    mib' mib' mib' mib' re' re' re' re' |
  }
>>
mib'16\ff sib sol sib mib' sib sol sib |
mib' sol lab sib do' re' mib' fa' |
<<
  \tag #'violino1 {
    sol'16 mib' sib mib' sol' mib' sib mib' |
    sol' sib do' re' mib' fa' sol' lab' |
    sib' sol' mib' sol' sib' sol' mib' sol' |
    sib'
  }
  \tag #'violino2 {
    mib'16 sib sol sib mib' sib sol sib |
    mib' sol lab sib do' re' mib' fa' |
    sol' mib' sib mib' sol' mib' sib mib' |
    sol'  \startHaraKiri
  }
>> mib'16 fa' sol' lab' sib' do'' re'' |
mib'' sib' sol'' sib' fa'' sib' lab'' sib' |
sol'' sib' sib'' sib' re'' sib' fa'' sib' |
mib'' sib' sol'' sib' fa'' sib' lab'' sib' |
sol'' sib' sib'' sib' re'' sib' fa'' sib' |
mib'' sib' mib'' sib' sol'' mib'' sol'' mib'' |
sib'' lab'' sol'' fa'' mib'' reb'' do'' sib' |
do'' lab' sol' lab' sib' lab' sol' lab' |
do'' lab' sol' lab' sib' lab' sol' lab' |
fa'' do'' fa'' do'' la'' fa'' la'' fa'' |
do''' sib'' la'' sol'' fa'' mib'' re'' do'' |
re'' sib' la' sib' do'' sib' la' sib' |
re'' sib' la' sib' do'' sib' la' sib' |
<<
  \tag #'violino1 {
    fa'' sib' lab''! sib' sol'' sib' fa'' sib' |
    sol'' sib' sib'' sib' lab'' sib' sol'' sib' |
    fa'' sib' lab'' sib' sol'' sib' fa'' sib' |
    sol'' sib' sib'' sib' lab'' sib' sol'' sib' |
  }
  \tag #'violino2 {
    \stopHaraKiri re'' sib' fa'' sib' mib'' sib' re'' sib' |
    mib'' sib' sol'' sib' fa'' sib' mib'' sib' |
    re'' sib' fa'' sib' mib'' sib' re'' sib' |
    mib'' sib' sol'' sib' fa'' sib' mib'' sib' | \startHaraKiri
  }
>>
fa''16 mib'' re'' do'' sib' lab' sol' fa' |
fa'' mib'' re'' do'' sib' lab' sol' fa' |
mib'8. mib'16 sol'8. sol'16 |
sib'8. sib'16 lab' sol' fa' mib' |
lab'8 lab'16 sib' do''8 re'' |
mib''16 mib'' re'' mib'' sol'' mib'' re'' mib'' |
do'' mib'' re'' mib'' sol'' mib'' re'' mib'' |
sib' mib'' re'' mib'' sol'' mib'' re'' mib'' |
do'' mib'' re'' do'' sib' lab' sol' fa' |
mib' mib'' re'' mib'' sol'' mib'' re'' mib'' |
do'' mib'' re'' mib'' sol'' mib'' re'' mib'' |
sib' mib'' re'' mib'' sol'' mib'' re'' mib'' |
do'' mib'' re'' do'' sib' lab' sol' fa' |
mib'8 <<
  \tag #'violino1 { sib16 sib sib8 sib | sib4 }
  \tag #'violino2 { \stopHaraKiri sol16 sol sol8 sol | sol4 }
>> r |
