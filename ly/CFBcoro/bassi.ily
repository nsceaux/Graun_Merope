\clef "bass" mib8. mib16 sol8. sol16 |
sib8. sib16 lab sol fa mib |
lab8 lab16 sib do'8 re' |
mib'8. mib16 mib4 |
mib8. mib16 sol8. sol16 |
sib8 sib lab16 sol fa mib |
lab8 lab16 sib do'8 re' |
mib'8. mib16 mib4 |
mib8 mib mib mib |
mib mib mib mib |
mib mib mib mib |
mib mib mib mib |
re re re re |
mib fa sol lab |
sib sib sol mib |
sib sib, sib, sib, |
re re re re |
mib mib mib mib |
sol sol sol sol |
lab lab lab lab |
mi mi mi mi |
fa fa fa fa |
re re re re |
mib mib mib mib |
lab lab lab lab |
sol sol sol sol |
lab lab lab lab |
sol sol sol sol |
re re mib mib |
sib, sib, sib, sib, |
sib,\p sib, sib, sib, |
sib, sib, sib, sib, |
sib,\f sib, sib, sib, |
mib fa sol lab |
sib sib sib, sib, |
mib\ff mib mib mib |
mib4 r |
mib8 mib mib mib |
mib4 r |
mib8 mib mib mib |
mib4 r |
mib8 mib re re |
mib mib fa fa |
sol sol re re |
mib mib fa fa |
sol sol sol sol |
sol sol sol sol |
lab lab lab lab |
lab lab lab lab |
la la la la |
la la la la |
sib sib sib sib |
sib sib sib sib |
sib, sib, sib, sib, |
sib, sib, sib, sib, |
sib, sib, sib, sib, |
sib, sib, sib, sib, |
sib, sib, sib, sib, |
re re re re |
mib8. mib16 sol8. sol16 |
sib8. sib16 lab sol fa mib |
lab8 lab16 sib do'8 re' |
mib'4 r8 mib |
lab4 r8 lab |
sol4 r8 sol |
lab4 sib |
mib r |
lab r |
sol r |
lab sib |
mib8 mib16 mib mib8 mib |
mib4 r |
