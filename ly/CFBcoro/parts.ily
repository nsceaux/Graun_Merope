\piecePartSpecs
#`((corni #:instrument "Corni [in E♭]")
   (violini)
   (violino1)
   (violino2)
   (viola)
   (bassi #:instrument "Cembalo")
   (silence #:on-the-fly-markup , #{ \markup\tacet#72 #}))
