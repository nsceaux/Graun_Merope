<<
  \tag #'canto1 {
    \clef "soprano/treble" R2*4 |
    mib'4 sol' |
    sib' r |
    lab' do''8 re'' |
    mib''8. mib'16 mib'4 |
    do''4. mib''16[ do''] |
    do''8([ sib']) sib'4 |
    do''4. mib''16[ do''] |
    do''8[ sib'] sib'4 |
    fa''4. lab'8 |
    sol'[ lab'] sib'[ do''] |
    re''4( mib''8) sol' |
    sol' fa' r4 |
    lab' lab' |
    sol' r |
    reb'' reb'' |
    do'' r8 do'' |
    sol''4 sib' |
    sib'8. lab'16 lab'8 lab' |
    fa''4 lab' |
    lab'8. sol'16 sol'8 r |
    do''4. re''8 |
    mib''[ sib'] sib'4 |
    do''4. re''8 |
    mib''[ sib'] sib' mib'' |
    lab'4 sol' |
    sol'8[ fa'] r4 |
    la'4.^\p mib''8 |
    mib''([ re'']) re''4 |
    re''4.^\f lab''8 |
    sol'' fa''16([ mib''] re''[ do'']) sib'[ lab'] |
    sol'4 fa' |
    mib' r |
    R2*26 |
  }
  \tag #'canto2 {
    \clef "soprano/treble" R2*4 |
    mib'4 sol' |
    sib' r |
    lab'4 do''8 re'' |
    mib''8. mib'16 mib'4 |
    lab'4. do''16[ lab'] |
    lab'8([ sol']) sol'4 |
    lab'4. do''16[ lab'] |
    lab'8[ sol'] sol'4 |
    lab'4. fa'8 |
    mib'4. lab'8 |
    fa'4 mib' |
    mib'8 re' r4 |
    fa'4 fa' |
    mib' r |
    sib' sib' |
    lab' r8 lab' |
    sol'4 sol' |
    sol'8. fa'16 fa'8 fa' |
    fa'4 fa' |
    fa'8. mib'16 mib'8 r |
    mib'4. fa'8 |
    sol'4 sol' |
    mib'4. fa'8 sol'4 sol'8 mib' |
    fa'4 mib' |
    mib'8[ re'] r4 |
    mib'4.^\p la'8 |
    la'[ sib'] sib'4 |
    lab'!4.^\f re''8 |
    mib'' re''16([ do''] sib'[ lab']) sol'[ fa'] |
    mib'4 re' |
    mib' r |
    R2*26 |
  }
  \tag #'tenore {
    \clef "tenor/G_8" R2*4 |
    mib4 sol |
    sib r |
    lab do'8 re' |
    mib'8. mib16 mib4 |
    mib'4. mib'8 |
    mib'4 mib' |
    mib'4. mib'8 |
    mib'4 mib' |
    sib4. sib8 |
    mib'4. mib'8 |
    sib4. sib8 |
    sib8 sib r4 |
    sib4 sib |
    sib r |
    mib' mib' |
    mib' r8 do' |
    do'4 do' |
    do'8. do'16 do'8 do' |
    sib4 sib |
    sib8. sib16 sib8 r |
    lab4. lab8 |
    sib4 mib' |
    lab4. lab8 |
    sib4 mib'8 sib |
    sib4 sib |
    sib r |
    do'4.^\p do'8 |
    do'([ sib]) sib4 |
    fa'4.^\f fa'8 |
    mib'[ lab] sib do' |
    sib4 sib |
    sol r |
    R2*26 |
  }
  \tag #'basso {
    \clef "bass/bass" R2*4 |
    mib4 sol |
    sib r |
    lab do'8 re' |
    mib'8. mib16 mib4 |
    mib4. mib8 |
    mib4 mib |
    mib'4. mib'8 |
    mib'4 mib |
    re4. re8 |
    mib[ fa] sol[ lab] |
    sib4( sol8) mib |
    sib sib, r4 |
    re re |
    mib r |
    sol sol |
    lab r8 lab |
    mi4 mi |
    fa8. fa16 fa8 fa |
    re4 re |
    mib8. mib16 mib8 r |
    lab4. lab8 |
    sol4 sol |
    lab4. lab8 |
    sol4 sol8 sol |
    re4 mib |
    sib, r |
    R2*2 |
    sib4. sib8 |
    mib[ fa] sol lab |
    sib4 sib, |
    mib r |
    R2*26 |
  }
>>
