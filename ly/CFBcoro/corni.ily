\clef "treble" \transposition mib <>
<<
  \tag #'(corno1 corni) {
    do''8. do''16 mi''8. mi''16 | sol''4
  }
  \tag #'(corno2 corni) {
    do'8. do'16 mi'8. mi'16 | sol'4
  }
>> r8 do'' |
fa'' fa''16 sol'' la''8 si'' |
do'''8. do''16 do''4 |
<<
  \tag #'(corno1 corni) {
    do''8. do''16 mi''8. mi''16 | sol''4
  }
  \tag #'(corno2 corni) {
    do'8. do'16 mi'8. mi'16 | sol'4
  }
>> r8 do'' |
fa'' fa''16 sol'' la''8 si'' |
do'''8. do''16 do''4 |
do''2~ |
do''8 do''16 do'' do''8 do'' |
do''2~ |
do''8 do''16 do'' do''8 do'' |
<<
  \tag #'(corno1 corni) { re''4. re''8 | mi''4 }
  \tag #'(corno2 corni) { sol'4. sol'8 | do''4 }
>> r4 |
<<
  \tag #'(corno1 corni) { re''4 mi'' | mi''8 re'' }
  \tag #'(corno2 corni) { sol'4 do'' | do''8 sol' }
>> r4 |
<<
  \tag #'(corno1 corni) { fa''4 fa'' | mi'' }
  \tag #'(corno2 corni) { re''4 re'' | do'' }
>> r4 |
<<
  \tag #'(corno1 corni) {
    sol''4 sol'' |
    la'' s8 la'' |
    sol''4 sol'' |
    sol''8. fa''16 fa''8 fa'' |
    fa''4 fa'' |
    fa''8. mi''16 mi''8 sol'' |
    la'' la''4 si''8 |
    do'''
  }
  \tag #'(corno2 corni) {
    do''4 do'' |
    do'' s8 do'' |
    mi''4 mi'' |
    mi''8. re''16 re''8 re'' |
    re''4 re'' |
    re''8. do''16 do''8 mi'' |
    do''8 do''4 re''8 |
    mi''
  }
  { s2 | s4 r8 }
>> sol''8 sol''4 |
<<
  \tag #'(corno1 corni) {
    la''8 la''4 si''8 |
    do'''
  }
  \tag #'(corno2 corni) {
    do''8 do''4 re''8 |
    mi''
  }
>> sol''8 sol''
<<
  \tag #'(corno1 corni) {
    sol''8 |
    fa''4 mi'' |
    mi''8 re''
  }
  \tag #'(corno2 corni) {
    do''8 |
    re''4 do'' |
    do''8 sol'
  }
>> r4 |
R2*2 |
<<
  \tag #'(corno1 corni) {
    fa''4. fa''8 |
    mi'' fa'' sol'' fa'' |
    mi''4 re'' |
    do''8 do''16 do'' do''8 do'' |
    do''4
  }
  \tag #'(corno2 corni) {
    re''4. re''8 |
    do'' re'' mi'' re'' |
    do''4 sol' |
    sol'8 sol'16 sol' sol'8 sol' |
    mi'4
  }
>> r4 |
<<
  \tag #'(corno1 corni) {
    mi''8 mi''16 mi'' mi''8 mi'' |
    mi''4 s |
    sol''8 sol''16 sol'' sol''8 sol'' |
    sol''4 s |
  }
  \tag #'(corno2 corni) {
    do''8 do''16 do'' do''8 do'' |
    do''4 s |
    mi''8 mi''16 mi'' mi''8 mi'' |
    mi''4 s |
  }
  { s2 | s4 r | s2 | s4 r | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''8 sol''4 sol''8~ |
    sol'' sol''4 sol''8~ |
    sol'' sol''4 sol''8~ |
    sol'' sol''4 sol''8 | }
  { mi''4 re'' |
    mi'' re'' |
    sol'' re'' |
    mi'' fa'' | }
>>
sol''8 <<
  \tag #'(corno1 corni) {
    sol''8 sol'' sol'' |
    sol'' sol'' sol'' sol'' |
    la''4 s |
    la'' s |
    la''8 la'' la'' la'' |
    la'' la'' la'' la'' |
    sol''4 s |
    sol'' s |
    re'' s |
    mi'' s |
    re''8 fa'' mi'' re'' |
    mi'' sol'' fa'' mi'' |
    re''4 s |
    re'' s |
    do''8. do''16 mi''8. mi''16 |
    sol''4
  }
  \tag #'(corno2 corni) {
    do''8 do'' do'' |
    do'' do'' do'' do'' |
    do''4 s |
    do'' s |
    re''8 re'' re'' re'' |
    re''8 re'' re'' re'' |
    re''4 s |
    re'' s |
    sol' s |
    do'' s |
    sol'8 re'' do'' sol' |
    do'' mi'' re'' do'' |
    sol'4 s |
    sol' s |
    do'8. do'16 mi'8. mi'16 |
    sol'4
  }
  { s4. | s2 | s4 r | s r | s2*2 |
    s4 r | s r | s r | s r | s2*2 | s4 r | s r | }
>> r8 do'' |
fa'' fa''16 sol'' la''8 si'' |
do'''4 r8 <<
  \tag #'(corno1 corni) {
    sol''8 |
    la''4 s8 la'' |
    sol''4 s8 mi'' |
    s4 re'' |
  }
  \tag #'(corno2 corni) {
    mi''8 |
    fa''4 s8 fa'' |
    do''4 s8 do'' |
    s4 sol' |
  }
  { s8 | s4 r8 s | s4 r8 s | re''4 s | }
>>
do''4 r |
do'' r |
do'' r |
do'' sol' |
<<
  \tag #'(corno1 corni) {
    sol'8 sol'16 sol' sol'8 sol' | sol'4
  }
  \tag #'(corno2 corni) {
    mi'8 mi'16 mi' mi'8 mi' | mi'4
  }
>> r4 |
