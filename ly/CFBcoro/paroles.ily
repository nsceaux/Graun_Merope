Del mor -- tal, tu, che sei l’ar -- bi -- tra,
O di -- vi -- na Pro -- vi -- den -- za,
O di -- vi -- na Pro -- vi -- den -- za,
Com -- pi o -- mai
com -- pi o -- mai
la tua grand o -- pe -- ra
la tua grand o -- pe -- ra
L’in -- no -- cen -- za
l’in -- no -- cen -- za in sol -- le -- var
\tag #'(canto1 canto2 tenore) { L’in -- no -- cen -- za }
\tag #'(canto1 canto2) { l’in -- no -- cen -- za __ }
\tag #'(tenore basso) { l’in -- no -- cen -- za }
in sol -- le -- var.
