\piecePartSpecs
#`((violini #:score-template "score-violini2")
   (violino1 #:notes "violino1")
   (violino2 #:notes "violino2")
   (viola #:score "score-viola")
   (fagotti #:score "score-fagotti")
   (bassi)
   (silence #:on-the-fly-markup , #{ \markup\tacet#233 #}))
