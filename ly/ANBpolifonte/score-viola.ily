\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'viola1 \includeNotes "viola" >>
    \new Staff << \global \keepWithTag #'viola2 \includeNotes "viola" >>
  >>
 \layout { }
}