\clef "bass" fa8 fa fa fa fa fa |
sib, sib, do do do, do, |
fa,4 fa, fa, |
do, do, do, |
fa,8 fa fa fa fa fa |
sib, sib, do do do, do, |
fa, fa fa fa fa fa |
mi mi mi mi mi mi |
fa fa sib, sib, si, si, |
do4\p do do |
do do do |
do do do |
do\f do do |
fa re re |
mi mi mi |
fa re\p re |
mi mi mi |
fa8 sib,\f do4 do, |
fa,2 r4 |
fa8\p fa fa fa fa fa |
sib, sib, do do do do |
fa,4 fa, fa, |
do, do, do, |
fa,8 fa fa fa fa fa |
sib, sib, do do do do |
fa,4 fa, fa, |
do, do, do, |
fa,8 fa, fa, fa, fa, fa, |
fa, fa, fa, fa, fa, fa, |
do do do do do do |
do do do do do do |
sol, sol, sol, sol, sol, sol, |
sol, sol, sol, sol, sol, sol, |
do do re re mi mi |
fa fa fa fa fad fad |
sol4\f sol sol |
sol8\p sol sol sol sol sol |
sol4\f sol sol |
sol8\p sol sol sol sol sol |
sol sol sol sol sol sol |
do do re re mi mi |
fa fa sol sol sol, sol, |
la,4 la, la, |
si, si, si, |
do la, la, |
si, si, si, |
do r8 do re mi |
fa4 r8 re mi fad |
sol4 r8 sol, la, si, |
do4 la la |
sol sol sol |
fa\pocof sol sol, |
do8\f do do do do do |
fa, fa, sol, sol, sol, sol, |
do do do do do do |
si, si, si, si, si, si, |
do8 fa sol4 sol, |
do8 do'16\p re' do'8 sib la sol |
fa fa fa fa fa fa |
sib, sib, do do do, do, |
fa,4 fa, fa, |
do, do, do, |
fa,8 fa, fa, fa, fa, fa, |
la, la, la, la, la, la, |
sib,4 sib, sib, |
fad, fad, fad, |
sol,8 sol, sol, sol, sol, sol, |
sol, sol, sol, sol, sol, sol, |
re4 re re |
re8 re re re re re |
sol,4 sol, sol, |
sol,8 sol, sol, sol, sol, sol, |
do do do do do do |
do do do do do do |
do do do do do do |
fa,4 r8 fa, sol, la, |
sib,4 r8 sol, la, si, |
do4 r8 do re mi |
fa4 sib, si, |
do\f do do |
do8\p do do do do do |
do4\f do do |
do8\p do do do do do |
do do do do do do |
fa, fa, sol, sol, la, la, |
sib, sib, do do do, do, |
fa,4 la, la, |
sib, sib, sib, |
r8 do\pocof do do do do |
do4 la,\p la, |
sib, sib, sib, |
r8 do\pocof do do do do |
re sib, do4 do |
re8 sib,\f do4 do, |
fa,8 fa\ff fa fa fa fa |
sib, sib, do do do do |
fa fa fa fa fa fa |
mi mi mi mi mi mi |
fa fa sib, sib, si, si, |
do4 do do |
do do do |
fa re re |
mi mi mi |
fa re\p re |
mi mi mi |
fa8 sib,\f do4 do |
re8 sib, do4 do, |
fa,8\fermata fa16 sol la8 sol fa mi |
re\p re re re re re |
sol, sol, la, la, la, la, |
re4 re re |
la, la, la, |
re,8 re re re re re |
sol sol do do do do |
fa,4 fa, fa, |
do, do, do, |
fa,8 fa, fa, fa, fa, fa, |
fa, fa, fa, fa, fa, fa, |
sib, sib, sib, sib, sib, sib, |
sol, sol, sol, sol, sol, sol, |
la,4\f la, la, |
la,8\p la, la, la, la, la, |
la,4\f la, la, |
la,8 la, la, la, la, la, |
la, la, la, la, la, la, |
re re mi mi fa fa |
sol sol la la la, la, |
sib,4 sib, sib, |
la, la, la, |
sib, sib, sib, |
la, la, la, |
sib,8 sol16\f la sib8 la sol fa |
mib8 mib16 fa sol8 fa mib re |
dod4 re sol |
la2 la,4 |
re,8 re do sib, la, sol, |
fa,\ff fa fa fa fa fa |
sib, sib, do do do do |
fa, fa fa fa fa fa |
mi mi mi mi mi mi |
fa sib do'4 do |
re8 sib, do4 do, |
fa,2 r4 |
