\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'fagotto1 \includeNotes "viola" >>
    \new Staff << \global \keepWithTag #'fagotto2 \includeNotes "viola" >>
  >>
 \layout { }
}