\clef "treble" la'4-! la'-! do''-! |
re''8( do''16 sib') \appoggiatura la'4 sol'2 |
la' r4 |
R2. |
la'4-! la'-! do''-! |
re''8( do''16 sib') \appoggiatura la'4 sol'2 |
la'4 r8 la' la' fa' |
do''4 r8 do'' do'' sol'' |
fa'' fa''16*2/3( sol'' la'') sol''4( fa'')\trill |
mi'' r r |
r8 mi'16 fa' mi'8 mi'16 fa' sol'8 sol'16 fa' |
mi'4-! r r |
r8 mi''16 fa'' mi''8 mi''16 fa'' sol''8 fa''16 mi'' |
fa''4 fa' fa' |
sol' sol' do' |
fa' fa'\p fa' |
sol' sol' do' |
fa'8 sib'\f la'8.(\trill sol'32 fa') sol'8.(\trill fa'32 mi') |
fa'2 r4 |
la'4-!\p la'-! do''-! |
re''8( do''16 sib') \appoggiatura la'4 sol'2 |
la' r4 |
R2. |
la'4-! la'-! do''-! |
re''8( do''16 sib') \appoggiatura la'4 sol'2 |
la'2 r4 |
R2. |
fa'4-! la'-! do''-! |
fa''-! la''-! fa''-! |
fa''-! mi''-! mi''\f~ |
mi'' mi''-!\p do''-! |
re'' re''2\f~ |
re''4 re''-!\p si'-! |
do''2. |
do'' |
si'4 r r |
r8 si re' sol' si' re'' |
si'2 r4 |
r8 si re' sol' si' re'' |
si'2. |
do''~ |
do''4 \appoggiatura do''4 si'2\trill |
do''4 do'' do'' |
re'' re'' re'' |
do''8 do''4 si'16( la') sol'8 fad' |
sol' la'4 sol'16( fa'!) mi'8 re' |
mi'16 fa' sol' la' sib'!2~ |
sib'8 la'16 si' do''2~ |
do''8 si'16 do'' re''8 sol'4 fa'8 |
mi'4 do''8.(\trill si'32 do'') fa'4 |
r do''8.(\trill si'32 do'') mi'4 |
r8 fa''\pocof mi''8.( re''32 do'') re''8.(\trill do''32 si') |
do''8 mi'\f mi'4 sol' |
la'8( sol'16 fa') \appoggiatura mi'4 re'2 |
mi'4 r8 mi' mi' do' |
sol'4 r8 sol' sol' re'' |
do''8 do'' mi''8.(\trill re''32 do'') re''8.(\trill do''32 si') |
do''2 r4 |
la'4-!\p la'-! do''-! |
re''8( do''16 sib') \appoggiatura la'4 sol'2 |
la' r4 |
R2. |
la'4-! la'-! do''-! |
fa'2 do''4 |
do'' sib' r |
R2. |
sol'4-! sib'-! re''-! sol''-! sib''-! sib'-! |
la' r r |
fad''4 re''8 la'' sol'' fad'' |
sol''4 r r |
sol'' sol'' sol'' |
sol'' sol'' sol'' |
fa'' fa'' fa'' |
mi'' sol' sol' |
fa' mib'2 |
re'8 mi'! fa'2 |
mi'16( fa' sol' la') sib'2 |
la'4 sol' fa' |
mi'4 r r |
r8 mi' sol' do'' mi'' mi'' |
do''2 r4 |
r8 mi' sol' do'' mi'' mi'' |
do''4 sol''2 |
fa''2.~ |
fa''8. sib'16 \appoggiatura la'4 sol'2 |
fa'4 do''8.(\trill sib'32 do'') fa'4 |
r re''8.(\trill do''32 re'') sol'8 sol''\pocof |
sol''4~ sol''8 do''16 sib' la'8 sol' |
fa'4 do''8.(\trill\p sib'32 do'') fa'4 |
r re''8.(\trill do''32 re'') sol'8 sol''\pocof |
sol''4~ sol''8 do''16 sib' la'8 sol' |
fa' sib'' la''8.(\trill sol''32 fa'') sol''8.(\trill fa''32 mi'') |
fa''8 sib'\f la'8.(\trill sol'32 fa') sol'8.(\trill fa'32 mi') |
fa'8 la'\ff la'4 do'' |
re''8.( do''32 sib') \appoggiatura la'4 sol'2 |
la'4 r8 la' la' fa' |
do''4 r8 do'' do'' sol'' |
fa'' fa''16*2/3( sol'' la'') sol''4( fa''\trill) |
mi'' r r |
r8 mi''16 fa'' mi''8 mi''16 fa'' sol''8 fa''16 mi'' |
fa''4 fa' fa' |
sol' sol' do' |
fa' fa'\p fa' |
sol' sol' do' |
fa'8 sib'\f la'8.(\trill sol'32 fa') sol'8.(\trill fa'32 mi') |
fa'8 sib' la'8.(\trill sol'32 fa') sol'8.(\trill fa'32 mi') |
fa'2\fermata r4 |
fa'4-!\p fa'-! la'-! |
\appoggiatura do''16 sib'8( la'16 sol') \appoggiatura fa'4 mi'2 |
fa' r4 |
R2. |
fa'4-! fa'-! la'-! |
sib'8.(\trill la'32 sib') do''4 sib' |
la'4 r r |
R2. |
la'4-! do''-! fa''-! |
la''-! do'''-! do''-! |
do'' sib' r |
re''2. |
dod''4 r r |
r8 dod' mi' la' dod'' mi'' |
dod''2 r4 |
r8 dod' mi' la' dod'' mi'' |
dod''2. |
re''~ |
re''4 \appoggiatura re''4 dod''2 |
re''4 re'' re'' |
dod''! dod'' dod'' |
re''8 re''4 do''16( sib') la'8 sold' |
la' sib'4 la'16( sol') fa'8 mi' |
re' sol'16\f la' sib'8 la' sol' fa' |
mib' mib'16 fa' sol'8 fa' mib' re' |
dod'8 sol'' r fa'' r mi'' |
re''4 dod''2\trill |
re''8 re' do' sib la sol |
la'4-!\ff do''-! fa''-! |
re''8( do''16 sib') \appoggiatura la'4 sol'2 |
la'4 r8 la' la' fa' |
do''4 r8 do'' do'' sol'' |
fa'' fa'' la''8.(\trill sol''32 fa'') sol''8.(\trill fa''32 mi'') |
fa''8 sib' la'8.(\trill sol'32 fa') sol'8.(\trill fa'32 mi') |
fa'2 r4 |
