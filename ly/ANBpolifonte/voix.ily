\clef "soprano/treble" R2.*19 |
do'4 fa' la' |
\appoggiatura do''16 sib'8([\melisma la'16 sol']) fa'4\melismaEnd mi' |
fa'8.([\trill mi'32 fa']) do'4 r |
R2. |
do'4 fa' la' |
\appoggiatura do''16 sib'8([\melisma la'16 sol']) fa'4\melismaEnd mi' |
fa'8.[\trill mi'32 fa'] do'4 r |
sib2. |
la4( do') fa' |
la'( do''4.) la'8 |
la'4 sol' do''4~ |
do'' \tuplet 3/2 { sol'8[ fa'] mi' } \tuplet 3/2 { sol'8[ fa'] mi' } |
fa'4 si'2~ |
si'4 fa'8*2/3[ mi'] re' fa'[ mi'] re' |
mi'4\trill\melisma fa'\trill\melismaEnd sol'\trill |
la'16[\melisma sol' fa' mi'] re'4\melismaEnd do' |
si r r |
sol4 si8 re' sol' si' |
sol'8.[\trill fad'16] sol'4 r |
sol4 si8 re' sol' si' |
re''[ fa'] fa'2 |
mi'4\trill fa'\trill sol'\trill |
\appoggiatura sib'16 la'8[ sol'16 fa'] \appoggiatura mi'4 re'2 |
do'8\melisma do''4 si'16[ la'] sol'8[ fad'] |
sol'8 la'4 sol'16[ fa'!] mi'8[ re'] |
mi'8 do''4 si'16[ la'] sol'8[ fad'] |
sol'8 la'4 sol'16[ fa'!] mi'8[ re'] |
mi'16[ fa' sol' la'] sib'!2~ |
sib'8[ la'16 si'] do''2~ |
do''8[ si'16 do''] re''16[ do'' si' la'] sol'8[ fa'] |
mi'4\melismaEnd do''8.[\trill si'32 do''] fa'4 |
r do'' do' |
r8 la' sol'8.[\trill fa'32 mi'] fa'8.[\trill mi'32 re'] |
do'2 r4 |
R2.*5 |
do'4 fa' la' |
\appoggiatura do''16 sib'8([\melisma la'16 sol']) fa'4\melismaEnd mi' |
fa'8.[\trill mi'32 fa'] do'4 r |
R2. |
do'4 fa' la' |
do''16[\melisma sib' la' sol'] fa'4\melismaEnd mib' |
mib' re' r |
do'2. |
sib4( re') sol' |
sib'( re''4.) sol'8 |
fad'8.[ mi'16] re'4 r |
la'4 re'8 do'' sib' la' |
sib'4 r r |
sib' re''8*2/3[ do''] sib' la'[ sol'] fa' |
mi'8\melisma sol'4 sib' la'16[ sol'] |
fa'8 la'4 do'' sib'16[ la'] |
sol'8 sib'4 re'' do''16[ sib'] |
\appoggiatura sib' la'8[ sol'16 fa'] mib'2 |
re'16[ fa' mi'! sol'] fa'2 |
mi'16[ fa' sol' la'] sib'2 |
la'8[ sib'16 do''] re''[ do'' sib' la'] sol'[ fa' mi' re'] |
do'2\melismaEnd r4 |
do'4 mi'8 sol' do'' sol' |
mi'8.[\trill re'16] do'4 r |
do'4 mi'8 sol' do'' sol' |
mi'[ do'] sib'2 |
la'4\trill sib'\trill do''\trill |
re'8.[ sol'16] \appoggiatura fa'4 mi'2 |
fa'4 do''\trill fa' |
r re''\trill sol'8 sib' |
sib'4~ sib'8[ la'16 sol' fa'8] mi' |
fa'4 do''\trill fa' |
r re''\trill sol'8 sib' |
sib'4.\melisma la'16[ sol' fa'8]\melismaEnd mi' |
fa' re'' do''8.([\trill sib'32 la']) sib'8.([\trill la'32 sol']) |
fa'8 re'' do''8.([\trill sib'32 la']) sib'8.([\trill la'32 sol']) |
fa'4 r r |
R2.*13 |
la'4 re' la' |
\appoggiatura do''16 sib'8[\melisma la'16 sol'] fa'4\melismaEnd mi' |
re' la r |
R2. |
la'4 re' la' |
sib'8.([\trill la'32 sib'] do''4) sib' |
la'8.[\trill sol'16] fa'4 r |
R2. |
do'4 fa' la' |
do''8.[\trill\melisma re''16] mib''4.\melismaEnd mib'8 |
mib'4 re' sib'~ |
sib' \tuplet 3/2 { sib'8[ la'] sol' } fa'8*2/3[ mi'] re' |
\appoggiatura re'4 dod'2 r4 |
la4 dod'8 mi' la' dod'' |
la'8.[\trill sold'16] la'4 r |
la4 dod'8 mi' la' dod'' |
mi''[ sol'] sol'2 |
fa'4 sol' la' |
\appoggiatura do''16 sib'8[ la'16 sol'] \appoggiatura fa'4 mi'2 |
re'8\melisma re''4 do''16[ sib'] la'8[ sold'] |
la' sib'4 la'16[ sol'] fa'8[ mi'] |
re'8 re''4 do''16[ sib'] la'8[ sold'] |
la'8 sib'4 la'16[ sol'] fa'8[ mi'] |
re'2\melismaEnd r4 |
R2. |
sib'4 la' sol' |
fa'8*2/3[ mi' re'] mi'2\trill |
re' r4 |
R2.*7 |
