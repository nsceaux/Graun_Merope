\clef "treble" do''4-! fa''-! la''-! |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.\trill( mi''32 fa'') do''4 r |
R2. |
do''4-! fa''-! la''-! |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.(\trill mi''32 fa'') do''8 do'' fa'' la'' |
sol''8.(\trill fa''32 sol'') do''8 do'' sol'' sib'' |
la''8 la''16*2/3( sib'' do''') sib''4( la'')\trill |
sol'' r r |
r8 sol'16 la' sol'8 sol'16 la' sib'8 sib'16 la' |
sol'4-! r r |
r8 sol''16 la'' sol''8 sol''16 la'' sib''8 la''16 sol'' |
la''8 fa''4 mi''16( re'') do''8 si' |
do''8 re''4 do''16( sib'!) la'8 sol' |
la' fa''4\p mi''16( re'') do''8 si' |
do'' re''4 do''16( sib'!) la'8 sol' |
la'8 re''\f do''8.(\trill sib'32 la') sib'8.(\trill la'32 sol') |
fa'2 r4 |
do''4-!\p fa''-! la''-! |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.(\trill mi''32 fa'') do''4 r |
R2. |
do''4-! fa''-! la''-! |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.(\trill mi''32 fa'') do''4 r |
R2. |
la'4-! do''-! fa''-! |
la''-! do'''-! la''-! |
la''-! sol''-! do'''\f~ |
do''' sol''-!\p mi''-! |
fa'' si''2\f~ |
si''4 fa''-!\p re''-! |
mi''\trill fa''\trill sol''\trill |
la''2. |
re''4 r r |
r8 sol' si' re'' sol'' si'' |
sol''8.(\trill fad''16) sol''4 r |
r8 sol' si' re'' sol'' si'' |
re'''( fa'') fa''2 |
mi''4\trill fa''\trill sol''\trill |
\appoggiatura sib''16 la''8( sol''16 fa'') \appoggiatura mi''4 re''2\trill |
do''8 do'''4 si''16( la'') sol''8 fad'' |
sol'' la''4 sol''16( fa''!) mi''8 re'' |
mi''4 do'' do'' |
re'' re'' re'' |
do'' r8 mi'' fa'' sol'' |
do''4 r8 fad'' sol'' la'' |
re''4 r8 si' do'' re'' |
sol'4 do''8.(\trill si'32 do'') fa'4 |
r do''8.(\trill si'32 do'') mi'4 |
r8 la''\pocof sol''8.(\trill fa''32 mi'') fa''8.(\trill mi''32 re'') |
do''8 sol'\f do''4 mi'' |
\appoggiatura sol''16 fa''8( mi''16 re'') \appoggiatura do''4 si'2 |
do''8.(\trill si'32 do'') sol'8 sol' do'' mi'' |
re''8.(\trill do''32 re'') sol'8 sol' re'' fa'' |
mi''8 la'' sol''8.(\trill fa''32 mi'') fa''8.(\trill mi''32 re'') |
do''2 r4 |
do''4-!\p fa''-! la''-! |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.(\trill mi''32 fa'') do''4 r |
R2. |
do''4-! fa''-! la''-! |
do'''16( sib'' la'' sol'') fa''4 mib'' |
mib'' re'' r |
R2. |
sib'4-! re''-! sol''-! |
sib''-! re'''-! sol''-! |
fad''4 r r |
la'' re''8 do''' sib'' la'' |
sib''4 r r |
sib''4 sib'' sib'' |
sib'' sib'' sib'' |
la'' la'' la'' |
sol'' sib' sib' |
la' r8 la' sib' do'' |
fa'4 r8 si' do'' re'' |
sol'4 r8 mi'' fa'' sol'' |
do''4 re''2 |
sol'4 r r |
r8 do'' mi'' sol'' do''' sol'' |
mi''8.\trill re''16 do''4 r |
r8 do'' mi'' sol'' do''' sol'' |
mi'' do'' sib''2 |
la''4\trill sib''\trill do'''\trill |
re''8.( sol''16) \appoggiatura fa''4 mi''2 |
fa''4 do''8.(\trill sib'32 do'') fa'4 |
r re''8.(\trill do''32 re'') sol'8 sib''\pocof |
sib''4~ sib''8 la''16 sol'' fa''8 mi'' |
fa''4 do''8.(\trill\p sib'32 do'') fa'4 |
r re''8.(\trill do''32 re'') sol'8 sib''\pocof |
sib''4~ sib''8 la''16 sol'' fa''8 mi'' |
fa''8 re''' do'''8.(\trill sib''32 la'') sib''8.(\trill la''32 sol'') |
fa''8 re''\f do''8.(\trill sib'32 la') sib'8.(\trill la'32 sol') |
fa'8 do''\ff fa''4 la'' |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.(\trill mi''32 fa'') do''8 do'' fa'' la'' |
sol''8.(\trill fa''32 sol'') do''8 do'' sol'' sib'' |
la'' la''16*2/3( sib'' do''') sib''4( la''\trill) |
sol'' r r |
r8 sol''16 la'' sol''8 sol''16 la'' sib''8 la''16 sol'' |
la''8 fa''4 mi''16( re'') do''8 si' |
do''8 re''4 do''16( sib') la'8 sol' |
la'8 fa''4\p mi''16( re'') do''8 si' |
do'' re''4 do''16( sib'!) la'8 sol' |
la' re''\f do''8.(\trill sib'32 la') sib'8.(\trill la'32 sol') |
fa'8 re'' do''8.(\trill sib'32 la') sib'8.(\trill la'32 sol') |
fa'2\fermata r4 |
la'4-!\p re''-! fa''-! |
\appoggiatura la''16 sol''8( fa''16 mi'') re''4( dod'') |
re''8.(\trill dod''32 re'') la'4 r |
R2. |
la'4-! re''-! fa''-! |
fa'' mi''2\trill |
fa''4 r r |
R2. |
do''4-! fa''-! la''-! |
do'''-! mib'''-! mib''-! |
mib'' re'' r |
sib''2. |
mi''4 r r |
r8 la' dod'' mi'' la'' dod''' |
la''8.\trill sold''16 la''4 r |
r8 la' dod'' mi'' la'' dod''' |
mi'''( sol'') sol''2 |
fa''4\trill sol''\trill la''\trill |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2\trill |
re''8 re'''4 do'''16( sib'') la''8 sold'' |
la'' sib''4 la''16 sol'' fa''8 mi'' |
re''4 re'' re'' |
dod'' dod'' dod'' |
re''8 sol'16\f la' sib'8 la' sol' fa' |
mib' mib'16 fa' sol'8 fa' mib' re' |
dod'8 sib'' r la'' r sol'' |
fa''4 mi''2\trill |
re''8 re' do' sib la sol |
do''4-!\ff fa''-! la''-! |
\appoggiatura do'''16 sib''8( la''16 sol'') \appoggiatura fa''4 mi''2 |
fa''8.(\trill mi''32 fa'') do''8 do'' fa'' la'' |
sol''8.(\trill fa''32 sol'') do''8 do'' sol'' sib'' |
la'' re''' do'''8.(\trill sib''32 la'') sib''8.(\trill la''32 sol'') |
fa''8 re'' do''8.(\trill sib'32 la') sib'8.(\trill la'32 sol') |
fa'2 r4 |
