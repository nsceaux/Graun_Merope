\tag #'(viola1 viola2 viole) \clef "alto"
\tag #'(fagotto1 fagotto2 fagotti) \clef "bass"
R2.*2 |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    do'16 re' do'8 do' do' do' |
    sib sib16 do' sib8 sib sib sib |
    la4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    la16 sib la8 la la la |
    sol sol16 la sol8 sol sol sol |
    fa4
  }
>> r4 r |
R2.*4 |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { sol16 la sol8 sol16 la sib8 sib16 la | sol4 }
  \tag #'(viola2 fagotto2 viole fagotti) { mi16 fa mi8 mi16 fa sol8 sol16 fa | mi4 }
>> r4 r |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { sol16 la sol8 sol16 la sib8 sib16 la | sol4 }
  \tag #'(viola2 fagotto2 viole fagotti) { mi16 fa mi8 mi16 fa sol8 sol16 fa | mi4 }
>> r4 r |
r8 fa'4 mi'16( re') do'8 si |
do' re'4 do'16( sib!) la8 sol |
la fa'4\p mi'16( re') do'8 si |
do' re'4 do'16( sib!) la8 sol |
la <>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    re'8 do'8.(\trill sib32 la) sib8.(\trill la32 sol) |
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    sib8 la8.(\trill sol32 fa) sol8.(\trill fa32 mi) |
  }
>>
fa8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { la16 sib la8 la la la | la4 }
  \tag #'(viola2 fagotto2 viole fagotti) { fa16 sol fa8 fa fa fa | fa4 }
>> r4 r |
R2. |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    do'16 re' do'8 do' do' do' |
    sib sib16 do' sib8 sib sib sib |
    la4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    la16 sib la8 la la la |
    sol sol16 la sol8 sol sol sol |
    fa4
  }
>> r4 r |
R2. |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    do'16 re' do'8 do' do' do' |
    sib sib16 do' sib8 sib sib sib |
    la4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    la16 sib la8 la la la |
    sol sol16 la sol8 sol sol sol |
    fa4
  }
>> r4 r |
R2.*7 |
r8 <>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) { re'16 mi' re'8 re'16 mi' fa'8 fa'16 mi' | re'4 }
  \tag #'(viola2 fagotto2 viole fagotti) { si16 do' si8 si16 do' re'8 re'16 do' | si4 }
>> r4 r |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { re'16 mi' re'8 re'16 mi' fa'8 fa'16 mi' | re'4 }
  \tag #'(viola2 fagotto2 viole fagotti) { si16 do' si8 si16 do' re'8 re'16 do' | si4 }
>> r4 r |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) { re'16 mi' re'8 re'16 mi' fa'8 mi'16 re' | mi'4 }
  \tag #'(viola2 fagotto2 viole fagotti) { si16 do' si8 si16 do' re'8 do'16 si | do'4 }
>> r4 r |
R2.*11 |
<>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) { sol4 do' mi' | }
  \tag #'(viola2 fagotto2 viole fagotti) { mi4 mi sol | }
>>
\twoVoices #'(viola1 viola2 viole) <<
  { \appoggiatura sol'16 fa'8( mi'16 re') \appoggiatura do'4 si2 |
    do'8.(\trill si32 do') sol8 sol do' mi' |
    re'8.(\trill do'32 re') sol8 sol re' fa' | }
  { la8( sol16 fa) \appoggiatura mi4 re2 |
    mi4 r8 mi sol do' |
    sol4 r8 sol si re' | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { \appoggiatura sol'16 fa'8( mi'16 re') \appoggiatura do'4 si2 |
    do'8.(\trill si32 do') sol8 sol do' mi' |
    re'8.(\trill do'32 re') sol8 sol re' fa' | }
  { la8( sol16 fa) \appoggiatura mi4 re2 |
    mi4 r8 mi sol do' |
    sol4 r8 sol si re' | }
>>
<<
  \tag #'(viola1 fagotto1 viole fagotti) {
    mi'8 fa' mi'8.(\trill re'32 do') re'8.(\trill do'32 si) |
    do'2
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    do'8 la sol8.(\trill fa32 mi) fa8.(\trill mi32 re) |
    do2
  }
>> r4 |
R2.*2 |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    do'16 re' do'8 do' do' do' |
    sib sib16 do' sib8 sib sib sib |
    la4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    la16 sib la8 la la la |
    sol sol16 la sol8 sol sol sol |
    fa4
  }
>> r4 r |
R2. |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    re'16 mib' re'8 re' re' re' |
    do' do'16 re' do'8 do' do' do' |
    sib4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    sib16 do' sib8 sib sib sib |
    la la16 sib la8 la la la |
    sol4
  }
>> r4 r |
R2. |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { la16 sib la8 la16 sib do'8 do'16 sib | la4 }
  \tag #'(viola2 fagotto2 viole fagotti) { fad16 sol fad8 fad16 sol la8 la16 sol | fad4 }
>> r4 r |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { sib16 do' sib8 sib16 do' re'8 re'16 do' | sib4 }
  \tag #'(viola2 fagotto2 viole fagotti) { sol16 la sol8 sol16 la sib8 sib16 la | sol4 }
>> r4 r |
R2.*7 |
r8 <>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) { sol16 la sol8 sol16 la sib8 sib16 la | sol4 }
  \tag #'(viola2 fagotto2 viole fagotti) { mi16 fa mi8 mi16 fa sol8 sol16 fa | mi4 }
>> r4 r |
r8 <>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) { sol16 la sol8 sol16 la sib8 sib16 la | sol4 }
  \tag #'(viola2 fagotto2 viole fagotti) { mi16 fa mi8 mi16 fa sol8 sol16 fa | mi4 }
>> r4 r |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) { sol16 la sol8 sol16 la sib8 la16 sol | la4 }
  \tag #'(viola2 fagotto2 viole fagotti) { mi16 fa mi8 mi16 fa sol8 fa16 mi | fa4 }
>> r4 r |
R2.*9 |
<>\f <<
  \tag #'(viola1 viole) { do'4 fa' la' | }
  \tag #'(viola2 viole) { la4 do' fa' | }
  \tag #'(fagotto1 fagotti) { la4 do' fa' | }
  \tag #'(fagotto2 fagotti) { fa4 la la | }
>>
<<
  \twoVoices #'(viola1 viola2 viole) <<
    { \appoggiatura do''16 sib'8( la'16 sol') \appoggiatura fa'4 mi'2 | }
    { re'8( do'16 sib) \appoggiatura la4 sol2 | }
  >>
  \tag #'(fagotto1 fagotti) { re'8( do'16 sib) \appoggiatura la4 sol2 | }
  \tag #'(fagotto2 fagotti) { sib8( la16 sol) \appoggiatura fa4 mi2 | }
>>
<<
  \tag #'(viola1 viole) {
    fa'4 la'2 |
    sol'4 sib'2 |
    la'8 la'16*2/3( sib' do'') sib'4( la')\trill |
    sol'8
  }
  \tag #'(viola2 viole) {
    la4 fa'2 |
    do'4 sol'2 |
    fa'8 fa'16*2/3( sol' la') sol'4( fa')\trill |
    mi'8
  }
  \tag #'(fagotto1 fagotti) {
    la4 do'2~ |
    do'4 sib2 |
    la8 la16*2/3( sib do') sib4( la)\trill |
    sol8
  }
  \tag #'(fagotto2 fagotti) {
    fa4 la2 |
    sol4 sol2 |
    fa8 fa16*2/3( sol la) sol4( fa)\trill |
    mi8
  }
>>
<<
  \tag #'(viola1 fagotto1 viole fagotti) { sol16 la sol8 sol16 la sib8 sib16 la | sol4 }
  \tag #'(viola2 fagotto2 viole fagotti) { mi16 fa mi8 mi16 fa sol8 sol16 fa | mi4 }
>> r4 r |
r8 fa'4 mi'16( re') do'8 si |
do' re'4 do'16( sib!) la8 sol |
la8 fa'4\p mi'16( re') do'8 si |
do' re'4 do'16( sib!) la8 sol |
la <>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    re'8 do'8.(\trill sib32 la) sib8.(\trill la32 sol) |
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    sib8 la8.(\trill sol32 fa) sol8.(\trill fa32 mi) |
  }
>>
fa8 <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    re'8 do'8.(\trill sib32 la) sib8.(\trill la32 sol) |
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    sib8 la8.(\trill sol32 fa) sol8.(\trill fa32 mi) |
  }
>>
fa2\fermata r4 |
R2.*2 |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    la16 sib la8 la la la |
    sol sol16 la sol8 sol sol sol |
    fa4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    fa16 sol fa8 fa fa fa |
    mi mi16 fa mi8 mi mi mi |
    re4
  }
>> r4 r |
R2. |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    do'16 re' do'8 do' do' do' |
    sib sib16 do' sib8 sib sib sib |
    la4
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    la16 sib la8 la la la |
    sol sol16 la sol8 sol sol sol |
    fa4
  }
>> r4 r |
R2.*3 |
r8 <>\f <<
  \tag #'(viola1 fagotto1 viole fagotti) { mi'16 fa' mi'8 mi'16 fa' sol'8 sol'16 fa' | mi'4 }
  \tag #'(viola2 fagotto2 viole fagotti) { dod'16 re' dod'8 dod'16 re' mi'8 mi'16 re' | dod'4 }
>> r4 r |
r8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { mi'16 fa' mi'8 mi'16 fa' sol'8 sol'16 fa' | mi'4 }
  \tag #'(viola2 fagotto2 viole fagotti) { dod'16 re' dod'8 dod'16 re' mi'8 mi'16 re' | dod'4 }
>> r4 r |
r8 <>\p <<
  \tag #'(viola1 fagotto1 viole fagotti) { mi'16 fa' mi'8 mi'16 fa' sol'8 fa'16 mi' | fa'4 }
  \tag #'(viola2 fagotto2 viole fagotti) { dod'16 re' dod'8 dod'16 re' mi'8 re'16 dod' | re'4 }
>> r4 r |
R2.*10 |
<>\f <<
  \tag #'(viola1 viole) { do'4 fa' la' | }
  \tag #'(viola2 viole) { la4 do' fa' | }
  \tag #'(fagotto1 fagotti) { la4 do' fa' | }
  \tag #'(fagotto2 fagotti) { fa4 la la | }
>>
<<
  \twoVoices #'(viola1 viola2 viole) <<
    { \appoggiatura do''16 sib'8( la'16 sol') \appoggiatura fa'4 mi'2 | }
    { re'8( do'16 sib) \appoggiatura la4 sol2 | }
  >>
  \tag #'(fagotto1 fagotti) { re'8( do'16 sib) \appoggiatura la4 sol2 | }
  \tag #'(fagotto2 fagotti) { sib8( la16 sol) \appoggiatura fa4 mi2 | }
>>
<<
  \tag #'(viola1 viole) {
    fa'4 la'2 |
    sol'4 sib'2 |
    la'8 re'' do''8.(\trill sib'32 la') sib'8.(\trill la'32 sol') |
  }
  \tag #'(viola2 viole) {
    la4 fa'2 |
    do'4 sol'2 |
    fa'8 fa' la'8.(\trill sol'32 fa') sol'8.(\trill fa'32 mi') |
  }
  \tag #'(fagotto1 fagotti) {
    la4 do'2~ |
    do'4 sib2 |
    la8 re' do'8.(\trill sib32 la) sib8.(\trill la32 sol) |
  }
  \tag #'(fagotto2 fagotti) {
    fa4 la2 |
    sol4 sol2 |
    fa8 fa la8.(\trill sol32 fa) sol8.(\trill fa32 mi) |
  }
>>
<<
  \tag #'(viola1 viola2 viole) fa'8
  \tag #'(fagotto1 fagotto2 fagotti) fa8
>> <<
  \tag #'(viola1 fagotto1 viole fagotti) {
    re'8 do'8.(\trill sib32 la) sib8.(\trill la32 sol) |
  }
  \tag #'(viola2 fagotto2 viole fagotti) {
    sib8 la8.(\trill sol32 fa) sol8.(\trill fa32 mi) |
  }
>>
fa8 <<
  \tag #'(viola1 fagotto1 viole fagotti) { la16 sib la8 la la la | }
  \tag #'(viola2 fagotto2 viole fagotti) { fa16 sol fa8 fa fa fa | }
>>
