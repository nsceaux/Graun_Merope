\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff << \global \includeNotes "violino1" >>
        \new Staff << \global \includeNotes "violino2" >>
      >>
      %\new GrandStaff \with { instrumentName = "Viola" } <<
      %  \new Staff << \global \keepWithTag #'viola1 \includeNotes "viola" >>
      %  \new Staff << \global \keepWithTag #'viola2 \includeNotes "viola" >>
      %>>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \keepWithTag #'viole \includeNotes "viola"
      >>
      \new Staff \with { instrumentName = "Fagotti" } <<
        \global \keepWithTag #'fagotti \includeNotes "viola"
      >>
      %\new GrandStaff \with { instrumentName = "Fagotti" } <<
      %  \new Staff << \global \keepWithTag #'fagotto1 \includeNotes "viola" >>
      %  \new Staff << \global \keepWithTag #'fagotto2 \includeNotes "viola" >>
      %>>
    >>
    \new Staff \with { instrumentName = \markup\character Polifonte }
    \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*7\break s2.*7\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*6\pageBreak
        s2.*6\break \grace s4 s2.*4\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*5\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}