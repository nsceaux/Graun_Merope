\clef "soprano/treble" r2 r4 la'8 la' |
mib''8. mib''16 mib''8 r16 mib'' do''8 do'' r16 do'' do'' sib' |
sol'8 sol' r4 r2 |
sib'8 sib'16. sib'32 sib'8 re'' si' si' r4 |
si'8. si'16 si'8 la'16 sol' do''8 do'' r4 |
r2 do''8 do''16 do'' do''8. reb''16 |
sib'8 sib' r sib' reb'' reb'' reb''8. do''16 |
lab'8 lab' r4 r2 |
do''8 r16 do'' fa''8 fa''16 fa'' do''8 do'' do''8. reb''16 |
sib'8 sib' sib'8 sib'16 do'' reb''4 r |
reb''?8 reb''16 reb'' reb''8 do'' lab'8 lab' r4 |
do''4 sib'8 lab' re''! re'' r4 |
re''8. re''16 re''8 mib'' mib'' sib' r4 |
R1 |
r8 sib'16 sib' sib'8 sib'16 sib' mib''4 mib''8 mib''16. fa''32 |
re''8 re'' r fa'' fa'' lab' lab'8. sib'16 |
sol'8 sol' r4 sol'8 sol'16 sol' sol'8 do'' |
si'! si' r8 fa'' fa'' si' r do'' |
do'' sol' r4 r2 |
r4 sol' sol'8 sol'16 sol' sol'8 sol' |
do'' do'' r4 do''8 do''16 do'' do''8. mib''16 |
do''8. do''16 do''8 si'! re'' re'' r4 |
sol'8. sol'16 sol'8 sol'16 la' si'!8 si' la' sol' |
do''8 do'' do'' do''16 reb'' sib'4 r8 sol' |
do'' do'' do'' sol' lab' lab' r4 |
r2 r4 r8 do'' |
lab' lab' lab'8. do''16 do''8 fa' r4 |
sib'4 r8 sib' re'' re''16 re'' do''8 re'' |
sib'4 re''8 re''16 fa'' lab'4 lab'8 sib' |
sol' sol' r sib'16 sib' mib''8 mib'' r do'' |
la'! la' r16 la' la' sib' sib'8 fa' r4 |
R1 |
