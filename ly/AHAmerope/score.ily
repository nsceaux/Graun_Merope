\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violini" } <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \keepWithTag #'viola \includeNotes "violini"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Merope } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*2\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break s1*3\pageBreak
        s1*2\break s1*3\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}