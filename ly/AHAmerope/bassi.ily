\clef "bass" fad8. fad16 fad8. fad16 fad4~ fad4\p~ |
fad1 |
sol16 sol32\f la sib16 sol fad-! sol-! mib-! re-! dod16 sol32 la sib16 sol fad[ sol] re32[ do sib, la,] |
sol,8 r r4 r8. fa16 fa4-! |
r2 mib16 do'32 re' mib'16 do' si! do' lab sol |
fad do'32 re' mib'16 do' si[ do'] sol32[ fa mib re] do8 r r4 |
mi!1\p |
fa16 fa32\f sol lab16 fa mi! fa reb do si,! fa32 sol lab16 fa mi![ fa] do32[ sib, lab, sol,] |
fa,8 r r4 r2 |
sol1\p~ |
sol2 lab~ |
lab1~ |
lab4 r r sib\f |
mib16 mib32 fa sol16 mib re mib do sib, la,! mib32 fa sol16 mib re[ mib] sib,32[ lab, sol, fa,] |
mib,8 r r4 r2 |
mib1\p |
r8. mib16\f mib4-! r2 |
r8. re16 re4-! r8. re16 re4-! |
do16 do'32 re' mib'16 do' si! do' lab sol fad do'32 re' mib'16 do' si[ do'] sol32[ fa mib re] |
do8 r r4 r2 |
lab1~ |
lab2 sol~ |
sol1 |
mib2 mi~ |
mi r16 fa32\f sol lab16 fa mi! fa reb do |
si,! fa32 sol lab16 fa mi![ fa] do32[ sib, lab, sol,] fa,8 r r4 |
r2 r8. re16\p re4~ |
re1~ |
re |
r8. mib16 mib4-! r8. mib16 mib4-! |
r8. mib16 mib4-! r4 fa\f-! |
sib,-! r r2 |
