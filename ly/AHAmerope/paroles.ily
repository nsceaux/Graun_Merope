Quan -- to è bar -- baro oh stel -- le! il mio de -- sti -- no!
Osa un ti -- ran -- no in -- de -- gno
ol -- trag -- giar -- mi a tal se -- gno?
Non sa -- rà mai com -- pi -- to
un no -- do sì ab -- bor -- ri -- to…
Sì, l’in -- fame uc -- ci -- sor del ca -- ro fi -- glio
pria sve -- ne -- rà ques -- ta mia de -- stra ar -- di -- ta,
quindi a me stes -- sa tron -- che -- rà la vi -- ta.
O -- gni Nu -- me del Ciel pur mi si mos -- tri
a vo -- glia sua cruc -- cio -- so;
ven -- di -- car posso an -- co -- ra e fi -- glio, e spo -- so.
Ch’io al -- le fu -- ne -- ree fa -- ci
quel -- le dell’ I -- me -- neo vo -- les -- si u -- ni -- re?
Ch’io po -- tes -- si le -- va -- re il me -- sto ci -- glio
ver -- so quel Ciel, che più non ve -- de il fi -- glio?
Get -- ta -- ta ò la mia sor -- te. Sì, quest al -- ma
tran -- quil -- la o -- mai, giac -- chè ai di -- sa -- stri è a -- vez -- za,
li pre -- ve -- de, li in -- sul -- ta, e li di -- sprez -- za.
