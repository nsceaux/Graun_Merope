\tag #'(violino1 violino2 violini) \clef "treble" |
\tag #'viola \clef "alto"
<>
<<
  \tag #'(violino1 violini) {
    mib''8. mib''16 mib''8. mib''16 mib''4~ mib''\p~ |
    mib''1 |
  }
  \tag #'(violino2 violini) {
    do''8. do''16 do''8. do''16 do''4~ do''\p~ |
    do''1 |
  }
  \tag #'viola {
    la'8. la'16 la'8. la'16 la'4~ la'\p~ |
    la'1 |
  }
>>
r16 sol'32\f la' sib'16 sol' fad'-! sol'-! mib'-! re'-! dod' sol'32 la' sib'16 sol' fad'[ sol'] re'32[ do' sib la] |
sol8 r r4 r8. <<
  \tag #'(violino1 violini) { si'!16 si'4-! | }
  \tag #'(violino2 violini) { sol'16 sol'4-! | }
  \tag #'viola { re'16 re'4-! | }
>>
r2 r16 do''32 re'' mib''16 do'' si'! do'' lab' sol' |
fad'[ do''32 re''] mib''16[ do''] si'![ do''] sol'32[ fa' mib' re'] do'8 r r4 |
<>\p <<
  \tag #'(violino1 violini) { reb''1 | }
  \tag #'(violino2 violini) { sib'1 | }
  \tag #'viola { sol'1 | }
>>
r16 <>\f <<
  \tag #'(violino1 violino2 violini) {
    fa''32 sol'' lab''16 fa'' mi''! fa'' reb'' do'' si'! fa''32 sol'' lab''16 fa'' mi''[ fa''] do''32[ sib' lab' sol'] |
    fa'8
  }
  \tag #'viola {
    fa'32 sol' lab'16 fa' mi'! fa' reb' do' si! fa'32 sol' lab'16 fa' mi'[ fa'] do'32[ sib lab sol] |
    fa8
  }
>> r8 r4 r2 |
<>\p
<<
  \twoVoices #'(violino1 violino2 violini) <<
    { reb''1~ |
      reb''2 do''~ |
      do'' re''!~ |
      re''4 }
    { sib'1~ |
      sib'2 lab'~ |
      lab'1~ |
      lab'4 }
  >>
  \tag #'viola {
    mib'1~ |
    mib'~ |
    mib'2 fa'~ |
    fa'4
  }
>> r4 r <>\f <<
  \tag #'(violino1 violini) { lab''4 | sol''16 }
  \tag #'(violino2 violini) { fa''4 | mib''16 }
  \tag #'viola { re''4 | mib''16 }
>>
<<
  \tag #'(violino1 violino2 violini) {
    mib''32 fa'' sol''16 mib'' re'' mib'' do'' sib' la'! mib''32 fa'' sol''16 mib'' re''[ mib''] sib'32[ lab' sol' fa'] |
    mib'8
  }
  \tag #'viola {
    mib'32 fa' sol'16 mib' re' mib' do' sib la! mib'32 fa' sol'16 mib' re'[ mib'] sib32[ lab sol fa] |
    mib8
  }
>> r r4 r2 |
<>\p <<
  \tag #'(violino1 violini) { re''1 | }
  \tag #'(violino2 violini) { lab'1 | }
  \tag #'viola { fa'1 | }
>>
r8. <>\f <<
  \tag #'(violino1 violini) { mib''16 mib''4-! }
  \tag #'(violino2 violini) { sol'16 sol'4-! }
  \tag #'viola { sib16 sib4-! }
>> r2 |
r8. <<
  \tag #'(violino1 violini) { fa''16 fa''4-! }
  \tag #'(violino2 violini) { si'!16 si'4-! }
  \tag #'viola { re'16 re'4-! }
>> r8. <<
  \tag #'(violino1 violini) { si'!16 si'4-! }
  \tag #'(violino2 violini) { fa'16 fa'4-! }
  \tag #'viola { re'16 re'4-! }
>>
r16 do''32 re'' mib''16 do'' si'![ do'' lab' sol'] fad'[ do''32 re'' mib''16 do''] si'![ do''] sol'32[ fa' mib' re'] |
do'8 r r4 r2 |
<>\p <<
  \twoVoices #'(violino1 violino2 violini) <<
    { fad''1~ |
      fad''2 sol''~ |
      sol''1~ |
      sol''~ |
      sol''4 }
    { do''1~ |
      do''2 si'!~ |
      si'1 |
      do''2 sib'~ |
      sib'4 }
  >>
  \tag #'viola {
    lab'1~ |
    lab'2 re'~ |
    re'1 |
    mib'2 do'~ |
    do'4
  }
>> r4 r16 <>\f
<<
  \tag #'(violino1 violino2 violini) {
    fa''32 sol'' lab''16 fa'' mi''! fa'' reb'' do'' |
    si'!16 fa''32 sol'' lab''16 fa'' mi''[ fa''] do''32[ sib' lab' sol'] fa'8
  }
  \tag #'viola {
    fa'32 sol' lab'16 fa' mi'! fa' reb' do' |
    si! fa'32 sol' lab'16 fa' mi'[ fa'] do'32[ sib lab sol] fa8
  }
>> r8 r4 |
r2 r8. <>\p <<
  \tag #'(violino1 violini) { fa''16 fa''4~ | fa''1~ | fa'' | }
  \tag #'(violino2 violini) { sib'16 sib'4~ | sib'1~ | sib' | }
  \tag #'viola { re'16 re'4~ | re'1~ | re' | }
>>
r8. <<
  \tag #'(violino1 violini) { sib'16 sib'4-! }
  \tag #'(violino2 violini) { sol'16 sol'4-! }
  \tag #'viola { mib'16 mib'4-! }
>> r8. <<
  \tag #'(violino1 violini) { mib''16 mib''4-! | }
  \tag #'(violino2 violini) { do''16 do''4-! | }
  \tag #'viola { sol'16 sol'4-! | }
>>
r8. <<
  \tag #'(violino1 violini) { fa''16 fa''4-! }
  \tag #'(violino2 violini) { do''16 do''4-! }
  \tag #'viola { la'!16 la'4-! }
>> r4 <>\f <<
  \tag #'(violino1 violini) { la''4-! | sib''-! }
  \tag #'(violino2 violini) { mib''4-! | re''-! }
  \tag #'viola { do''4-! | sib'-! }
>> r4 r2 |
