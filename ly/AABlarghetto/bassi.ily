\clef "bass" do'4\f r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4 re sol |
do sol8. fa16 mib8. re16 |
do4\p r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4\f fa sol |
do r r |
R2.*14 |
r4 sib8.\p lab16 sol8. fa16 |
mib4\f r8. lab16 sib8. do'16 |
reb'4 r8. sol16 lab8. sib16 |
do'4 fa sib |
mib sib8. lab16 sol8. fa16 |
mib4\p r8. lab16 sib8. do'16 |
reb'4 r8. sol16 lab8. sib16 |
do'4\f lab sib |
mib r r |
R2.*7 |
r4 re'8.\p do'16 sib8. la16 |
sol4\f r8. do16 re8. mib16 |
fa4 r8. sib,16 do8. re16 |
mib4 la, re |
sol, re8. do16 sib,8. la,16 |
sol,4\p r8. do16 re8. mib16 |
fa4 r8. sib,16 do8. re16 |
mib4\f do re |
sol, r r |
R2.*6 |
r4 sol8.\p fa16 mib8. re16 |
do4\f r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4 re sol |
do sol8.\p fa16 mib8. re16 |
do4 r r |
R2.*8 |
r4 sol8.\p fa16 mib8. re16 |
do4\f r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4 re sol |
do sol8. fa16 mib8. re16 |
do4\p r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4\f fa sol |
do la, fa, |
sib, la, fa, |
sib,\p la, fa, |
sib,2. |
