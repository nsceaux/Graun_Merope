\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Flauti" } <<
      \global \keepWithTag #'flauti \includeNotes "flauti"
    >>
    \new Staff \with { instrumentName = "Violini" } <<
      \global \keepWithTag #'violini \includeNotes "violini"
    >>
    \new Staff \with { instrumentName = "Viola" } <<
      \global \includeNotes "viola"
    >>
    \new Staff \with { instrumentName = "Basso" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2.*5\break s2.*7\pageBreak
        s2.*6\break s2.*7\break s2.*6\pageBreak
        \grace s8 s2.*6\break s2.*6\break s2.*6\pageBreak
        s2.*7\break s2.*7\break s2.*6\pageBreak
        s2.*6\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}