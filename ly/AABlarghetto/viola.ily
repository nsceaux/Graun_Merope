\clef "alto" do'4\f r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4 re sol |
do sol8. fa16 mib8. re16 |
do4\p r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4\f fa sol |
do r r |
re'2\p r4 |
mib'2 r4 |
fa'4 sol' sol |
do' sol'8. fa'16 mib'8. re'16 |
do'2 r4 |
r8 fa' lab'8.( sol'32 fa') sol'8. lab'16 |
sib'2. |
r8 mib'16( fa' sol'8) mib'16( fa' sol'8) sol'16( lab' |
sib'2.) |
mib'2 mi'4 |
fa'2 sol'4 |
lab'2 la'4 |
sib'2 la'4 |
sib'2 la'4 |
sib'2 r4 |
mib4\f r8. lab16 sib8. do'16 |
reb'4 r8. sol16 lab8. sib16 |
do'4 fa sib |
mib sib8. lab16 sol8. fa16 |
mib4\p r8. lab16 sib8. do'16 |
reb'4 r8. sol16 lab8. sib16 |
do'4\f lab sib |
mib r r |
r8 sib\p sib8.( do'32 re') sib8.( do'32 re') |
si2. |
r8 do' do'8.( re'32 mib') do'8.( re'32 mib') |
la4 fad'2 |
sol4 sol'2 |
do' dod'4 |
re'2 dod'4 |
re'2 r4 |
sol'\f r8. do'16 re'8. mib'16 |
fa'4 r8. sib16 do'8. re'16 |
mib'4 la re' |
sol re'8. do'16 sib8. la16 |
sol4\p r8. do'16 re'8. mib'16 |
fa'4 r8. sib16 do'8. re'16 |
mib'4\f do' re' |
sol r r |
r8 sol'\p fa'8*2/3( sol' mib') re'( mib' do') |
si2. |
do'2. |
fa'2 fad'4 |
sol'2.~ |
sol'2 fad'4 |
sol'2 r4 |
do'4\f r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4 re sol |
do sol8.\p fa16 mib8. re16 |
do4 r8. fa'16 sol'8. lab'16 |
sib'4 r8. mib'16 fa'8. sol'16 |
lab'4 fa' sol' |
mi'2. |
fa' |
sib' |
mib' |
lab' |
lab |
sol2 r4 |
do'\f r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4 re sol |
do sol8. fa16 mib8. re16 |
do4\p r8. fa16 sol8. lab16 |
sib4 r8. mib16 fa8. sol16 |
lab4\f fa sol |
do do' la |
sib do' la |
sib\p do' la |
fa2. |
