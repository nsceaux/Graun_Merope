\clef "treble" R2.*7 |
r8 do'' mib''8.( re''32 do'') si'8 do'' |
fa' do'' mib''8.( re''32 do'') si'8( do'') |
sol' do'' mib''8.( re''32 do'') si'8( do'') |
<<
  \tag #'(flauto1 flauti) {
    s8. lab''16 \appoggiatura lab''16 sol''8. fa''16 \appoggiatura fa'' mib''8. re''16 |
    \appoggiatura { re''16.[ fa''32] } mib''4 re''2 |
    s8 mib''8 sol''8.( fa''32 mib'' fa''8.) sol''16 |
    lab''2.~ |
    lab''8 fa''16( sol'' lab''8) sol''16( lab'' sib''8.) lab''16 |
    sol''2.~ |
    sol''8 fa''16( sol'' lab''8) sol''16( lab'' sib''8.) lab''16 |
    sol''8. do'''16 do'''4( sib'') |
    s8. lab''16 lab''4( sol'') |
    s8. fa''16 fa''4( mib''4)\trill |
    re''8 fa''16*2/3( sol'' lab'') \appoggiatura lab''4 sol''2 |
    fa''8 fa''16*2/3( sol'' lab'') \appoggiatura lab''4 sol''2 |
    \appoggiatura sol''4 fa''2
  }
  \tag #'(flauto2 flauti) {
    s8. fa''16 \appoggiatura fa'' mib''8. re''16 \appoggiatura re'' do''8. si'16 |
    \appoggiatura { si'16.[ re''32] } do''4 si'2 |
    s8 do'' mib''8.( re''32 do'' re''8.) mib''16 |
    fa''2.~ |
    fa''8 re''16( mib'' fa''8) mib''16( fa'' sol''8.) fa''16 |
    mib''2.~ |
    mib''8 re''16( mib'' fa''8) mib''16( fa'' sol''8.) fa''16 |
    mib''8. lab''16 lab''4( sol'') |
    s8. fa''16 fa''4( mib'') |
    s8. re''16 re''4( do''4)\trill |
    sib'8 re''16*2/3( mib'' fa'') \appoggiatura fa''4 mib''2 |
    re''8 re''16*2/3( mib'' fa'') \appoggiatura fa''4 mib''2 |
    \appoggiatura mib''4 re''2
  }
  { lab'8. s16 s2 s2. |
    r8 s s2 s2.*5 |
    r8. s16 s2 |
    r8. s16 s2 }
>> r4 |
R2.*7 |
<<
  \tag #'(flauto1 flauti) {
    s8 sol'' sol''8.( lab''32 sib'') sol''8.( lab''32 sib'') |
    \appoggiatura lab''8 sol''4 fa''2 |
    s8 fa'' fa''8.( sol''32 lab'') fa''8.( sol''32 lab'') |
    \appoggiatura sol''8 fa''4 mib''2 |
    s8 do''' do'''8.( re'''32 mib''') re'''8. do'''16 |
    sib''8. sib''16 sib''8.( do'''32 re''') do'''8. sib''16 |
    la''8.( sib''32 do''') \appoggiatura do'''4 sib''2 |
    la''8.( sib''32 do''') \appoggiatura do'''4 sib''2 |
    \appoggiatura sib''4 la''2
  }
  \tag #'(flauto2 flauti) {
    s8 mib'' mib''8.( fa''32 sol'') mib''8.( fa''32 sol'') |
    \appoggiatura fa''8 mib''4 re''2 |
    s8 re'' re''8.( mib''32 fa'') re''8.( mib''32 fa'') |
    \appoggiatura mib''8 re''4 do''2 |
    s8 la'' la''8.( sib''32 do''') sib''8. la''16 |
    sol''8. sol''16 sol''8.( la''32 sib'') la''8. sol''16 |
    fad''8.( sol''32 la'') \appoggiatura la''4 sol''2 |
    fad''8.( sol''32 la'') \appoggiatura la''4 sol''2 |
    \appoggiatura sol''4 fad''2
  }
  { r8 s s2 s2. | r8 s s2 s2. | r8 }
>> r4 |
R2.*7 |
r8 sol'' fa''8*2/3( sol'' mib'') re''( mib'' do'') |
\appoggiatura do''4 si'4.*7/6 do''16 re''8. mib''16 |
<<
  \tag #'(flauto1 flauti) {
    \tuplet 3/2 { fa''8 fa''[( sol'')] } lab''8*2/3( sol'' fa'') lab''( sol'' fa'') |
    s mib''( fa'') sol''( fa'' mib'') sol''( fa'' mib'') |
    re''( mib'' fa'') \appoggiatura fa''4 mib''2\trill |
    re''8*2/3( si'' do''') re'''( do''' si'') re'''( do''' si'') |
  }
  \tag #'(flauto2 flauti) {
    \tuplet 3/2 { re''8 re''([ mib'']) } fa''8*2/3( mib'' re'') fa''( mib'' re'') |
    s do''( re'') mib''( re'' do'') mib''( re'' do'') |
    si'( do'' re'') \appoggiatura re''4 do''2\trill |
    si'8*2/3( re'' mib'') fa''( mib'' re'') fa''( mib'' re'') |
  }
  { s2. r8*2/3 }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''8*2/3([ sol'' fa'']) }
  { mib''8.[ re''16] }
>>
<<
  \tag #'(flauto1 flauti) {
    \appoggiatura fa''4 mib''2 |
    \appoggiatura mib''4 re''2
  }
  \tag #'(flauto2 flauti) {
    \appoggiatura re''4 do''2 |
    \appoggiatura do''4 si'2
  }
>> r4 |
R2.*4 |
<<
  \tag #'(flauto1 flauti) {
    sol''8.(\trill fa''32 sol'') lab''2 |
    fa''8.(\trill mib''32 fa'') sol''2 |
    mib''8.(\trill re''32 mib'') fa''8. fa''16 mib''8. re''16 |
  }
  \tag #'(flauto2 flauti) {
    mib''8.(\trill re''32 mib'') fa''2 |
    re''8.(\trill do''32 re'') mib''2 |
    do''8.(\trill si'32 do'') re''8. re''16 do''8. si'16 |
  }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do''8*2/3 sib'' do''' reb'''( do''' sib'') sib''( lab'' sol'') |
    lab''2.~ |
    lab''8*2/3 lab'' sib'' do'''( sib'' lab'') lab'' sol'' fad'' |
    sol''2.~ |
    sol''8*2/3 do'''( sib'') lab'' lab''( sol'') fa'' sol''( mi'') |
    fa''4 do'''4. fad''8 |
    \appoggiatura fad''4 sol''2
  }
  { do''2.~ |
    do''8*2/3 fa''( sol'') lab''( sol'' fa'') mib''( re'' do'') |
    re''2.~ |
    re''8*2/3 mib''( fa'') sol''( fa'' mib'') re''( do'' si') |
    do''2.~ |
    do''8*2/3 lab''( sol'') fa'' fa''( mib'') re'' mib''( do'') |
    \appoggiatura do''4 si'2 }
>> r4 |
R2.*11 |
