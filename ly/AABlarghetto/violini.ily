\clef "treble" <>
<<
  \tag #'(violino1 violini) {
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8.( re'32 mib') fa'8. fa'16 mib'8. re'16 |
    \appoggiatura re'8 mib'4 re'2 |
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8.( re'32 mib') fa'8. fa'16 mib'8. re'16 |
  }
  \tag #'(violino2 violini) {
    mib'8.( re'32 mib') fa'2 |
    re'8.( do'32 re') mib'2 |
    do'8.( si32 do') re'8. re'16 do'8. si16 |
    \appoggiatura si8 do'4 si2 |
    mib'8.( re'32 mib') fa'2 |
    re'8.( do'32 re') mib'2 |
    do'8.( si32 do') re'8. re'16 do'8. si16 |
  }
  { s2.\f\trill s\trill s\trill s s\p\trill s\trill s\trill\f }
>>
do'4 r r |
re'2\p r4 |
mib'2 r4 |
fa'4 sol' sol |
do' sol'8. fa'16 mib'8. re'16 |
do'2 r4 |
r8 fa' lab'8.( sol'32 fa') sol'8. lab'16 |
sib'2. |
r8 mib'16( fa' sol'8) mib'16( fa' sol'8) sol'16( lab' |
sib'2.) |
mib'2 mi'4 |
fa'2 sol'4 |
lab'2 la'4 |
sib'2 la'4 |
sib'2 la'4 |
sib'2 r4 |
<<
  \tag #'(violino1 violini) {
    sib'8.( lab'32 sib') do''2 |
    lab'8.( sol'32 lab') sib'2 |
    sol'8.( fa'32 sol') lab'8. lab'16 sol'8. fa'16 |
    \appoggiatura fa'8 sol'4 fa'2 |
    sib'8.( lab'32 sib') do''2 |
    lab'8.( sol'32 lab') sib'2 |
    sol'8.( fa'32 sol') lab'8. lab'16 sol'8. fa'16 |
  }
  \tag #'(violino2 violini) {
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8.( re'32 mib') fa'8. fa'16 mib'8. re'16 |
    \appoggiatura re'8 mib'4 re'2 |
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8.( re'32 mib') fa'8. fa'16 mib'8. re'16 |
  }
  { s2.\f\trill s\trill s\trill s s\p\trill s\trill s\trill\f }
>>
mib'4 r r |
r8 sib\p sib8.( do'32 re') sib8.( do'32 re') |
si2. |
r8 do' do'8.( re'32 mib') do'8.( re'32 mib') |
la4 fad'2 |
sol4 sol'2 |
do'2 dod'4 |
re'2 dod'4 |
re'2 r4 |
<<
  \tag #'(violino1 violini) {
    re''8.( do''32 re'') mib''2 |
    do''8.( sib'32 do'') re''2 |
    sib'8.( la'32 sib') do''8. do''16 sib'8. la'16 |
    \appoggiatura la'8 sib'4 la'2 |
    re''8.( do''32 re'') mib''2 |
    do''8.( sib'32 do'') re''2 |
    sib'8.( la'32 sib') do''8. do''16 sib'8. la'16 |
  }
  \tag #'(violino2 violini) {
    sib'8.( la'32 sib') do''2 |
    la'8.( sol'32 la') sib'2 |
    sol'8.( fad'32 sol') la'8. la'16 sol'8. fad'16 |
    \appoggiatura fad'8 sol'4 fad'2 |
    sib'8.( la'32 sib') do''2 |
    la'8.( sol'32 la') sib'2 |
    sol'8.( fad'32 sol') la'8. la'16 sol'8. fad'16 |
  }
  { s2.\f\trill s\trill s\trill s s\p\trill s\trill s\trill\f }
>>
sol'4 r r |
r8 sol'\p fa'8*2/3( sol' mib') re'( mib' do') |
si2. |
do'2. |
fa'2 fad'4 |
sol'2.~ |
sol'2 fad'4 |
sol'2 r4 |
<<
  \tag #'(violino1 violini) {
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8. re'32 mib' fa'8. fa'16 mib'8. re'16 |
    \appoggiatura re'8 mib'4 re'2 |
  }
  \tag #'(violino2 violini) {
    mib'8.( re'32 mib') fa'2 |
    re'8.( do'32 re') mib'2 |
    do'8.( sib32 do') re'8. re'16 do'8. si16 |
    \appoggiatura si8 do'4 si2 |
  }
  { s2.\f\trill s\trill s\trill }
>>
r4 r8. fa'16 sol'8. lab'16 |
sib'4 r8. mib'16 fa'8. sol'16 |
lab'4 fa' sol' |
mi'2. |
fa' |
sib' |
mib' |
lab' |
lab |
sol2 r4 |
<<
  \tag #'(violino1 violini) {
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8.( re'32 mib') fa'8. fa'16 mib'8. re'16 |
    \appoggiatura re'8 mib'4 re'2 |
    sol'8.( fa'32 sol') lab'2 |
    fa'8.( mib'32 fa') sol'2 |
    mib'8.( re'32 mib') fa'8. fa'16 mib'8. re'16 |
  }
  \tag #'(violino2 violini) {
    mib'8.( re'32 mib') fa'2 |
    re'8.( do'32 re') mib'2 |
    do'8.( si32 do') re'8. re'16 do'8. si16 |
    \appoggiatura si8 do'4 si2 |
    mib'8.( re'32 mib') fa'2 |
    re'8.( do'32 re') mib'2 |
    do'8.( si32 do') re'8. re'16 do'8. si16 |
  }
  { s2.\f\trill s\trill s\trill s s\p\trill s\trill s\trill\f }
>>
do'8. do''16 fa'4( mib')\trill |
re'8 re''16( sib') fa'4( mib')\trill |
re'8\p re''16( sib') fa'4( mib')\trill |
\appoggiatura mib'2 re'2. |
