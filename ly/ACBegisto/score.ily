\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          \global \includeNotes "violino1"
        >>
        \new Staff <<
          \global \includeNotes "violino2"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = "Canto" } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2*6\break s2*8\break s2*8\pageBreak
        s2*7\break s2*6\break s2*7\pageBreak
        s2*7\break s2*8\break s2*7\pageBreak
        s2*7\break s2*7\break s2*6\pageBreak
        s2*8\break s2*7\break s2*7\pageBreak
        s2*6\break s2*7\break s2*6\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}