\clef "treble" dod''8.( re''16 mi''8) re'' |
dod''( si') la' dod'' |
si' la'4 si'16( sold') |
la'8 dod''16( si') si'4 |
dod''8.\p( re''16 mi''8) re'' |
dod''( si') la' dod'' |
si'( la'4) si'16( sold') |
la'8 dod''16( si') si'4 |
re''8.( mi''16 fad''8) re'' |
dod''8.( re''16 mi''8) dod'' |
\appoggiatura mi''16 re''8 dod''16( si') \appoggiatura si'8 dod''4 |
si' r |
dod''8.( re''16 mi''8) re'' |
dod''( si') la' dod'' |
si' la'4 si'16( sold') |
la'8 dod''16( si') si'4 |
re''8.([ mi''16 fad''8]) re'' |
dod''8.( re''16 mi''8) dod'' |
\appoggiatura mi''16 re''8 dod''16 si' dod''8 dod'' |
dod''4( si'8) r |
mi''8.( fad''16 mi''8) mi'' |
mi''( dod'') dod'' mi'' |
mi''8.( fad''16 mi''8) mi'' |
mi''( dod'') dod'' mi'' |
mi''16( red'' fad'' mi'') la'8 sold' |
sold'4 fad' |
si'8.( dod''16 si'8) re''! |
re''16( dod'' mi'' re'') dod''4 |
dod''8.( red''16 dod''8) mi'' |
mi''16( red'' fad'' mi'') red''8 sold'' |
\appoggiatura sold''8*1/2 fad''8. mi''16 \appoggiatura mi''8*1/2 red''8. dod''16 |
dod''4( si'8) dod'' |
\appoggiatura dod''8 si'8. la'16 \appoggiatura la'8 sold'8. fad'16 |
mi'8 mi''4_\pocof sold''16( mi'') |
mi''8 mi''4 sold''16( mi'') |
mi''8.(\trill red''32 mi'') fad''8 la'\p |
sold'8.( la'16 si'8) dod'' |
si'8.( dod''16 si'8) mi''\pp |
si'8.( dod''16 si'8) dod''\pocof |
si'8 la' sold' fad' |
mi'2 |
re''8.(\f mi''16 fad''8) mi'' |
re''8( dod'' si') re'' |
dod'' si'4 dod''16( lad') |
si'8 re''16( dod'') dod''4 |
re''8.(\p mi''16 fad''8) mi'' |
re''( dod'') si' re'' |
dod''8( si'4) dod''16( lad') |
si'8 re''16( dod'') dod''4 |
re''8.( mi''16 fad''8) re'' |
dod''8.( re''16 mi''8) dod'' |
\appoggiatura mi''16 re''8 dod''16 si' \appoggiatura si'8 dod''4 |
si'4 r |
dod''8.( re''16 mi''8) re'' |
dod''( si') la' dod'' |
si' la'4 si'16( sold') |
la'8 dod''16( si') si'4 |
mi''8.([ fad''16 mi''8]) sol' |
sol'16( fad' la' sol') fad'4 |
fad''8.( sold''!16 fad''8) la' |
la'16( sold' si' la') sold'8 mi'' |
\appoggiatura mi''16 re''8 dod''16 si' dod''8 dod'' |
dod''4( si'8) r |
mi''8.( fad''16 mi''8) mi'' |
mi''( dod'') dod'' mi'' |
mi''8. fad''16 mi''8 mi'' |
mi''8[( dod'') dod''] mi''\pocof |
fad''16( mi'' fad''4) sold''8 |
la''8.\trill sold''32 fad'' mi''4 |
re''8.\p( mi''16 fad''8) re'' |
re''16( dod'' mi'' re'') dod''4 |
si'8.( dod''16 re''8) si' |
si'16( la' dod'' si') la'8 la'' |
\appoggiatura la''8 sold''8. fad''16 \appoggiatura fad''8 mi''8. re''16 |
re''4( dod''8) fad'' |
\appoggiatura fad''8 mi''8. re''16 \appoggiatura re''8 dod''8. si'16 |
la'8 la''4\pocof dod'''16( la'') |
la''8 la''4 dod'''16( la'') |
la''8.\trill( sold''32 fad'') mi''8 re''\p |
dod''8.( re''16 mi''8) fad'' |
mi''8.( fad''16 mi''8) la''\pp |
mi''8.( fad''16 mi''8) fad''\pocof |
mi'' re'' dod'' si' |
la'2 |
sold''8.( la''16 si''8) re'' |
re''16( dod'' mi'' re'') dod''4 |
la''8.( si''16 dod'''8) mi'' |
mi''16( re'' fad'' mi'') re''4 |
si''8.( dod'''16 re'''8) re'' |
dod''8.( re''16 mi''8.) fad''16 |
mi''8.([ fad''16 mi''8]) la''\p |
mi''8.([ fad''16 mi''8]) fad''8\f |
mi''8 re'' dod'' si' |
la'4 la |
%%%
do''8.\p( re''16 mi''8) re'' |
do''( si') la' do'' |
si'8( la'4) si'16( sold') |
la'8 do''16( si') si'4 |
re''8.([ mi''16 fa''8]) re'' |
do''8.( re''16 mi''8) do'' |
si'16( do'' re'' mi'') re''8 do'' |
do'' si' r4 |
mi''8.( fa''16 sol''8) fa'' |
mi''( re'') do'' mi'' |
re'' do''4 re''16( si') |
do''8 mi''16( re'') re''4 |
sol''8.([ la''16 sol''8]) sib' |
sib'16( la' do'' sib') la'4 |
la''8.( si''!16 la''8) do'' |
do''16( si' re'' do'') si'8 sol'' |
\appoggiatura sol''16 fa''8 mi''16 re'' mi''8 mi'' |
mi''4( re''8) r |
re''8.( mi''16 re''8) fa'' |
mi''8.( fa''16 mi''8) sol'' |
re''8.( mi''16 re''8) fa'' |
mi''8.( fa''16 mi''8) sol'' |
do''8.( re''16 do''8) mi'' |
la'8. la''16 sol''16( fa'') mi''( re'') |
\appoggiatura fa''16 mi''8 re''16 do'' \appoggiatura mi''16 re''8 do''16 si' |
do''8 do'''16(\pocof si'') la'' sol'' fad'' mi'' |
red''2 |
mi''8. re''!32 do'' si'8 do'' |
si'8. la'16 sol'8 fad' |
mi'2 |
sold''8.(\f la''16 si''8) re'' |
re''16( dod'' mi'' re'') dod''4 |
la''8.( si''16 dod'''8) mi'' |
mi''16( re'' fad'' mi'') re''4 |
si''8.( dod'''16 re'''8) re'' |
dod''8.( re''16 mi''8) fad'' |
mi'' re'' dod'' si' |
la'4 la |
