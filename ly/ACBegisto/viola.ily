\clef "alto" mi' mi' |
mi'8( re') dod' mi' |
re' dod'4 re'16 si |
dod'8 la mi'4 |
mi'\p mi' mi'8( re') dod' mi' |
re' dod'4 re'16( si) |
dod'8 la mi'4 |
sold sold |
la la |
mi' red' |
mi'8. re'!16 dod'8 si |
la4 la |
mi'8( re') dod' mi' |
re' dod'4 re'16( si) |
dod'8 la mi'4 |
sold sold |
la la |
mi' red' |
mi'8. red'16 mi'8 fad' |
sold'4 sold' |
la' la' |
sold' sold' |
la' la'8 sold' |
fad'4. mi'8 |
si8. dod'16 si8 la |
sold4 mi' |
mi'2 |
r4 fad' |
fad'4. si8 |
si4 si |
si si |
si si |
dod' sold\pocof |
la si |
dod' red' |
mi'\p mi' |
mi' mi' |
mi'4\pp mi'8 la\pocof |
si4 si |
mi'8. mi'16\f re'!8 dod' |
si4 si |
fad'8( mi' re') fad' |
mi' re'4 mi'16( dod') re'8 si fad'4 |
fad'\p fad' |
fad'8( mi') re' fad' |
mi' re'4 mi'16 dod' |
re'8 si fad'4 |
fad' mi' |
mi' mi' |
re' red' |
mi'8. re'!16 dod'8 si |
la4 la |
mi'8( re') dod' mi' |
re' dod'4 re'16( si) |
dod'8 la mi' re' |
dod'4 la |
la2 |
si4 si |
si4. mi8 |
mi'4 mi' |
mi'8. red'16 mi'8 fad' |
sold'4 sold' |
la' la' |
sold' sold' |
la' la' |
la'4\pocof la'8 si' |
mi'4 la'8 la |
mi'2\p |
mi' |
mi'4. re'8 |
re'16( dod' mi' re') dod'8 la |
mi'4 mi' |
mi' mi' |
mi' mi' |
fad' dod'\pocof |
re' mi' |
fad' sold' |
la'-\sug\p la' |
la' la' |
la'\pp la'8 re'\pocof |
mi'4 mi |
la8.\f si16 dod'8 la |
mi'4 mi' |
mi' mi' |
mi' mi' |
mi' mi' |
mi' mi' |
la' la' |
la' la' |
la'\p la'8 re'\f |
mi'4 mi |
la2 |
%%%
mi'4\p mi' |
mi'8( re' do') mi' |
re' do'4 re'16( si) |
do'8 la mi'4 |
sold4 sold |
la la |
mi' mi' |
mi'8. fa'16 mi'8 re' |
do'4 do' |
sol'8( fa') mi' sol' |
fa' mi'4 fa'16( re') |
mi'8 do' sol' fa' |
mi'4 do' |
do'2 |
re'4 re' |
re'4. sol8 |
sol'4 sol' |
sol8. fad16 sol8 la |
si4 si |
do' do' |
si si |
do' do' |
do' do' |
do' la' |
sol' sol |
la mi'\pocof |
fad'8 la' sol' fad' |
si'4. mi'8 |
mi'4 si |
mi2 |
mi'4\f mi' |
mi' mi' |
mi' mi' |
mi' mi' |
mi' mi' |
la' la'8 re' |
mi'4 mi |
la2 |
