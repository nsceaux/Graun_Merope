Bel de -- si -- o m’ac -- ce -- se il co -- re
di ten -- tar le tue ven -- det -- te;
fra pe -- ri -- gli il mi -- o va -- lo -- re
sol mo -- strar vo -- lea per te.
Fra pe -- ri -- gli il mio va -- lo -- re
il mi -- o va -- lo -- re
sol mo -- strar __ sol mo -- strar __ vo -- lea vo -- lea per te
vo -- lea vo -- lea per te
sol mo -- strar sol mo -- strar __ vo -- le -- a per te __ per te __
mo -- strar vo -- lea per te.

Bel de -- si -- o m’ac -- ce -- se il co -- re
di ten -- tar le tue ven -- det -- te;
fra pe -- ri -- gli il mi -- o va -- lo -- re
sol mo -- strar __
sol mo -- strar __ vo -- le -- a per te.
Fra pe -- ri -- gli il mi -- o va -- lo -- re
il mio va -- lo -- re
sol mo -- strar __ sol mo -- strar __ vo -- lea vo -- lea per te
vo -- lea vo -- lea per te
sol mo -- strar sol mo -- strar __ vo -- le -- a per te __ per te __
mo -- strar vo -- lea per te.


Il lan -- gui -- re in o -- zio vi -- le
di mia e -- tà mi par -- ve in -- de -- gno.
Rup -- pe il Cie -- lo il mi -- o di -- se -- gno,
e col -- pe -- vo -- le
e col -- pe -- vo -- le col -- pe -- vo -- le mi fe.
Rup -- pe il Cie -- lo il mio di -- se -- gno,
e __ col -- pe -- vo -- le col -- pe -- vo -- le mi fe
e col -- pe -- vo -- le col -- pe -- vo -- le mi fe.
