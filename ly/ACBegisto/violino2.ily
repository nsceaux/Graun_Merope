\clef "treble" la'8.( si'16 dod''8) si' |
la'( sold') la' mi' |
re' dod'4 re'16( si) |
dod'8 la'16( sold') sold'4 |
la'8.(\p si'16 dod''8) si' |
la'( sold') la' mi' |
re'( dod'4) re'16( si) |
dod'8 la'16( sold') sold'4 |
si'8.( dod''16 re''8) si' |
la'8.( si'16 dod''8) la' |
\appoggiatura dod''16 si'8 la'16( sold') \appoggiatura sold'8 la'4 |
sold'4 r |
la'8.( si'16 dod''8) si' |
la'( sold') la' mi' |
re' dod'4 re'16( si) |
dod'8 la'16( sold') sold'4 |
si'8.([ dod''16 re''8]) si' |
la'8.( si'16 dod''8) la' |
\appoggiatura dod''16 si'8 la'16 sold' la'8 la' |
la'4( sold'8) r |
si'4 si' |
dod''8( la') la' dod'' |
si'4 si' |
dod''8( la') la' dod'' |
la'4( red'8) mi' |
mi'4 red' |
mi'4. si'8 |
si'16( la' dod'' si') la'4 |
fad'4. dod''8 |
dod''16( si' red'' dod'') si'8 mi'' |
\appoggiatura mi''8*1/2 red''8. dod''16 \appoggiatura dod''8*1/2 si'8. la'16 |
la'4( sold'8) la' |
\appoggiatura la'8 sold'8. fad'16 \appoggiatura fad'8 mi'8. red'16 |
mi'8 sold'4\pocof si'16( sold') |
sold'8 sold'4 si'16( sold') |
sold'8 mi' si fad'\p |
mi'8.( fad'16 sold'8) la' |
sold'8.( la'16 sold'8) sold'\pp |
sold'8.( la'16 sold'8) la'\pocof |
sold' fad' mi' red' |
mi'2 |
si'8.(\f dod''16 re''8) dod'' |
si'( lad' si') fad' |
mi' re'4 mi'16( dod') |
re'8 si'16( lad') lad'4 |
si'8.(\p dod''16 re''8) dod'' |
si'( lad') si' fad' |
mi'( re'4) mi'16( dod') |
re'8 si'16( lad') lad'4 |
si'8.( dod''16 re''8) si' |
la'8.( si'16 dod''8) la' |
\appoggiatura dod''16 si'8 la'16 sold' \appoggiatura sold'8 la'4 |
sold' r |
la'8.( si'16 dod''8) si' |
la'( sold') la' mi' |
re' dod'4 re'16( si) |
dod'8 la'16( sold') sold'4 |
la'4. mi'8 |
mi'16( re' fad' mi') re'4 |
si4. fad'8 |
fad'16( mi' sold' fad') mi'8 dod'' |
\appoggiatura dod''16 si'8 la'16 sold' la'8 la' |
la'4( sold'8) r |
si'4 si' |
dod''8( la') la' dod'' |
si'4 si' |
dod''8[( la') la'] dod''\pocof |
re''16( dod'' re''4) si'8 |
dod''4 dod'' |
si'8.(\p dod''16 re''8) si' |
si'16( la' dod'' si') la'4 |
sold'8.( la'16 si'8) re' |
re'16( dod' mi' re') dod'8 dod'' |
\appoggiatura dod''8 si'8. re''16 \appoggiatura re''8 dod''8. si'16 |
si'4( la'8) re'' |
\appoggiatura re''8 dod''8. si'16 \appoggiatura si'8 la'8. sold'16 |
la'8 dod''4\pocof mi''16( dod'') |
dod''8 dod''4 mi''16( dod'') |
dod''8 la' si' si'\p |
la'8.( si'16 dod''8) re'' |
dod''8.( re''16 dod''8) dod''\pp |
dod''8.( re''16 dod''8) re''\pocof |
dod'' si' la' sold' |
la'2 |
si'8.( dod''16 re''8) si' |
si'16( la' dod'' si') la'4 |
dod''8.( re''16 mi''8) dod'' |
dod''16( si' re'' dod'') si'4 |
sold''8.( la''16 si''8) si' |
la'8.( si'16 dod''8) re'' |
dod''8.([ re''16 dod''8]) dod''\p |
dod''8.([ re''16 dod''8]) re''\f |
dod'' si' la' sold' |
la'4 la |
%%%
la'8.(\p si'16 do''8) si' |
la'( sold') la' mi' |
re'( do'4) re'16( si) |
do'8 la'16( sold') sold'4 |
si'8.([ do''16 re''8]) si' |
la'8.( si'16 do''8) la' |
sold'16( la' si' do'') si'8 la' |
la' sold' r4 |
do''8.( re''16 mi''8) re'' |
do''( si') do'' sol' |
fa' mi'4 fa'16( re') |
mi'8 do''16( si') si'4 |
do''4. sol'8 |
sol'16( fa' la' sol') fa'4 |
re'4. la'8 |
la'16( sol' si' la') sol'8 mi'' |
\appoggiatura mi''16 re''8 do''16 si' do''8 do'' |
do''4( si'8) r |
sol'4. re''8 |
do''8.( re''16 do''8) mi'' |
sol'4. re''8 |
do''8.( re''16 do''8) mi'' |
sol'4. sol'8 |
la'8. la''16 sol''16( fa'') mi''( re'') |
do''8 mi' \appoggiatura sol'8 fa'( mi'16 re') |
do'8 mi''16(\pocof re'') do'' si' la' sol' |
fad'2 |
mi'4. la'8 |
sol'8. fad'16 mi'8 red' |
mi'2 |
si'8.(\f dod''16 re''8) si' |
si'16( la' dod'' si') la'4 |
dod''8.( re''16 mi''8) dod'' |
dod''16( si' re'' dod'') si'4 |
sold''8.( la''16 si''8) si' |
la'8.( si'16 dod''8) re'' |
dod'' si' la' sold' |
la'4 la |
