\clef "bass" la, la, |
la, la, |
re mi |
la mi |
la,\p la, |
la, la, |
re mi |
la mi |
mi mi |
mi mi |
mi red |
mi8. re!16 dod8 si, |
la,4 la, |
la, la, |
re mi |
la mi |
mi mi |
mi mi |
mi red |
mi8. red16 mi8 fad |
sold4 sold |
la la |
sold sold |
la la8 sold |
fad4. mi8 |
si8. dod'16 si8 la |
sold4 sold |
la la |
lad lad |
si si |
si si |
si si |
si, si, |
dod sold,\pocof |
la, si, |
dod red |
mi4\p mi |
mi mi |
mi\pp mi8 la,\pocof |
si,4 si, |
mi8. mi16\f re!8 dod |
si,4 si, |
si, si, |
mi fad |
si fad |
si,\p si, |
si, si, |
mi fad |
si, fad |
si sold |
la la, |
re red |
mi8. re!16 dod8 si, |
la,4 la, |
la, la, |
re mi |
la mi8 re |
dod4 dod |
re re |
red red |
mi mi |
mi mi |
mi8. red16 mi8 fad |
sold4 sold |
la la |
sold sold |
la la |
la\pocof la |
la la |
sold\p sold |
la la |
mi mi |
mi mi |
mi mi |
mi mi |
mi mi |
fad dod\pocof |
re mi |
fad sold |
la-\sug\p la |
la la |
la\pp la8 re\pocof |
mi4 mi, |
la,8.\f si,16 dod8 la, |
mi4 mi |
mi mi |
mi mi |
mi mi |
mi mi |
la la |
la la |
la\p la8 re\f |
mi4 mi, |
la,2 |
%%%
la4\p la |
la la |
re mi |
la, mi |
mi mi |
mi mi |
mi mi |
mi8. fa16 mi8 re |
do4 do |
do do |
fa sol |
do' sol8 fa |
mi4 mi |
fa fa |
fad fad |
sol sol |
sol sol |
sol8. fad16 sol8 la |
si4 si |
do' do' |
si si |
do' do' |
mi mi |
fa fa |
sol sol, |
la, la\pocof~ |
la8 do' si la |
sol4. la8 |
si4 si, |
mi8.\f sold16 si8 mi' |
mi4 mi |
mi mi |
mi mi |
mi mi |
mi mi |
la la8 re |
mi4 mi, |
la,2 |
