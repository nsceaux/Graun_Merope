\clef "treble" do''8. si'16 |
do''4 sol'2 mib''8. do''16 |
re''4 sol'2 <<
  \tag #'violino1 {
    re''8. re''16 |
    \appoggiatura re''4 mib''4.. re''16 \appoggiatura re''4 mib''4.. re''16\p |
    \appoggiatura re''4 mib''4.. re''16 \appoggiatura re''4 mib''4.. re''16\f |
    mib''8.( re''16 fa''2) mib''4 |
    \appoggiatura fa''8 mib''4 re''
  }
  \tag #'violino2 {
    si'8. si'16 |
    \appoggiatura si'4 do''4.. si'16 \appoggiatura si'4 do''4.. si'16\p |
    \appoggiatura si'4 do''4.. si'16 \appoggiatura si'4 do''4.. si'16\f |
    do''8.( si'16 re''2) do''4 |
    \appoggiatura re''8 do''4 si'
  }
>> r4 do'8.\p si16 |
do'4 sol2 mib'8. do'16 |
re'4 sol2 <<
  \tag #'violino1 {
    re'8. re'16 |
    \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
    \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
    mib'8.( re'16 fa'2) mib'4 |
    \appoggiatura fa'8 mib'4 re'
  }
  \tag #'violino2 {
    si8. si16 |
    \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
    \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
    do'8.( si16 re'2) do'4 |
    do' si
  }
>> r4 do'8. si16 |
do'4 sol2 mib'8. do'16 |
re'4 sol2 <<
  \tag #'violino1 { re'8. re'16 | }
  \tag #'violino2 { si8. si16 | }
>>
mib'4 mib' fa' fa' |
<<
  \tag #'violino1 { sol'4 }
  \tag #'violino2 { sib4 }
>> r4 r2 |
r4 <<
  \tag #'violino1 { sol'4 lab' lab' | sol' }
  \tag #'violino2 { mib'4 fa' fa' | mib' }
>> r4 r2 |
r4 <<
  \tag #'violino1 { sol'4 lab' lab' | sol' }
  \tag #'violino2 { mib'4 fa' fa' | mib' }
>> r4 r <<
  \tag #'violino1 {
    mib'8. sib16 |
    sib8.( do'16) do'4 r fa'8. do'16 |
    do'8.( re'16) re'4 r sol'8. re'16 |
    mib'4 lab' lab' lab' |
    lab' lab' lab' lab' |
    sol' sol' sol' sol' |
    lab' lab' lab' lab' |
    sol' sol' sol' sol' |
    sol' sol' sol' sol' |
    la'2. la'4 |
    la'( sib')
  }
  \tag #'violino2 {
    sol8. sol16 |
    sol8.( lab16) lab4 r la8. la16 |
    la8.( sib16) sib4 r si8. si16 |
    do'4 mib' mib' mib' |
    re' re' re' re' |
    mib' mib' mib' mib' |
    fa' fa' fa' fa' |
    mib' mib' mib' mib' |
    mib' mib' mib' mib' |
    mib'2. mib'4 |
    mib'4( re')
  }
>> r4 do'8. re'16 |
re'4( \appoggiatura fa'16 mib'8 re'16 do') sib4-! do'8. re'16 |
re'4( \appoggiatura fa'16 mib'8 re'16 do') sib4-! <<
  \tag #'violino1 {
    lab'4 | sol' do'' \appoggiatura sol'4*1/2 fa'2\trill |
  }
  \tag #'violino2 {
    fa'4 | mib' mib'2 re'4\trill |
  }
>>
mib'2 r4 do''8.\f re''16 |
re''4( \appoggiatura fa''16 mib''8 re''16 do'') sib'4-! do''8.\p re''16 |
re''4( \appoggiatura fa''16 mib''8 re''16 do'') sib'4-! <>\f <<
  \tag #'violino1 {
    fa''8. lab'16 |
    sol'8*2/3([ mib'' re'']) do''([ sib' lab']) \appoggiatura sol'4 fa'2\trill |
  }
  \tag #'violino2 {
    lab'8. fa'16 |
    mib'4 mib'2 re'4\trill |
  }
>>
mib'2 r4 do'8.\p si16 |
do'4 sol2 mib'8. do'16 |
re'4 sol2 <<
  \tag #'violino1 {
    re'8. re'16 |
    \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
    \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
    mib'8. re'16 fa'2 mib'4 |
    mib' re' r2 |
    r4 mi' mi' mi' |
    mi'( fa') r2 |
    r4 re' re' re' |
    re'( mib') r sol'~ |
    sol' fa'8. sol'16 lab'4 lab' |
    re' r r2 |
    r4 re' re' re' |
    mib' r r2 |
    r4 mib' mib' mib' |
    fa' r r2 |
    r4 fa' fa' fa' |
    mib' mib' mib' mib' |
    mib' mib' mib' mib' |
    lab'2. lab'4 |
    re'2
  }
  \tag #'violino2 {
    si8. si16 |
    \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
    \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
    do'8. si16 re'2 do'4 |
    do' si r2 |
    r4 sib! sib sib |
    sib( lab) r2 |
    r4 lab lab lab |
    lab( sol) r sib |
    do' do' do' do' |
    si r r2 |
    r4 si si si |
    do' r r2 |
    r4 do' do' do' |
    re' r r2 |
    r4 re' re' re' |
    do' do' do' do' |
    do' do' do' do' |
    do'2. do'4 |
    \appoggiatura do'4 si2
  }
>> r4 do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! <<
  \tag #'violino1 {
    sol'4 |
    lab'2 \appoggiatura mib'4 re'2\trill |
    do'8. do''16 do''8. do''16 do''8. do''16 do''8. do''16 |
    do''8.\pocof do''16 do''8. do''16 do''8. do''16 do''8. do''16 |
    si'2 r4 re'8.\p re'16 |
    \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
    \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
    mib'4 re'2 re'4 |
    re'2\fermata
  }
  \tag #'violino2 {
    do'4 |
    do'2 \appoggiatura do'4 si2\trill |
    do'8. fad'16 fad'8. fad'16 fad'8. fad'16 fad'8. fad'16 |
    fad'8.\pocof fad'16 fad'8. fad'16 fad'8. fad'16 fad'8. fad'16 |
    sol'2 r4 si8.\p si16 |
    \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
    \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
    do'4 do'2 do'4 |
    \appoggiatura do'4 si2\fermata
  }
>> r4 do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! <<
  \tag #'violino1 {
    sol'4-!\f |
    r lab'-! r la'-! |
    r sol'\p-! r fad'-! |
    sol'2
  }
  \tag #'violino2 {
    do'4-!\f |
    r do'-! r mib'-! |
    r mib'\p-! r mib'-! |
    mib'2
  }
>> r2\fermata |
r2 r4 fa''8.\f mi''16 |
fa''4 do''2 lab''8. fa''16 |
sol''4 do''2 <<
  \tag #'violino1 {
    sib''8. sol''16 |
    \appoggiatura sol''4 lab''4.. fa''16 \appoggiatura fa''4 sol''4.. mib''16 |
    \appoggiatura mib''4 fa''4.. re''16 \appoggiatura re''4 mib''4.. do''16 |
    si'8. la'16 sol'4
  }
  \tag #'violino2 {
    sol''8. mi''16 |
    \appoggiatura mi''4 fa''4.. re''16 \appoggiatura re''4 mib''4.. do''16 |
    \appoggiatura do''4 re''4.. si'16 \appoggiatura si'4 do''4.. la'16 |
    sol'2
  }
>> r4 do''8. do''16 |
do''4( \appoggiatura fa''16 mib''8 re''16 do'') do''4-! do''8.\p do''16 |
do''4( \appoggiatura fa''16 mib''8 re''16 do'') do''4-! do'''8.\f sol''16 |
sib''8*2/3([ lab'' sol'']) fa''([ mib'' re'']) do''4( re'')\trill |
do''4( \appoggiatura fa''16 mib''8 re''16 do'') do''4-! do''8. sol'16 |
sib'8*2/3([ lab' sol']) fa'([ mib' re']) do'4( re')\trill |
do'2 r |
