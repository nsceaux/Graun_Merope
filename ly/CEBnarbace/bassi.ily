\clef "bass" do'8. re'16 |
mib'4 mib'8. re'16 mib'4 do' |
r4 si8. la16 si4 sol |
do'2 lab! |
fad\p sol |
do'4\f fa fa fad |
sol4 sol, r <>\p <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    do'8. si16 |
    do'4 sol2 mib'8. do'16 |
    re'4 sol2 <<
      \tag #'(fagotto1 fagotti) {
        re'8. re'16 |
        \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
        \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
        mib'8.( re'16 fa'2) mib'4 |
        \appoggiatura fa'8 mib'4 re'
      }
      \tag #'(fagotto2 fagotti) {
        si8. si16 |
        \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
        \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
        do'8.( si16 re'2) do'4 |
        do' si
      }
    >> r4 do'8. si16 |
    do'4 sol2 mib'8. do'16 |
    re'4 sol2 r4 |
    R1 |
    r4 <<
      \tag #'(fagotto1 fagotti) {
        sib8.( do'16) sib8.( do'16) sib8.( do'16) | sib4
      }
      \tag #'(fagotto2 fagotti) {
        sol8.( lab16) sol8.( lab16) sol8.( lab16) | sol4
      }
    >> r4 r2 |
    r4 <<
      \tag #'(fagotto1 fagotti) {
        sib8.( do'16) sib8.( do'16) sib8.( do'16) | sib4
      }
      \tag #'(fagotto2 fagotti) {
        sol8.( lab16) sol8.( lab16) sol8.( lab16) | sol4
      }
    >> r4 r2 |
    r4 <<
      \tag #'(fagotto1 fagotti) { sib8.( do'16) sib4 sib | }
      \tag #'(fagotto2 fagotti) { sol8.( lab16) sol4 sol | }
    >>
    r4 <<
      \tag #'(fagotto1 fagotti) { do'8. re'16 do'4 do' | }
      \tag #'(fagotto2 fagotti) { lab8. sib16 lab4 la | }
    >>
    r4 <<
      \tag #'(fagotto1 fagotti) { re'8. mib'16 re'4 re' | }
      \tag #'(fagotto2 fagotti) { sib8. do'16 sib4 si | }
    >>
    R1*7 |
    r2 r4 do'8. re'16 |
    re'4( \appoggiatura fa'16 mib'8 re'16 do') sib4-! do'8. re'16 |
    re'4( \appoggiatura fa'16 mib'8 re'16 do') sib4 r |
    R1 |
    r4
  }
  \tag #'bassi {
    do8. re16 |
    mib4 mib8. re16 mib4 do |
    r4 si,8. la,16 si,4 sol, |
    do2 lab, |
    fad, sol, |
    do4 fa, fa, fad, |
    sol, sol, r4 do8. re16 |
    mib4 mib8. re16 mib4 do |
    r4 si,8. la,16 si,4 sol, |
    do4 do re re |
    mib r r2 |
    r4 mib re sib, |
    mib r r2 |
    r4 mib re sib, |
    mib,4 r r mib, |
    lab,2 r4 fa, |
    sib,2 r4 sol, |
    do do do do |
    sib, sib, sib, sib, |
    sib, sib, sib, sib, |
    sib, sib, sib, sib, |
    mib r r mib |
    do r r do |
    fa, fa, fa, fa, |
    sib,2 r4 lab |
    sol2. lab4 |
    sol2. re4 |
    mib lab sib sib, |
    mib
  }
>> mib8.\f fa16 sol4 lab |
sol2. lab4\p |
sol2. re4\f |
mib4 lab sib sib, |
mib <<
  \tag #'(fagotto1 fagotto2 fagotti) {
    r4 r do'8. si16 |
    do'4 sol2 mib'8. do'16 |
    re'4 sol2 <<
      \tag #'(fagotto1 fagotti) {
        re'8. re'16 |
        \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
        \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
        mib'8.( re'16 fa'2) mib'4 |
        mib' re'
      }
      \tag #'(fagotto2 fagotti) {
        si8. si16 |
        \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
        \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
        do'8.( si16 re'2) do'4 |
        do' si
      }
    >> r2 |
    r4 <<
      \tag #'(fagotto1 fagotti) { sib2 sib4~ | sib4 lab8. sib16 do'4 }
      \tag #'(fagotto2 fagotti) { sol2 sol4~ | sol fa8. sol16 lab4 }
    >> r4 |
    r <<
      \tag #'(fagotto1 fagotti) { lab2 lab4~ | lab sol8. lab16 sib4 }
      \tag #'(fagotto2 fagotti) { fa2 fa4~ | fa mib8. fa16 sol4 }
    >> r4 |
    R1 |
    r4 <<
      \tag #'(fagotto1 fagotti) {
        re'8.( mib'16) re'8.( mib'16) re'8.( mib'16) | re'4
      }
      \tag #'(fagotto2 fagotti) {
        si8.( do'16) si8.( do'16) si8.( do'16) | si4
      }
    >> r4 r2 |
    r4 <<
      \tag #'(fagotto1 fagotti) {
        mib'8.( fa'16) mib'8.( fa'16) mib'8.( fa'16) | mib'4
      }
      \tag #'(fagotto2 fagotti) {
        do'8.( re'16) do'8.( re'16) do'8.( re'16) | do'4
      }
    >> r4 r2 |
    r4 <<
      \tag #'(fagotto1 fagotti) {
        fa'8.( sol'16) fa'8.( sol'16) fa'8.( sol'16) | fa'4
      }
      \tag #'(fagotto2 fagotti) {
        re'8.( mib'16) re'8.( mib'16) re'8.( mib'16) | re'4
      }
    >> r4 r2 |
    R1*3 |
    r2 r4 do'8. do'16 |
    do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! do'8. do'16 |
    do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! r |
    R1*3 |
    r2 r4 <<
      \tag #'(fagotto1 fagotti) {
        re'8. re'16 |
        \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
        \appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
        mib'4 re'2 re'4 |
        re'2\fermata
      }
      \tag #'(fagotto2 fagotti) {
        si8. si16 |
        \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
        \appoggiatura si4 do'4.. si16 \appoggiatura si4 do'4.. si16 |
        do'4 do'2 do'4 |
        \appoggiatura do'4 si2\fermata
      }
    >> r4 do'8. do'16 |
    do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! do'8. do'16 |
    do'4( \appoggiatura fa'16 mib'8 re'16 do') do'4-! r |
    R1*2 |
    r2 r\fermata |
    r4
  }
  \tag #'bassi {
    sol8.\p fa16 mib4 mib8. re16 |
    do4 do8. re16 mib4 do |
    r4 si,8. la,16 si,4 sol, |
    do2 lab, |
    fad, sol, |
    do4 fa, fa, fad, |
    r8. sol,16 si,8. re16 sol4 r |
    do4 do do do |
    fa, fa, fa, fa, |
    sib, sib, sib, sib, |
    mib, mib, mib, mib, |
    lab, lab, fa, fa, |
    sol, sol, sol, sol, |
    sol, sol, sol, sol, |
    sol, sol, sol, sol, |
    sol, sol, sol, sol, |
    sol, sol, sol, sol, |
    sol, sol, sol, sol, |
    do r r sib, |
    lab, r r sol, |
    fa, fa, fa, fa, |
    sol,2 r4 mib |
    fa2. mib4 |
    fa2. mib4 |
    fa fa sol sol, |
    lab, lab, lab, lab, |
    lab,8.\pocof lab,16 lab,8. lab,16 lab,8. lab,16 lab,8. lab,16 |
    sol,2 r4 sol,\p |
    do2 lab, |
    fad, sol, |
    do4 fa, fa, fad, |
    sol,2\fermata r4 mib |
    fa2. mib4 |
    fa2. mib4-!\f |
    fa4 r fad r |
    sol\p r lab! r |
    sol1\fermata |
    do4
  }
>> do'8.\f sib16 lab4 lab8. sol16 |
fa4 fa8. sol16 lab4 fa |
r4 mi8. re16 mi4 do |
fa2 fa |
fa fad |
sol4 sol8. fa16 mib4 mib |
fa2. mib4\p |
fa2. mib4\f |
fa4 fa sol sol |
lab2. mib4 |
fa fa sol sol, |
do2 r |
