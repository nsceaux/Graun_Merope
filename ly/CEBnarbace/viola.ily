\clef "alto" mib'8. fa'16 |
sol'4 sol'8. fa'16 sol'4 sol' |
r re'8. do'16 re'4 sol' |
do'2 lab'! |
fad'\p sol' |
sol'4\f lab' lab' la' |
sol' sol r mib8.-\sug\p fa16 |
sol4 sol8. fa16 sol4 sol |
r re8. do16 re4 sol |
do2 lab! |
fad sol |
sol4 lab2 la4 |
sol2 r |
r4 mib8. re16 mib4 sol |
r re8. do16 re4 sol |
do do re re |
mib r r2 |
r4 sib sib sib |
sib r r2 |
r4 mib' re' re' |
mib' r r mib |
lab2 r4 fa |
sib2 r4 sol |
do' do' do' do' |
sib sib sib sib |
sib sib sib sib |
re' re' re' re' |
mib' r r mib' |
do' r r do' |
do'2. do'4 |
do'( sib) r do' |
sib2. do'4 |
sib2. sib4 |
sib lab sib sib |
mib4 sol8.-\sug\f lab16 sib4 lab |
sib2. lab4\p |
sib2. sib4\f |
sib lab sib sib |
mib2 r4 sol8. fa16 |
mib4 mib8. fa16 sol4 sol |
r4 re8. do16 re4 sol |
do2 lab |
fad sol |
sol4 lab2 la4 |
sol2 r |
r4 sol sol sol |
sol( fa) r2 |
r4 fa fa fa |
fa( mib) r mib |
lab lab fa fa |
sol r r2 |
r4 sol sol sol |
sol r r2 |
r4 sol sol sol |
si r r2 |
r4 si si si |
do' r r sib! |
lab r r sol |
fa fa fa fa |
sol2 r4 sol |
lab2. sol4 |
lab2. mib4 |
fa fa sol sol |
lab lab lab lab |
lab8.\pocof lab16 lab8. lab16 lab8. lab16 lab8. lab16 |
sol2 r4 sol\p |
do2 lab |
fad sol |
sol4 lab lab la |
sol2\fermata r4 sol |
lab2. sol4 |
lab2. mib4-\sug\f |
r4 fa-! r do'-! |
r do'-! r do'-! |
do'2 r\fermata |
r2 r4 do''8. sib'16 |
lab'4 lab'8. sib'16 do''4 do'' |
r4 sol'8. fa'16 sol'4 do' |
fa'2 fa' |
fa' fad' |
sol'4 sol'8. fa'16 mib'4 sol' |
lab'2. sol'4\p |
lab'2. sol'4\f |
do' lab' mib' si\trill |
do'2. do'4 |
do' do' \appoggiatura do' si2\trill |
do'2 r |
