\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Fagotti" } <<
        \global \keepWithTag #'fagotti \includeNotes "bassi"
      >>
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = "Narbace" } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basso" } <<
      \global \keepWithTag #'bassi \includeNotes "bassi"
      \origLayout {
        s4 s1*5\pageBreak
        \grace s8 s1*4 s2 \bar "" \break \grace s4 s2 s1*4 s2 \bar "" \pageBreak
        \grace s8 s2 s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
        s1*5\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1*5\pageBreak
        s1*4\break \grace s4 s1*5\pageBreak
        s1*6\break s1*6\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
