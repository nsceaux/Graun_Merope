\clef "tenor/G_8" \override Voice.Script.avoid-slur = #'inside
r4 |
R1*5 |
r2 r4 do'8. si16 |
do'4 sol2 mib'8. do'16 |
re'4 sol2 re'8. re'16 |
\appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
\appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
mib'8.[\melisma re'16] fa'2 mib'4 |
\appoggiatura fa'8 mib'4\melismaEnd re' r do'8. si16 |
do'4 sol2 mib'8. do'16 |
re'4 sol2 re'8. re'16 |
mib'4. re'16[ do'] \appoggiatura sib8 lab4. lab8 |
sol4\melisma sib8.[ do'16] sib8.[ do'16 sib8. do'16] |
sib8*2/3[ do' re'] mib'[ fa' sol'] fa'[ mib' re'] do'[ sib lab] |
sol4 sib8.[ do'16] sib8.[ do'16] sib8.[ do'16] |
sib8*2/3[ do' re'] mib'[ fa' sol'] fa'[ mib' re'] do'[ sib lab] |
sol4\melismaEnd r r mib'8. sib16 |
sib8.[ do'16] do'4 r fa'8. do'16 |
do'8.[ re'16] re'4 r sol'8. re'16 |
mib'4. re'16[ do'] lab'4 lab' |
lab'8*2/3[\melisma sol' fa'] fa'[ mib' re'] re'[ do' sib] \appoggiatura sib16 lab8*2/3[ sol lab] |
sol[ lab sib] sib[ do' re'] mib'[ sib mib'] sol'[ mib' sol'] |
lab'[ sol' fa'] fa'[ mib' re'] re'[ do' sib] \appoggiatura sib16 lab8*2/3[ sol lab] |
sol4 mib'8*2/3[ sol' mib'] mib'[-.( mib'-. mib'-.)] mib'[-.( mib'-. mib'-.)] |
mib'4 mib'8*2/3[ sol' fa'] mib'[-.( mib'-. mib'-.)] mib'[-.( mib'-. mib'-.)] |
mib'8*2/3[ sol' fa'] mib'[ re' do'] do'[ sib la] sol'[ fa' mib'] |
\appoggiatura mib'4 re'2\melismaEnd r4 do'8. re'16 |
re'4( \appoggiatura fa'16 mib'8[ re'16 do']) sib4-! do'8. re'16 |
re'4( \appoggiatura fa'16 mib'8[ re'16 do']) sib4-! fa'8. lab16 |
sol8*2/3[ mib' re'] do'[ lab' fa'] \appoggiatura mib'8 re'4. mib'8 |
mib'2 r |
R1*3 |
r2 r4 do'8. si16 |
do'4 sol2 mib'8. do'16 |
re'4 sol2 re'8. re'16 |
\appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
\appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
mib'8.[\melisma re'16] fa'2 mib'4 |
\appoggiatura fa'8 mib'4\melismaEnd re'4 r sol'8. fa'16 |
\appoggiatura fa'4 mi'4. reb'8 \appoggiatura reb'4 do'4. sib8 |
sib4 lab r fa'8. mib'16 |
\appoggiatura mib'4 re'4.. do'16 \appoggiatura do'4 sib4.. lab16 |
lab4 sol r mib'8. sib16 |
do'4.. do'16 lab'4.. do'16 |
si4\melisma re'8.([ mib'16]) re'8.[( mib'16) re'8.( mib'16)] |
re'4 sol8*2/3[ lab si] si[ do' re'] fa'[ mib' re'] |
do'4 mib'8.([ fa'16]) mib'8.[( fa'16) mib'8.( fa'16)] |
mib'4 sol8*2/3[ lab si] do'[ re' mib'] sol'[ fa' mib'] |
re'4 fa'8.([ sol'16]) fa'8.([ sol'16]) fa'8.([ sol'16]) |
fa'4 sol8*2/3[ si do'] re'[ mib' fa'] lab'[ sol' fa'] |
mib'4 do'8*2/3[ mib' re'] do'-.([ do'-. do'-.]) do'-.([ do'-. do'-.]) |
do'4 do'8*2/3[ mib' re'] do'-.([ do'-. do'-.]) do'-.([ do'-. do'-.]) |
do'[ re' mib'] fa'[ sol' lab'] lab'[ sol' fa'] mib'[ re' do'] |
\appoggiatura do'4 si2\melismaEnd r4 do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8[ re'16 do']) do'4-! do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8[ re'16 do']) do'4-! sol'8. do'16 |
lab'8*2/3[ sol' fa'] mib'[ re' do'] \appoggiatura do'4 si4.\trill do'8 |
do'2 r4 do'8. do'16 |
fad'4.. fad'16 fad'4.. fad'16 |
sol'4 sol r re'8. re'16 |
\appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
\appoggiatura re'4 mib'4.. re'16 \appoggiatura re'4 mib'4.. re'16 |
mib'8.[\melisma re'16] lab'2 do'4 |
\appoggiatura do'4 si8.[\fermata la16]\melismaEnd sol4 r do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8[ re'16 do']) do'4-! do'8. do'16 |
do'4( \appoggiatura fa'16 mib'8[ re'16 do']) do'4-! sol'8. do'16 |
lab'4 do' r do' |
do'1~ |
do'2\fermata\melisma re'4.\melismaEnd do'8 |
do'2 r |
R1*11 |
