\clef "bass" \override Voice.Script.avoid-slur = #'inside
sol8 |
do' mib' lab do' mib sol do mib |
lab, do'16 si do'8 fad sol sol, r4 |
do'8\p mib' lab do' mib sol do mib |
fa re sol sol, lab\pocof do' mib do' |
fa re sol sol, do r r4 |
r8 sol\f mib do sol, r r4 |
r8 sol mib do sol, r r4 |
do'8\p mib' lab do' mib sol do mib |
fa re sol sol, lab\pocof do' mib do' |
fa re sol sol, do8. do'16\f sib lab sol fa |
mib8.-! lab16 sol fa mib re do4 r |
mib8\p mib mib mib mib mib mib sib, |
mib, mib mib mib mib mib mib sib, |
mib mib mib sib, mib4 lab |
sib r\fermata mib'8\p sol' do' mib' |
sol sib mib sol lab fa sib sib, |
do\pocof do' sol, sol lab, lab sib, sib |
mib8.-! mib'16\f re' do' sib lab sol8.-! do'16 sib lab sol fa |
mib4 r do16-.( do-. do-. do-.) do-.( do-. do-. do-.) |
fa( fa fa fa) fa( fa fa mib) re( re re re) re( re re re) |
sol( sol sol sol) sol( sol sol sol) sol( sol sol sol) sol( sol sol sol) |
sol4_\markup\italic ten. fad sol r\fermata |
do'8 mib' lab do' mib sol do mib |
fa re sol sol, lab\pocof do' mib do' |
fa re sol sol, do r r4 |
r8 sol\f mib do sol, sol\p mib do |
sol, sol-\sug\f mib do sol, sol-\sug\p mib do |
sol, sol\f mib do sol, r r4 |
do'8\p mib' lab do' mib sol do mib |
fa re sol sol, lab\pocof lab lab lab |
lab lab lab lab sol sol sol sol |
sol sol sol sol lab\f lab lab lab |
lab lab lab lab sol sol sol sol |
sol sol sol sol do8.\ff do'16 sib lab sol fa |
mib8.-! lab16 sol fa mib re do8 lab, lab, lab, |
lab, lab, lab, lab, sol, sol, sol, sol, |
sol, sol, sol, sol, do do'16 sib lab8 sol |
fa mib re sol do do16 sib, lab,8 sol, |
fa, mib, re, sol, do,8. sol,16 la, si, do re |
mib8. lab16 sol fa mib re do8.-! sol,16 la, si, do re |
mib8.-! lab16 sol fa mib re do8 do' sol mib |
do do' sol mib do do16 do do8 do |
do4 r r2 |
