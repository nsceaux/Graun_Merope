\clef "soprano/treble" r8 |
R1 |
r2 r4 r8 sol' |
do'' do'' do'' do'' do''16[ si'] do''8 r do'' |
re''8. fa''16 mib''[ re''] do''[ si'] do''4 r8 do'' |
re''8. lab''16 sol''[ fa''] mib''[ re''] do''8 sol'' mib'' do'' |
sol'' sol' r4 r8 sol'' mib'' do'' |
sol'' sol' r4 r r8 sol' |
do'' do'' do'' do'' do''16[ si'] do''8 r do'' |
re''8. fa''16 mib''[ re''] do''[ si'] do''4 r8 do'' |
re''8. lab''16 sol''[ fa''] mib''[ re''] do''4 r |
R1 |
sib'8 do''16. re''32 \appoggiatura re''8 mib''8. sib'16 \appoggiatura sib'8 do'' sib' r4 |
sib'8 do''16. re''32 re''16([ mib'' sol''16.]) sib'32 \appoggiatura sib'16 do''8 sib' r sib'16. sib'32 |
sib'2~ sib'8 sol'' \appoggiatura sol'' fa''8. mib''16 |
\appoggiatura mib''8 re''16.[ do''32] sib'8 r\fermata sib' mib'' mib'' mib'' mib'' |
mib''16[ re''] mib''8 r mib'' fa''8. lab''16 sol''[ fa''] mib''[ re''] |
mib''4 r8 mib'' fa'8. do''16 sib'[ lab'] sol'[ fa'] |
mib'4 r r2 |
r2 sib'8 do''16. reb''32 reb''8[ do''16.] sib'32 |
\appoggiatura sib'8*1/2 lab'16.[ sib'32] do''8 r4 do''8 re''16. mib''32 mib''8[ re''16.] do''32 |
\appoggiatura do''8 si'16.[ do''32] re''8 r mib''16 \appoggiatura re'' do'' sol'4 r8 mib''16 \appoggiatura re'' do'' |
si'[( lab'' sol'']) fa'' \appoggiatura fa''8 mib''4 re'' r8\fermata sol' |
do'' do'' do'' do'' do''16[ si'] do''8 r do'' |
re''8. fa''16 mib''[ re''] do''[ si'] do''4 r8 do'' |
re''8. lab''16 sol''[ fa''] mib''[ re''] do''8 sol'' mib'' do'' |
sol''8 sol' r sol'' sol'' sol' r sol'' |
sol'' sol' r sol'' sol''4 sol'' |
sol''8 sol' r4 r r8 sol' |
do'' do'' do'' do'' do''16[ si'] do''8 r do'' |
re''8. fa''16 mib''[ re''] do''[ si'] do''4 r |
fad''2 sol''8. sol'16 sol'8 lab''! |
sol'' fa'' mib'' re'' do''4 r |
fad''2 sol''8. sol'16 sol'8 lab'' |
sol'' fa'' mib'' re'' do''4 r |
R1*9 |
