\clef "alto" sol'8 |
do'' mib'' lab' do'' mib' sol' do' mib' |
lab do''16 si' do''8 fad' sol' sol r4 |
do''8\p mib'' lab' do'' mib' sol' do' mib' |
lab' r sol' r lab'\pocof do'' mib' do'' |
lab' r sol' r do' r r4 |
r8 sol'16 sol' mib' mib' do' do' sol8 r r4 |
r8 sol'16 sol' mib' mib' do' do' sol8 r r4 |
do''8\p mib'' lab' do'' mib' sol' do' mib' |
lab' r sol' r lab'\pocof do'' mib' do'' |
lab' r sol' r do'8. do''16\f sib' lab' sol' fa' |
mib'8. lab'16 sol' fa' mib' re' do'4 r |
R1*3 |
r4 r8\fermata r mib' sol' do' mib' |
sol sib mib sib' do'' r sib' r |
do'' r sib r do' r sib r |
mib8.-! mib''16\f re'' do'' sib' lab' sol'8.-! do''16 sib' lab' sol' fa' |
mib'4-! r r2 |
R1*2 |
r2 r4 r8\fermata r |
do''8 mib'' lab' do'' mib' sol' do' mib' |
lab' r sol' r lab'\pocof do'' mib' do'' |
lab' r sol' r do' r r4 |
r8 sol'16-\sug\f sol' mib' mib' do' do' sol8 sol'-\sug\p mib' do' |
sol sol'16-\sug\f sol' mib' mib' do' do' sol8 sol'-\sug\p mib' do' |
sol sol'16-\sug\f sol' mib' mib' do' do' sol8 r r4 |
do''8\p mib'' lab' do'' mib' sol' do' mib' |
lab' r sol' r do' do' do' do' |
do' do' do' do' sol' sol' sol' sol' |
sol' sol' sol' sol' do'-\sug\f do' do' do' |
do' do' do' do' sol' sol' sol' sol' |
sol' sol' sol' sol' do'8.\ff do''16 sib' lab' sol' fa' |
mib'8.-! lab'16 sol' fa' mib' re' do'8 lab lab lab |
lab lab lab lab sol re' re' re' |
re' re' re' re' do' do''16 sib' lab'8 sol' |
fa' mib' re' sol' do' do'16 sib lab8 sol |
fa mib re sol do8. sol16 la si do' re' |
mib'8. lab'16 sol' fa' mib' re' do'8.-! sol16 la si do' re' |
mib'8.-! lab'16 sol' fa' mib' re' do'8 do''16 do'' sol' sol' mib' mib' |
do'8 do''16 do'' sol' sol' mib' mib' do'8 do'16 do' do'8 do' |
do'4 r r2 |

