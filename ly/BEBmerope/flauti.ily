\clef "treble"
r8 |
R1*11 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sib'8 do''16. re''32 \appoggiatura re''8 mib''8. sib'16
    \appoggiatura sib'8 do'' sib'16. do''32 sib'16 sol' lab' fa' |
    sol'16. sib'32 do''16. re''32 re''16( mib'' sol''16.) sib'32
    \appoggiatura sib'16 do''8 sib'16. do''32 sib'16( sol') lab'( fa') |
    sol'8 sib'16. do''32 sib'16( sol') lab'( fa')
    sol'8 sol'' \appoggiatura sol'' fa''8. mib''16 |
    \appoggiatura mib''8 re''4 }
  { sol'8 lab'16. fa'32 \appoggiatura fa'8 sol'8. sol'16
    \appoggiatura sol'8 lab' sol'16. lab'32 sol'16 mib' fa' re' |
    mib'16. sol'32 lab'16. fa'32 fa'16( sol' sib'16.) sol'32
    \appoggiatura sol'16 lab'8 sol'16. lab'32 sol'16( mib') fa'( re') |
    mib'8 sol'16. lab'32 sol'16( mib') fa'( re')
    mib'8 sib' \appoggiatura sib' lab'8. sol'16 |
    \appoggiatura sol'8 fa'4
  }
>> r8\fermata r r2 |
R1*3 |
r2 \twoVoices #'(flauto1 flauto2 flauti) <<
  { sib'8 do''16. reb''32 reb''8 do''16. sib'32 |
    \appoggiatura sib'16 lab'16. sib'32 do''8 }
  { mi'8 mi'~ mi'16 sib' lab'16. sol'32 |
    \appoggiatura sol'16 fa'16. sol'32 lab'8 }
>> r16 \twoVoices #'(flauto1 flauto2 flauti) <<
  { \override Voice.Script.avoid-slur = #'inside
    do''16-.( do''-. do''-.) do''8 re''16. mib''32 mib''8 re''16. do''32 |
    \appoggiatura do''8 si'16. do''32 re''16. mib''32 fa''16( re'') mib''( do'')
    si'16. do''32 re''16. mib''32 fa''16( re'') mib''( do'') |
    si'( lab'') sol''( fa'') \appoggiatura fa''8 mib''4 re'' }
  { \override Voice.Script.avoid-slur = #'inside
    lab'16-.( lab'-. lab'-.) fad'8 fad' fad'16 do'' si'16. la'32 |
    \appoggiatura la'8 sol'16. la'32 si'16. do''32 re''16( si'!) do''( la'!)
    sol'16. la'32 si'16. do''32 re''16( si') do''( la') |
    sol'( fa'') mib''( re'') \appoggiatura re''8 do''4 si'! }
>> r8\fermata r |
R1*21 |
