\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with {
        instrumentName = \markup\center-column {
          Flauti ed Oboi
        }
      } << \global \keepWithTag #'flauti \includeNotes "flauti" >>
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Merope
      \consists "Metronome_mark_engraver"
    }
    \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s8 s1*2 s2 \bar "" \break s2 s1*2\break s1*3\pageBreak
        s1*3 s2 \bar "" \break \grace s8 s2 s1*2\break \grace s8 s1*3\pageBreak
        s1*3\break \grace s8 s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break s2 s1*2 s2 \bar "" \break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}