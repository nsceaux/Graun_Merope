\key do \minor \time 4/4 \partial 8
\tempo "Vivace" \midiTempo#128 s8 s1*11
\tempo "Adagio" \midiTempo#64 s1*3 s4.
\tempo "Allegro" \midiTempo#128 s8 s2 s1*3 s2
\tempo "Adagio" \midiTempo#64 s2 s1*2 s2..
\tempo "Allegro" \midiTempo#128 s8 s1*21 \bar "|."
