\piecePartSpecs
#`((flauti #:instrument ,#{\markup\center-column { Flauti ed Oboi }#})
   (oboi #:instrument ,#{\markup\center-column { Flauti ed Oboi }#})
   (violini)
   (violino1)
   (violino2)
   (viola)
   (bassi)
   (silence #:on-the-fly-markup , #{ \markup\tacet#43 #}))
