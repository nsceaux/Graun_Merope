\clef "treble" sol'8 |
do''16 do'' mib'' mib'' lab' lab' do'' do'' mib' mib' sol' sol' do' do' mib' mib' |
lab8 do''16 si' do''8 fad' sol' sol r4 |
<>\p <<
  \tag #'violino1 {
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib''
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
    do'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
  }
  \tag #'violino2 {
    do''16 do'' do'' do'' do'' do'' do'' do''
    do''16 do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
    do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
  }
  { s1 s2 s\pocof }
>> do''8 sol''16\p sol'' mib'' mib'' do'' do'' |
sol''8 sol'16\f sol' mib' mib' do' do' sol8 sol''16\p sol'' mib'' mib'' do'' do'' |
sol''8 sol'16\f sol' mib' mib' do' do' sol8 r r4 |
<>\p <<
  \tag #'violino1 {
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib''
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
    do'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
  }
  \tag #'violino2 {
    do''16 do'' do'' do'' do'' do'' do'' do''
    do''16 do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
    do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
  }
  { s1 s2 s\pocof }
>> do''8. do''16\f sib' lab' sol' fa' |
mib'8. lab'16 sol' fa' mib' re' do'4 r |
R1*3 |
r4 r8\fermata r8 <>\p <<
  \tag #'violino1 {
    sol''16 sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol''
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    mib'' sol' sol' sol' sol' sol' sol' sol'
    fa' fa' fa' fa' fa' fa' fa' fa' |
  }
  \tag #'violino2 {
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib''
    mib'' mib'' mib'' mib'' re'' re'' re'' re'' |
    mib'' mib' mib' mib' mib' mib' mib' mib'
    mib' mib' mib' mib' re' re' re' re' |    
  }
  { s2 s1 s\pocof }
>>
mib'8.-! mib''16\f re'' do'' sib' lab' sol'8.-! do''16 sib' lab' sol' fa' |
mib'4 r r2 |
R1*2 |
r2 r4 r8\fermata r |
<>\p <<
  \tag #'violino1 {
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib''
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
    do'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
  }
  \tag #'violino2 {
    do''16 do'' do'' do'' do'' do'' do'' do''
    do''16 do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
    do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
  }
  { s1 s2 s\pocof }
>> do''8 sol''16\p sol'' mib'' mib'' do'' do'' |
sol''8 sol'16\f sol' mib' mib' do' do' sol8 sol''16\p sol'' mib'' mib'' do'' do'' |
sol''8 sol'16\f sol' mib' mib' do' do' sol8 sol''16\p sol'' mib'' mib'' do'' do'' |
sol''8 sol'16\f sol' mib' mib' do' do' sol8 r r4 |
<>\p <<
  \tag #'violino1 {
    mib''16 mib'' mib'' mib'' mib'' mib'' mib'' mib''
    mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
    do'' do''' do''' do''' do''' do''' do''' do''' |
    do''' do''' do''' do''' do''' do''' do''' do'''
    si'' si'' si'' si'' si'' si'' lab''! lab'' |
    sol'' sol'' fa'' fa'' mib'' mib'' re'' re''
    do'' do''' do''' do''' do''' do''' do''' do''' |
    do''' do''' do''' do''' do''' do''' do''' do'''
    si'' si'' si'' si'' si'' si'' lab''! lab'' |
    sol'' sol'' fa'' fa'' mib'' mib'' re'' re''
  }
  \tag #'violino2 {
    do''16 do'' do'' do'' do'' do'' do'' do''
    do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
    do'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad''
    sol'' sol'' sol'' sol'' sol'' sol'' fa''! fa'' |
    mib'' mib'' re'' re'' do'' do'' si' si'
    do'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad''
    sol'' sol'' sol'' sol'' sol'' sol'' fa''! fa'' |
    mib'' mib'' re'' re'' do'' do'' si' si'
  }
  { s1 s2 s\pocof s1 s2 s\f }
>> do''8.\ff do''16 sib' lab' sol' fa' |
mib'8. lab'16 sol' fa' mib' re' do'16 <<
  \tag #'violino1 {
    fad'16 fad' fad' fad' fad' fad' fad' |
    do'' fad' fad' fad' fad' fad' fad' fad'
    sol' fa' fa' fa' fa' fa' fa' fa' |
    re'' fa' fa' fa' fa' fa' fa' fa'
    mib' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re''
    do'' mib' mib' mib' mib' mib' mib' mib' |
    re' re' re' re' re' re' re' re'
  }
  \tag #'violino2 {
    do'16 do' do' do' do' do' do' |
    do' do' do' do' do' do' do' do'
    si si si si si si si si |
    si si si si si si si si
    do' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' si' si'
    do'' do' do' do' do' do' do' do' |
    do' do' do' do' si si si si
  }
>> do'8.-! sol16 la si do' re' |
mib'8.-! lab'16 sol' fa' mib' re' do'8.-! sol16 la si do' re' |
mib'8.-! lab'16 sol' fa' mib' re' do'8 do''16 do'' sol' sol' mib' mib' |
do'8 do''16 do'' sol' sol' mib' mib' do'8 do'16 do' do'8 do' |
do'4 r r2 |
