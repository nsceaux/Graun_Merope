\clef "treble" \tag #'violino2 \startHaraKiri
sol' la'8 si' |
do'' re'' mi''4 |
re'' sol''~ |
sol''16 si'' la'' sol'' fad'' mi'' re'' do'' |
si' la' sol'8 sol'4~ |
sol'16 si' la' sol' fad' mi' re' do' |
si la sol8 r4 |
si''16 sol'' fad'' sol'' re'' sol'' fad'' sol'' |
si' re'' do'' re'' sol' si' la' si' |
re'8 sol' si' re'' |
sol''16 la'' si''8 r4 |
la''16 fad'' mi'' fad'' re'' fad'' mi'' fad'' |
la' re'' dod'' re'' fad' la' sol' la' |
re'8 fad' la' re'' |
fad''16 sol'' la''8 r4 |
sol''16 mi'' re'' mi'' dod'' sol'' fad'' mi'' |
fad'' re'' dod'' re'' fad'' re'' la'' fad'' |
sol'' mi'' re'' mi'' dod'' sol'' fad'' mi'' |
fad'' mi'' re'' mi'' fad'' sol'' la'' si'' |
do''' si'' la'' sol'' fad'' mi'' re'' do'' |
si' la' sol' fad' mi' re' do' si |
sol' fad' mi' re' do' si la sol |
re'4 r8 si |
do'-! dod'-! re'-! red'-! |
mi' fad' sol' red' |
mi' fad' sol' sold' |
la' do''16 si' la' sol' fad' mi' |
re'8 la'16 sol' fad' mi' re' do' |
si8 re'' si' sol' |
mi''8.-! sol''16 fad'' mi'' re'' do'' |
si'8 re'' si' sol' |
mi'8. mi''16 re'' do'' si' la' |
sol'8 sol'16 re' si8 re'16 si |
sol4 r |
sol'4\p la'8 si' |
do'' re'' mi''4 |
re''4 \twoVoices #'(violino1 violino2 violini) <<
  { sol''4~ |
    sol''8 fad''16 mi'' re''8 do'' |
    si'16 la' sol'8 }
  { \stopHaraKiri r8 sol' | la'4 la' | sol' \startHaraKiri }
>> sol'4\f~ |
sol'16 si' la' sol' fad' mi' re' do' |
si la sol8 r4 |
sol'\p la'8 si' |
do'' re'' mi''4 |
re'' r8 re''' |
re''' re'' r re''' |
re''' re'' r4 |
<>\pocof <<
  \tag #'(violino1 violini) { do''4 si' | la'16 }
  \tag #'(violino2 violini) { \stopHaraKiri la'4 sol' | fad'16 \startHaraKiri }
  { s2 | <>\f }
>> re''16 dod'' re'' la' re'' sol' re'' |
fad' re'' dod'' re'' fad' re'' mi' re'' |
re'8 la'\p re'' fad'' |
la''4 do'' |
si'16\f sol' fad' sol' re' sol' do' sol' |
si sol' fad' sol' si sol' la sol' |
sol8 si'\p mi'' sol'' |
si''4 re'' |
dod''8 r r4 |
dod''16 re'' mi'' re'' dod'' si' la' sold' |
la'8 r <<
  \tag #'(violino1 violini) { mi''8 s | mi'' s mi'' s | fad'' }
  \tag #'(violino2 violini) {
    \stopHaraKiri dod''8 s | dod'' s dod'' s | re''
  }
  { s8 r | s r s r | }
>> r8 r4 |
re''16 mi'' fad'' mi'' fad'' re'' dod'' re'' |
la'8 r <<
  \tag #'(violino1 violini) { fad''8 s | fad'' s fad'' s | sol'' }
  \tag #'(violino2 violini) {
    re''8 s | re'' s re'' s | mi''
  }
  { s8 r | s r s r | }
>> r8 r4 |
mi''16 fad'' sol'' fad'' sol'' mi'' re'' mi'' |
la'8 r <<
  \tag #'(violino1 violini) {
    sol''8 s |
    sol'' s sol'' s |
    fad'' fad'' fad'' fad'' |
    la'' la'' la'' la'' |
    si'' si'' si'' si'' |
    si'' si'' si'' si'' |
    la'' la'' la'' la'' |
    la'' la'' la'' la'' |
    sol'' sol'' sol'' sol'' |
    sol'' sol'' sol'' sol'' |
    fad'' s la' s |
    re'' s fad'' s |
  }
  \tag #'(violino2 violini) {
    mi''8 s |
    mi'' s mi'' s |
    re'' re'' re'' re'' |
    fad'' fad'' fad'' fad'' |
    re'' re'' re'' re'' |
    mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' |
    re'' re'' re'' re'' |
    re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' |
    re'' s fad' s |
    la' s re'' s |
  }
  { s8 r | s r s r | s2*8 | s8 r s r | s r s r | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { la''8 r16. la''32 la''8.\trill sol''32 la'' | }
  { r8 re'' re'' re'' | }
>>
<<
  \tag #'(violino1 violini) { si''8 s si'' s | }
  \tag #'(violino2 violini) { re''8 s mi'' s | \startHaraKiri }
  { s8 r s r | }
>>
<la mi' dod'' la''>4\f r8\fermata fad' |
sol' sold' la' lad' |
si' dod'' re'' lad' |
si' dod'' re'' red'' |
mi'' r r4 |
<>\f <<
  \tag #'(violino1 violini) {
    dod'''4-! re'''-! |
    s8 si'' la'' sol'' |
    fad''16 fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    s8 si'' la'' sol'' |
    fad''4-! mi''-! |
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri sol''4-! fad''-! |
    s8 sol'' fad'' mi'' |
    re''16 re'' re'' re'' re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    s8 sol'' fad'' mi'' |
    re''4-! dod''-! | \startHaraKiri
  }
  { s2 | r8 s4.\p | s2*2 | re''8 s4.\pocof | }
>>
fad''16\f re'' dod'' re'' la' re'' dod'' re'' |
fad' la' sol' la' re' fad' mi' fad' |
la8 re' fad' la' |
re''16 mi'' fad''8 r4 |
mi''16 dod'' si' dod'' la' dod'' si' dod'' |
mi' la' sold' la' dod' mi' re' mi' |
la8 dod' mi' la' |
mi''16 fad'' sol''8 r4 |
fad''16 mi'' re'' dod'' re'' dod'' si' la' |
si' la' sol' fad' sol' fad' mi' re' |
la'4 r8 fad' |
sol' sold' la' lad' |
si' dod'' re'' lad' |
si' dod'' re'' red'' |
mi'' <<
  \tag #'(violino1 violini) { si''8 la'' sol'' | }
  \tag #'(violino2 violini) { \stopHaraKiri sol''8 fad'' mi'' | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { fad''32 mi'' re''8. mi''4\trill | }
  { re''4 dod'' | }
>>
re''8 re''16 la' fad'8 la'16 fad' |
re'4 r |
la'4\p si'8 do''! |
re'' mi'' fa''4 |
mi'' \twoVoices #'(violino1 violino2 violini) <<
  { la''4~ |
    la''8 sol''16 fa'' mi''8 re'' |
    do''16 si' la'8 }
  { r8 la' |
    si'4 sold' |
    la' \startHaraKiri }
>> la'4\f~ |
la'16 do'' si' la' sold' fad' mi' re' |
do' si la8 r4 |
sol'4\p la'8 si' |
do'' re'' mi''4 |
re'' r8 re''' |
re''' re'' r re''' |
re''' re'' r4 |
<>\pocof <<
  \tag #'(violino1 violini) { do''4 si' | la'16 }
  \tag #'(violino2 violini) { \stopHaraKiri la'4 sol' | fad'16 \startHaraKiri }
  { s2 | <>\f }
>> re''16 dod'' re'' la' re'' sol' re'' |
fad' re'' dod'' re'' fad' re'' mi' re'' |
re'8 la'\p[ re'' fad''] |
la''4 do'' |
si'16\f sol' fad' sol' re' sol' do' sol' |
si sol' fad' sol' si sol' la sol' |
sol8 sol' si' re'' |
sol''4 fa'' |
mi''8 r r4 |
mi''16 fa'' sol'' fa'' mi'' re'' do'' si' |
do''8 r <<
  \tag #'(violino1 violini) { sol''8 s | do''' s mi'' s | re'' }
  \tag #'(violino2 violini) {
    \stopHaraKiri mi''8 s | mi'' s do'' s | si'
  }
  { s8 r | s r s r }
>> r8 r4 |
re''16 sol'' fad''! mi'' re'' do'' si' la' |
sol'8 r <<
  \tag #'(violino1 violini) { sol''8 s | si'' s re'' s | do'' }
  \tag #'(violino2 violini) { re''8 s | sol'' s sol' s | fad' }
  { s8 r | s r s r }
>> r8 r4 |
do''16 re'' mi'' re'' do'' si' la' sol' |
fad'8 r <<
  \tag #'(violino1 violini) { fad''8 s | la'' s do'' }
  \tag #'(violino2 violini) { do''8 s | fad'' s fad' }
  { s8 r | s r }
>> r8 |
\twoVoices #'(violino1 violino2 violini) <<
  { si'8 sol'' r si' |
    do'' la'' r do'' |
    re'' si'' r re'' |
    mi'' do''' r mi'' | }
  { r8 si' re'' sol'' |
    r do'' fad'' la'' |
    r re'' sol'' si'' |
    r mi'' la'' do''' | \startHaraKiri }
>>
re''8\f do''16 si' la' sol' fad' mi' |
<re' la' fad''>4 r8\fermata si8 |
do' dod' re' red' |
mi' fad' sol' red' |
mi' fad' sol' sold' |
la' r r4 |
<>\f <<
  \tag #'(violino1 violini) {
    fad''4-! sol''-! |
    s8 mi'' re'' do'' |
    si'16 si' si' si' si' si' si' si' |
    la' la' la' la' la' la' la' la' |
    si'8
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri do''4-! si'-! |
    s8 do'' si' la' |
    sol'16 sol' sol' sol' sol' sol' sol' sol' |
    fad' fad' fad' fad' fad' fad' fad' fad' |
    sol'8 \startHaraKiri
  }
  { s2 | r8 s4.\p | }
>> sol''8\pocof mi'' si' |
do''4-! re''-! |
si''16\f sol'' fad'' sol'' re'' sol'' fad'' sol'' |
si' re'' do'' re'' sol' si' la' si' |
re'8 sol' si' re'' |
sol''16 la'' si''8 r4 |
la''16 fad'' mi'' fad'' re'' fad'' mi'' fad'' |
la' re'' dod'' re'' fad' la' sol' la' |
re'8 fad' la' re'' |
fad''16 sol'' la''8 r4 |
si''16 la'' sol'' fad'' sol'' fad'' mi'' re'' |
mi'' re'' do'' si' do'' si' la' sol' |
re''4 r8 si |
do' dod' re' red' |
mi' fad' sol' red' |
mi' fad' sol' sold' |
la'8 do''16 si' la' sol' fad' mi' |
re'8 la'16 sol' fad' mi' re' do' |
si8 re'' si' sol' |
mi''8.-! sol''16 fad'' mi'' re'' do'' |
si'8 re'' si' sol' |
mi'8.-! mi''16 re'' do'' si' la' |
sol'8 sol'16 re' si8 re'16 si |
sol4 r |
