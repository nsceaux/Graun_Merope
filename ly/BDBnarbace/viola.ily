\clef "alto" sol' la'8 si' |
do'' re'' mi''4 |
re'' r8 re' |
mi'4 fad' |
sol' r8 re |
mi4 fad |
sol4 r |
re'8 re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
mi' mi' mi' dod' |
re' re' re' fad' |
mi' mi' mi' dod' |
re' re' re' re' |
fad' fad' fad' fad' |
sol'4 r |
sol'16 fad' mi' re' do' si la sol |
re'4 r8 si |
do'-! dod'-! re'-! red'-! |
mi' fad' sol' red' |
mi' fad' sol' sold' |
la' do''16 si' la' sol' fad' mi' |
re'8 la'16 sol' fad' mi' re' do' |
si4 r8 re' |
do' la re' fad' |
sol'4 r8 re' |
do' la fad fad' |
sol' sol'16 re' si8 re'16 si |
sol4 r |
sol4\p la8 si |
do' re' mi'4 |
re'4 r8 re' |
mi'4 fad' |
sol' r8 re\f |
mi4 fad |
sol r |
sol\p la8 si |
do' re' mi'4 |
re' r8 re'' |
re'' re' r re'' |
re'' re' r4 |
re'4\pocof re' |
re'16\f re'' dod'' re'' la' re'' sol' re'' |
fad' re'' dod'' re'' fad' re'' mi' re'' |
re'4 r |
r8 re'\p re' re' |
re'16\f sol' fad' sol' re' sol' do' sol' |
si sol' fad' sol' si sol' la sol' |
sol4 r |
r8 mi'-\sug\p mi' mi' |
mi' mi' mi' mi' |
mi' mi' mi' mi' |
dod' la' la' la' |
la' la' la' la' |
la' la' la' la' |
la' la' la' la' |
la' la' la' la' |
la' la' la' la' |
dod' dod' dod' dod' |
dod' dod' dod' dod' |
dod' dod' dod' dod' |
dod' dod' dod' dod' |
re' re' re' re' |
re' re' re' re' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
fad' fad' fad' fad' |
fad' fad' fad' fad' |
mi' mi' mi' mi' |
mi' mi' mi' mi' |
re' r re' r |
fad' r la' r |
re' re' fad' fad' |
sol' sol' sol' sol' |
la'4 r8\fermata fad' |
sol' sold' la' lad' |
si' dod'' re'' lad' |
si' dod'' re'' red'' |
mi'' r r4 |
mi''4-!\f re''-! |
sol'2\p |
la'8 la' la' la' |
la' la' la' la' |
si' mi'[\pocof fad' si'] |
la'4 la |
re'8\f re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
la la la la |
la la la la |
la la la la |
la la la la |
re'8 r re''16 dod'' si' la' |
si' la' sol' fad' sol' fad' mi' re' |
la'4 r8 fad' |
sol' sold' la' lad' |
si' dod'' re'' lad' |
si' dod'' re'' red'' |
mi'' mi' fad' si' |
la'4 la' |
fad'8 re''16 la' fad'8 la'16 fad' |
re'4 r |
la4\p si8 do'! |
re' mi' fa'4 |
mi' r8 mi' |
fa'4 sold' |
la' r8 mi'\f |
fa'4 si |
la r |
sol4\p la8 si |
do' re' mi'4 |
re' r8 re'' |
re'' re' r re'' |
re'' re' r4 |
re'4\pocof re' |
re'16 re'' dod'' re'' la' re'' sol' re'' |
fad' re'' dod'' re'' fad' re'' mi' re'' |
re'4 r |
r8 re'\p re' re' |
re'16\f sol' fad' sol' re' sol' do' sol' |
si sol' fad' sol' si sol' la sol' |
sol4 r |
r8 sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
la' la' la' la' |
la' la' la' la' |
la' la' la' la' |
la' la' la' la' |
la' la' la' la' |
fad' fad' la' la' |
sol' sol' re'' re'' |
sol' sol' do'' do'' |
re'' do''16 si' la' sol' fad' mi' |
re'4 r8\fermata si\p |
do' dod' re' red' |
mi' fad' sol' red' |
mi' fad' sol' sold' |
la' r r4 |
la'4-!\f sol'-! |
do'2\p |
re'8 re' re' re' |
re' re' re' re' |
sol sol'[\pocof mi' si] |
do'4 re' |
sol'8\f sol' sol' sol' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
re' re' re' re' |
sol'8 r sol'16 fad' mi' re' |
mi' re' do' si do' si la sol |
re'4 r8 si |
do' dod' re' red' |
mi' fad' sol' red' |
mi' fad' sol' sold' |
la' do''16 si' la' sol' fad' mi' |
re'8 la'16 sol' fad' mi' re' do' |
si4 r8 re' |
do' la re' fad' |
sol'4 r8 re' |
do' la fad fad' |
sol' sol'16 re' si8 re'16 si |
sol4 r |
