\clef "treble" \transposition sol
do''4 re''8 mi'' |
fa'' sol'' la''4 |
sol'' r8 <<
  \tag #'(corno1 corni) { sol''8 | fa''4 re'' | mi'' }
  \tag #'(corno2 corni) { do''8 | re''4 sol' | do'' }
>> r8 do'' |
re''4 sol' |
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { mi'4 }
>> r4 |
<<
  \tag #'(corno1 corni) {
    mi''4 mi''8 mi'' |
    mi''4 mi''8 mi'' |
    mi''4 mi'' |
  }
  \tag #'(corno2 corni) {
    do''4 do''8 do'' |
    do''4 do''8 do'' |
    do''4 do'' |
  }
>>
r8 <<
  \tag #'(corno1 corni) {
    mi''8 mi'' mi'' |
    re''4 re''8 re'' |
    re''4 re''8 re'' |
    re''4 re'' |
  }
  \tag #'(corno2 corni) {
    do''8 do'' do'' |
    sol'4 sol'8 sol' |
    sol'4 sol'8 sol' |
    sol'4 sol' |
  }
>>
r8 <<
  \tag #'(corno1 corni) { re''8 re'' re'' | }
  \tag #'(corno2 corni) { sol'8 sol' sol' | }
>>
do''4 re'' |
sol' r8 sol' |
do''4 re'' |
sol'8 <<
  \tag #'(corno1 corni) {
    re''8 re'' re'' |
    re'' re'' re'' re'' |
    mi''4
  }
  \tag #'(corno2 corni) {
    sol'8 sol' sol' |
    sol' sol' sol' sol' |
    do''4
  }
>> r4 |
<<
  \tag #'(corno1 corni) { mi''8 mi'' mi'' mi'' | re''4 }
  \tag #'(corno2 corni) { do''8 do'' do'' do'' | sol'4 }
>> r4 |
R2*5 |
r4 r8 <<
  \tag #'(corno1 corni) { mi''8 | re''4 re'' | mi'' }
  \tag #'(corno2 corni) { do''8 | do''4 sol' | do'' }
>> r8 <<
  \tag #'(corno1 corni) {
    mi''8 |
    re''4 re'' |
    do''8 sol'16 sol' sol'8 sol' |
    sol'4
  }
  \tag #'(corno2 corni) {
    do''8 |
    do''4 sol' |
    mi'8 mi'16 mi' mi'8 mi' |
    mi'4
  }
>> r4 |
R2*4 |
r4 r8 do'' |
re''4 sol' |
sol' r4 |
R2*2 |
r8 sol'\p sol'4 |
r8 sol' sol'4 |
R2 |
<>\pocof <<
  \tag #'(corno1 corni) { re''4 mi'' | re''8 }
  \tag #'(corno2 corni) { sol'4 do'' | sol'8 }
>> sol'8 re'' do'' |
sol' sol' re'' do'' |
sol'4 r |
R2 |
r8 do'' sol' sol' |
sol' do'' sol' sol' |
do'4 r |
R2 |
re''2 |
re'' |
re''8 r re'' r |
re'' r re'' r |
re''2 |
re'' |
re''8 r re'' r |
re'' r re'' r |
re''2 |
re'' |
re''8 r re'' r |
re'' r re'' r |
<<
  \tag #'(corno1 corni) {
    re''8 s re'' s |
    re'' s re'' s |
    mi''2 |
    mi''8 mi'' mi'' mi'' |
  }
  \tag #'(corno2 corni) {
    sol'8 s sol' s |
    sol' s sol' s |
    do''2 |
    do''8 do'' do'' do'' |
  }
  { s8 r s r | s r s r | }
>>
re''2~ |
re''8 <<
  \tag #'(corno1 corni) { re''8 re'' re'' | }
  \tag #'(corno2 corni) { sol'8 sol' sol' | }
>>
do''2~ |
do''8 do'' do'' do'' |
<<
  \tag #'(corno1 corni) {
    re''8 s re'' s |
    re'' s re'' s |
    s re'' re'' re'' |
    mi'' s mi'' s |
  }
  \tag #'(corno2 corni) {
    sol'8 s sol' s |
    sol' s sol' s |
    s sol' sol' sol' |
    do'' s do'' s |
  }
  { s8 r s r | s r s r | r s4. | s8 r s r | }
>>
re''4 r\fermata |
R2*4 | \allowPageTurn
re''4-!\f mi''-! |
R2*5 |
<<
  \tag #'(corno1 corni) {
    re''4 re''8 re'' |
    re''4 re''8 re'' |
    re''4 re'' |
  }
  \tag #'(corno2 corni) {
    sol'4 sol'8 sol' |
    sol'4 sol'8 sol' |
    sol'4 sol' |
  }
>>
r8 <<
  \tag #'(corno1 corni) { re''8 re'' re'' | }
  \tag #'(corno2 corni) { sol'8 sol' sol' | }
>>
re''4 re''8 re'' |
re''4 re''8 re'' |
re''4 re'' |
r8 re'' re'' re'' |
<<
  \tag #'(corno1 corni) { re''8 s4. | mi''8 s4. | }
  \tag #'(corno2 corni) { sol'8 s4. | do''8 s4. | }
  { s8 r r4 | s8 r r4 | }
>>
re''8 r r4 |
R2*3 |
r8 <<
  \tag #'(corno1 corni) { mi''8 re'' mi'' | }
  \tag #'(corno2 corni) { do''8 sol' do'' | }
>>
re''4 re'' |
sol'8 sol'16 sol' sol'8 sol' |
sol'4 r |
R2*9 |
r8 sol'\p sol'4 |
r8 sol' sol'4 |
R2 |
<>\pocof <<
  \tag #'(corno1 corni) { re''4 mi'' | re''8 }
  \tag #'(corno2 corni) { sol'4 do'' | sol'8 }
  { s2 | s8\f }
>> sol'8 re'' do'' |
sol' sol' re'' do'' |
sol'8 r8 r4 |
R2 |
r8 do''\f sol' sol' |
sol' do'' sol' sol' |
do'4 r |
R2 |
do''8 r r4 |
do''8 r r4 |
do''8 r do'' r |
do''8 r do'' r |
<<
  \tag #'(corno1 corni) {
    mi''2 |
    mi'' |
    mi''8 s mi'' s |
    mi'' s mi'' s |
  }
  \tag #'(corno2 corni) {
    do''2 |
    do'' |
    do''8 s do'' s |
    do'' s do'' s |
  }
  { s2*2 | s8 r s r | s r s r | }
>>
re''2 |
re'' |
re''8 r <<
  \tag #'(corno1 corni) {
    fa''8 s |
    fa'' s fa'' s |
    mi''4 s8 mi'' |
    fa''4 s8 fa'' |
    sol''4 s8 sol'' |
    la''4
  }
  \tag #'(corno2 corni) {
    re''8 s |
    re'' s re'' s |
    do''4 s8 do'' |
    re''4 s8 re'' |
    mi''4 s8 mi'' |
    fa''4
  }
  { s8 r | s r s r | s4 r8 s | s4 r8 s | s4 r8 s | }
>> r8 re'' |
<>\f <<
  \tag #'(corno1 corni) { re''8 }
  \tag #'(corno2 corni) { sol'8 }
>> sol'8 sol' sol' |
sol'4 r\fermata |
R2*4 |
<<
  \tag #'(corno1 corni) { re''4-! }
  \tag #'(corno2 corni) { sol'4-! }
>> do''4-! |
R2*5 |
<<
  \tag #'(corno1 corni) {
    mi''4 mi''8 mi'' |
    mi''4 mi''8 mi'' |
    mi''4 mi'' |
    s8 mi''8 mi'' mi'' |
    re''4 re''8 re'' |
    re''4 re''8 re'' |
    re''4 re'' |
    s8 re'' re'' re'' |
    mi'' s mi'' s |
    mi'' s mi'' s |
    re''4
  }
  \tag #'(corno2 corni) {
    do''4 do''8 do'' |
    do''4 do''8 do'' |
    do''4 do'' |
    s8 do''8 do'' do'' |
    sol'4 sol'8 sol' |
    sol'4 sol'8 sol' |
    sol'4 sol' |
    s8 sol' sol' sol' |
    do'' s do'' s |
    do'' s do'' s |
    sol'4
  }
  { s2*3 | r8 s4. | s2*3 | r8 s4. | s8 r s r | s r s r | }
>> r4 |
R2*5 |
r4 r8 <<
  \tag #'(corno1 corni) { mi''8 | re''4 re'' | mi'' }
  \tag #'(corno2 corni) { do''8 | do''4 sol' | do'' }
>> r8 <<
  \tag #'(corno1 corni) {
    mi''8 |
    re''4 re'' |
    do''8 sol'16 sol' sol'8 sol' |
    sol'4
  }
  \tag #'(corno2 corni) {
    do''8 |
    do''4 sol' |
    mi'8 mi'16 mi' mi'8 mi' |
    mi'4
  }
>> r4 |
