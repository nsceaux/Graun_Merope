\clef "tenor/G_8" R2*34 |
sol4 la8 si |
do' re' mi'4 |
re' sol'~ |
sol'8 fad'16 mi' re'8 do' |
si16[ la] sol8 r4 |
R2*2 |
sol4 la8 si |
do' re' mi'4 |
re'8 re' re' re |
r re' re' re |
r la' fad' re' |
do'4 si\trill |
la r |
R2 |
r8 la re' fad' |
la'4 do' |
si16[ la] sol8 r4 |
R2 |
r8 si mi' sol' |
si'4 re' |
dod'16[\melisma re' mi' re'] dod'[ si la sold] |
la2 |
dod'16[ re' mi' re'] mi'[ dod' si la] |
dod'[ re' mi' re'] mi'[ dod' si la] |
re'[ mi' fad' mi'] fad'[ re' dod' re'] |
la2 |
re'16[ mi' fad' mi'] fad'[ re' dod' re'] |
la[ re' dod' re'] fad'[ re' dod' re'] |
mi'[ fad' sol' fad'] sol'[ mi' re' mi'] |
la2 |
mi'16[ fad' sol' fad'] sol'[ mi' re' dod'] |
re'[ fad' sol' fad'] sol'[ mi' re' dod'] |
fad'[ mi' re' dod'] re'[ dod' si la] |
fad'[ mi' re' dod'] re'[ dod' si la] |
si8[ dod'16 re'] mi'[ fad' sol' la'] |
si'[ la' sol' fad'] mi'[ re' dod' si] |
la8[ si16 dod'] re'[ mi' fad' sol'] |
la'[ sol' fad' mi'] re'[ dod' si la] |
sol8[ la16 si] dod'[ re' mi' fad'] |
sol'[ fad' mi' re'] dod'[ si la sol] |
fad8 r16. la32 la8.[\trill sol32 la] |
re'8 r16. fad'32 fad'8.[\trill mi'32 fad'] |
la'4~ la'8[ sol'16 la'] |
si'16[ la' sol' fad'] mi'[ re' dod' si] |
la4\fermata\melismaEnd r8 fad |
sol sold la lad |
si[ dod'] re' lad |
si dod' re' red' |
mi' r sol'4 |
dod' re' |
r8 sol' fad' mi' |
la2 |
mi'\trill |
re'8 si' la' sol' |
fad'16[ mi' re'8] mi'4 |
re' r |
R2*17 |
la4 si8 do'! |
re' mi' fa'4 |
mi' la'~ |
la'8 sol'16 fa' mi'8 re' |
do'16[ si] la8 r4 |
R2*2 |
sol4 la8 si |
do' re' mi'4 |
re'8 re' re' re |
r re' re' re |
r la' fad' re' |
do'4 si |
la r |
R2 |
r8 la re' fad' |
la'4 do' |
si16[ la] sol8 r4 |
R2 |
r8 sol si re' |
sol'4 fa' |
mi'16[\melisma fa' sol' fa'] mi'[ re' do' si] |
do'2 |
mi'16[ do' re' mi'] fa'[ sol' la' si'] |
do''[ si' la' sol'] fa'[ mi' re' do'] |
re'[ sol' fad'! mi'] re'[ do' si la] |
sol2 |
re'16[ si do' re'] mi'[ fad' sol' la'] |
si'[ la' sol' fad'] mi'[ re' do' si] |
do'[ re' mi' re'] do'[ si la sol] |
fad2 |
do'16[ la si do'] re'[ mi' fad' sol'] |
la'[ sol' fad' mi'] re'[ do' si la] |
si8[ sol'16 fad'] mi'[ re' do' si] |
do'8[ la'16 sol'] fad'[ mi' re' do'] |
re'8[ si'16 la'] sol'[ fad' mi' re'] |
mi'8[ do''16 si'] la'[ sol' fad' mi'] |
re'2\trill~ |
re'4\melismaEnd r8\fermata si |
do' dod' re' red' |
mi'[ fad'] sol' red' |
mi' fad' sol' sold' |
la' r do'4 |
fad' sol' |
r8 mi' re' do' |
si32([ la sol8.])~ sol4 |
la2\trill |
si8 sol' mi' si |
do'4 re' |
sol r |
R2*21 |
