\clef "bass" sol la8 si |
do' re' mi'4 |
re' r8 si |
do'4 re' |
sol r8 si, |
do4 re |
sol,4 r |
sol8 sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol sol sol sol |
fad fad fad fad |
fad fad fad fad |
fad fad fad fad |
fad fad fad fad |
mi mi la la |
re re re re |
mi mi la la |
re re re re |
re re re re |
sol4 r |
sol16 fad mi re do si, la, sol, |
re4 r8 si, |
do-! dod-! re-! red-! |
mi fad sol red |
mi fad sol sold |
la do'16 si la sol fad mi |
re8 la16 sol fad mi re do |
si,4 r8 si, |
do do re re |
sol4 r8 si, |
do do re re |
sol, sol, sol, sol, |
sol,4 r |
sol\p la8 si |
do' re' mi'4 |
re'4 r8 si |
do'4 re' |
sol r8 si,\f |
do4 re |
sol, r |
sol\p la8 si |
do' re' mi'4 |
re' r8 re' |
re' re r re' |
re' re r4 |
fad\pocof sol |
re8\f re' la sol |
fad re' fad mi |
re4 r |
r8 fad\p fad fad |
sol,\f sol re do |
si, sol si, la, |
sol,4 r |
r8 sold-\sug\p sold sold |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
la la la la |
re' re' re' re' |
re' re' re' re' |
sol sol sol sol |
sol sol sol sol |
fad fad fad fad |
fad fad fad fad |
mi mi mi mi |
mi mi mi mi |
re re re re |
re re re re |
fad fad fad fad |
sol sol sol sol |
la4\fermata r8 fad |
sol sold la lad |
si dod' re' lad |
si dod' re' red' |
mi' r r4 |
la4-!\f si-! |
sol2\p |
la8 la la la |
la la la la |
si dod[\pocof re sol] |
la4 la, |
re8\f re re re |
re re re re |
re re re re |
re re re re |
dod dod dod dod |
dod dod dod dod |
dod dod dod dod |
dod dod dod dod |
re8 r re'16 dod' si la |
si la sol fad sol fad mi re |
la4 r8 fad |
sol sold la lad |
si dod' re' lad |
si dod' re' red' |
mi' dod re sol |
la4 la, |
re8 re re re |
re4 r |
la4\p si8 do'! |
re' mi' fa'4 |
mi' r8 do' |
re'4 mi' |
la r8 do\f |
re4 mi |
la, r |
sol\p la8 si |
do' re' mi'4 |
re' r8 re' |
re' re r re' |
re' re r4 |
fad4\pocof sol |
re8\f re' la sol |
fad re' fad mi |
re4 r |
r8 re\p re re |
sol,\f sol re do |
si, sol si, la, |
sol,4 r |
r8 si,\p si, si, |
do do do do |
do do do do |
do' do' do' do' |
do' do' do' do' |
si si si si |
si si si si |
si si si si |
si si si si |
la la la la |
la la la la |
la la la la |
la la la la |
sol sol sol sol |
la la la la |
si si si si |
do' do' do' do' |
re' do'16 si la sol fad mi |
re4 r8\fermata si,\p |
do dod re red |
mi fad sol red |
mi fad sol sold |
la r r4 |
re\f mi |
do2\p |
re8 re re re |
re re re re |
sol, sol[\pocof mi si,] |
do4 re |
sol8\f sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol sol sol sol |
fad fad fad fad |
fad fad fad fad |
fad fad fad fad |
fad fad fad fad |
sol r sol16 fad mi re |
mi re do si, do si, la, sol, |
re4 r8 si, |
do dod re red |
mi fad sol red |
mi fad sol sold |
la do'16 si la sol fad mi |
re8 la16 sol fad mi re do |
si,4 r8 si, do do re re |
sol4 r8 si, |
do do re re |
sol, sol, sol, sol, |
sol,4 r |
