\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      %\new GrandStaff \with { instrumentName = "Corni" } <<
      %  \new Staff <<
      %    \keepWithTag #'corni \global
      %    \keepWithTag #'corno1 \includeNotes "corni"
      %  >>
      %  \new Staff <<
      %    \keepWithTag #'corni \global
      %    \keepWithTag #'corno2 \includeNotes "corni"
      %  >>
      %>>
      \new Staff \with { instrumentName = "Corni" } <<
        \keepWithTag #'corni \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      %\new GrandStaff \with { instrumentName = "Violini" } <<
      %  \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
      %  \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      %>>
      \new Staff \with { instrumentName = "Violini" } <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Narbace }
    \withLyrics <<
      \global \keepWithTag #'merope \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2*6\pageBreak
        s2*7\break s2*7\pageBreak
        s2*8\break s2*8\pageBreak
        s2*7\break s2*7\pageBreak
        s2*7\break s2*7\pageBreak
        s2*6 s4 \bar "" \break s4 s2*6\pageBreak
        s2*6 s4 \bar "" \break s4 s2*7\pageBreak
        s2*7 s4 \bar "" \break s4 s2*9\pageBreak
        s2*8\break s2*7\pageBreak
        s2*8\break s2*7\pageBreak
        s2*8\break s2*7\pageBreak
        s2*7\break s2*7\pageBreak
        s2*8\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
