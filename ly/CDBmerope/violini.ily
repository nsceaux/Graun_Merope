\clef "treble"
<<
  \tag #'violino1 {
    la' re''8. dod''16 re''8. mi''16 |
    fad''4 mi'' r8. re''16 |
    \appoggiatura re''4 mi''4.. dod''16 re''8. la'16 |
    \appoggiatura la'8 sol'4 fad' r |
  }
  \tag #'violino2 {
    fad'4 la'8. sol'16 la'8. dod''16 |
    re''4 dod'' r8. la'16 |
    si'4.. sol'16 la'8. fad'16 |
    mi'4 re' r |
  }
>>
re''16 la' si' dod'' re'' mi'' fad'' sol'' la'' do'' si' la' |
si' sol'' fad'' mi'' re'' do'' si' la' sol'4\trill |
mi''16 si' dod''! red'' mi'' fad'' sold'' la'' si'' re'' dod'' si' |
dod'' la'' sold'' fad'' mi'' re'' dod'' si' la'4\trill |
<<
  \tag #'violino1 {
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    si'8. si''16 la''8.\trill sol''16 fad''8.\trill mi''16 |
    fad''8.\p(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    si'8.\f si''16 la''8.\trill sol''16 fad''8.\trill mi''16 |
  }
  \tag #'violino2 {
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    si'8. sol''16 fad''8.\trill mi''16 re''8.\trill dod''16 |
    re''8.-\sug\p(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    si'8.-\sug\f sol''16 fad''8.\trill mi''16 re''8.\trill dod''16 |
  }
>>
re''4 re' r |
<>\p <<
  \tag #'violino1 {
    la'4 re''8. dod''16 re''8. mi''16 |
    fad''4 mi'' r8. re''16 |
    \appoggiatura re''4 mi''4.. dod''16 re''8. la'16 |
    \appoggiatura la'8 sol'4 fad' r |
  }
  \tag #'violino2 {
    fad'4 la'8. sol'16 la'8. dod''16 |
    re''4 dod'' r8. la'16 |
    si'4.. sol'16 la'8. fad'16 |
    mi'4 re' r |
  }
>>
<>\pocof <<
  \tag #'violino1 {
    la'4 re''8. dod''16 re''8. mi''16 |
    fad''4 mi'' r |
    mi''4\p si'8. si'16 dod''8. re''16 |
    dod''8.\f(\trill si'32 la') dod''8.(\trill si'32 la') la''4-! |
    mi''4\p si'8. si'16 dod''8. re''16 |
    dod''8.\f(\trill si'32 la') dod''8.(\trill si'32 la') la''4-! |
    fad''4..\p fad'16 sold'8. la'16 |
    \appoggiatura la'4 sold'2 r4 |
  }
  \tag #'violino2 {
    fad'4 la'8. sol'16 la'8. dod''16 |
    re''4 dod'' r |
    si'2.\p |
    la'8.\f(\trill sold'32 la') la'8.(\trill sold'32 la') dod''4-! |
    si'2.\p |
    la'8.\f(\trill sold'32 la') la'8.(\trill sold'32 la') dod''4-! |
    la'4..-\sug\p re'16 re'8. dod'16 |
    \appoggiatura dod'4 si2 r4 |
  }
>>
<<
  \tag #'violino1 {
    si'4 si'8*2/3( dod'' re'') dod''( re'' mi'') |
    mi''( re'' dod'') si'2 |
    si'4 si'8*2/3( dod'' re'') dod''( re'' mi'') |
    mi''( re'' dod'') si'2 |
  }
  \tag #'violino2 {
    sold'4 sold'8*2/3( la' si') la'( si' dod'') |
    dod''( si' la') sold'2 |
    sold'4 sold'8*2/3( la' si') la'( si' dod'') |
    dod''( si' la') sold'2 |
  }
>>
la'4\pocof dod''8. la'16 dod''8. mi''16 |
la''4 mi''2 |
la'4 dod''8. la'16 dod''8. mi''16 |
la''4 <>\p <<
  \tag #'violino1 {
    mi''4 mi'' |
    fad'' fad'' fad'' |
    mi'' mi'' mi'' |
    re'' re'' re'' |
    dod'' dod'' dod'' |
    fad'' fad'' fad'' |
    fad'' fad'' fad'' |
    mi'' mi'' mi'' |
    mi'' mi'' mi'' |
    re'' re'' re'' |
    re''2. |
    re'' |
  }
  \tag #'violino2 {
    la'4 la' |
    la' si' si' |
    si' la' la' |
    la' sold' sold' |
    la' la' la' |
    la' la' la' |
    sold' sold' sold' |
    sold' sold' sold' |
    fad' fad' fad' |
    fad' fad' fad' |
    si'2. |
    si' |
  }
>>
<<
  \tag #'violino1 {
    dod''8*2/3( re'' mi'') re''4 si'\trill |
    dod''8.\pocof(\trill si'32 la') dod''8.(\trill si'32 la') la''4-! |
    dod''8.(\trill si'32 la') dod''8.(\trill si'32 la') la''4-! |
    fad'8. fad''16 mi''8.\trill re''16 dod''8.\trill si'16 |
    la'2.\trill |
    mi''4\f la''8. sold''16 la''8. si''16 |
    dod'''4 si'' r8. la''16 |
    \appoggiatura la''4*1/2 si''4.. sold''16 la''8. mi''16 |
    \appoggiatura mi''8 re''4 dod'' r |
  }
  \tag #'violino2 {
    la'8*2/3( si' dod'') si'4 sold'\trill |
    la'8.\pocof(\trill sold'32 la') la'8.(\trill sold'32 la') dod''4-! |
    la'8.(\trill sold'32 la') la'8.(\trill sold'32 la') dod''4-! |
    fad'8. re''16 dod''8.\trill si'16 la'8.\trill sold'16 |
    la'2.\trill |
    dod''4-\sug\f mi''8. re''16 mi''8. sold''16 |
    la''4 sold'' r8. mi''16 |
    \appoggiatura mi''4*1/2 fad''4.. re''16 mi''8. dod''16 |
    si'4 la' r |
  }
>>
<>\p <<
  \tag #'violino1 {
    la'4 re''8. dod''16 re''8. mi''16 |
    fad''4 mi'' r8. re''16 |
    \appoggiatura re''4 mi''4.. dod''16 re''8. la'16 |
    \appoggiatura la'8 sol'4 fad' r |
    do''2. |
    si'8.\f(\trill la'32 sol') si'8.(\trill la'32 sol') sol''4-! |
    re''2.\p |
    dod''8.\f( si'32 la') dod''8.( si'32 la') la''4-! |
    re''4\p dod''8. re''16 mi''8. fad''16 |
    fad''4( mi''16)
  }
  \tag #'violino2 {
    fad'4 la'8. sol'16 la'8. dod''16 |
    re''4 dod'' r8. la'16 |
    \appoggiatura la'4 si'4.. sol'16 la'8. fad'16 |
    mi'4 re' r |
    la'2. |
    sol'8.\f(\trill fad'32 sol') sol'8.(\trill fad'32 sol') si'4-! |
    si'2.\p |
    la'8.\f(\trill sold'32 la') la'8.(\trill sold'32 la') dod''4-! |
    la'4\p sol'8. fad'16 la'8. re''16 |
    re''4( dod''16)
  }
>> la'16\f si' dod'' re'' mi'' fad'' sol'' |
la''4 <>\p <<
  \tag #'violino1 {
    la''4 la'' |
    si'' si'' si'' |
    si'' si'' si'' |
    la'' la'' la'' |
    la'' la'' la'' |
    sol'' sol'' sol'' |
    sol'' sol'' sol'' |
    fad''4 fad''2~ |
    fad''4 mi'' sol'' |
    fad'' fad''2~ |
    fad''4 mi'' sol'' |
    fad'' re''2 |
    \appoggiatura re''4 dod''2.\fermata |
  }
  \tag #'violino2 {
    re''4 re'' |
    re'' re'' re'' |
    re'' mi'' mi'' |
    mi'' mi'' mi'' |
    re'' re'' re'' |
    re'' re'' re'' |
    dod'' dod'' dod'' |
    re'' re''2~ |
    re''4 dod'' mi'' |
    re'' re''2~ |
    re''4 dod'' mi'' |
    re'' sold'2 |
    \appoggiatura sold'4 la'2.\fermata |
  }
>>
<>\pocof <<
  \tag #'violino1 {
    la'4 re''8. dod''16 re''8. mi''16 |
    fad''4 mi'' r |
    si''4\p si''( sol'') |
    fad'' mi''2\trill |
    fad''8.\pocof(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    si'8.\p si''16 la''8.\trill sol''16 fad''8.\trill mi''16 |
    fad''8.\f(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    si'8.\p si''16 la''8.\trill sol''16 fad''8.\trill mi''16 |
  }
  \tag #'violino2 {
    fad'4 la'8. sol'!16 la'8. dod''16 |
    re''4 dod'' r |
    re''4\p sol''( mi'') |
    re''4 dod''2\trill |
    re''8.\pocof(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    si'8.\p sol''16 fad''8.\trill mi''16 re''8.\trill dod''16 |
    re''8.\f(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    si'8.\p sol''16 fad''8.\trill mi''16 re''8.\trill dod''16 |
  }
>>
re''8. fad'16 sol'4 la' |
si'8. re''16 sol'4\pocof la' |
re'4\f~ re'8 mi'16 fad' sol' la' si' dod'' |
re'' la' si' dod'' re'' mi'' fad'' sol'' la'' do'' si' la' |
si' sol'' fad'' mi'' re'' do'' si' la' sol'4\trill |
mi''16 si' dod''! red'' mi'' fad'' sold'' la'' si'' re'' dod'' si' |
dod'' la'' sold'' fad'' mi'' re'' dod'' si' la'4\trill |
<<
  \tag #'violino1 {
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') re'''4-! |
    si'8. si''16 la''8.\trill sol''16 fad''8.\trill mi''16 |
  }
  \tag #'violino2 {
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') fad''4-! |
    si'8. sol''16 fad''8.\trill mi''16 re''8.\trill dod''16 |
  }
>>
re''4 re'\fermata r |
<>\p <<
  \tag #'violino1 {
    re''4. mi''8 re''4 |
    re''( sol'') re'' |
    \appoggiatura re''2 do''2. |
    si'4 r re'' |
    re''4( sol'') do'' |
    \appoggiatura do''2 si'2. |
    la'2
  }
  \tag #'violino2 {
    si'4. do''8 si'4 |
    si'2 si'4 |
    \appoggiatura si'2 la'2. |
    sol'4 r si' |
    si'2 la'4 |
    \appoggiatura la'2 sol'2. |
    fad'2
  }
>> r8.\fermata re'16 |
sol'4.. la'16 si'8. do''16 |
re''4 re' r8. sol'16\pocof |
do''4.. re''16 mi''8. fad''16 |
sol''4 sol' r |
<>\p <<
  \tag #'violino1 {
    si''8*2/3 si'' si'' si'' si'' si'' si'' si'' si'' |
    si'' si'' si'' si'' si'' si'' si'' si'' si'' |
    si''\f do''' do''' do''' do''' do''' do''' do''' do''' |
    dod'''!\p dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' |
    dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' |
    dod'''\f re''' re''' re''' re''' re''' re''' re''' re''' |
    re'''\p re''' re''' re''' re''' re''' re''' re''' re''' |
    re''' re''' re''' re''' re''' re''' re''' re''' re''' |
    do''' do''' do''' do''' do''' do''' do''' do''' do''' |
    do''' do''' do''' do''' do''' do''' do''' do''' do''' |
    si'' si'' si'' si'' si'' si'' si'' si'' si'' |
    si'' si'' si'' si'' si'' si'' si'' si'' si'' |
    la'' la'' la'' la'' la'' la'' la'' la'' la'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol'' sol'' sol'' la'' la'' la'' si'' si'' si'' |
    la''8. do''16 \appoggiatura si'4 la'2\trill |
  }
  \tag #'violino2 {
    sol''8*2/3 sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    fa''\f mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi''\p mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol''\f fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sol''\p sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re'' re'' re'' re'' re'' re'' re'' re'' re'' |
    re'' re'' re'' re'' re'' re'' re'' re'' re'' |
    do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    si' si' si' do'' do'' do'' re'' re'' re'' |
    mi'' mi'' mi'' fad'' fad'' fad'' sol'' sol'' sol'' |
    mi''8. la'16 \appoggiatura sol'4 fad'2\trill |
  }
>>
<>\f <<
  \tag #'violino1 {
    si'8.(\trill la'32 sol') si'8.(\trill la'32 sol') sol''4-! |
    si'8.(\trill la'32 sol') si'8.(\trill la'32 sol') sol''4-! |
    mi'8.\p mi''16 re''8.\trill do''16 si'8.\trill la'16 |
    si'8.\f(\trill la'32 sol') si'8.(\trill la'32 sol') sol''4-! |
    si'8.(\trill la'32 sol') si'8.(\trill la'32 sol') sol''4-! |
  }
  \tag #'violino2 {
    sol'8.(\trill fad'32 sol') sol'8.(\trill fad'32 sol') si'4-! |
    sol'8.(\trill fad'32 sol') sol'8.(\trill fad'32 sol') si'4-! |
    mi'8.\p do''16 si'8.\trill la'16 sol'8.\trill fad'16 |
    sol'8.\f(\trill fad'32 sol') sol'8.(\trill fad'32 sol') si'4-! |
    sol'8.(\trill fad'32 sol') sol'8.(\trill fad'32 sol') si'4-! |
  }
>>
mi''8.\p sol''16 do''8. mi''16 la'8. do''16 |
re'4 <<
  \tag #'violino1 { la'2\trill }
  \tag #'violino2 { fad'2\trill }
>>
sol'2 r4 |
re''16\f la' si' dod''! re'' mi'' fad'' sol'' la'' do'' si' la' |
si' sol'' fad'' mi'' re'' do'' si' la' sol'4\trill |
mi''16 si' dod''! red'' mi'' fad'' sold'' la'' si'' re'' dod'' si' |
dod'' la'' sold'' fad'' mi'' re'' dod'' si' la'4\trill |
<<
  \tag #'violino1 {
    sol''8.(\trill fad''32 mi'') sol''8.(\trill fad''32 mi'') sol''8.(\trill fad''32 mi'') |
    fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') fad''8.(\trill mi''32 re'') |
  }
  \tag #'violino2 {
    mi''8.(\trill re''32 dod'') mi''8.(\trill re''32 dod'') mi''8.(\trill re''32 dod'') |
    re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') re''8.(\trill dod''32 re'') |
  }
>>
si''8. la''16 sold''8.\trill fad''16 mi''8.\trill re''16 |
dod''8.\trill si'16 la'4 r |
