\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Corni" } <<
        \keepWithTag #'corni \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = "Merope" } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basso" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2.*6\break s2.*6\pageBreak
        s2.*6\break \grace s8 s2.*6\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        \grace s8 s2.*5\break s2.*6\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*5\break s2.*7\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*7\break s2.*6\pageBreak
        s2.*7\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}