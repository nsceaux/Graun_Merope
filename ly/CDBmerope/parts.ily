\piecePartSpecs
#`((corni #:instrument "Corni [in D]")
   (violini)
   (violino1)
   (violino2)
   (viola)
   (bassi)
   (silence #:on-the-fly-markup , #{ \markup\tacet#226 #}))
