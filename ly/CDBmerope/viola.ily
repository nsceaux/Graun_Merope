\clef "alto" re'4 fad'8. mi'16 fad'8. la'16 |
la'4 la' r8. fad'16 |
sol'4.. mi'16 fad'8. re'16 |
la4 la r |
la' la' re' |
re' si si |
si' si' mi' |
mi' dod' dod' |
re' la' la' |
si' si' si' |
sol' la' la |
la'-\sug\p la' la' |
si' si' si' |
sol'\f la' la |
re'2 r4 |
re'4\p fad'8. mi'16 fad'8. la'16 |
re''4 la' r8. fad'16 |
sol'4.. mi'16 fad'8. re'16 |
la4 la r |
re'4\pocof fad'8. mi'16 fad'8. la'16 |
la'4 la' r |
mi'2.-\sug\p mi'4\f mi' mi' |
mi'2.\p |
mi'4-\sug\f mi' mi' |
re' re' re' |
mi'4.. mi16 sold8. si16 |
mi'4 mi' mi' |
mi'4.. mi'16 si8. sold16 |
mi4 mi mi |
mi4.. mi16 mi'8. re'16 |
dod'4 la r |
r8. la16\pocof dod'8. mi'16 la'8. mi'16 |
dod'4 la r |
r8. la16-\sug\p dod'8. la16 dod'8. la16 |
re'4 re' re' |
dod' dod' dod' |
si si si |
la la la |
re' re' re' |
si si si |
dod' dod' dod' |
la la la |
si si fad' |
mi'2. |
mi' |
la4 re' mi' |
r mi'\pocof mi' |
r fad' fad' |
re' mi' mi |
la2.\trill |
la4\f dod'8. si16 dod'8. mi'16 |
mi'4 mi' r8. dod''16 |
re''4.. si'16 dod''8. la'16 |
mi'4 mi' r |
re'4\p fad'8. mi'16 fad'8. la'16 |
la'4 la' r8. fad'16 |
sol'4.. mi'16 fad'8. re'16 |
la4 la r |
re'2. |
re'4-\sug\f re' re' |
mi'2.-\sug\p |
mi'4-\sug\f mi' mi' |
fad'4-\sug\p mi'8. re'16 dod'8. re'16 |
la4 la'-\sug\f sol' |
fad' fad'\p fad' |
sol' sol' sol' |
sol' sol' sol' |
fad' fad' fad' |
fad' fad' fad' |
mi' mi' mi' |
mi' mi' mi' |
re' r8*2/3 re' mi' fad' mi' re' |
la'2 dod'4 |
re' r8*2/3 re' mi' fad' mi' re' |
la'2 dod'4 |
re' si2 |
la2.\fermata |
re'4\pocof fad'8. mi'16 fad'8. la'16 |
la'4 la' r |
sol'2.\p |
la'4 la' la' |
la'\pocof la' la' |
si' si' si' |
sol'\p la' la |
la'-\sug\f la' la' |
si' si' si' |
sol'\p la' la |
re'8. fad'16 sol'4 la' |
si'8. fad'16\pocof sol'4 la' |
re'2\f r4 |
la'4 la' re' |
re' si si |
si' si' mi' |
mi' mi' dod' |
re' la' la' |
si' si' si' |
sol' la' la |
re'2\fermata r4 |
sol'4\p sol' sol' |
sol'2. |
r4 re' fad' |
sol' sol r |
r4 r re'~ |
re' mi' dod'! |
re'2 r8.\fermata re'16 |
sol'4.. la'16 si'8. do''16 |
re''4 re' r8. sol16-\sug\pocof |
do'4.. re'16 mi'8. fad'16 |
sol'4 sol r |
re''-\sug\p  re'' re'' |
re'' re'' sol' |
sol'-\sug\f sol' sol' |
la'-\sug\p la' la' |
mi' mi' la' |
la'-\sug\f la' la' |
sol'-\sug\p sol' sol' |
sol' sol' sol' |
la' la' la' |
fad' fad' fad' |
sol' sol' sol' |
mi' mi' mi' |
re' fad' fad' |
sol' la' si' |
do'' la' re' |
mi' re'2 |
re'4-\sug\pocof re' re' |
mi' mi' mi' |
do'\p re' re |
re'-\sug\f re' re' |
mi' mi' mi' |
do'2.\p |
re'2 re4 |
sol2 r4 |
la'-\sug\f la' re' |
re' re' si |
si' si' mi' |
mi' mi' dod' |
dod' dod' la' |
la' la' la' |
mi'2. |
mi'2 r4 |
