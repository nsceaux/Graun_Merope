\clef "bass" re4 fad8. mi16 fad8. la16 |
re'4 la r8. fad16 |
sol4.. mi16 fad8. re16 |
dod4 re r |
fad4 fad fad |
sol sol sol |
sold sold sold |
la la la |
re' re' re' |
si si si |
sol la la, |
re'\p re' re' |
si si si |
sol\f la la, |
re2 r4 |
re\p fad8. mi16 fad8. la16 |
re'4 la r8. fad16 |
sol4.. mi16 fad8. re16 |
dod4 re r |
re\pocof fad8. mi16 fad8. la16 |
re'4 la r |
sold2.\p |
la4\f la la |
sold2.\p |
la4\f la la |
re\p re re |
mi4.. mi16 sold8. si16 |
mi'4 mi' mi' |
mi'4.. mi'16 si8. sold16 |
mi4 mi mi |
mi4.. mi16 mi'8. re'16 |
dod'4 la r |
r8. la,16\pocof dod8. mi16 la8. mi16 |
dod4 la, r |
r8. la16\p dod'8. la16 dod'8. la16 |
re'4 re' re' |
dod' dod' dod' |
si si si |
la la la |
re re re |
mi mi mi |
dod dod dod |
re re re |
si, si, si, |
sold, sold, sold, |
sold, sold, sold, |
la, re mi |
la\pocof la la |
fad fad fad |
re mi mi, |
la,8. la16 mi8. la16 dod8. mi16 |
la,4\f dod8. si,16 dod8. mi16 |
la4 mi r8. dod'16 |
re'4.. si16 dod'8. la16 |
sold4 la r |
re4\p fad8. mi16 fad8. la16 |
re'4 la r8. fad16 |
sol4.. mi16 fad8. re16 |
dod4 re r |
fad2. |
sol4\f sol sol |
sold2.\p |
la4\f la la |
fad'4\p mi'8. re'16 dod'8. re'16 |
la4 la\f sol |
fad fad\p fad |
sol sol sol |
sol sol sol |
fad fad fad |
fad fad fad |
mi mi mi |
mi mi mi |
re r8*2/3 re mi fad mi re |
la2 la,4 |
re r8*2/3 re mi fad mi re |
la2 la,4 |
re si,2 |
la,2.\fermata |
re4\pocof fad8. mi16 fad8. la16 |
re'4 la r |
sol2.\p |
la4 la la |
re'\pocof re' re' |
si si si |
sol\p la la, |
re'\f re' re' |
si si si |
sol\p la la, |
re8. fad16 sol4 la |
si8. fad16\pocof sol4 la |
re2\f r4 |
fad4 fad fad |
sol sol sol |
sold sold sold |
la la la |
re' re' re' |
si si si |
sol la la, |
re2\fermata r4 |
sol4\p sol sol |
sol2. |
r4 re' re |
sol sol, r |
r4 r fad |
sol mi dod! |
re2 r8.\fermata re16 |
sol4.. la16 si8. do'16 |
re'4 re r8. sol16\pocof |
do'4.. re'16 mi'8. fad'16 |
sol'4 sol r |
sol\p sol sol |
sol sol sol |
do\f do do |
la\p la la |
la la la |
re\f re re |
si\p si si |
do' do' do' |
la la la |
si si si |
sol sol sol |
la la la |
re re re |
sol la si |
do' do' si |
do' re' re |
sol\pocof sol sol |
mi mi mi |
do\p re re, |
sol\f sol sol |
mi mi mi |
do2.\p |
re2 re,4 |
sol,8. si16\f la8. sol16 fad8. mi16 |
fad4 fad fad |
sol sol sol |
sold sold sold |
la la la |
la la la |
re' re' re' |
sold2. |
la4.. sol!16\p fad8. mi16 |
