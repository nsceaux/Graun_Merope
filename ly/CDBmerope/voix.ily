\clef "soprano/treble" R2.*15 |
la'4 re''8. dod''16 re''8. mi''16 |
fad''4 mi'' r8. re''16 |
\appoggiatura re''4 mi''4.. dod''16 re''8. la'16 |
\appoggiatura la'8 sol'4 fad' r |
la'4 re''8. dod''16 re''8. mi''16 |
fad''4 mi'' r |
mi''4 si'8. si'16 dod''8. re''16 |
dod''4 r r |
mi''4 si'8. si'16 dod''8. re''16 |
dod''4 r r |
fad''4.. fad'16 sold'8. la'16 |
\appoggiatura la'4 sold'2 r4 |
si'4 si'8*2/3[ dod''] re'' dod''[ re''] mi'' |
mi''([ re'' dod'']) si'2 |
si'4 si'8*2/3[ dod''] re'' dod''[ re''] mi'' |
mi''[ re'' dod''] si'2 |
la'4 dod''8. la'16 dod''8. mi''16 |
la''4 mi''2 |
la'4 dod''8. la'16 dod''8. mi''16 |
la''4 mi''4. mi''8 |
fad''8*2/3[\melisma mi'' re''] re''[ dod'' si'] fad''[ mi'' re''] |
mi''[ la' si'] dod''[ re'' mi''] fad''[ sold'' la''] |
re''[ dod'' si'] si'[ la' sold'] fad''[ mi'' re''] |
dod''[ la' si'] dod''[ re'' mi''] fad''[ sold'' la''] |
fad''4~ fad''8*2/3[ mi'' re''] dod''[ si' la'] |
sold'[ si' la'] sold'[ la' si'] si'[ dod'' re''] |
mi''4~ mi''8*2/3[ re'' dod''] si'[ la' sold'] |
fad'[ la' sold'] fad'[ sold' la'] la'[ si' dod''] |
re''[ fad'' mi''] re''[ dod'' si'] la'[ sold' fad'] |
mi'[ sold' fad'] mi'[ si' la'] sold'[ re'' dod''] |
si'[ fad'' mi''] re''2\melismaEnd |
dod''8*2/3([ re'']) mi'' re''4 si'\trill |
la' r r |
dod'' r r |
fad''8. fad''16 mi''8.[\trill re''16] dod''8.[\trill si'16] |
la'2.\trill |
R2.*4 |
la'4 re''8. dod''16 re''8. mi''16 |
fad''4 mi'' r8. re''16 |
\appoggiatura re''4 mi''4.. dod''16 re''8. la'16 |
\appoggiatura la'8 sol'4 fad' r |
do''4 mi''8*2/3[ re''] do'' mi''[ re''] do'' |
si'8.[\trill la'16] sol'4 r |
re''4 fad''8*2/3[ mi''] re'' fad''[ mi''] re'' |
dod''8.[\trill si'16] la'4 r |
re''4 dod''8. re''16 mi''8. fad''16 |
fad''4( mi'') r |
r re''4. la'8 |
si'4~ si'8[\melisma dod''16 re''] mi''16[ fad'' sol'' la''] |
si''8*2/3[ la'' sol''] sol''[ fad'' mi''] re''[ dod'' si'] |
la'4~ la'8[ si'16 dod''] re''[ mi'' fad'' sol''] |
la''8*2/3[ sol'' fad''] fad''[ mi'' re''] dod''[ si' la'] |
sol'4~ sol'8[ la'16 si'] dod''[ re'' mi'' fad''] |
sol''8*2/3[ fad'' mi''] mi''[ re'' dod''] si'[ la' sol'] |
fad'4 re''2~ |
re''8*2/3[ dod'' re''] mi''[ re'' dod''] si'[ la' sol'] |
fad'4 re''2~ |
re''8*2/3[ dod'' re''] mi''[ re'' dod''] si'[ la' sol'] |
fad'4\melismaEnd r8. re''16 re''8. sold'16 |
\appoggiatura sold'4 la'2.\fermata |
la'4 re''8. dod''16 re''8. mi''16 |
fad''4 mi'' r |
si'4 si'8*2/3[ dod'' re''] mi''[ fad'' sol''] |
fad''[ mi'' re''] mi''2\trill |
re''4 r r |
fad'' r r |
si'8. sol''16 fad''8.[\trill mi''16] re''8.[\trill dod''16] |
re''4 r r |
fad'' r r |
si'8. sol''16 fad''8.[\trill mi''16] re''8.[\trill dod''16] |
re''8. fad'16 sol'4 la' |
si'8. re''16 sol'4 la' |
re' r r |
R2.*8 |
re''4. mi''8 re''4 |
re''( sol'') re'' |
\appoggiatura re''2 do''2. |
si'4 r re'' |
re''4( sol'') do'' |
\appoggiatura do''2 si'2. |
la'2 r8.\fermata re'16 |
sol'4.. la'16 si'8. do''16 |
re''4 re' r8. sol'16 |
do''4.. re''16 mi''8. fad''16 |
sol''4 sol' r |
sol' si' re'' |
sol' fa''2 |
fa''4 mi'' r |
mi'' re''16[ dod''!8.] si'16[ la'8.] |
mi''4 sol'2 |
\appoggiatura sol'4 fad'2 r4 |
re''4 do''16[ si'8.] la'16[ sol'8.] |
mi''4~ mi''8*2/3[\melisma do'' re''] mi''[ fad'' sol''] |
la''8.[ sol''16 fad''8.\trill mi''16 re''8.\trill do''16] |
re''4~ re''8*2/3[ si' do''] re''[ mi'' fad''] |
sol''8.[ fad''16 mi''8.\trill re''16 do''8.\trill si'16] |
do''4~ do''8*2/3[ la' si'] do''[ re'' mi''] |
fad''8.[ mi''16 re''8.\trill do''16 si'8.\trill la'16] |
si'4\trill do''\trill re''\trill |
mi''\trill fad''\trill sol'' |
mi''8*2/3[ re'' do'']\melismaEnd \appoggiatura si'4 la'2\trill |
sol'4 r r |
sol'' sol' r |
mi'' re''8. do''16 si'8. la'16 |
sol'2 r4 |
sol'' sol' r |
mi'' do'' la' |
re' la'2\trill |
sol' r4 |
R2.*8 |
