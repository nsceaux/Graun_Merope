\clef "treble" \transposition re <>
<<
  \tag #'(corno1 corni) {
    mi''4 sol''8. fa''16 sol''8. si''16 |
    do'''4 si'' s8. sol''16 |
    la''4.. fa''16 sol''8. sol''16 |
    fa''4 mi'' s |
    sol''4 sol'' sol'' |
    la'' s s |
    la'' la'' la'' |
    sol'' s s |
    mi''4 mi''8 mi'' mi'' mi'' |
    mi''4 mi''8 mi'' mi'' mi'' |
    la''4 sol''8. fa''16 mi''8. re''16 |
    mi''4 mi''8 mi'' mi'' mi'' |
    mi''4 mi''8 mi'' mi'' mi'' |
    la''4 sol''8. fa''16 mi''8. re''16 |
    do''4 sol'
  }
  \tag #'(corno2 corni) {
    do''4 mi''8. re''16 mi''8. sol''16 |
    sol''4 sol'' s8. mi''16 |
    fa''4.. re''16 mi''8. mi''16 |
    re''4 do'' s |
    do'' do'' do'' |
    do'' s s |
    re'' re'' re'' |
    re'' s s |
    do'' do''8 do'' do'' do'' |
    do''4 do''8 do'' do'' do'' |
    fa''4 mi''8. re''16 do''8. sol'16 |
    do''4 do''8 do'' do'' do'' |
    do''4 do''8 do'' do'' do'' |
    fa''4 mi''8. re''16 do''8. sol'16 |
    mi'4 mi'
  }
  { s2. | s2 r8. s16 | s2. | s2 r4 | s2. | s4 r r | s2. | s4 r r | }
>> r4 |
R2.*4 |
<>\pocof <<
  \tag #'(corno1 corni) {
    mi''4 sol''8. fa''16 sol''8. si''16 |
    do'''4 si''
  }
  \tag #'(corno2 corni) {
    do''4 mi''8. re''16 mi''8. sol''16 |
    sol''4 sol''
  }
>> r4 |
R2. |
<>\f <<
  \tag #'(corno1 corni) { re''4 re''8 re'' re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol'8 sol' sol' sol' | }
>>
R2. |
<>\f <<
  \tag #'(corno1 corni) { re''4 re''8 re'' re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol'8 sol' sol' sol' | }
>>
R2.*2 |
re''2.\p |
re''4 re''8 re'' re'' re'' |
re''2.~ |
re''4 re''8 re'' re'' re'' |
re''4 r r |
<<
  \tag #'(corno1 corni) {
    re''4 s s |
    re'' s s |
    re'' s s |
  }
  \tag #'(corno2 corni) {
    sol'4 s s |
    sol' s s |
    sol' s s |
  }
  { s4 r r | s r r | s r r | }
>>
R2.*12 |
<<
  \tag #'(corno1 corni) {
    re''4 re''8 re'' re'' re'' |
    sol''4 sol''8 sol'' sol'' sol'' |
  }
  \tag #'(corno2 corni) {
    sol'4 sol'8 sol' sol' sol' |
    mi''4 mi''8 mi'' mi'' mi'' |
  }
>>
R2.*2 |
<<
  \tag #'(corno1 corni) {
    re''4 sol''8. fad''16 sol''8. la''16 |
    si''4 la'' s8. sol''16 |
    \appoggiatura sol''8 la''4.. la''16 sol''8. sol''16 |
    la''4 sol''
  }
  \tag #'(corno2 corni) {
    sol'4 re''8. do''16 re''8. fad''16 |
    sol''4 fad'' s8. re''16 |
    mi''4.. do''16 re''8. re''16 |
    re''4 re''
  }
  { s2. | s2 r8. }
>> r4 |
R2.*5 | \allowPageTurn
do''4 do''8 do'' do'' do'' |
R2. |
<<
  \tag #'(corno1 corni) { re''4 re''8 re'' re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol'8 sol' sol' sol' | }
>>
R2. |
r4 <<
  \tag #'(corno1 corni) { re''8 re'' re'' re'' | do''4 }
  \tag #'(corno2 corni) { sol'8 sol' sol' sol' | sol'4 }
>> r4 r |
R2.*6 |
<>\p <<
  \tag #'(corno1 corni) {
    mi''2. |
    s4 re'' fa'' |
    mi''2. |
    s4 re'' fa'' |
    mi'' s s |
    re''2
  }
  \tag #'(corno2 corni) {
    do''2. |
    s4 sol' re'' |
    do''2. |
    s4 sol' re'' |
    do'' s s |
    sol'2
  }
  { s2. | r4 s s | s2. | r4 s s | s r r | }
>> r4\fermata |
<<
  \tag #'(corno1 corni) {
    mi''4 sol''8. fa''16 sol''8. si''16 |
    do'''4 si''
  }
  \tag #'(corno2 corni) {
    do''4 mi''8. re''16 mi''8. sol''16 |
    sol''4 sol''    
  }
>> r4 |
R2.*2 |
<<
  \tag #'(corno1 corni) {
    mi''4 mi''8 mi'' mi'' mi'' |
    mi''4 mi''8 mi'' mi'' mi'' |
  }
  \tag #'(corno2 corni) {
    do''4 do''8 do'' do'' do'' |
    do''4 do''8 do'' do'' do'' |
  }
>>
R2. |
<<
  \tag #'(corno1 corni) {
    mi''4 mi''8 mi'' mi'' mi'' |
    mi''4 mi''8 mi'' mi'' mi'' |
  }
  \tag #'(corno2 corni) {
    do''4 do''8 do'' do'' do'' |
    do''4 do''8 do'' do'' do'' |
  }
>>
R2.*4 |
<<
  \tag #'(corno1 corni) {
    sol''4 sol'' sol'' |
    la'' s s |
    la'' la'' la'' |
    sol'' s s |
    mi''4 mi''8 mi'' mi'' mi'' |
    mi''4 mi''8 mi'' mi'' mi'' |
    la''4 sol''8. fa''16 mi''8. re''16 |
    do''2
  }
  \tag #'(corno2 corni) {
    do''4 do'' do'' |
    re'' s s |
    re'' re'' re'' |
    re'' s s |
    do''4 do''8 do'' do'' do'' |
    do''4 do''8 do'' do'' do'' |
    fa''4 mi''8. re''16 do''8. sol'16 |
    mi'2
  }
  { s2. | s4 r r | s2. | s4 r r | s2.*3 s2\fermata }
>> r4 |
R2.*6 |
r4 r r8\fermata r |
R2.*20 |
do''4 do''8 do'' do'' do'' |
re''4 re''8 re'' re'' re'' |
R2. |
do''4 do''8 do'' do'' do'' |
re''4 re''8 re'' re'' re'' |
R2.*3 |
<<
  \tag #'(corno1 corni) {
    sol''4 sol'' sol'' |
    la'' s s |
    la''4 la'' la'' |
    sol'' s s |
    fa''4 fa''8 fa'' fa'' fa'' |
    mi''4 mi''8 mi'' mi'' mi'' |
  }
  \tag #'(corno2 corni) {
    do''4 do'' do'' |
    do'' s s |
    re'' re'' re'' |
    re'' s s |
    re'' re''8 re'' re'' re'' |
    do''4 do''8 do'' do'' do'' |
  }
  { s2. | s4 r r | s2. | s4 r r | }
>>
\twoVoices #'(corno1 corno2 corni) << re''2. re'' >>
<<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol' }
>> r4 |
