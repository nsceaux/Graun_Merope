\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Corni" } <<
      \keepWithTag #'corni \global
      \keepWithTag #'corni \includeNotes "corni"
    >>
    \new Staff \with { instrumentName = "Violini" } <<
      \global \keepWithTag #'violini \includeNotes "violini"
    >>
    \new Staff \with { instrumentName = "Viola" } <<
      \global \includeNotes "viola"
    >>
    \new Staff \with { instrumentName = "Basso" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2.*9\pageBreak
        \grace s2 s2.*9\break s2.*8\break s2.*9\pageBreak
        \grace s4 s2.*7\break \grace s16 s2.*7\break s2.*8\pageBreak
        s2.*7\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}