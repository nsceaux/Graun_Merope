\clef "bass" mib4 sol lab |
sib, sib lab |
sol re mib |
fa sib, mib |
lab lab lab |
sol sol sol |
re mib mib, |
sib, sib8 lab sol fa |
mib4\p sol lab |
sib, sib lab |
sol re mib |
fa sib, mib |
lab\f lab lab |
sol sol sol |
lab sib sib, |
<< { mib sol sib } \\ { mib, sol, sib, } >> |
mib' mib' mib' |
sib2 r4 |
sib sib sib |
do do' sib |
la la la |
sib2 r4 |
re re mib |
fa, fa mib |
re\p sib, mib |
fa, fa mib |
re la, sib, |
do fa, sib, |
mib\f mib mib |
mib mib mib |
re re' re' |
la la la |
sib do' re' |
sib do' re' |
mib'2 r4 |
fa mib re |
mib fa fa, |
sib, sib8\p lab sol fa |
mi2. |
fa2 sib4 |
do'2 do4 |
fa lab do' |
fa'2. |
re' |
mib'2 sib4 |
mib mib' sib |
mib mib' sib |
mib' do' lab |
sib sib8.\f lab16 sol8. fa16 |
mib4 sol lab |
sib, sib lab |
sol re mib |
fa sib, mib |
lab lab lab |
sol sol sol |
re mib mib, |
sib, sib8 lab sol fa |
mib4\p sol lab |
sib, sib lab |
sol re mib |
fa sib, mib |
lab\f lab lab |
sol sol sol |
lab sib sib, |
mib sol sib |
mib2 r4 |
r mib, mib, |
mib, r r |
r mib, mib, |
mib,2 r4 |
