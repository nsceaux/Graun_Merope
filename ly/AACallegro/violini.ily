\clef "treble" \tag #'violino2 \startHaraKiri
sib''4 sib''4.\trill do'''8 |
\appoggiatura mib''2 re''2. |
mib''8( sol'') fa''( lab'') sol''( sib'') |
\appoggiatura sib''4 lab''2 sol''4 |
do''' mib''2\trill |
sib''4 mib''2\trill |
\twoVoices #'(violino1 violino2 violini) <<
  { fa''8*2/3([ sol'' lab'']) }
  { \stopHaraKiri fa''4 }
>>
<<
  \tag #'(violino1 violini) {
    \appoggiatura lab''4 sol''2 |
    \appoggiatura sol''4 fa''2
  }
  \tag #'(violino2 violini) {
    \appoggiatura fa''4 mib''2 |
    \appoggiatura mib''4 re''2 \startHaraKiri
  }
>> r4 |
sib''4\p sib''4.\trill do'''8 |
\appoggiatura mib''2 re''2. |
mib''8( sol'') fa''( lab'') sol''( sib'') |
\appoggiatura sib''4 lab''2 sol''4 |
do'''\f mib''2\trill |
sib''4 mib''2\trill |
do''8*2/3( sib' lab') sol'4 fa'\trill |
mib'2 r4 |
sol''8*2/3( lab'' sib'') lab''4 sol'' |
sol''8( fa'') mib''( re'') do''( sib') |
sib''4 sib''8*2/3( la'' sol'') fa''( mib'' re'') |
\appoggiatura re''2 mib''2. |
mib''8*2/3( fa'' sol'') fa''4 mib'' |
mib''8( re'') do''( sib') la'( sib') |
sib''4 sib''8*2/3( la'' sol'') fa''( mib'' re'') |
\appoggiatura re''2 do''2. |
fa''4\p fa''4.\trill sol''8 |
\appoggiatura sib'2 la'2. |
sib'8( re'') do''( mib'') re''( fa'') |
\appoggiatura fa''4 mib''2 re''4 |
sol''\f sib'2\trill |
la''4 do''2\trill |
sib''4 fa''2\trill |
do'''4 fa''2\trill |
re'''8( sib'') la''( sib'') fa''( sib'') |
re'''( sib'') la''( sib'') fa''( sib'') |
sol''( sib'') mib''( sol'') do''( mib'') |
sib'4( la'8.) fa''16 sib'8. sib''16 |
sol''8. mib''16 re''4 do''\trill |
sib'2 r4 |
<>\p <<
  \tag #'(violino1 violini) {
    \appoggiatura do'''16 sib''8.( la''16 sib''8. do'''16) reb'''4~ |
    reb'''( do''') sib''~ |
    sib'' sib''8( lab'') lab''( sol'') |
    sol''2( lab''4)^! |
    \appoggiatura sib''16 lab''8.( sol''16 lab''8. sib''16) do'''4~ |
    do'''( sib'') lab''~ |
    lab'' lab''8( sol'') sol''( fa'') |
    fa''4( sol''8.) sib''16 lab''8. fa''16 |
    fa''4( sol''8.) sib''16 lab''8. fa''16 |
    sol''8.
  }
  \tag #'(violino2 violini) {
    \stopHaraKiri
    \appoggiatura lab''16 sol''8.( fa''16 sol''8. lab''!16) sib''4~ |
    sib''( lab'') sol''~ |
    sol'' sol''8( fa'') fa''( mi'') |
    mi''2( fa''4)^! |
    \appoggiatura sol''16 fa''8.( mi''16 fa''8. sol''16) lab''4~ |
    lab''( sol'') fa''4~ |
    fa'' fa''8( mib'') mib''( re'') |
    re''4( mib''8.) sol''16 fa''8. re''16 |
    re''4( mib''8.) sol''16 fa''8. re''16 |
    mib''8. \startHaraKiri
  }
>> sib''16 lab''8. sol''16 fa''8. mib''16 |
re''8. do''16 sib'4 r |
sib''4\f sib''4.\trill do'''8 |
\appoggiatura mib''2 re''2. |
mib''8( sol'') fa''( lab'') sol''( sib'') |
\appoggiatura sib''4 lab''2 sol''4 |
do''' mib''2\trill |
sib''4 mib''2\trill |
\twoVoices #'(violino1 violino2 violini) <<
  { fa''8*2/3([ sol'' lab'']) }
  { \stopHaraKiri fa''4 }
>>
<<
  \tag #'(violino1 violini) {
    \appoggiatura lab''4 sol''2 |
    \appoggiatura sol''4 fa''2
  }
  \tag #'(violino2 violini) {
    \appoggiatura fa''4 mib''2 |
    \appoggiatura mib''4 re''2 \startHaraKiri
  }
>> r4 |
sib''4\p sib''4.\trill do'''8 |
\appoggiatura mib''2 re''2. |
mib''8( sol'') fa''( lab'') sol''( sib'') |
\appoggiatura sib''4 lab''2 sol''4 |
do'''\f mib''2\trill |
sib''4 mib''2\trill |
do''8*2/3( sib' lab') sol'4 fa'\trill |
mib'2 r4 |
mib'4 sol''8 mib'' sib' sol' |
mib'4 <<
  \tag #'(violino1 violini) { sib4 sib | sib }
  \tag #'(violino2 violini) { \stopHaraKiri sol4 sol | sol }
>> sol''8 mib'' sib' sol' |
mib'4 <<
  \tag #'(violino1 violini) { sib4 sib | sib2 }
  \tag #'(violino2 violini) { sol4 sol | sol2 }
>> r4 |
