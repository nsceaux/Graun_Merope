\clef "treble" \transposition mib
<>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''4. la''8 | sol''2. | }
  { mi''4 do'' fa'' | \appoggiatura mi''2 re''2. }
>>
<<
  \tag #'(corno1 corni) { sol''4 fa'' mi'' | }
  \tag #'(corno2 corni) { mi''4 re'' do'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura sol''4 fa''2 mi''4 | }
  { re''4 sol' do'' | }
>>
<<
  \tag #'(corno1 corni) {
    la''2. |
    sol'' |
    fa''4 \appoggiatura fa''4 mi''2 |
  }
  \tag #'(corno2 corni) {
    do''2. |
    do'' |
    re''4 \appoggiatura re''4 do''2 |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura mi''4 re''2 }
  { sol'2 }
>> r4 |
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''4. la''8 | sol''2. | }
  { mi''4 do'' fa'' | \appoggiatura mi''2 re''2. }
>>
<<
  \tag #'(corno1 corni) { sol''4 fa'' mi'' | }
  \tag #'(corno2 corni) { mi''4 re'' do'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura sol''4 fa''2 mi''4 | }
  { re''4 sol' do'' | }
>>
<>\f <<
  \tag #'(corno1 corni) {
    la''2. |
    sol'' |
    fa''4 mi'' re'' |
    do''2
  }
  \tag #'(corno2 corni) {
    do''2. |
    do'' |
    re''4 do'' sol' |
    mi'2
  }
>> r4 |
<<
  \tag #'(corno1 corni) { mi''4 fa'' mi'' | re''2 }
  \tag #'(corno2 corni) { do''4 re'' do'' | sol'2 }
>> r4 |
<<
  \tag #'(corno1 corni) { re''4 re'' sol'' | }
  \tag #'(corno2 corni) { sol'4 sol' re'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2. | }
  { \appoggiatura re''2 do''2. }
>>
<<
  \tag #'(corno1 corni) {
    la''4 la'' la'' |
    sol''2 s4 |
    sol''4 sol'' sol'' |
  }
  \tag #'(corno2 corni) {
    re''4 re'' re'' |
    re''2 s4 |
    re''4 re'' mi'' |
  }
  { s2. s2 r4 }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura sol''2 fad''2. }
  { re''2. }
>>
R2.*4 |
<<
  \tag #'(corno1 corni) {
    sol''2. |
    la'' |
    si'' |
    la'' |
    si''4 la'' s |
    si'' la'' s |
    mi''2
  }
  \tag #'(corno2 corni) {
    mi''2. |
    fad'' |
    sol'' |
    re'' |
    sol''4 fad'' s |
    sol'' fad'' s |
    do''2
  }
  { s2.*4 s2 sol''4 s2 sol''4 }
>> r4 |
re''2 re''4 |
mi'' re'' re'' |
<<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol'2 }
>> r4 |
R2.*11 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''4. la''8 | sol''2. | }
  { mi''4 do'' fa'' | \appoggiatura mi''2 re''2. }
>>
<<
  \tag #'(corno1 corni) { sol''4 fa'' mi'' | }
  \tag #'(corno2 corni) { mi''4 re'' do'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura sol''4 fa''2 mi''4 | }
  { re''4 sol' do'' | }
>>
<<
  \tag #'(corno1 corni) {
    la''2. |
    sol'' |
    fa''4 \appoggiatura fa''4 mi''2 |
  }
  \tag #'(corno2 corni) {
    do''2. |
    do'' |
    re''4 \appoggiatura re''4 do''2 |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura mi''4 re''2 }
  { sol'2 }
>> r4 |
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol''4 sol''4. la''8 | sol''2. | }
  { mi''4 do'' fa'' | \appoggiatura mi''2 re''2. }
>>
<<
  \tag #'(corno1 corni) { sol''4 fa'' mi'' | }
  \tag #'(corno2 corni) { mi''4 re'' do'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { \appoggiatura sol''4 fa''2 mi''4 | }
  { re''4 sol' do'' | }
>>
<>\f <<
  \tag #'(corno1 corni) {
    la''2. |
    sol'' |
    fa''4 mi'' re'' |
    do''2
  }
  \tag #'(corno2 corni) {
    do''2. |
    do'' |
    re''4 do'' sol' |
    mi'2
  }
>> r4 |
<<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { mi'2 }
>> r4 |
R2.*4 |
