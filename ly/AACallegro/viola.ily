\clef "alto" sol'4 mib'4. lab'8 |
\appoggiatura sol'2 fa'2. |
sib4 sib mib' |
mib'( re') mib' |
mib' do'' do'' |
mib' sib' sib' |
sib'2 sib'4 |
sib'2 r4 |
sol'\p mib'4. lab'8 |
\appoggiatura sol'2 fa'2. |
sib4 sib mib' |
mib'( re') mib' |
mib'\f do'' do'' |
mib' sib' sib' |
lab'8*2/3 sol' fa' mib'4 re'\trill |
mib'2 r4 |
sib' sib' sib' |
sib'2 r4 |
re'4 re' sib' |
sol'2. |
fa'4 fa' fa' |
fa'2 r4 |
fa' fa' sol' |
\appoggiatura sib2 la2. |
sib4\p re'4. mib'8 |
\appoggiatura re'2 do'2. |
fa'4 fa' sib' |
sib'( la') sib' |
sib\f sol' sol' |
do' << { la' la' } \\ { fa' fa' } >> |
fa' sib' sib' |
fa' do'' do'' |
fa' mib' fa' |
fa' mib' fa' |
mib' sol' mib' |
\appoggiatura re'4 do'2 sib4 |
sib \appoggiatura sib la2\trill |
sib r4 |
mi'2.\p |
fa'2 sib'4 |
do''2 do'4 |
fa' lab' do'' |
fa'2. |
re' |
mib'2 sib4 |
mib mib' sib |
mib mib' sib |
mib' mib' do' |
sib2 r4 |
sol'4.\f mib'8 lab'4 |
\appoggiatura sol'2 fa'2. |
sib4 sib mib' |
mib'( re') mib' |
mib' do'' do'' |
mib' sib' sib' |
sib'2 sib'4 |
sib'2 r4 |
sol'4.\p mib'8 lab'4 |
\appoggiatura sol'2 fa'2. |
sib4 sib mib' |
mib'( re') mib' |
mib'\f do'' do'' |
mib' sib' sib' |
mib' mib' re'\trill |
mib'2 r4 |
mib'2 r4 |
r mib mib |
mib r r |
r mib mib |
mib2 r4 |
