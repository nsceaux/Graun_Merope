\clef "treble" <>
<<
  \tag #'(violino1 violini) {
    sib'4. re''8 re''4 do'' |
    s8 do''16( re'' mib''8) mib'' mib''4 re'' |
    s8 re''16( mib'' fa''8) sol'' \appoggiatura sol''4 fa''4. sol''8 |
    \appoggiatura sol''4 fa''4. sol''8 \appoggiatura sol''4 fa''4.
  }
  \tag #'(violino2 violini) {
    re'4. sib'8 sib'4 la' |
    s8 la'16( sib' do''8) do'' do''4 sib' |
    s8 sib'16( do'' re''8) mib'' \appoggiatura mib''4 re''4. mib''8 |
    \appoggiatura mib''4 re''4. mib''8 \appoggiatura mib''4 re''4.
  }
  { s1 | r8 s2.. | r8 s4. s s8\p | s4. s8\f }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { sib''8 | do''8.( re''32 mib'') re''8 do'' re''16( do'' sib'8) }
  { sib'8 | sib'4. la'8 sib'4 }
>> r4 |
r8 re''16( mib'' re''8) mi'' \appoggiatura mi''4 fa''4. fa''8 |
sol''16( mi'') sib'4 sib'8 \appoggiatura do''16 sib'8 la' r4 |
r8 sol''16( lab'') sol''8 la'' \appoggiatura la''4 sib''4. sib''8 |
do'''16( la'') mib''4 mib''8 re''16 mib'' fa'' sol'' fa''4~ |
fa''8 do'''16 sib'' la'' sol'' fa'' mib'' re'' mib'' fa'' sol'' fa''4~ |
fa''8 do'''16 sib'' la'' sol'' fa'' mib'' re'' fa'' mib'' re'' sol'' mib'' re'' do'' |
sib'4( la'8) r do'''4.\p fa''8 |
\appoggiatura fa''4 mi''4. do'''8 do'''4. mib''8 |
\appoggiatura fa''16 mib''8 re'' r4 sib''4.\f re''8 |
\appoggiatura re''4 do''4. sol''8 sol''4. sib'8 |
la'16 sib' do'' re'' mib'' fa'' sol'' la'' sib''8.\trill la''32 sol'' fa''8 mib'' |
\appoggiatura mib''16 re''8 do''16 sib' do''4\trill sib'8 fa' sib4 |
%% segno
<>\p <<
  \tag #'(violino1 violini) {
    sib'4. re''8 re''4 do'' |
    s8 do''16( re'' mib''8) mib'' mib''4 re'' |
    s8 re''16( mib'' fa''8) sol'' \appoggiatura sol''4 fa''4. sol''8 |
    \appoggiatura sol''4 fa''4. sol''8 \appoggiatura sol''4 fa''4.
  }
  \tag #'(violino2 violini) {
    re'4. sib'8 sib'4 la' |
    s8 la'16( sib' do''8) do'' do''4 sib' |
    s8 sib'16( do'' re''8) mib'' \appoggiatura mib''4 re''4. mib''8 |
    \appoggiatura mib''4 re''4. mib''8 \appoggiatura mib''4 re''4.
  }
  { s1 | r8 s2.. | r8 s2.. | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { sib''8 | do''8.( re''32 mib'') re''8 do'' re''16( do'' sib'8) }
  { sib'8 | sib'4. la'8 sib'4 }
>> r4 |
r8 <<
  \tag #'(violino1 violini) {
    re''16( mib'' re''8) mi'' \appoggiatura mi''4 fa''4. fa''8 |
  }
  \tag #'(violino2 violini) {
    fa'16( sol' fa'8) sol' \appoggiatura sol'4 la'4. la'8 |
  }
>> \twoVoices #'(violino1 violino2 violini) <<
  { sol''16( mi'') sib'4 }
  { sib'8( sol'4) }
>> <<
  \tag #'(violino1 violini) { sib'8 sib' la' }
  \tag #'(violino2 violini) { sol'8 sol' fa' }
>> r4 |
r8 <<
  \tag #'(violino1 violini) {
    re''16( mib'' re''8) mi'' \appoggiatura mi''4 fa''4. fa''8 |
  }
  \tag #'(violino2 violini) {
    fa'16( sol' fa'8) sol' \appoggiatura sol'4 la'4. la'8 |
  }
>> \twoVoices #'(violino1 violino2 violini) <<
  { sol''16( mi'') sib'4 }
  { sib'8( sol'4) }
>> <<
  \tag #'(violino1 violini) { la'8 \appoggiatura la'8 sol'4 }
  \tag #'(violino2 violini) { fa'8 \appoggiatura fa'8 mi'4 }
>> r4 |
<<
  \tag #'(violino1 violini) {
    do''8 do'' do'' do'' re'' re'' re'' re'' |
    re'' re'' re'' re'' do'' do'' do'' do'' |
    do'' do'' do'' do'' sib' sib' sib' sib' |
    sib' sib' sib' sib' la'16 sib' do'' re'' do''8 la'' |
  }
  \tag #'(violino2 violini) {
    fa'8 fa' fa' fa' fa' fa' fa' fa' |
    sol' sol' sol' sol' sol' sol' sol' sol' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    mi' mi' mi' mi' fa'16 sol' la' sib' la'8 do'' |
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { sol''8 mi'' fa'' sol'' }
  { do''8 do''4 do''8 }
>> <<
  \tag #'(violino1 violini) { la'16 sib' do'' re'' do''8 la'' | }
  \tag #'(violino2 violini) { fa'16 sol' la' sib' la'8 do'' | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { sol''8 mi'' fa'' sol'' }
  { do''8 do''4 do''8 }
>>
<<
  \tag #'(violino1 violini) {
    la'8 mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' fa'' fa'' fa'' |
    mi'' mi'' mi'' mi'' sol'' sib'' sib'' sib'' |
  }
  \tag #'(violino2 violini) {
    fa'8 do'' do'' do'' |
    sib' sib' sib' sib' si' re'' re'' re'' |
    do'' do'' do'' do'' mi'' sol'' sol'' sol'' |
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { la''16 sol'' fa'' mi'' }
  { fa''8 la'16 sol' }
>> <<
  \tag #'(violino1 violini) {
    fa''8 la'' \appoggiatura la''4 sol''4. fa''8 |
    mi''16( re'') do''8
  }
  \tag #'(violino2 violini) {
    la'8 do'' \appoggiatura do''4 sib'4. la'8 |
    sol'16( fa') mi'8
  }
>> r4 \twoVoices #'(violino1 violino2 violini) <<
  { sol''4. do''8 |
    \appoggiatura do''4 si'4. sol''8 sol''4. sib'8 |
    sib'8 la' s4 fa''4. la'8 |
    \appoggiatura la'4 sol'4. re''8 re''4. fa'8 | }
  { r8 mi' mi'4 |
    r8 fa' fa'4 r8 sol' sol' sol' |
    sol'8 fa' s4 r8 do' do'4 |
    r8 re' re'4 r8 re' re' re' | }
  { s2 s1 s4 r }
>>
<<
  \tag #'(violino1 violini) {
    fa'8( mi') s sib' sib'( la') s re'' |
    re''( do'') s fa'' fa''( mi'') s la'' |
    la''( sol'')
  }
  \tag #'(violino2 violini) {
    re'8( do') s sol' sol'( fa') s sib' |
    sib'( la') s la' la'( sol') s fa'' |
    fa''( mi'')
  }
  { s4 r8 s s4 r8 s | s4 r8 s s4 r8 }
>> r4 mi''16(\pocof do''') do'''4 mi''8 |
fa''8.\trill mi''32 re'' do''8 sib' la'4 sol'\trill |
<<
  \tag #'(violino1 violini) {
    fa''4. la''8 la''4 sol'' |
    s8 sol''16( la'' sib''8) sib'' sib''4 la'' |
    s8 la''16( sib'' do'''8) re''' \appoggiatura re'''4 do'''4. re'''8 |
    \appoggiatura re'''4 do'''4. re'''8 \appoggiatura re'''4 do'''4.
  }
  \tag #'(violino2 violini) {
    la'4. fa''8 fa''4 mi'' |
    s8 mi''16( fa'' sol''8) sol'' sol''4 fa'' |
    s8 fa''16( sol'' la''8) sib'' \appoggiatura sib''4 la''4. sib''8 |
    \appoggiatura sib''4 la''4. sib''8 \appoggiatura sib''4 la''4.
  }
  { s1\f | r8 s2.. | r8 s4. s s8\p | s4. s8\f }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { fa''8 | sol'8. la'32 sib' la'8 sol' la'16 sol' fa'8 }
  { fa'8 | fa'4. mi'8 fa'4 }
>> r4 |
<<
  \tag #'(violino1 violini) {
    sib'4. re''8 re''4 do'' |
    s8 do''16( re'' mib''8) mib'' mib''4 re'' |
    s8 re''16( mib'' fa''8) sol''8 \appoggiatura sol''4 lab''4. sol''8 |
    \appoggiatura sol''4 fa''4. mib''8 \appoggiatura mib''4 re''4. do''8 |
    \appoggiatura do''4 sib'4. lab'8 lab' sol'
  }
  \tag #'(violino2 violini) {
    re'4. sib'8 sib'4 la' |
    s8 la'16( sib' do''8) do'' do''4 sib' |
    s8 sib'16( do'' re''8) mib'' \appoggiatura mib''4 fa''4. mib''8 |
    \appoggiatura mib''4 re''4. do''8 \appoggiatura do''4 sib'4. lab'8 |
    \appoggiatura lab'4 sol'4. fa'8 fa' mib'
  }
  { s1\p | r8 s2.. | r8 s2.. }
>> r4 |
\twoVoices #'(violino1 violino2 violini) <<
  { r8 sib'16( do'' sib'8) sol'' \appoggiatura sol''4 fa''4. la'8 |
    \appoggiatura la'4 sib'4. fa''8 fa'' mib'' r4 |
    r8 do''16 re'' do''8 mib'' }
  { sol'4 r r8 sib' sib'4 |
    r8 fa' fa'4 r8 sol' sol' sol' |
    fa'4 r8 do'' }
>> <<
  \tag #'(violino1 violini) { \appoggiatura mib''4 re''4. }
  \tag #'(violino2 violini) { \appoggiatura do''4 sib'4. }
>> fa''8\pocof |
fa''8.\trill mib''32 fa'' sol''8 sib' \appoggiatura sib' la'4 r |
<>\p <<
  \tag #'(violino1 violini) {
    lab'8 lab' lab' lab' sol' sib' sib' sib' |
    do'' do'' do'' do'' do'' do'' do'' do'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    fa'' fa'' fa'' mib'' re''16 mib'' fa'' sol'' fa''8 re''' |
  }
  \tag #'(violino2 violini) {
    fa'8 fa' fa' fa' mib' sol' sol' sol' |
    sol' sol' sol' sol' la' la' la' la' |
    la' la' la' la' sib' sib' sib' sib' |
    sib' sib' sib' sib' do'' do'' do'' do'' |
    do'' do'' do'' do'' sib'16 do'' re'' mib'' re''8 fa'' |
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { do'''8 do''16 re'' \appoggiatura fa''16 mib''8 re''16 do'' }
  { fa''8 la' sib' do'' }
>> <<
  \tag #'(violino1 violini) { re''16 mib'' fa'' sol'' fa''8 re''' | }
  \tag #'(violino2 violini) { sib'16 do'' re'' mib'' re''8 fa'' | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { do'''8 do''16 re'' \appoggiatura fa''16 mib''8 re''16 do'' }
  { fa''8 la' sib' do'' }
>> <<
  \tag #'(violino1 violini) { re''16 mib'' fa'' sol'' lab''8 lab'' | }
  \tag #'(violino2 violini) { sib'16 do'' re'' mib'' fa''8 fa'' | }
  { s4. s8\pocof }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { sol''16 fa'' mib'' re'' do''8 sib' la'!16 sol' fa'8 s4 |
    fa''4. sib'8 \appoggiatura sib'4 la'4. fa''8 |
    fa''4. reb''8 reb'' do'' s4 |
    fa''4. sib'8 \appoggiatura sib'4 la'4. fa''8 |
    fa''4. re''!8 re''[( do'')] }
  { sib'4 sol' fa' s4 |
    r8 fa' fa'4 r8 mib' mib'4 |
    r8 reb' reb' sib' sib' la' s4 |
    r8 fa' fa'4 r8 mib' mib'4 |
    r8 re' re' sib' sib'8[( la')] }
  { s2. r4 | s1-\sug\p | s2. r4 | s1\pocof }
>> <<
  \tag #'(violino1 violini) {
    s8 mib'' |
    mib''( re'') s sol'' sol''( fa'') s mib'' |
    mib''( re'') s do'' \appoggiatura do''4
  }
  \tag #'(violino2 violini) {
    s8 do'' |
    do''( sib') s mib'' mib''( re'') s do'' |
    do''( sib') s la' \appoggiatura la'4
  }
  { r8 s\p | s4 r8 s s4 r8 s | s4 r8 }
>> sib'2\fermata |
do''16\f re'' mib'' fa'' sol''8 la'' sib''8.\trill la''32 sol'' fa''8 <<
  \tag #'(violino1 violini) {
    mib''8 |
    re''4 do''\trill s4. re''8 |
    re''4 do'' s8 do''16( re'' mib''8) mib'' |
  }
  \tag #'(violino2 violini) {
    do''8 |
    sib'4 la'\trill s4. sib'8 |
    sib'4 la' s8 la'16( sib' do''8) do'' |
  }
  { s8 | s2 sib'4.\ff s8 | s2 r8 }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { re''16 mib'' fa'' sol'' fa''4~ fa''8 do'''16 sib'' la'' sol'' fa'' mib'' |
    re'' mib'' fa'' sol'' fa''4~ fa''8 do'''16 sib'' la'' sol'' fa'' mib'' | }
  { sib'16 do'' re'' mib'' re''8 re'' do'' la' sib' do'' |
    sib'16 do'' re'' mib'' re''8 re'' do'' la' sib' do'' | }
>>
re''16 sib' do'' re'' mib'' fa'' sol'' la'' sib''8.\trill la''32 sol'' fa''8 mib'' |
\appoggiatura mib''16 re''8 do''16 sib' do''4\trill sib'8 fa' sib4\fermata |
<>\p <<
  \tag #'(violino1 violini) {
    sol'4. sib'8 sib'4( la'8) mib'' |
    mib''4( re''8) do''
  }
  \tag #'(violino2 violini) {
    sib4. sol'8 sol'4( fad'8) do'' |
    do''4( sib'8) la'
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { sib'16 la' sol'8 r4 }
  { sol'8 re' sib sol }
>>
<<
  \tag #'(violino1 violini) {
    do''4. mib''8 mib''4( re''8) lab'' |
    lab''4( sol''8) fa''
  }
  \tag #'(violino2 violini) {
    mib'4. do''8 do''4( si'8) fa'' |
    fa''4( mib''8) re''
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { mib''16( re'') do''8 }
  { do''4 }
>> r4 |
<<
  \tag #'(violino1 violini) {
    lab''4-! sol''-! s2 |
    fa''4-! mib''-! s2 |
    s do'' |
  }
  \tag #'(violino2 violini) {
    do''4-! do''-! s2 |
    si'4-! do''-! s2 |
    s fad'
  }
  { s2\f r | s2 r | r s\p | }
>>
<>\f \twoVoices #'(violino1 violino2 violini) <<
  { si'16 sol' la' si' }
  { sol'8 la'16 si' }
>> do''16 re'' mib'' fa'' sol''4.\p \twoVoices #'(violino1 violino2 violini) <<
  { fa''8 | \appoggiatura fa''4 mi''4. sol''16( mi'') do''4 sib' | }
  { si'8 | do'' sol' sol' sol' sol' mi' fa' sol' | }
>>
<<
  \tag #'(violino1 violini) {
    sib'8 la' s4 re''-! do''-! |
    s2 re''4-! do''-! |
    s1 |
    sib'2 la' |
    sib'4. re''8 re''4 do'' |
    s8 do''16( re'' mib''8) mib'' mib''4 re'' |
  }
  \tag #'(violino2 violini) {
    sol'8 fa' s4 sib'-! la'-! |
    s2 sib'4-! la'-! |
    s1 |
    mi'2 fa' |
    re'4. sib'8 sib'4 la' |
    s8 la'16( sib' do''8) do'' do''4 sib' |
  }
  { s4 r s2\f | r2 s | R1 | s\p | s\f | r8 }
>>
