Quan -- do a fron -- te d’un pe -- ri -- glio
o -- gni spe -- me è al -- fin smar -- ri -- ta al -- fin __ smar -- ri -- ta
è __ vil -- tà bra -- mar la vi -- ta;
è __ vir -- tù sa -- per mo -- rir
è vil -- tà __ bra -- mar la vi -- ta;
è vil -- tà bra -- mar la vi -- ta;
è vir -- tù sa -- per mo -- rir, sa -- per mo -- rir, sa -- per mo -- rir,
è vir -- tù __ sa -- per mo -- rir.

Quan -- do a fron -- te d’un pe -- ri -- glio
o -- gni spe -- me è al -- fin smar -- ri -- ta al -- fin smar -- ri -- ta
è __ vil -- tà bra -- mar la vi -- ta;
è __ vir -- tù sa -- per mo -- rir
è vil -- tà __ bra -- mar __ la vi -- ta;
è vil -- tà bra -- mar la vi -- ta;
è vir -- tù sa -- per mo -- rir, sa -- per mo -- rir, sa -- per mo -- rir,
è __ vir -- tù __ sa -- per mo -- rir.

Or che gia -- ce e -- stin -- to il fi -- glio,
or che ve -- do al so -- glio e -- let -- to
un in -- de -- gno, un vil sog -- get -- to,
che mi re -- sta da sof -- frir?
or che ve -- do al so -- glio e -- let -- to
un in -- de -- gno, un vil sog -- get -- to,
che mi re -- sta da sof -- frir?
