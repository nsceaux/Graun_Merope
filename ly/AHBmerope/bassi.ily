\clef "bass" r8 sib, re sib, r fa la do' |
fa'4 r r8 sib, re fa |
sib4 r r8 sib, sib4 |
r8 sib,\p sib4 r8 sib,\f sib re |
mib do fa fa, r sib, re fa |
sib4 r r8 la la la |
mi4 mi r8 fa16 sol la sib do' re' |
mib'4 r r8 re' re' re' |
la4 la sib r8 sib |
la fa sol la sib4 r8 sib |
la fa sol la sib4 mib |
r8 fa do la, fa, fa\p fa4 |
r8 sol sol4 r8 la la4 |
r8 sib fa re sib, re\f re4 |
r8 mib mib4 r8 mi mi mi |
fa fa fa fa sol4 la |
sib8 re mib fa sib,4 r |
%%%
r8 sib,\p re sib, r fa la do' |
fa'4 r r8 sib, re fa |
sib4 r r8 sib, sib4 |
r8 sib, sib4 r8 sib, sib re |
mib do fa fa, r8 sib, re fa |
sib4 r r8 la la la |
mi4 mi r8 fa fa sol16 la |
sib4 r r8 la la la |
mi4 fa do8 do'16 re' do'8 sib |
la la la la sib sib sib sib |
sib sib sib sib la la la la |
la la la la sol sol sol sol |
sol sol sol sol fa4 r8 fa |
mi do re mi fa4 r8 fa |
mi do re mi fa fa fa fa |
sib sib sib sib sol sol sol sol |
do' do' do' do' do' do' do' do' |
fa fa fa fa sib sib sib sib |
do' do' sol mi do do do4 |
r8 re re4 r8 mi mi4 |
r8 fa do la, fa, la, la,4 |
r8 sib, sib,4 r8 si, si,4 |
r8 do do4 r8 do do4 |
r8 do do4 r8 do do4 |
r8 do16\pocof re mi fa sol la sib8 sib sib sib |
la sol la sib do'4 do |
fa,8\f fa la fa r do mi sol |
do'4 r r8 fa la do' |
fa'4 r r8 fa fa'4 |
r8 fa\p fa'4 r8 fa,\f fa la, |
sib, sol, do do, fa, fa16 mib! re8 do |
sib, sib\p re' sib r fa la do' |
fa'4 r r8 sib, re fa |
sib4 r r8 sib, sib4 |
r8 sib, sib4 r8 sib, sib4 |
r8 sib, sib sib, r mib sol sib |
mib'4 r r8 re' re'4 |
r8 re' re'4 r8 do'16 re' do'8 sib |
la4 r r8 sib sib4 |
r8 re\pocof mib mi fa fa16 sol fa8 mib |
re\p re re re mib mib mib mib |
mib mib mib mib fa fa fa fa |
fa fa fa fa sol sol sol sol |
sol sol sol sol la la la la |
la la la la sib4 r8 sib |
la fa sol la sib4 r8 sib |
la fa sol la sib4 r8 re\pocof |
mib4 mi fa r |
r8 reb'\p reb'4 r8 do' do'4 |
r8 sib sib4 r8 fa fa, fa |
r8 re'!\pocof re'4 r8 do' do'4 |
r8 sib sib4 r8 fa fa,4 |
r8 fa\p fa4 r8 fa fa4 |
r8 fa fa fa sol2\fermata |
mib8\f mib mib mib re do re mib |
fa4 fa, sib,8\ff sib re' sib |
r8 fa la do' fa'4 r8 fa |
sib4 r8 sib la fa sol la |
sib4 r8 sib la fa sol la |
sib sib, sib, do re re re mib |
fa4 fa, sib,\fermata r4 |
r8 sol\p sib sol r8 re re'4 |
r8 re mi fad sol4 sol, |
r8 do' mib' do' r8 sol, sol4 |
r8 sol la si do'4 r |
fa4-!\f mib-! r2 |
re4-! do-! r2 |
lab1\p |
sol8\f sol sol fa mi\p mi mi re |
do do do do do do re mi |
fa4 r sib-!\f fa |
r2 sib,4-! fa-! |
r2 sol\p~ |
sol fa |
r8 sib,\f re sib, r fa la do' |
fa'4 r r8 sib,[ re fa8*3/4] \custosNote sib4*1/8 |

