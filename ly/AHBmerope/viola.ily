\clef "alto" r8 sib re' sib r fa la do' |
fa'4 r r8 sib re' fa' |
sib'4 r r8 sib sib'4 |
r8 sib\p sib'4 r8 sib\f sib' fa' |
sol'4 fa' r8 sib re' fa' |
sib'4 r r8 do' do' do' |
do'4 do' r8 fa16 sol la sib do' re' |
mib'4 r r8 fa' fa' fa' |
fa'4 fa' fa' r8 re'' |
do'' la' sib' do'' fa'4 r8 re'' |
do'' la' sib' do'' fa'4 mib' |
re'( do'8) r r la'\p la'4 |
r8 sib' sib'4 r8 fa' fa' do'' |
do'' sib' r4 r8 fa'\f fa'4 |
r8 sol' sol'4 r8 do' do' do' |
do' la' la' fa' re' sib do' do'' |
fa' sib'4 la'8 sib' fa' sib4 |
%% segno
r8 sib\p re' sib r fa la do' |
fa'4 r r8 sib re' fa' |
sib'4 r r8 sib sib'4 |
r8 sib sib'4 r8 sib sib' fa' |
sol'4 fa' r8 sib re' fa' |
sib'4 r r8 do' do' do' |
do'4 do' r8 fa' fa' sol'16 la' |
sib'4 r r8 do' do' do' |
do'4 do' r8 do'16 re' do'8 sib |
la la la la sib sib sib sib |
sib sib sib sib la la la la |
la la la la sol sol sol sol |
sol sol sol sol fa4 r8 fa' |
mi'8 do' re' mi' fa'4 r8 fa' |
mi' do' re' mi' fa' la la la |
sib fa' fa' fa' sol' si si si |
do' sol' sol' sol' sol' mi' mi' mi' |
fa' do' do' do' re'2 |
r8 do'' sol' mi' do' do' do'4 |
r8 re' re'4 r8 do' do'4 |
r8 fa' do' la fa la la4 |
r8 sib sib4 r8 sol sol4 |
r8 do' do'4 r8 do' do'4 |
r8 do' do'4 r8 do' do'4 |
r8 do'16\pocof re' mi' fa' sol' la' sol'4. sol'8 |
la' sib' la' sol' fa'4 mi' |
r8 fa'\f la' fa' r do' mi' sol' |
do''4 r r8 fa la do' |
fa'4 r r8 fa fa'4 |
r8 fa\p fa'4 r8 fa\f fa' do' |
re'4 do' do' r |
r8 sib\p re' sib r fa la do' |
fa'4 r r8 sib re' fa' |
sib'4 r r8 sib sib'4 |
r8 sib sib'4 r8 sib sib'4 |
r8 sib sib sib r mib sol sib |
mib'4 r r8 re' re'4 |
r8 re' re'4 r8 do'16 re' do'8 sib |
la4 r r8 fa' fa'4 |
r8 sib\pocof sib sol' do' fa'16 sol' fa'8 mib' |
re'\p sib sib sib mib' mib' mib' mib' |
mib' mib' mib' mib' fa' fa' fa' fa' |
fa' fa' fa' fa' sol' sol' sol' sol' |
sol' sol' sol' sol' la' la' la' la' |
la' la' la' la' sib'4 r8 sib' |
la' fa' sol' la' sib'4 r8 sib' |
la' fa' sol' la' sib'4 r8 re'\pocof |
mib'4 mi' fa' r |
r8 reb'\p reb'4 r8 do' do'4 |
r8 sib sib4 r8 fa' fa fa' |
r8 re'!-\pocof re'4 r8 do' do'4 |
r8 sib sib4 r8 fa' fa4 |
r8 fa'\p fa'4 r8 fa' fa'4 |
r8 fa' fa' mib' \appoggiatura mib'4 re'2\fermata |
sol'8\f sol' sol' do'' fa' mib' fa' sol' |
fa'4 mib'\trill re'8\ff re' fa' sib' |
r8 fa la do' fa'4 r8 la' |
sib'4 r8 sib' la' fa' sol' la' |
sib'4 r8 sib' la' fa' sol' la' |
sib' re' re' mib' fa' fa' fa' sol' |
fa' sib'4 la'8\trill sib' fa' sib4\fermata |
%%%
r8 sol\p sib sol r8 re re'4 |
r8 fad' sol' la' sol' re' sib sol |
r8 do' mib' do' r sol sol'4 |
r8 si' do'' re'' sol'4 r |
fa'4-!\ff mib'-! r2 |
re'4-! do'-! r2 |
r lab\p |
sol8\f re' re' re' do'\p do' do' fa' |
sol' do' do' do' mi' do' re' mi' |
fa'4 r fa'-!\f fa'-! |
r2 fa'4-! fa'-! |
R1 |
sol2\p fa |
r8 sib\f re' sib r fa la do' |
fa'4 r r8 sib[ re' fa'8*3/4] \custosNote sib'4*1/8 |
