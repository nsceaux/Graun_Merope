\clef "soprano/treble" R1*17 |
sib'4. re''8 re''4 do'' |
r8 do''16[ re'' mib''8] mib'' mib''4 re'' |
r8 re''16[ mib'' fa''8] sol'' \appoggiatura sol''4 fa''4. sol''8 |
\appoggiatura sol''4 fa''4. sol''8 \appoggiatura sol''4 fa''4. sib''8 |
do''8.[( re''32 mib'') re''8] do'' re''16[ do''] sib'8 r4 |
r8 re''16([ mib'' re''8]) mi''8 \appoggiatura mi''4 fa''4. fa''8 |
sol''16([\melisma mi'']) sib'4\melismaEnd sib'8 sib' la' r4 |
r8 re''16([ mib'' re''8]) mi'' \appoggiatura mi''4 fa''4. fa''8 |
sol''16([\melisma mi'']) sib'4\melismaEnd la'8 \appoggiatura la'8 sol'4 r |
do''4 do'' re''16[\melisma do'' sib'8] r16 sib'[ do'' re''] |
mi''[ fa'' sol'' la''] sib''8[ re''] do''16[ sib' la'8] r16 la'16[ sib' do''] |
re''[ mi'' fa'' sol''] la''8[ do''] sib'16[ la' sol'8] r16 sol'[ la' sib'] |
do''[ re'' mi'' fa''] sol''8[ sib'] la'16[ sib' do'' re''] do''4~ |
do''8[ sol''16 fa''] mi''[ re'' do'' sib'] la'[ sib' do'' re''] do''4~ |
do''8[ sol''16 fa''] mi''[ re'' do'' sib'] la'[ sib' do'' re''] mib''4 |
re''16[ do'' sib' la'] sib'4 si'16[ do'' re'' mi''] fa''4 |
mi''16[ re'' do'' si'] do''4 mi''16[ fa'' sol'' la''] sib''4 |
la''16[ sol'' fa'' mi''] fa''8\melismaEnd la'' \appoggiatura la''4 sol''4. fa''8 |
mi''16[ re''] do''8 r4 sol''4. do''8 |
\appoggiatura do''4 si'4. sol''8 sol''4. sib'8 |
sib' la' r4 fa''4. la'8 |
\appoggiatura la'4 sol'4. re''8 re''4. fa'8 |
fa'([ mi']) r8 sib' sib'([ la']) r8 re'' |
re''([ do'']) r fa'' fa''([ mi'']) r la'' |
la''([ sol'']) r4 mi''4. mi''8 |
fa''8.\trill[ mi''32 re'' do''8] sib' la'4 sol'\trill |
fa' r r2 |
R1*4 |
sib'4. re''8 re''4 do'' |
r8 do''16[ re'' mib''8] mib'' mib''4 re'' |
r8 re''16[ mib'' fa''8] sol'' \appoggiatura sol''4 lab''4. sol''8 |
\appoggiatura sol''4 fa''4. mib''8 \appoggiatura mib''4 re''4. do''8 |
\appoggiatura do''4 sib'4. lab'8 lab' sol' r4 |
r8 sib'16([ do'' sib'8]) sol'' \appoggiatura sol''4 fa''4. la'8 |
\appoggiatura la'4 sib'4. fa''8 fa'' mib'' r4 |
r8 do''16[ re'' do''8] mib'' \appoggiatura mib''4 re''4. fa''8 |
fa''8.[ mib''32 fa'' sol''8] sib' \appoggiatura sib'8 la'4 r |
fa''4 lab' sol'16[\melisma lab' sib'8] r16 sib'[ do'' re''] |
mib''[ fa'' sol'' fa''] mib''[ re'' do'' sib'] la'![ sib' do''8] r16[ do'' re'' mib''] |
fa''[ sol'' la'' sol''] fa''[ mib'' re'' do''] sib'[ do'' re''8] r16 re''[ mib'' fa''] |
sol''[ la'' sib'' la''] sol''[ fa'' mib'' re''] do''[ re'' mib''8] r16 mib''[ fa'' sol''] |
la''[ sib'' do''' sib''] la''[ sol'' fa'' mib''] re''[ mib'' fa'' sol''] fa''4~ |
fa''8[ do''16 re''] \appoggiatura fa'' mib''8[ re''16 do''] re''[ mib'' fa'' sol''] fa''4~ |
fa''8[ do''16 re''] \appoggiatura fa''16 mib''8[ re''16 do''] re''[ mib'' fa'' sol''] lab''8\melismaEnd lab'' |
sol''16[\melisma fa'' mib'' re''] do''8\melismaEnd sib' la'!16[ sol'] fa'8 r4 |
fa''4. sib'8 \appoggiatura sib'4 la'4. fa''8 |
fa''4. reb''8 reb'' do'' r4 |
fa''4. sib'8 \appoggiatura sib'4 la'4. fa''8 |
fa''4. re''!8 re''([ do'']) r8 mib'' |
mib''([ re'']) r sol'' sol''([ fa'']) r mib'' |
mib''[ re''] r8 do'' do''4\fermata\melisma sib'4\melismaEnd |
do''16[\melisma re'' mib'' fa''] sol''8\melismaEnd la'' sib''8.[ la''32 sol'' fa''8] mib'' |
re''4 do''\trill sib' r |
R1*5 |
%%%
sol'4. sib'8 sib'4( la'8) mib'' |
mib''4( re''8) do'' sib'16[ la'] sol'8 r4 |
do''4. mib''8 mib''4( re''8) lab'' |
lab''4( sol''8) fa'' mib''16[ re''] do''8 sol''8. do''16 |
lab''4 sol'' r8 do'' do''8. do''16 |
fa''4 mib'' r sol''8. do''16 |
do''4 do'' r do''8. si'16 |
re''4 r sol''4. fa''8 |
\appoggiatura fa''4 mi''4. sol''16([ mi'']) do''4 sib' |
sib'8 la' fa''8. do''16 re''4 do'' |
r8. do''16 fa''8. do''16 re''4 do'' |
r4 fa''8. re''16 sib'4 sib' |
r sib'8. la'16 do''2 |
R1*2 |

