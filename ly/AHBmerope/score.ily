\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violini" } <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Merope } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*4\break s1*4\break s1*4\pageBreak
        \grace s4 s1*4 s2 \bar "" \break s2 s1*3 s2 \bar "" \break \grace s4 s2 s1*3 s2 \bar "" \pageBreak
        \grace s4 s2 s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \break s2 s1*3\pageBreak
        \grace s4 s1*4 s2 \bar "" \break s2 s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break \grace s4 s1*4\break s1*4\pageBreak
        s1*3 s2 \bar "" \break \grace s4 s2 s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*4\break s1*4\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}