\bookpart {
  \paper { systems-per-page = 4 page-count = 5 }
  %% 0-1
  \ouverture "Sinfonia"
  \includeScore "AAAsinfonia"
}
\bookpart {
  \paper { systems-per-page = 4 page-count = 3 }
  \includeScore "AABlarghetto"
}
\bookpart {
  \paper { systems-per-page = 4 page-count = 2 }
  \includeScore "AACallegro"
}

\bookpart {
  \act "ATTO PRIMO"
  \scene "Scena Prima" "Scena I"
  \sceneDescription\markup\column {
    \wordwrap-center { Atrio del Palazzo Reale. }
    \wordwrap-center { Polifonte, Eroce. }
  }
  %% 1-1
  \pieceToc\markup\wordwrap {
    Aria. Polifonte: \italic { Di questo aciaro il lampo }
  }
  \includeScore "AAGpolifonte"
}
\bookpart {
  \scene "Scena Terza" "Scena III"
  \sceneDescription\markup\wordwrap-center {
    Egisto incatenato fra le Guardie, e suddetti.
  }
  %% 1-2
  \pieceToc\markup\wordwrap {
    Aria. Egisto: \italic { Bel desio m’accese il core }
  }
  \includeScore "ACBegisto"
}
\bookpart {
  \paper { systems-per-page = 4 page-count = 8 }
  \scene "Scena Ottava" "Scena VIII"
  \sceneDescription\markup\wordwrap-center {
    Merope, Ismenia, Ericle.
  }
  %% 1-3
  \pieceToc\markup\wordwrap {
    Recit. Merope: \italic { Quanto è barbaro oh stelle! il mio destino! }
  }
  \includeScore "AHAmerope"
  %% 1-4
  \pieceToc\markup\wordwrap {
    Aria. Merope: \italic { Quando a fronte di un periglio }
  }
  \includeScore "AHBmerope"
}
\bookpart {
  \scene "Scena Nona" "Scena IX"
  \sceneDescription\markup\column {
    \wordwrap-center {
      Piazza con Tempio accanto all’ ingresso del quale si vedrà il Mausoleo
      di Cresfonte.
    }
    \wordwrap-center {
      Narbace solo.
    }
  }
  %% 1-5
  \pieceToc\markup\wordwrap {
    Aria. Narbace: \italic { Ah che dovunque movo }
  }
  \includeScore "AIBnarbace"
}
\bookpart {
  \paper { page-count = 3 }
  \scene "Scena Duodecima" "Scena XII"
  \sceneDescription\markup\wordwrap-center {
    Merope, Ismenia, Ericle, Egisto incatenato,
    Guardie, Sacrificatori, poi Narbace.
  }
  %% 1-6
  \pieceToc\markup\wordwrap {
    Arioso. Egisto: \italic { Sì, ti lascio, o Padre mio! }
  }
  \includeScore "ALBegisto"
}
\bookpart {
  \scene "Scena Decimaquarta" "Scena XIV"
  \sceneDescription\markup\column {
    \wordwrap-center { Polifonte col suo Seguito. }
    \wordwrap-center { Merope, Ismenia, Erick. }
  }
  %% 1-7
  \pieceToc\markup\wordwrap {
    Aria. Polifonte: \italic { A consolar quel duolo }
  }
  \includeScore "ANBpolifonte"
}
