\clef "treble" <>
<<
  \tag #'(violino1 violini) {
    s8 sold' si' mi'' s la' dod'' mi'' |
    s sold' si' mi'' s dod'' si' la' |
    s sold' si' mi'' s la' dod'' mi'' |
    s sold' si' mi'' s dod'' la' sold' |
    sold' fad' s4 s16 lad' dod'' mi'' sold'' mi'' red'' dod'' |
    red''8 s s4 s16 lad' dod'' mi'' sold'' mi'' red'' dod'' |
    red''8 mi'' fad'' sold'' s16 lad' dod'' mi'' sold'' mi'' red'' dod'' |
    red''8 dod'' sold''16 mi'' red'' dod'' red''8 mi'' mi'' red'' |
    dod'' dod'' dod'' dod'' s16 lad' dod'' fad'' lad'' fad'' dod'' lad' |
    s16 fad'' s sold'' s sold'' s sold'' s red'' s red'' s dod'' s dod'' |
    s red'' s mi'' s red'' s dod'' s red'' s red'' s dod'' s dod'' |
  }
  \tag #'(violino2 violini) {
    s8 mi' sold' si' s mi' la' dod'' |
    s mi' sold' si' s la' sold' fad' |
    s mi' sold' si' s mi' la' dod'' |
    s mi' sold' si' s la' fad' mi' |
    mi' red' s4 s16 fad' lad' dod'' mi'' dod'' si' lad' |
    si'8 s s4 s16 fad' lad' dod'' mi'' dod'' si' lad' |
    si'8 si' si' si' s16 fad' lad' dod'' mi'' dod'' si' lad' |
    si'8 lad' mi''16 dod'' si' lad' si'8 dod'' dod'' si' |
    lad' lad' lad' lad' s16 fad' lad' dod'' fad'' dod'' lad' fad' |
    s16 si' s si' s si' s si' s si' s si' s lad' s lad' |
    s si' s si' s si' s si' s si' s si' s lad' s lad' |
  }
  { r8 s4.\p ^"pizzic." r8 s4. |
    r8 s4. r8 s4. |
    r8 s4. r8 s4. |
    r8 s4. r8 s4. |
    s4 r4 r16 s8. s4 |
    s8 r r4 r16 s8. s4 |
    s2 r16 s8. s4 |
    s1 |
    s2 r16 s8.\f s4 |
    r16 s\p r s r s r s r s r s r s r s |
    r s r s r s r s r s r s r s r s |
  }      
>>
r16 si\f red' fad' si' fad' red' si
<<
  \tag #'(violino1 violini) {
    s16 lad' dod'' mi'' sold'' mi'' red'' dod'' |
    red''8 red'' red'' red'' red'' red'' red'' red'' |
    mi''16 sold' sold' sold' sold' sold' sold' sold' sold'8 fad' s4 |
    s16 sold' si' mi'' s sold' si' mi'' s la' dod'' mi'' s la' dod'' mi'' |
    s sold' si' mi'' s sold' si' mi'' s re'' s re'' s re'' s re'' |
    s dod'' s dod'' s dod'' s mi'' s mi'' s mi'' s mi'' s mi'' |
    s red'' s red'' s red'' s red'' s dod'' s dod'' s dod'' s dod'' |
    s si' s si' s si' s la'' s sold'' s red'' s mi'' s sid' |
    s dod'' s dod'' s dod'' s dod'' s si' s si' s la' s la' |
    s sold' s sold' s si' s si' dod''8 si' s la'' |
    sold'' red'' mi'' sid' s16 dod'' dod'' dod'' dod'' dod'' la' la' |
    la' la' sold' sold' fad' fad' fad' fad' s16 sold' si' mi'' sold'' mi'' si' sold' |
  }
  \tag #'(violino2 violini) {
    s16 fad' lad' dod'' mi'' dod'' si' lad' |
    si'8 si' si' si' la'! la' la' la' |
    sold'16 mi' mi' mi' mi' mi' mi' mi' mi'8 red' s4 |
    s16 mi' sold' si' s mi' sold' si' s mi' la' dod'' s mi' la' dod'' |
    s mi' sold' si' s mi' sold' si' s si' s si' s si' s si' |
    s la' s la' s la' s la' s dod'' s dod'' s dod'' s dod'' |
    s si' s si' s si' s si' s lad' s lad' s lad' s lad' |
    s fad' s fad' s fad' s fad'' s mi'' s la' s si' s mi' |
    s mi' s mi' s la' s la' s sold' s sold' s fad' s fad' |
    s mi' s mi' s mi' s mi' la'8 sold' s fad'' |
    mi'' la' si' mi' s16 la' la' la' la' la' fad' fad' |
    red' red' mi' mi' mi' mi' red' red' s mi' sold' si' mi'' si' sold' mi' |
  }
  { r16 s8. s4 |
    s1\p |
    s2.\f r4 |
    r16 s8.\p r16 s8. r16 s8. r16 s8. |
    r16 s8. r16 s8. r16 s r s r s r s |
    r16 s r s r s r s r s r s r s r s |
    r16 s r s r s r s r s r s r s r s |
    r16 s r s r s r s r s r s r s r s |
    r16 s r s r s r s r s r s r s r s |
    r16 s r s r s r s s4 r8 s\f |
    s2 r16 s8.\p s4 |
    s2 r16 s8. s4 |
  }
>>
