\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Violini" } <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
      %\new GrandStaff \with { instrumentName = "Violini" } <<
      %  \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
      %  \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      %>>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Egisto } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basso" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break s2 s1*2 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
        s2 s1 s2 \bar "" \break s2 s1*2\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}