\clef "alto" r8 si\p ^"pizzic." mi' sold' r dod' mi' la' |
r si mi' sold' la4 si |
r8 si mi' sold' r dod' mi' la' |
r si mi' sold' la4 la |
r16 si red' fad' si' fad' red' si fad4 fad |
r16 si red' fad' si' fad' red' si fad'4 fad' |
si8 dod' red' mi' fad' fad' fad' fad' |
mid' fad' r8 fad' fad' fad' fad' mid' |
fad' fad' fad' fad' r16 dod'\f fad' lad' dod'' lad' fad' dod' |
r16 red'\p r mi' r mi' r mi' r fad' r fad' r fad' r fad' |
r si r sold' r fad' r mid' fad'8 fad' fad fad |
r16 si\f red' fad' si' fad' red' si fad'8 fad' fad' fad' |
fad'\p fad' fad' fad' fad' fad' fad' fad' |
lad16\f lad lad lad lad lad lad lad si8 si r4 |
r16 si\p red' fad' r si red' fad' r dod' mi' la' r dod' mi' la' |
r si mi' sold' r si mi' sold' r sold' r sold' r sold' r sold' |
r mi' r mi' r mi' r mi' r fad' r fad' r fad' r fad' |
r fad' r fad' r fad' r fad' r mi' r mi' r mi' r mi' |
r red' r red' r red' r si r mi' r fad' r sold' r sold |
r la r la r mi' r mi' r mi' r mi' r red' r red' |
r mi' r mi' r sold r sold la8 mi' r si\f |
mi' fad' sold' sold la\p la la la |
si si si si mi4 r |
