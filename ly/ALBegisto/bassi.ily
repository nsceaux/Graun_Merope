\clef "bass" mi4\p ^"pizzic." mi mi mi |
mi mi la, si, |
mi mi mi mi |
mi mi la, la, |
r16 si, red fad si fad red si, fad,4 fad, |
r16 si, red fad si fad red si, fad4 fad |
si,8 dod red mi fad fad fad fad |
mid fad r fad si lad si mid |
r16 fad\f lad dod' mi' dod' lad fad mi8 mi mi mi |
red\p mi mi mi fad fad fad fad |
si, sold fad mid fad fad fad, fad, |
r16 si,\f red fad si fad red si, fad8 fad fad fad |
r16 si,\p red fad si fad red si, r si, red fad si fad red si, |
lad,8\f lad, lad, lad, r16 si, red fad si la? sold fad |
mi8\p mi mi mi mi mi mi mi |
mi mi mi mi mi mi mi mi |
la, la, la, la, lad, lad, lad, lad, |
si, si, si, si, si, si, si, si, |
si, si, si, red mi fad sold sold |
la la la la si si si, si, |
mi mi sold, sold, la, mi, r red\f |
mi fad sold sold, la,\p la, la, la, |
si, si, si, si, mi,4 r |
