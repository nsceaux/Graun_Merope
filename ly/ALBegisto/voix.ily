\clef "soprano/treble" si'4 r8. mi''16 mi''8 dod'' r8. mi''16 |
mi''8 si' r mi'' mi''8. red''32[ dod''] si'8[ la']\trill |
sold'4 r dod''4. mi''16[ dod''] |
\appoggiatura dod''4 si'4. mi''8 mi''[~ mi''32\melisma red'' dod'' si'] la'8\melismaEnd sold' |
sold' fad' r4 dod''~ dod''8[ red''16] mi'' |
red''4 r dod''~ dod''8[ red''16] mi'' |
red''16.[ mi''32] mi''16.[ fad''32] fad''8[\trill sold''16.] si'32 lad'4 r8. dod''16 |
\grace { dod''16.[ mi''32] } red''8 dod'' r dod'' red''16( fad''8) mi''16 \appoggiatura mi''8 red''4 |
dod'' r dod''4. dod''8 |
fad'' sold''~ sold''8[ fad''32 mi'' red'' dod''] si'8.[ lad'32 si' dod''8.] mi'16 |
red'8 mi'' \appoggiatura mi''16 red''8 \appoggiatura red''16 dod''8 red''32[\melisma si'16. fad''32 red''16.] mi''8.\melismaEnd dod''16 |
si'4 r r2 |
fad''8 si' r8. fad''16 fad''[ la'] la'8 r4 |
mi''8.[ sold''32 fad''] mi''8. sold'16 sold'8 fad' r4 |
si'4. mi''8 mi''16 dod'' dod''8 r8. mi''16 |
mi''[ si'] si'8 r si'16 dod'' re''8 re'' re''16[ dod''32 re'' mi''16.] re''32 |
dod''8 r r dod''16. red''!32 mi''8 mi'' mi''16[ red''32 mi'' fad''16.] mi''32 |
red''8 r r fad'' fad''16 lad' lad'8 r mi'' |
red''16[ dod''] si'8 r fad''16 la'' sold''16.[ red''32] red''16.[ mi''32] mi''16.[ sid'32] sid'16.[ dod''32] |
dod''4~ dod''16[ red''32 mi''] red''[ dod'' si' la'] sold'8.[ si'16] \appoggiatura si'8 la'4 |
sold'8 r mi''8. mi''16 dod''8 si' r fad''16 la'' |
sold''16.[ red''32] red''16.[ mi''32] mi''16.[ sid'32] sid'16.[ dod''32] dod''4. la'8 |
la'8[ sold'16*2/3 fad' mi'] \appoggiatura mi'8 fad'4\trill mi' r |
