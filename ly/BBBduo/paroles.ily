\tag #'egisto {
  Cessa o -- mai di quel ti -- ran -- no
  di cal -- ma -- re
  di cal -- ma -- re il reo fu -- ror
  il reo fu -- ror il reo fu -- ror.
}
\tag #'merope {
  Puoi ve -- der, se ma -- dre io so -- no
  dall’ a -- cer -- bo mio do -- lor
  dall’ a -- cer -- bo mio do -- lor.
  Puoi ve -- der puoi ve -- der
  dall’ a -- cer -- bo mio do -- lor.
}
\tag #'egisto {
  Deh m’af -- fer -- mi per __ tuo fi -- glio
  la gran -- dez -- za
  la gran -- dez -- za
  la gran -- dez -- za del tuo cor
  la gran -- dez -- za del tuo cor
  la gran -- dez -- za del tuo cor.
}
\tag #'merope {
  Ah che ob -- blio nel tuo pe -- ri -- glio
  del mio gra -- do
  ah che ob -- bli -- o del mio gra -- do o -- gni splen -- dor
  o -- gni splen -- dor!
}
\tag #'egisto {
  Qual or -- ror! qual gior -- no è que -- sto!
  qual gior -- no è que -- sto
  qual gior -- no è que -- sto è que -- sto!
  e re -- spi -- ro, o stel -- le an -- cor!
  Qual or -- ror! qual gior -- no è que -- sto!
  qual gior -- no è que -- sto
  qual or -- ror qual gior -- no è que -- sto!
  e __ re -- spi -- ro,
  e __ re -- spi -- ro,
  e __ re -- spi -- ro, o stel -- le an -- cor __
  o stel -- le an -- cor
  re -- spi -- ro o stel -- le an -- cor!
}
\tag #'merope {
  Qual or -- ror! qual gior -- no è que -- sto!
  qual gior -- no è que -- sto
  qual gior -- no è que -- sto!
  e re -- spi -- ro, o stel -- le an -- cor!
  Qual or -- ror! qual gior -- no è que -- sto!
  qual gior -- no è que -- sto
  qual or -- ror qual gior -- no è que -- sto!
  e __ re -- spi -- ro,
  e __ re -- spi -- ro,
  e __ re -- spi -- ro, o stel -- le an -- cor __
  o stel -- le an -- cor __
  o stel -- le an -- cor!
}
\tag #'merope {
  De’ tuoi Re tu ve -- di in lu -- i
  il sol ger -- me, che an -- cor re -- sta;
  e il de -- stin de’ gior -- ni su -- i
  da te sol da te sol da te sol di -- pen -- de -- rà,
  di -- pen -- de -- rà
  da te sol di -- pen -- de -- rà
}
\tag #'egisto {
  Se del san -- gue io son d’Al -- ci -- de
  i pe -- ri -- gli non pa -- ven -- to
  i pe -- ri -- gli non pa -- ven -- to.
  Il __ va -- lor, ch’in me __ ri -- sen -- to,
  scin -- til -- la -- re an -- cor sa -- prà
  an -- cor sa -- prà an -- cor sa -- prà.
}
\tag #'egisto {
  Giu -- sti Nu -- mi di -- fen -- de -- te,
  ven -- di -- ca -- te, pro -- teg -- ge -- te
  l’in -- no -- cen -- za e la pie -- tà!
  di -- fen -- de -- te,
  ven -- di -- ca -- te, pro -- teg -- ge -- te
  l’in -- no -- cen -- za e la pie -- tà __
  e la pie -- tà __ e la pie -- tà!
}
\tag #'merope {
  Giu -- sti Nu -- mi di -- fen -- de -- te,
  ven -- di -- ca -- te, pro -- teg -- ge -- te
  l’in -- no -- cen -- za e la pie -- tà!
  Giu -- sti Nu -- mi di -- fen -- de -- te,
  ven -- di -- ca -- te,
  l’in -- no -- cen -- za e la pie -- tà __
  e la pie -- tà __ e la pie -- tà!
}
