\clef "treble" R2*15 |
r4 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { la'16. re''32 |
    \appoggiatura re''8 dod''8. mi''16 \appoggiatura mi''8 re''8. fad''16 |
    la'8( sol')\trill fad' la'16.[ do''32] |
    si'16.( red''32) red''16.( mi''32) mi''16.( fad''32 sol''16) \appoggiatura fad''16 mi'' |
    \appoggiatura re''8 dod''4 fad''8. mi''16 |
    red''8. fad''32 mi'' re''16. dod''64 re'' mi''16 re'' |
    \appoggiatura re''16 dod''8 mi''16. fad''32 \appoggiatura la'16 sold'8 mi''16. fad''32 |
    \appoggiatura la'16 sold'8 si'16( dod'') re''( mi'' fad'' sold'') |
    la'' re'' dod'' si' la'8 si'\trill |
    la'4 }
  { fad'16. fad'32 |
    \appoggiatura fad'8 mi'8. sol'16 \appoggiatura sol'8 fad'8. la'16 |
    fad'8( mi') re' re'~ |
    re'16.( fad'32) fad'16.( sol'32) sol'16.( la'32 si'16) \appoggiatura la'16 sold' |
    \appoggiatura fad'8 mi'4 r16 fad'( lad' dod'') |
    \override Script.avoid-slur = #'inside
    fad'16(-. fad'-. fad'-. fad'-.) si'-.( si'-. si'-. si'-.) |
    \appoggiatura si' la'8 la'16. re''32 \appoggiatura dod''16 si'8 la'16. re''32 |
    \appoggiatura dod''16 si'8 sold'16( la') si'( dod'' re'') si' |
    la'4. sold'8\trill |
    la'4
  }
>> r4 |
R2*15 |
r4 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { dod''16. re''32 |
    mi''16 mi''8 mi''16 mi''16.( fad''64 sol'') fad''16 mi'' |
    \appoggiatura mi''16 re''8 dod'' }
  { la'16. si'32 |
    dod''16 dod''8 dod''16 dod''16.( re''64 mi'') re''16 dod'' |
    \appoggiatura dod''16 si'8 lad' }
>> r16 fad'( si' re'') |
r16 sol'( dod'' mi'') r la'( red'' fad'') |
r si'( mi'' sol'') r \twoVoices #'(flauto1 flauto2 flauti) <<
  { si''16( sol'' mi'') |
    \appoggiatura re''8 dod''8. dod''16 \appoggiatura dod''8 re''8. re''16 |
    \appoggiatura re''8 dod''8. mi''16 \appoggiatura mi''8 re''4 |
    dod'' }
  { sol''16( mi'' dod'') |
    \appoggiatura si'8 lad'8. lad'16 \appoggiatura lad'8 si'8. si'16 |
    \appoggiatura si'8 lad'8. dod''16 \appoggiatura dod''8 si'4 |
    lad' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''2 |
    sol''~ |
    sol''8 sol'' fad'' mid'' |
    mid''( fad'') fad''4~ |
    fad''8 fad'' mi'' red'' |
    red''8( mi'') mi''4~ |
    mi''8 sol'' fad'' mi'' |
    \appoggiatura mi''4 re''2 |
    dod''4 }
  { si'2~ |
    si'8 re'' dod'' sid' |
    sid' dod'' dod''4~ |
    dod''8 dod'' si' lad' |
    lad'? si' si'4~ |
    si'8 si' lad' sold' |
    lad' mi'' re'' dod'' |
    \appoggiatura dod''4 si'2 |
    lad'4 }
>> r4 |
R2*4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { si'4. mi''8 |
    mi'' red'' r la'' |
    la''4( sol''8) fad'' |
    sol''2 |
    sol'' |
    fad''~ |
    fad''8 fad'' sol'' fad'' |
    fad'' mi'' mi''4~ |
    mi''8 mi'' fad'' mi'' |
    mi'' re'' re'' re'' |
    dod'' }
  { r8 mi' mi'4 |
    r8 la' la' fad'' |
    fad''4( mi''8) red'' |
    mi''4 si' |
    do''2~ |
    do''8 do'' si' lad' |
    lad' si' si'4~ |
    si'8 si' dod'' si' |
    si' lad' lad'4~ |
    lad'8 si' si' si' |
    lad' }
>> fad''8 re'' si' |
fad'' fad' fad' fad' |
fad'4 r |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { dod''8.( re''16 mi''8) mi'' | mi'' re'' }
  { lad'8.( si'16 dod''8) dod'' | dod'' si' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8.( mi''16 fad''8) fad'' | fad'' mi'' }
  { si'8.( dod''16 re''8) re'' | re'' dod'' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''8.( fad''16 sol''8) sol'' | sol'' fad'' }
  { dod''8.( re''16 mi''8) mi'' | mi'' re'' }
>> r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''8 |
    re''8.(\trill dod''32 re'' mi''8) dod'' |
    re''8.(\trill dod''32 re'' mi''8) dod'' |
    re''8.(\trill dod''32 re'' mi''8) dod'' |
    si''8.( la''32 sol'' fad''8) mi'' |
    \appoggiatura mi''16 re''8 dod''16 si' dod''4\trill |
    si'8 si''4 si''8~ |
    si'' re''4 re''8~ |
    re'' si''4 si''8 |
    lad''8-! sol''-! fad''-! mi''-! |
    \appoggiatura mi''16 re''8 dod''16 si' dod''4\trill |
    si'4 }
  { dod''8 |
    si'8.(\trill lad'32 si' dod''8) lad' |
    si'8.(\trill lad'32 si' dod''8) lad' |
    si'8.(\trill lad'32 si' dod''8) lad' |
    re''8 mi'' re'' dod'' |
    si'4 lad'\trill |
    si'8 re''4 re''8~ |
    re'' si'4 si'8~ |
    si' re''4 re''8 |
    dod''-! mi''-! re''-! dod''-! |
    si'4 lad'\trill |
    si'4 }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''16.( mi''32 re''8) re'' |
    re''16.( mi''32 re''8) re'' |
    re''16.( dod''64 re'' mi''8) re'' |
    dod''16.[( re''64 mi'') mi''8] }
  { si'16.( dod''32 si'8) si' |
    si'16.( dod''32 si'8) si' |
    si'16.( la'64 si' dod''8) si' |
    la'16.[( si'64 dod'') dod''8] }
>> r8 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do''16.([ re''32 do''8]) do'' |
    do''16.([ re''32 do''8]) do'' |
    do''16.([ si'64 do'']) re''8 do'' |
    si'16.[( do''64 re'')] re''8 }
  { la'16.([ si'32 la'8]) la' |
    la'16.([ si'32 la'8]) la' |
    la'16.[ sol'64 la'] si'8 la' |
    sol'16.[ la'64 si'] si'8 }
>> r8 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''8 \appoggiatura re'' do''4 |
    fad''8 \appoggiatura mi'' re''4 |
    sol''8( do'') si' |
    si'[ la'] }
  { sol'8 la'4~ |
    la'8 si'4~ |
    si'8( la') sol' |
    sol'[ fad'] }
>> r8 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8 re''8.( mi''16) |
    \appoggiatura re''8 dod''!4 r8 |
    do''8 do''8. re''16 |
    \appoggiatura do''8 si'4 r8 |
    mi'8( mi'') \grace re''16 do''8 |
    \appoggiatura si' la'4 re''8 |
    \appoggiatura fa''16 mi'' re''32 do'' \appoggiatura si'8 la'4\trill |
    sol'16[ re''] re''16.[( mi''32 re''16) do''] |
    si'8 re''16.[( mi''32 re''16) do''] |
    si'16[ sol''] sol''8[ fad''16*2/3( mi'' fad'')] |
    fad''8([ mi'']) }
  { fad'8 fad' sold'~ |
    sold' la' sol'\trill |
    fad' fad'4~ |
    fad'8 sol' fad'\trill |
    mi'( do'') \grace si'16 la'8 |
    \appoggiatura sol'8 fad'4 sol'8~ |
    sol' \appoggiatura sol' fad'4\trill |
    sol'16[ si'] si'16.[( do''32 si'16) la'] |
    sol'16[ si'] si'16.[( do''32 si'16) la'] |
    sol'16[ mi''] mi''8[ re''16*2/3( dod''! re'')] |
    re''8([ dod'']) }
>> r8 |
R2*24 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { la''4. do''8 | do'' si' }
  { do''4. la'8 | la' sol' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { si''4. re''8 | re'' dod'' }
  { re''4. si'8 | si' la' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { dod'''4. mi''8 | mi'' re'' }
  { mi''4. dod''8 | dod'' si' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi'''4. sol''8 | sol'' fad'' }
  { sol''4. mi''8 | mi'' re'' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''8 mi''4( fa''8) |
    mi'' mi''4( fa''8) |
    mi'' mi''4( fa''8) |
    fa''( mi'') }
  { dod''8 dod''4( re''8) |
    dod''8 dod''4( re''8) |
    dod''8 dod''4( re''8) |
    re''8 dod'' }
>> r4 |
R2 |
r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8 la'' | \appoggiatura la''16 sol''8 fad'' }
  { la'8 re'' | dod'' re'' }
>> r4 |
r \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8 la'' | \appoggiatura la''16 sol''8 fad'' }
  { la'8 re'' | dod'' re'' }
>> r4 |
r \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8 la'' | \appoggiatura la''16 sol''8 fad'' }
  { la'8 re'' | dod'' re'' }
>> r4 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''8 fad''4( mi''8) |
    sol''8 fad''4( mi''8) |
    sol''8 fad''4( mi''8) |
    fad''8.(\trill sol''16 la''8) sol'' |
    fad''32( mi'' re''8.) mi''4\trill |
    fad''8.(\trill sol''16 la''8) sol'' |
    fad''32( mi'' re''8.) mi''4\trill |
    re''8 re'''4 re'''8~ |
    re''' fad''4 fad''8~ |
    fad'' re'''4 re'''8 |
    dod'''8-! si''-! la''-! sol''-! |
    fad''32( mi'' re''8.) mi''4\trill | }
  { mi''8 re''4( dod''8) |
    mi''8 re''4( dod''8) |
    mi''8 re''4( dod''8) |
    re''8.(\trill mi''16 fad''8) mi'' |
    re''4 dod''\trill |
    re''8.(\trill mi''16 fad''8) mi'' |
    re''4 dod'' |
    re''8 fad''4 fad''8~ |
    fad'' re''4 re''8~ |
    re'' fad''4 fad''8 |
    mi''8-! sol''-! fad''-! mi''-! |
    re''4 dod''\trill | }
>>
re''8 la'16 fad' re'8 re' |
re'4 r |
