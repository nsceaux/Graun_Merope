\clef "soprano/treble" <<
  %% Merope
  \tag #'(merope) {
    R2*15 |
    r4 r8 la'16. re''32 |
    \appoggiatura re''8 dod''8. mi''16 \appoggiatura mi''8 re''8. fad''16 |
    la'8([ sol'])\trill fad' la'16. do''32 |
    si'16.([ red''32]) red''16.([ mi''32]) mi''16.([ fad''32 sol''16]) \appoggiatura fad''16 mi'' |
    \appoggiatura re''8 dod''4 fad''8. mi''16 |
    red''8. fad''32[ mi''] re''16.[ dod''64 re'' mi''16] re'' |
    \appoggiatura re'' dod''8 mi''16. fad''32 \appoggiatura la'16 sold'8 mi''16. fad''32 |
    \appoggiatura la'16 sold'8 si'16 dod'' re''([ mi'' fad'' sold''] |
    la'') re''16[ dod'' si'] la'8 si'\trill |
    la'4 r |
    R2*15 |
    r4 r8 dod''16. re''32 |
    mi''8 mi'' mi''16.[ fad''64 sol'' fad''16] mi'' |
    \appoggiatura mi''16 re''8 dod'' si'8. re''16 |
    re'' dod'' r8 red''8. fad''16 |
    fad'' mi'' r8 sol''[ \appoggiatura fad''16 mi''] \appoggiatura re'' dod'' |
    \appoggiatura si'8 lad'8. dod''16 \appoggiatura dod''16 re''8. re''16 |
    \appoggiatura re''8 dod''8. mi''16 \appoggiatura mi''8 re''8. re''16 |
    dod''8 r r4 |
    si' si' |
    sol'' r |
    r8 sol'' fad'' mid'' |
    mid''[ fad''] fad''4 |
    r8 fad'' mi'' red'' |
    red''[ mi''] mi''4 |
    r8 sol'' fad'' mi'' |
    \appoggiatura mi''4 re''2 |
    dod''4 r |
    R2*4 |
    si'4. mi''8 |
    mi'' red'' r la'' |
    la''4( sol''8) fad'' |
    sol''4 r |
    r sol''8. sol''16 |
    fad''4 r |
    r8 fad'' sol'' fad'' |
    fad'' mi'' r4 |
    r8 mi'' fad'' mi'' |
    mi'' re'' re''8. re''16 |
    dod''8 fad'' re'' si' |
    fad'' fad' r4 |
    R2 |
    dod''8.([ re''16 mi''8]) mi'' |
    mi'' re'' r4 |
    re''8.([ mi''16 fad''8]) fad'' |
    fad'' mi'' r4 |
    mi''8.([ fad''16 sol''8]) sol'' |
    sol'' fad'' r mi'' |
    re''8.[\trill dod''32 re'' mi''8] dod'' |
    re''8.[\trill dod''32 re'' mi''8] dod'' |
    re''8.[\trill dod''32 re'' mi''8] dod'' |
    si''8.[( la''32 sol'') fad''8] mi'' |
    \appoggiatura mi''16 re''8[ dod''16 si'] dod''4\trill |
    si' r |
    R2*5 |
    re''16.[ mi''32 re''8] re'' |
    re''16.[ mi''32 re''8] re'' |
    re''16.([ dod''64 re'' mi''8]) re'' |
    dod''16.([ re''64 mi'']) mi''8 r |
    do''16.([ re''32 do''8]) do'' |
    do''16.([ re''32 do''8]) do'' |
    do''16.([ si'64 do'' re''8]) do'' |
    si'16.([ do''64 re'']) re''8 r |
    mi'' \appoggiatura re''8 do''4 |
    fad''8 \appoggiatura mi''8 re''4 |
    sol''8([ do'']) si' |
    si' la' r |
    re'' re''8.([ mi''16]) |
    \appoggiatura re''8 dod''!4 r8 |
    do''8 do''8.[ re''16] |
    \appoggiatura do''8 si'4 r8 |
    mi''8[ re''16 do''] si'[ la'] |
    \appoggiatura sol'8 fad'4 re''8 |
    \appoggiatura fa''16 mi''[ re''32 do''] \appoggiatura si'8 la'4\trill |
    sol'16 re'' re''16.[ mi''32 re''16] do'' |
    si'8 re''16.[ mi''32 re''16] do'' |
    si' mi'' mi''8[ \tuplet 3/2 { re''16 dod''!] re'' } |
    re''8([ dod'']) r |
    R2*24 |
    do''4. do''8 |
    do'' si' r4 |
    re''4. re''8 |
    re'' dod'' r4 |
    mi''4. mi''8 |
    mi''8 re'' r4 |
    sol''4. sol''8 |
    sol''8 fad'' r4 |
    mi''8 mi''4( fa''8) |
    mi''8 mi''4( fa''8) |
    mi''8 mi''4( fa''8) |
    fa''([ mi'']) r4 |
    re''4. re''8 |
    \appoggiatura fad''16 mi''8 re'' r4 |
    r re''8 re'' |
    \appoggiatura fad''16 mi''8 re'' r4 |
    r re''8 re'' |
    \appoggiatura fad''16 mi''8 re'' r4 |
    R2 |
    sol''8 fad''4( mi''8) |
    sol''8 fad''4( mi''8) |
    sol''8 fad''4( mi''8) |
    |
    fad''8.([\trill sol''16 la''8]) sol'' |
    fad''32[ mi'' re''8.] mi''4\trill |
    fad''8.([\trill sol''16 la''8]) sol'' |
    fad''32[ mi'' re''8.] mi''4\trill |
    re''4 r |
    R2*6 |
  }
  %% Egisto
  \tag #'(egisto) {
    la'4 la' |
    si'16([ dod''] re''4) re''8 |
    mi''8.([ fad''32 sol'' fad''8]) mi'' |
    fad'' mi'' r4 |
    re''4 fad''16([ mi'' re'' dod'']) |
    si'([ re'' dod'' mi'']) re''8 r |
    re''4 fad''16([ mi'' re'' dod'']) |
    si'[\melisma dod'' re'' mi''] fad''[ sol'' la'' si''] |
    \appoggiatura re''8 dod''4\melismaEnd re''8 sol'' |
    fad''32([ mi'' re''8.]) mi''4\trill |
    re'' re'' |
    sol' la' |
    si' re'' |
    sol' la' |
    re' r |
    R2*9 |
    la'4 mi'' |
    fad''16([ sold''] la''4) la'8 |
    si'8.([ dod''32 re'' dod''8]) si' |
    dod'' si' r4 |
    mi''4 re''16([ dod'' si' la']) |
    fad''[ mi''] re''8 r4 |
    fad''4 mi''16[ red'' dod'' si'] |
    sold''[ fad''] mi''8 r4 |
    la'8 si'16[\melisma dod''] re''[ mi'' fad'' sold'']\melismaEnd |
    la''8.[\trill sold''32 fad'' mi''8] re'' |
    dod''32[ si' la'8.] si'4\trill |
    la'4 mi''8 la' |
    si' fad'' mi'' sold' |
    la'4 mi''8 la' |
    si' fad'' mi'' sold' |
    la'4 r |
    R2*7 |
    fad'4 fad' |
    re'' r |
    r8 re'' dod'' sid' |
    sid'[ dod''] dod''4 |
    r8 dod'' si' lad' |
    lad'[ si'] si'4 |
    r8 si' lad' sold' |
    lad'[ mi''] re'' dod'' |
    \appoggiatura dod''4 si'2 |
    lad'4 r |
    fad'4. si'8 |
    si' lad' r mi'' |
    mi''4( re''8) dod'' |
    re''4 r |
    R2*3 |
    r4 si'8. si'16 |
    do''4 r |
    r8 do'' si' lad' |
    lad' si' r4 |
    r8 si' dod'' si' |
    si' lad' r4 |
    r si'8. si'16 |
    lad'8 fad'' re'' si' |
    fad'' fad' r4 |
    R2 |
    lad'8.([ si'16 dod''8]) dod'' |
    dod'' si' r4 |
    si'8.([ dod''16 re''8]) re'' |
    re'' dod'' r4 |
    dod''8.([ re''16 mi''8]) mi'' |
    mi'' re'' r dod'' |
    si'8.[\trill lad'32 si' dod''8] lad' |
    si'8.[\trill lad'32 si' dod''8] lad' |
    si'8.[\trill lad'32 si' dod''8] lad' |
    re''8 mi'' re'' dod'' |
    si'4 lad' |
    si' r |
    R2*5 |
    R4.*23 |
    la'4 la' |
    si'16([ dod''] re''4) re''8 |
    mi''8.([ fad''32 sol'' fad''8]) mi'' |
    fad'' mi'' r4 |
    re''4 fad''16([ mi'' re'' dod'']) |
    si'([ re'' dod'' mi'']) re''8 r |
    re''4 fad''16([ mi'' re'' dod'']) |
    si'([ re'' dod'' mi'']) re''8 r |
    sol'' fad''4( mi''8) |
    fad''16([ mi'' re'' dod''] si'8) fad'' |
    mi''16([ re'' dod'' si'] la'8) mi'' |
    re'' dod'' r4 |
    la'4( dod''8) mi'' |
    la''4( mi''8) dod'' |
    la'4( dod''8) mi'' |
    la''4( mi''8) dod'' |
    la'4( dod''8) mi'' |
    la''8.[\trill sold''32 fad'' mi''8] re'' |
    dod''4 si'\trill |
    la'4 r |
    r8 fad'' mi'' sold' |
    la'4 r |
    r8 fad'' mi'' sold' |
    la'4 r |
    la'4. la'8 |
    la'8 sol' r4 |
    si'4. si'8 |
    si' la' r4 |
    dod''4. dod''8 |
    dod'' si' r4 |
    mi''4. mi''8 |
    mi'' re'' r4 |
    dod''8 dod''4( re''8) |
    dod''8 dod''4( re''8) |
    dod''8 dod''4( re''8) |
    re''[ dod''] r4 |
    R2 |
    r4 re''8 la' |
    sol' fad' r4 |
    r re''8 la' |
    sol' fad' r4 |
    r re''8 la' |
    sol' fad' r4 |
    mi''8 re''4( dod''8) |
    mi''8 re''4( dod''8) |
    mi''8 re''4( dod''8) |
    re''8.[\trill mi''16 fad''8] mi'' |
    re''4 dod''\trill |
    re''8.[\trill mi''16 fad''8] mi'' |
    re''4 dod'' |
    re''4 r |
    R2*6 |
  }
>>
