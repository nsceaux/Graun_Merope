\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Flauti" } <<
        \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Merope
      \consists "Metronome_mark_engraver"
    } \withLyrics <<
      \global \keepWithTag #'merope \includeNotes "voix"
    >> \keepWithTag #'merope \includeLyrics "paroles"
    \new Staff \with { instrumentName = \markup\character Egisto } \withLyrics <<
      \global \keepWithTag #'egisto \includeNotes "voix"
    >> \keepWithTag #'egisto \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s2*6\pageBreak
        s2*7\break s2*6\pageBreak
        \grace s8 s2*5\break s2*7\pageBreak
        s2*6 s4 \bar "" \break s4 s2*4 s4 \bar "" \pageBreak
        s4 s2*4 s4 \bar "" \break s4 s2*5\pageBreak
        s2*7\break s2*7\pageBreak
        s2*6\break s2*7\pageBreak
        s2*7\break s2*4 s4.*3\pageBreak
        s4.*8\break s4.*8\pageBreak
        s4.*4 s2*2\break s2*7\pageBreak
        s2*7\break s2*7\pageBreak
        s2*7\break s2*8\pageBreak
        \grace s16 s2*8\break s2*7\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}