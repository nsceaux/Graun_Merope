\clef "treble"
\twoVoices #'(violino1 violino2 violini) <<
  { la'8 la' la' la' |
    si'16( dod'' re''4) re''8 |
    mi''8.( fad''32 sol'') fad''8 mi'' |
    fad'' mi'' }
  { fad'8 fad' mi' mi' |
    re'4 re''~ |
    re''4. dod''8 |
    re'' dod'' }
>> r4 |
\twoVoices #'(violino1 violino2 violini) <<
  { re''4 fad''16( mi'' re'' dod'') |
    si'( re'' dod'' mi'') re''8 }
  { la'8 la' la' la' |
    re' mi' fad' }
>> r8 |
\twoVoices #'(violino1 violino2 violini) <<
  { re''4 fad''16( mi'' re'' dod'') |
    si'( dod'' re'' mi'') fad''( sol'' la'' si'') |
    \appoggiatura re''8 dod''4 re''8 sol'' |
    fad''32( mi'' re''8.) mi''4\trill |
    re''16 fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re''\pocof fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    fad''\f re'' la' fad' re'8 re' |
    re'4 }
  { la'8 la' la' la' |
    re'4 r |
    r8 la' la' mi'' |
    re''4 dod''\trill |
    re''16 re'' re'' re'' re'' re'' re'' re'' |
    re'' re'' re'' re'' dod'' dod'' dod'' dod'' |
    re''\pocof re'' re'' re'' re'' re'' re'' re'' |
    re'' re'' re'' re'' dod'' dod'' dod'' dod'' |
    re''\f re'' la' fad' re'8 re' |
    re'4 }
>> r4 |
R2*8 |
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { la'4 mi'' |
    fad''16( sold'' la''4) la'8 |
    si'8.( dod''32 re'') dod''8 si' |
    dod'' si' }
  { dod'8 dod' si si |
    la4 la'~ |
    la'4. sold'8 |
    la' sold' }
>> r4 |
\twoVoices #'(violino1 violino2 violini) <<
  { mi''4 re''16( dod'' si' la') |
    fad''\f re'' la' fad' re'8 }
  { la'8 la' la' la' |
    re''8\f la'16 fad' re'8 }
>> r8 |
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { fad''4 mi''16( red'' dod'' si') |
    sold''16\f mi'' si' sold' mi'8 }
  { si'8 si' si' si' |
    si'16\f mi'' si' sold' mi'8 }
>> r8 |
<>\p la'8 si'16 dod'' re'' mi'' fad'' sold'' |
la''8.(\trill sold''32 fad'') \twoVoices #'(violino1 violino2 violini) <<
  { mi''8 re'' |
    dod''4 si'\trill |
    la'16 dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    si' si' si' si' si' si' si' si' |
    si' dod''\pocof dod'' dod'' dod'' dod'' dod'' dod'' |
    si' si' si' si' si' si' si' si' |
    dod''\f la' mi' dod' la8 la |
    la4 }
  { mi''8 si' |
    la'4 sold'\trill |
    la'16 la' la' la' la' la' la' la' |
    la' la' la' la' sold' sold' sold' sold' |
    la' la'\pocof la' la' la' la' la' la' |
    la' la' la' la' sold' sold' sold' sold' |
    la'8\f mi'16 dod' la8 la |
    la4 }
>> r4 |
R2*6 |
fad'16\p fad' fad' fad' fad' fad' fad' fad' |
\twoVoices #'(violino1 violino2 violini) <<
  { re''16 re'' re'' re'' re'' re'' re'' re'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol'' sol'' sol'' sol'' fad'' fad'' mid'' mid'' |
    mid'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' fad'' fad'' fad'' mi'' mi'' red'' red'' |
    red'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' sol'' sol'' fad'' fad'' mi'' mi'' |
    \appoggiatura mi''4 re''2 |
    dod''4 }
  { si'16 si' si' si' si' si' si' si' |
    si' si' re'' re'' dod'' dod'' sid' sid' |
    sid' dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    dod'' dod'' dod'' dod'' si' si' lad' lad' |
    lad' si' si' si' si' si' si' si' |
    si' si' si' si' lad' lad' sold' sold' |
    lad' lad' mi'' mi'' re'' re'' dod'' dod'' |
    \appoggiatura dod''4 si'2 |
    lad'4 }
>> r4 |
\twoVoices #'(violino1 violino2 violini) <<
  { fad'4. si'8 |
    si' lad' r mi'' |
    mi''4( re''8) dod'' |
    re''4 }
  { r8 si si4 |
    r8 mi' mi' dod'' |
    dod''4( si'8) lad' |
    si'4 }
>> r4 |
R2*3 |
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { sol''16 sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' fad'' fad'' fad'' sol'' sol'' fad'' fad'' |
    fad'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' fad'' fad'' mi'' mi'' |
    mi''\pocof re'' re'' re'' re'' re'' re'' re'' |
    dod'' dod'' fad'' fad'' }
  { si'16 si' si' si' si' si' si' si' |
    do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' lad' lad' |
    lad' lad' si' si' si' si' si' si' |
    si' si' si' si' dod''! dod'' si' si' |
    si' lad' lad' lad' lad' lad' lad' lad' |
    lad'\pocof si' si' si' si' si' si' si' |
    lad' lad' fad'' fad'' }
>> re''16 re'' si' si' |
fad''\f fad' fad' fad' fad' fad' fad' fad' |
fad'4 r |
R2 |
r8 <>\p \twoVoices #'(violino1 violino2 violini) <<
  { re''16.( mi''32 re''8) re'' | }
  { si'16.( dod''32 si'8) si' | }
>>
R2 |
r8 \twoVoices #'(violino1 violino2 violini) <<
  { mi''16.( fad''32 mi''8) mi'' | }
  { dod''16.( re''32 dod''8) dod'' | }
>>
R2 |
r8 \twoVoices #'(violino1 violino2 violini) <<
  { fad''16.( sol''32 fad''8) mi'' |
    re''8.(\trill dod''32 re'' mi''8) dod'' |
    re''8.(\trill dod''32 re'' mi''8) dod'' |
    re''8.(\trill dod''32 re'' mi''8) dod'' |
    si''8.( la''32 sol'' fad''8) mi'' |
    \appoggiatura mi''16 re''8 dod''16 si' dod''4\trill |
    si'16\f si'' si'' si'' si'' si'' si'' si'' |
    si'' re'' re'' re'' re'' re'' re'' re'' |
    re'' si'' si'' si'' si'' si'' si'' si'' |
    lad''8-! sol''-! fad''-! mi''-! |
    \appoggiatura mi''16 re''8 dod''16 si' dod''4\trill |
    si' }
  { re''16.( mi''32 re''8) dod'' |
    si'8.(\trill lad'32 si' dod''8) lad' |
    si'8.(\trill lad'32 si' dod''8) lad' |
    si'8.(\trill lad'32 si' dod''8) lad' |
    re'' mi'' re'' dod'' |
    si'4 lad'\trill |
    si'16\f re'' re'' re'' re'' re'' re'' re'' |
    re'' si' si' si' si' si' si' si' |
    si' re'' re'' re'' re'' re'' re'' re'' |
    dod''8-! mi''-! re''-! dod''-! |
    si'4 lad'\trill |
    si' }
>> r4 |
R4.*23 |
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { la'4 la' |
    si'16( dod'' re''4) re''8 |
    mi''8.( fad''32 sol'') fad''8 mi'' |
    fad'' mi'' }
  { fad'8 fad' mi' mi' |
    re'4 re''~ |
    re''4. dod''8 |
    re'' dod'' }
>> r4 |
\twoVoices #'(violino1 violino2 violini) <<
  { re'' fad''16( mi'' re'' dod'') |
    si'( re'' dod'' mi'') re''8 }
  { la'8 la' la' la' |
    re' mi' fad' }
>> r8 |
\twoVoices #'(violino1 violino2 violini) <<
  { re''4 fad''16( mi'' re'' dod'') |
    si'( re'' dod'' mi'') re''8 }
  { la'8 la' la' la' |
    re' mi' fad' }
>> r8 |
\twoVoices #'(violino1 violino2 violini) <<
  { sol''8 fad''4( mi''8) |
    fad''16( mi'' re'' dod'') si'8 fad'' |
    mi''16( re'' dod'' si') la'8 mi'' |
    re'' dod'' }
  { mi''8 re''4 dod''8 |
    re''4 r8 si' |
    si'4 r8 la' |
    sold' la' }
>> r4 |
la'16 la' la' la' dod'' dod'' mi'' mi'' |
la'' la'' la'' la'' mi'' mi'' dod'' dod'' |
la' la' la' la' dod'' dod'' mi'' mi'' |
la'' la'' la'' la'' mi'' mi'' dod'' dod'' |
la'\pocof la' la' la' dod'' dod'' mi'' mi'' |
\twoVoices #'(violino1 violino2 violini) <<
  { la''8.(\trill sold''32 fad'') mi''8 re'' |
    dod''4 si'\trill |
    la'16 dod''\p dod'' dod'' dod'' dod'' dod'' dod'' |
    si' si' si' si' si' si' si' si' |
    la' dod''\pocof dod'' dod'' dod'' dod'' dod'' dod'' |
    si' si' si' si' si' si' si' si' | }
  { dod''8 re'' dod'' si' |
    la'4 sold'\trill |
    la'16 la'\p la' la' la' la' la' la' |
    la' la' la' la' sold' sold' sold' sold' |
    la' la'\pocof la' la' la' la' la' la' |
    la' la' la' la' sold' sold' sold' sold' | }
>>
la'16 la' mi' dod' la4 |
\twoVoices #'(violino1 violino2 violini) <<
  { la''16\f do''\p do'' do'' do'' do'' do'' do'' |
    do'' si' si' si' si' si' si' si' |
    si''\f re''\p re'' re'' re'' re'' re'' re'' |
    re'' dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    dod'''\f mi''\p mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' re'' re'' re'' re'' re'' re'' re'' |
    la'\f sol''\p sol'' sol'' sol'' sol'' sol'' sol'' |
    sol'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    mi''8 mi''4( fa''8) |
    mi''8 mi''4( fa''8) |
    mi''8 mi''4( fa''8) |
    fa''( mi'') }
  { do''16\f la'\p la' la' la' la' la' la' |
    la' sol' sol' sol' sol' sol' sol' sol' |
    re''\f si'\p si' si' si' si' si' si' |
    si' la' la' la' la' la' la' la' |
    mi''\f dod''\p dod'' dod'' dod'' dod'' dod'' dod'' |
    dod'' si' si' si' si' si' si' si' |
    la'\f mi''\p mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' re'' re'' re'' re'' re'' re'' re'' |
    dod''8 dod''4( re''8) |
    dod''8 dod''4( re''8) |
    dod''8 dod''4( re''8) |
    re''( dod'') }
>> r4 |
\twoVoices #'(violino1 violino2 violini) <<
  { re''4. re''8 | \appoggiatura fad''16 mi''8 re'' }
  { la'8 la' la' la' | si' la' }
>> r4 |
r \twoVoices #'(violino1 violino2 violini) <<
  { re''8 re'' | \appoggiatura fad''16 mi''8 re'' }
  { la'8 la' | si' la' }
>> r4 |
r \twoVoices #'(violino1 violino2 violini) <<
  { re''8 re'' | \appoggiatura fad''16 mi''8 re'' }
  { la'8 la' | si' la' }
>> r4 |
R2 |
\twoVoices #'(violino1 violino2 violini) <<
  { sol''8 fad''4( mi''8) |
    sol''8 fad''4( mi''8) |
    sol''8 fad''4( mi''8) |
    fad''8.(\trill sol''16 la''8) sol'' |
    fad''32( mi'' re''8.) mi''4\trill |
    fad''8.(\pocof\trill sol''16 la''8) sol'' |
    fad''32( mi'' re''8.) mi''4\trill |
    |
    re''16\f re''' re''' re''' re''' re''' re''' re''' |
    re''' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' re''' re''' re''' re''' re''' re''' re''' |
    dod'''8-! si''-! la''-! sol''-! |
    fad''32( mi'' re''8.) mi''4\trill | }
  { mi''8 re''4( dod''8) |
    mi''8 re''4( dod''8) |
    mi''8 re''4( dod''8) |
    re''8.(\trill mi''16 fad''8) mi'' |
    re''4 dod''\trill |
    re''8.(\pocof\trill mi''16 fad''8) mi'' |
    re''4 dod''\trill |
    re''16\f fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' re'' re'' re'' re'' re'' re'' re'' |
    re'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    mi''8-! sol''-! fad''-! mi''-! |
    re''4 dod''\trill | }
>>
re''8 la'16 fad' re'8 re' |
<re' la' fad''>4 r |
