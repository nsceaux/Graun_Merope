\clef "bass"
\override Script.avoid-slur = #'inside
re'8 re' dod' dod' |
si si fad fad |
sol sol la la |
re la la sol |
fad fad fad fad |
sol sol fad fad |
fad fad fad fad |
sol4 r |
r8 sol fad sol |
la4 la, |
si fad |
sol la |
si fad\pocof |
sol la |
re8\f re re re |
re4 r8 re'\p |
la la la la |
la la, re fad |
sol sol sol sol |
la16(-. la-. la-. la-.) lad(-. lad-. lad-. lad-.) |
si(-. si-. la!-. la-.) sold(-. sold-. sold-. sold-.) |
la8 dod16. re32 mi8 dod16. re32 |
mi8 mi mi mi |
fad16 fad( mi re) mi8 mi, |
la8 la sold sold |
fad fad dod dod |
re re mi mi |
la, mi mi re |
dod dod dod dod |
re\f re re re |
red\p red red red |
mi\f mi mi mi |
fad4\p r8 re |
dod si, dod re |
mi4 mi, |
fad dod |
re mi |
fad dod\pocof |
re mi |
la,8\f la, la, la, |
la,4 r |
r8 lad\p lad lad |
si fad re16(-. re-. re-. re-.) |
mi(-. mi-. mi-. mi-.) fad-.( fad-. fad-. fad-.) |
sol( sol sol sol) mi( mi mi mi) |
fad( fad fad fad) mid( mid mid mid) |
fad( fad fad fad) mid4 |
fad8 mi re dod |
si,8 si, si, si, |
mi mi mi mi |
la la la la |
re re re re |
sol sol sol sol |
dod dod dod dod |
fad4 r8 lad |
si4 mid |
fad r |
r8 re re4 |
r8 dod dod4 |
r8 fad fad fad |
r si si, si |
r sol sol4 |
r8 fad fad4 |
r8 si si si |
mi mi mi mi |
mi mi mi mi |
red red red red |
re! re re re |
dod dod dod dod |
fad fad fad fad |
si\pocof si mid mid |
fad fad' re' si |
fad' fad\f fad fad |
fad4 r |
r8 fad fad, fad |
r8 fad fad, fad |
r8 fad fad, fad |
r8 fad fad, fad |
r8 lad lad, lad |
si4 mi |
fad8 si mi fad |
si, si mi fad |
si, si mi fad |
si, dod re mi |
fad4 fad, |
si,8 si\f si, la, |
sol, sol sol, fad, |
mid, mid mid mid |
fad lad si mi |
fad4 fad, |
si, r |
si8 si la |
sold sold fad |
mi mi mi |
la la, r |
la la sol |
fad fad mi |
re re re |
sol sol, r |
do16-.( do-. do-. do-. do-. do-.) |
re( re re re re re) |
mi( mi fad fad sol sol) |
re-.( re-. re-. re-. do-. do-.) |
si,-.( si,-. si,-. si,-. si,-. si,-.) |
la,-.( la,-. la,-. la,-. la,-. la,-.) |
re-.( re-. re-. re-. re-. re-.) |
sol,-.( sol,-. sol,-. sol,-. sol,-. sol,-.) |
do( do do do do do) |
re( re do do si, si,) |
do( do re re re, re,) |
sol,8 r16 sol, si, re |
sol8 r16 sol, si, re |
sol4 sold8 |
la4 r8 |
re'8 re' dod' dod' |
si si fad fad |
sol sol la la |
re la la sol |
fad fad fad fad |
sol sol fad fad |
fad fad fad fad |
sol sol fad fad |
mi4 la |
re r8 re |
dod4 r8 dod |
si, la, r4 |
r8 la mi dod |
r la, dod mi |
r la mi dod |
r la, dod mi |
r la\pocof mi dod |
la, si, dod re |
mi re mi mi, |
la,4 fad\p |
re mi |
fad dod\pocof |
re mi |
la, r |
fad8\f fad[\p fad fad] |
sol sol sol sol |
sold8\f sold[\p sold sold] |
la la la la |
lad\f lad[\p lad lad] |
si si si si |
dod'\f dod'[\p dod' dod'] |
re' re' re' re' |
la4 sold |
la sold |
la sold |
la4 r |
R2 |
r4 fad8 fad |
mi re r4 |
r fad8 fad |
mi re r4 |
r fad8 fad |
mi re r4 |
sol4 la |
sol la |
sol la |
re r8 sol |
la4 la, |
re\pocof r8 sol |
la4 la, |
re8\f re' re dod |
si, si si, la, |
sold, sold sold sold |
la8 dod' re' sol |
la4 la, |
re8 re re re |
re4 r |
