\clef "alto" re'8 re' dod' dod' |
si si' la' la' |
si' si' la' la' |
la' la' la' sol' |
fad' fad' re' re' |
re' mi' fad' r |
fad'8 fad' re' re' |
sol'4 r |
r8 mi' fad' si' |
la'4 sol'\trill |
fad' fad' |
sol' la' |
si' fad'\pocof |
sol' la' |
re'8\f re' re' re' |
re'4 r |
R2*8 |
la8\p la sold sold |
fad fad' mi' mi' |
fad' fad' mi' mi' |
mi' mi' mi' re' |
dod' dod' dod' dod' |
re'\f re' re' re' |
red'\p red' red' red' |
mi'\f mi' mi' mi' |
fad'4\p r8 fad' |
mi' re' mi' fad' |
mi'4 re'\trill |
dod'4 dod' |
re' mi' |
fad' dod'\pocof |
re' mi' |
la8\f la la la |
la4 r |
R2*6 |
fad'16\p fad' mi' mi' re' re' dod' dod' |
si si si si si si si si |
mi' mi' mi' mi' mi' mi' mi' mi' |
la' la' la' la' la' la' la' la' |
re' re' re' re' re' re' re' re' |
sol' sol' sol' sol' sol' sol' sol' sol' |
dod' dod' dod' dod' dod' dod' dod' dod' |
fad'4 r8 fad' |
fad'4 mid' |
fad' r |
r8 re' re'4 |
r8 dod' dod'4 |
r8 fad' fad' fad' |
r si' si si' |
R2*3 |
mi'16\p mi' mi' mi' mi' mi' mi' mi' |
mi' mi' mi' mi' mi' mi' mi' mi' |
red' red' red' red' red' red' red' red' |
re'! re' re' re' re' re' re' re' |
dod' dod' dod' dod' dod' dod' dod' dod' |
dod' dod' dod' dod' dod' dod' dod' dod' |
si\pocof si si si mid' mid' mid' mid' |
fad'8 fad' re' si |
fad' fad\f fad fad |
fad4 r |
R2 |
r8 fad'\p fad fad' |
R2 |
r8 fad' fad fad' |
R2 |
si'4 mi' |
fad'8 si' mi' fad' |
si si' mi' fad' |
si si' mi' fad' |
si dod' re' mi' |
fad'4 fad |
si16\f re' re' re' re' re' re' re' |
re' si si si si si si si |
mid8 mid' mid' mid' |
fad' lad' si' mi' |
fad'4 fad |
si r |
R4.*23 |
re'8\p re' dod' dod' |
si si' la' la' |
si' si' la' la' |
la' la' la' sol' |
fad' fad' re' re' |
re' mi' fad' r |
fad' fad' re' re' |
re' mi' fad' r |
mi'4 la' |
re' r8 re' |
dod'4 r8 dod' |
si la r4 |
la'16 la' la' la' mi' mi' dod' dod' |
la la la la dod' dod' mi' mi' |
la' la' la' la' mi' mi' dod' dod' |
la la la la dod' dod' mi' mi' |
la' la' la' la' mi' mi' dod' dod' |
la8 si dod' re' |
mi' re' mi' mi |
la4 fad'\p |
re' mi' |
fad' dod'\pocof |
re' mi' |
la4 r |
re'16\f re'\p re' re' re' re' re' re' |
re' re' re' re' re' re' re' re' |
mi'\f mi'\p mi' mi' mi' mi' mi' mi' |
mi' mi' mi' mi' mi' mi' mi' mi' |
fad'\f fad'\p fad' fad' fad' fad' fad' fad' |
fad' fad' fad' fad' fad' fad' fad' fad' |
la'\f la'\p la' la' la' la' la' la' |
la' la' la' la' la' la' la' la' |
la'4 sold' |
la' sold' |
la' sold' |
la' r |
fad'8 fad' fad' fad' |
sol' fad' r4 |
r fad'8 fad' |
sol' fad' r4 |
r fad'8 fad' |
sol' fad' r4 |
R2 |
sol'4 la' |
sol' la' |
sol' la' |
re' r8 sol' |
la'4 la |
re'\pocof r8 sol' |
la'4 la |
re'8\f re'' re' dod' |
si si' si la |
sold sold' sold' sold' |
la' mi'-! fad'-! sol'-! |
la'4 sol' |
fad'8 la'16 fad' re'8 re' |
re'4 r |
