\key re \major \midiTempo#120
\time 2/4 s2*15 s4
\tempo "Adagio" \midiTempo#60 s4 s2*8
\tempo "Vivace" \midiTempo#120 s2*16 s4
\tempo "Adagio" \midiTempo#60 s4 s2*6
\tempo "Vivace" \midiTempo#120 s2*44
\tempo "Adagio" \midiTempo#60 \time 3/8 s4.*23
\tempo "Vivace" \midiTempo#120 \time 2/4 s2*57 \bar "|."
