\bookpart {
  \act "ATTO SECONDO"
  \scene "Scena Seconda" "Scena II"
  \sceneDescription\markup\wordwrap-center {
    Polifonte, Merope, Ericle, Ismenia, Egisto, Eroce.
  }
  %% 2-1
  \pieceToc\markup\wordwrap {
    Aria. Egisto, Merope: \italic { Cessa omai di quel Tiranno }
  }
  \includeScore "BBBduo"
}
\bookpart {
  \paper {
    page-count = 4
    systems-per-page = 4
  }
  %% 2-2
  \pieceToc\markup\wordwrap {
    Aria. Polifonte: \italic { Arbitra della sorte }
  }
  \includeScore "BBDpolifonte"
}
\bookpart {
  \paper {
    systems-per-page = 4
  }
  \scene "Scena Quarta" "Scena IV"
  \sceneDescription\markup\wordwrap-center {
    Merope, Narbace.
  }
  %% 2-3
  \pieceToc\markup\wordwrap {
    Aria. Narbace: \italic { Alle vendetti i Numi }
  }
  \includeScore "BDBnarbace"
}
\bookpart {
  \scene "Scena Quinta" "Scena V"
  \sceneDescription\markup\wordwrap-center {
    Ismenia, Merope, Narbace.
  }
  %% 2-4
  \pieceToc\markup\wordwrap {
    Recit. Merope: \italic { Ebben: sì dagli stessi }
  }
  \includeScore "BEArecit"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% 2-5
  \pieceToc\markup\wordwrap {
    Aria. Merope: \italic { M’opprime un mostro indegno }
  }
  \includeScore "BEBmerope"
}
