\bookpart {
  \act "ATTO TERZO"
  \scene "Scena Quarta" "Scena IV"
  \sceneDescription\markup\wordwrap-center {
    Merope, Egisto, Narbace, Ericle.
  }
  %% 3-1
  \pieceToc\markup\wordwrap {
    Aria. Merope: \italic { Par che lo scorga il Cielo }
  }
  \includeScore "CDBmerope"
}

\bookpart {
  \paper { systems-per-page = 3 }
  \scene "Scena Quinta" "Scena V"
  \sceneDescription\markup\wordwrap-center {
    Narbace, Ericle.
  }
  %% 3-2
  \pieceToc\markup\wordwrap {
    Aria. Narbace: \italic { Mille ogetti di terrore }
  }
  \includeScore "CEBnarbace"
}

\bookpart {
  \paper { min-systems-per-page = 2 }
  \scene "Scena Sesta" "Scena VI"
  \sceneDescription\markup\center-column {
    \line { Interno di un Tempio con Ara in mezzo. }
    \line {
      Merope, Polifonte, Ismenia, Eroce.
      Sacerdoti, Popolo, Guardie, poi Egisto.
    }
  }
  %% 3-3
  \pieceToc\markup\wordwrap {
    Coro: \italic { Del mortal, tu, che sei l’arbitra }
  }
  \includeScore "CFBcoro"
}

\bookpart {
  \scene "Scena Settima" "Scena VII"
  \sceneDescription\markup\column {
    \line {
      Gran Piazza di Messene, con veduta di buona parte della Città.
    }
    \wordwrap-center {
      Merope, Ismenia indi Egisto, e poi Narbace.
    }
    \wordwrap-center {
      Sacerdoti, Popolo, Soldati. Si vede in lontano strascinato
      il corpo di Polifonte coperto però di un tapeto.
    }
  }
  %% 3-4
  \pieceToc\markup\wordwrap {
    Recit. Egisto, Popolo: \italic { Amici ancora dubitar potrete }
  }
  \includeScore "CGArecit"
  %% 3-5
  \pieceToc\markup\wordwrap {
    Aria. Egisto: \italic { Se alle tempeste in seno }
  }
  \includeScore "CGBegisto"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 3-6
  \pieceToc\markup\wordwrap {
    Coro: \italic { Felice può dirsi, può dirsi beato }
  }
  \includeScore "CGCcoro"
}
