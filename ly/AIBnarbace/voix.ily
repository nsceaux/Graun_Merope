\clef "tenor/G_8" do'4 do'8 do' do'([ fa']) fa'([ lab']) |
lab'[ si] si2. |
sol'4 sib!8 sib \appoggiatura do'8 sib4 lab8[ sol] |
lab[ sib] do'4 reb'2~ |
reb'4 do'8 reb' do'4 sib |
\appoggiatura sib2 lab1 |
sol2 r |
lab8[ do'] do' mib' mib'[ solb'] solb'4 |
solb'8[ fa'] fa'2. |
sib8[ re'!] re' fa' fa'[ lab'] lab'4 |
lab'8[ sol'] sol'2 r8 sol' |
sol'4. do'8 do'4. sol'8 |
lab'4. fa'8 fa'4. reb'8 |
reb'4( lab'2) do'4 |
\appoggiatura do'4 si2 r |
sol'8[ re'] re' re' re'[ mib'] mib'4 |
mib' re' r r8 sol' |
sol'4. sol8 sol4. mib'8 |
\appoggiatura mib'4 re'2 r |
sol'8[ do'] do' do' do'4 do' |
lab'2~ lab'8*2/3[\melisma sol' fa'] mib'[ re' do'] |
sib4 lab'( sol' fad') |
sol'2~ sol'8*2/3[ fa' mib'] re'[ do' sib] |
lab4 sol'( fa' mi') |
fa'2~ fa'8*2/3[ mib' re'] do'[ si la] |
sol4 fa'( mib' re') |
mib'8*2/3[ do' re'] mib'[ re' do'] fa'[ re' mib'] fa'[ mib' re'] |
sol'[ mib' fa'] sol'[ fa' mib'] lab'[ sol' fa'] mib'[ re' do'] |
si8[ la?]\melismaEnd sol4 r r8 sol |
fa'4. fa'8 fa'4. fa'8 |
fa'4 r r r8 sol |
lab'4. lab'8 lab'4. lab'8 |
sol'4 lab'8*2/3[ sol' fa'] mib'4 fa'8*2/3[ mib' re'] |
do'4.( re'8) re'2\trill |
do'4 lab'8*2/3[ sol' fa'] mib'[ re' mib'] fa'[ mib' re'] |
do'4.( re'8) re'2\trill |
do' r |
R1*4 |
do'4 do'8 do' do'[ fa'] fa'[ lab'] |
lab'[ si] si2. |
sol'4 sib!8 sib \appoggiatura do' sib4 lab8[ sol] |
lab[ sib] do'4 reb'2~ |
reb'4 do'8 reb' do'4 sib |
\appoggiatura sib2 lab1 |
sol2 r |
do'8([ mib']) mib' solb' solb'[ mib'] mib'[ do'] |
do'[ reb'] reb'2 r8 sib' |
sib'4. mib'8 mib'4 reb' |
do'4 r r2 |
lab'4 reb'8 reb' reb'4 do' |
do'8[ sib] sib4 r r8 sol' |
sol'4. do'8 do'4 sib |
lab8[ la] la[ sib] sib[ si] si[ do'] |
do'4( reb'2) fa4 |
\appoggiatura fa4 mi2 r |
sol'4 sib8 sib \appoggiatura do'8 sib4 lab8[ sol] |
lab4 sol r2 |
sol'4 sib8 sib \appoggiatura do'8 sib4 lab8[ sol] |
lab4 sol r2 |
do'4 do'8 do' do'4 do' |
lab'2~ lab'8*2/3[\melisma sol' fa'] mib'[ reb' do'] |
sib4 lab'( sol' fad') |
sol'2~ sol'8*2/3[ fa' mib'] reb'[ do' sib] |
lab4 sol'( fa' mi') |
fa'2~ fa'8*2/3[ mib' reb'] do'[ sib lab] |
sol4 fa'( mi' fa') |
mi'8[ re']\melismaEnd do'4 r r8 sol' |
sol'4. sib8 sib4. sib8 |
sib4 r r r8 sib' |
sib'4. sib8 sib4. sib8 |
lab4 reb'8*2/3([ do' sib]) lab4 sib8*2/3[ lab sol] |
fa4.( sol8) sol2\trill |
fa4 fa'8*2/3[ mib' reb'] do'[ sib do'] reb'[ do' sib] |
lab[\melisma sol lab] sib[ lab sol]\melismaEnd sol2\trill |
fa2 r |
R1*13 |
%%%
do'2. |
do'4 sib r |
reb' reb'4. reb'8 |
reb'4 do' r |
fa'4 mib'8[ reb'] do'[ sib] |
lab8( mib'4 reb') do'8 |
do'4 sib r |
mib'4 reb'16[ do'8.] sib16[ lab8.] |
solb4( mib'4.) solb8 |
solb4 fa r |
fa' mib'16[ re'!8.] do'16[ sib8.] |
lab4( fa'4.) lab8 |
lab4 sol r |
lab8([ do']) sib([ reb']) do'([ mib']) fa[ reb'] do'4( sib)\trill |
lab8 lab reb'4 do' |
r8 do' fa'4 mib' |
reb'8[ lab'] sol'[ fa'] mib'[ reb'] |
do'2.~ |
do'~ |
do'2\fermata  sib4 |
lab2. |
%%%
R1*8 |
