\clef "treble" <>\p
\twoVoices #'(violino1 violino2 violini) <<
  { do'' do''8 do'' do''( fa'') fa''( lab'') |
    lab''( si') si'2. |
    sol''8( sib'!) sib' sib' \appoggiatura do''8 sib'4 lab'8 sol' | }
  { lab'4 lab'8 lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol'4 do' | }
>>
<<
  \tag #'(violino1 violini) {
    lab'8( sib') do''4 reb''2~ |
    reb''4 do''8. reb''16 do''4 sib' |
    \appoggiatura sib'2 lab'1 |
    sol'2
  }
  \tag #'(violino2 violini) {
    fa'8( sol') lab'4 sib'2~ |
    sib'4 lab'8. sib'16 lab'4 sol' |
    \appoggiatura sol'2 fa'1 |
    mi'2
  }
>> r2 |
lab'8( do'') do''-! mib''-! mib''( solb'') solb''-! solb''-! |
solb''(\f fa'') fa''( mib'') mib''( reb'') reb''( do'') |
sib'(\p re''!) re''-! fa''-! fa''( lab'') lab''-! lab''-! |
lab''(\f sol'') sol''( fa'') fa''( mib'') mib''( reb'') |
do''(\p mi'') mi''-! sol''-! sol''( sib'') sib''-! sib''-! |
\twoVoices #'(violino1 violino2 violini) <<
  { lab''8( fa'') fa''-! fa''-! fa''( reb'') reb''-! reb''-! |
    reb''8( fa'' lab''2) lab'4 |
    \appoggiatura do''4 si'2 }
  { lab''8 lab' lab' lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol'2 }
>> r2 |
\twoVoices #'(violino1 violino2 violini) <<
  { sol''8( re'') re''-! re''-! }
  { si'4 si'8-! si'-! }
>> <<
  \tag #'(violino1 violini) { re''8( mib'') mib''-! mib''-! | mib''4 re'' }
  \tag #'(violino2 violini) { si'8( do'') do''-! do''-! | do''4 si' }
>> r2 |
\twoVoices #'(violino1 violino2 violini) <<
  { sol''8( re'') re''-! re''-! }
  { si'4 si'8-! si'-! }
>> <<
  \tag #'(violino1 violini) { re''8( mib'') mib''-! mib''-! | mib''4( re'') }
  \tag #'(violino2 violini) { si'8( do'') do''-! do''-! | do''4 si' }
>> r2 |
<<
  \tag #'(violino1 violini) {
    sol''8 sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
  }
  \tag #'(violino2 violini) {
    do''8 do'' do'' do'' do'' do'' do'' do'' |
  }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { lab''4 lab''2 lab''4~ |
    lab'' lab''( sol'' fad'') |
    sol''4 sol''2 sol''4~ |
    sol'' sol''( fa'' mi'') |
    fa''4 fa''2 fa''4~ |
    fa'' fa''( mib'' re'') | }
  { do''8 do'' do'' do'' do'' do'' do'' do'' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol' sol' sol' sol' | }
>>
<<
  \tag #'(violino1 violini) {
    mib''8 mib'' mib'' mib'' fa'' fa'' fa'' fa'' |
    sol'' sol'' sol'' sol'' lab'' lab'' lab'' lab'' |
  }
  \tag #'(violino2 violini) {
    sol'8 do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' do'' do'' |
  }
>>
sol-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
<>\p <<
  \tag #'(violino1 violini) { fa''8 fa''4 fa'' fa'' fa''8 | }
  \tag #'(violino2 violini) { re''8 re''4 re'' re'' re''8 | }
>>
sol-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
<>\p <<
  \tag #'(violino1 violini) { lab''8 lab''4 lab'' lab'' lab''8 | sol''4 }
  \tag #'(violino2 violini) { fa''8 fa''4 fa'' fa'' fa''8 | mib''4 }
>> lab''8*2/3([ sol'' fa'']) mib''([ re'' mib'']) fa''([ mib'' re'']) |
do''4.( re''8) re''2\trill |
do''4 lab''8*2/3([\pocof sol'' fa'']) mib''([ re'' mib'']) fa''([ mib'' re'']) |
do''8. do'''16 do''8. re''16 re''2\trill |
do'' r |
<>\f \twoVoices #'(violino1 violino2 violini) <<
  { do'''8( sol'') sol''-! sol''-! }
  { mi''4 mi''8-! mi''-! }
>>
<<
  \tag #'(violino1 violini) {
    sol''8( lab'') lab''-! lab''-! |
    \appoggiatura sib''8 lab''4 sol''
  }
  \tag #'(violino2 violini) {
    mi''8( fa'') fa''-! fa''-! |
    \appoggiatura sol''8 fa''4 mi''
  }
>> r2 |
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { do'''8( sol'') sol''-! sol''-! }
  { mi''4 mi''8-! mi''-! }
>>
<<
  \tag #'(violino1 violini) {
    sol''8( lab'') lab''-! lab''-! |
    \appoggiatura sib''16 lab''4 sol''
  }
  \tag #'(violino2 violini) {
    mi''8( fa'') fa''-! fa''-! |
    \appoggiatura sol''16 fa''4 mi''
  }
>> r2 |
<<
  \tag #'(violino1 violini) { do''4 do''8 do'' }
  \tag #'(violino2 violini) { fa'4 lab'8 lab' }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { do''8( fa'') fa''( lab'') |
    lab''8( si') si'2. |
    sol''8( sib'!) sib' sib' \appoggiatura do''8 sib'4 lab'8( sol') | }
  { lab'8 lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol'8 sol' sol' sol' sol'4 do' | }
>>
<<
  \tag #'(violino1 violini) {
    lab'8( sib') do''4 reb''2~ |
    reb''4 do''8. reb''16 do''4 sib' |
    \appoggiatura sib'2 lab'1 |
    sol'2
  }
  \tag #'(violino2 violini) {
    fa'8( sol') lab'4 sib'2~ |
    sib'4 lab'8. sib'16 lab'4 sol' |
    \appoggiatura sol'2 fa'1 |
    mi'2
  }
>> r2 |
do''8( mib'') mib''( solb'') solb''( mib'') mib''-! do''-! |
do''(\f reb'') reb''-! fa''-! fa''( sib'') sib''-! reb'''-! |
reb'''(\p sib'') sib''-! sol''-! sol''( mib'') mib''-! reb''-! |
reb''(\f do'') do''-! mib''-! mib''( lab'') lab''-! do'''-! |
do'''(\p lab'') lab''-! fa''-! fa''( reb'') reb''-! do''-! |
do''(\f sib') sib'-! reb''-! reb''( sol'') sol''-! sib''-! |
sib''(\p sol'') sol''-! mi''-! mi''( do'') do''-! sib'-! |
lab'( la') la'( sib') sib'( si') si'( do'') |
do''( reb'') reb''2 fa'4 |
\appoggiatura fa'4 mi'2 r |
<<
  \tag #'(violino1 violini) {
    sol''8( sib') sib'-! sib'-! \appoggiatura do''8 sib'4 lab'8( sol') |
    lab'4 sol'
  }
  \tag #'(violino2 violini) {
    sib'8( sol') sol'-! sol'-! \appoggiatura lab'8 sol'4 fa'8( mi') |
    fa'4 mi'
  }
>> r2 |
<<
  \tag #'(violino1 violini) {
    sol''8( sib') sib'-! sib'-! \appoggiatura do''8 sib'4 lab'8( sol') |
    lab'4 sol'
  }
  \tag #'(violino2 violini) {
    sib'8( sol') sol'-! sol'-! \appoggiatura lab'8 sol'4 fa'8( mi') |
    fa'4 mi'
  }
>> \twoVoices #'(violino1 violino2 violini) <<
  { do''2~ |
    do''4 do''2 do''4 |
    lab''4 lab''2 lab''4~ |
    lab''4 lab''( sol'' fad'') |
    sol''4 sol''2 sol''4~ |
    sol'' sol''( fa'' mi'') |
    fa''4 fa''2 fa''4~ |
    fa'' fa''( mi'' fa'') |
  }
  { sol'2 |
    lab' sib' |
    lab'8 do'' do'' do'' do'' do'' do'' do'' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol' sol' sol' sol' |
  }
>>
do'8\f do''( reb'' do'') do' do''( reb'' do'') |
<>\p <<
  \tag #'(violino1 violini) { sol''8 sol''4 sol'' sol'' sol''8 | }
  \tag #'(violino2 violini) { sib'8 sib'4 sib' sib' sib'8 | }
>>
do'8\f do''( reb'' do'') do' do''( reb'' do'') |
<>\p <<
  \tag #'(violino1 violini) { sib''8 sib''4 sib'' sib'' sib'8 | lab'4 }
  \tag #'(violino2 violini) { sol''8 sol''4 sol'' sol'' sol'8 | fa'4 }
>> reb''8*2/3([ do'' sib']) lab'([ sol' lab']) sib'([ lab' sol']) |
fa'4.( sol'8) sol'2\trill |
fa'4 fa''8*2/3([\pocof mib'' reb'']) do''([ sib' do'']) reb''([ do'' sib']) |
lab'([ sol' lab']) sib'([ lab' sol']) sol'2\trill |
fa'8-!\f fa''( solb'' fa'') fa'-! mib''( fa'' mib'') |
fa'-! reb''( mib'' reb'') mi'-! do''( reb'' do'') |
fa'8 lab''( sol'' fa'') mib''( reb'') do''( si') |
do''4 r r2 |
\twoVoices #'(violino1 violino2 violini) <<
  { do'''8( sol'') sol''-! sol''-! }
  { mi''4 mi''8-! mi''-! }
>> <<
  \tag #'(violino1 violini) {
    sol''8( lab'') lab''-! lab''-! |
    \appoggiatura sib''8 lab''4 sol''
  }
  \tag #'(violino2 violini) {
    mi''8( fa'') fa''-! fa''-! |
    \appoggiatura sol''8 fa''4 mi''
  }
>> r2 |
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { do'''8( sol'') sol''-! sol''-! }
  { mi''4 mi''8-! mi''-! }
>> <<
  \tag #'(violino1 violini) {
    sol''8( lab'') lab''-! lab''-! |
    \appoggiatura sib''8 lab''4 sol''
  }
  \tag #'(violino2 violini) {
    mi''8( fa'') fa''-! fa''-! |
    \appoggiatura sol''8 fa''4 mi''
  }
>> r2 |
<>\f <<
  \tag #'(violino1 violini) {
    sol''8( sib') sib' sib' \appoggiatura do''8 sib'4 lab'8 sol' |
    lab'4
  }
  \tag #'(violino2 violini) {
    sib'8( sol') sol' sol' \appoggiatura lab'8 sol'4 fa'8 mi' |
    fa'4
  }
>> reb''8*2/3([ do'' sib']) lab'([ sol' lab']) sib'([ lab' sol']) |
fa'4.( sol'8) sol'2\trill |
fa'4 fa''8*2/3([ mib'' reb'']) do''([ sib' do'']) reb''([ do'' sib']) |
lab'([ sol' lab']) sib'([ lab' sol']) sol'2\trill |
fa'4 do' fa'2\fermata |
%%%
<<
  \tag #'(violino1 violini) {
    do''2. |
    do''4 sib' s |
    reb''-! reb''-! reb''-! |
    reb''-! do''-! s |
  }
  \tag #'(violino2 violini) {
    lab'2. |
    lab'4 sol' s |
    <sol' sib'>-! q-! q-! |
    q-! lab'-! s |
  }
  { s2.\p | s2 r4 | s2.\pocof | s2 r4 }
>>
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { r8 fa''( mib'' reb'' do'' sib') |
    lab'8 mib''4 reb'' do''8 | }
  { fa'2. |
    r8 mib' sol'4 lab' | }
>>
<<
  \tag #'(violino1 violini) { do''4 sib' }
  \tag #'(violino2 violini) { lab'4 sol' }
>> r4 |
\override Script.avoid-slur = #'inside
<<
  \tag #'(violino1 violini) {
    s8 mib''(-. mib''-. mib''-. mib''-. mib''-.) |
    s8 mib''(-. mib''-. mib''-. mib''-.) solb' |
    solb'4 fa' s |
    s8 fa''(-. fa''-. fa''-. fa''-. fa''-.) |
    s8 fa''(-. fa''-. fa''-. fa''-.) lab' |
    lab'4 sol' s |
  }
  \tag #'(violino2 violini) {
    s8 solb'(-. solb'-. solb'-. solb'-. solb'-.) |
    s8 solb'(-. solb'-. solb'-. solb'-.) mib' |
    mib'4 reb' s |
    s8 lab'(-. lab'-. lab'-. lab'-. lab'-.) |
    s8 lab'(-. lab'-. lab'-. lab'-.) fa' |
    fa'4 mib' s |
  }
  { r8 s s2 | r8 s s2 | s2 r4 |
    r8 s s2 | r8 s s2 | s2 r4 | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { lab'8( do'') sib'( reb'') do''( mib'') | }
  { mib'2. | }
>>
<<
  \tag #'(violino1 violini) {
    s8 reb'') \appoggiatura do''4 sib'2\trill |
    s4 reb''-! do''-! |
    s fa''-! mib''-! |
  }
  \tag #'(violino2 violini) {
    s8 sib') \appoggiatura lab'4 sol'2\trill |
    s4 sib'-! lab'-! |
    s lab'-! lab'-! |
  }
  { fa'8( s s2 | lab'4 s2 | r4 s2\pocof }
>>
<>\p \twoVoices #'(violino1 violino2 violini) <<
  { reb''8 lab'' sol'' fa'' mib'' reb'' | }
  { lab'2 sib'4 | }
>>
<<
  \tag #'(violino1 violini) {
    do''4-! do''-! do''-! |
    do'' do'' do'' |
    do''2
  }
  \tag #'(violino2 violini) {
    lab'4-! lab'-! lab'-! |
    lab' lab' lab' |
    lab'2
  }
  { s2. | s\pocof }
>> r4 |
R2. |
\twoVoices #'(violino1 violino2 violini) <<
  { do''4 do''8 do'' do'' fa'' fa'' lab'' |
    lab''( si') si'2. |
    sol''8 sib'! sib' sib' \appoggiatura do''8 sib'4 lab'8 sol' |
    lab'4 }
  { lab'4 lab'8 lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol'4 do' |
    fa' }
>> reb''8*2/3([ do'' sib']) lab'[ sol' lab'] sib'[ lab' sol'] |
fa'4.( sol'8) sol'2\trill |
fa'4 fa''8*2/3([ mib'' reb'']) do''([ sib' do'']) reb''([ do'' sib']) |
lab'([ sol' lab']) sib'([ lab' sol']) sol'2\trill |
fa'4 do' fa'2 |
