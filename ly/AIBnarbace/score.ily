\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      %\new GrandStaff \with { instrumentName = "Violini" } <<
      %  \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
      %  \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      %>>
      \new Staff \with { instrumentName = "Violini" } <<
        \global \keepWithTag #'violini \includeNotes "violini"
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Narbace }
    \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*4\break s1*5\break s1*5\pageBreak
        \grace s4 s1*5\break s1*5\break s1*4 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*4\break s1*6\pageBreak
        s1*4\break s1*4\break s1*4\pageBreak
        s1*4 s2 \bar "" \break s2 s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4 \break s1*4\break s1*5\pageBreak
        \grace s8 s1*5\break s1*4 s2.\break s2.*6\pageBreak
        s2.*5\break s2.*5\break s2.*5\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}