\clef "alto" fa'\p fa'8 fa' fa' fa' fa' fa' |
re' re' re' re' re' re' re' re' |
do' do' do' do' do' do' do' do' |
do'2 r4 fa' |
mi'1 |
fa'2 si |
do8-!\f do'( reb' do') do-! do'( reb' do') |
mib'1\p |
lab\f |
fa'\p |
sib\f |
sol'\p |
do'8 do' do' do' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' fa' fa' |
sol8-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
sol4 r r2 |
sol8-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
sol4 r r2 |
sol8-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
mib'8 mib' mib' mib' mib' mib' mib' mib' |
fa' fa' fa' fa' fa' fa' fa' fa' |
re' re' re' re' re' re' re' re' |
mib' mib' mib' mib' mib' mib' mib' mib' |
do' do' do' do' do' do' do' do' |
re' re' re' re' re' re' re' re' |
si si si si si si si si |
do' do' do' do' re' re' re' re' |
mib' mib' mib' mib' fa' fa' fa' fa' |
sol8-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
si4\p si si si |
sol8-!\f sol'( lab' sol') sol-! sol'( lab' sol') |
re'4\p re' re' re' |
mib' fa' r2 |
r4 do'2 si4\trill |
do'4 do'\pocof r2 |
r4 do'2 si4\trill |
do'8-!\f do''( reb'' do'') do'-! do''( reb'' do'') |
do'4 r r2 |
do'8-! do''( reb'' do'') do'-! do''( reb'' do'') |
do'4\p r r2 |
do'8-! do''( reb'' do'') sib-! sib'( do'' sib') |
lab'8 fa' fa' fa' fa' fa' fa' fa' |
re' re' re' re' re' re' re' re' |
do' do' do' do' do' do' do' do' |
do'2 r4 fa' |
mi'1 |
fa'2 si |
do'\f mi' |
fa'1\p |
fa'\f |
mib'\p |
mib'\f |
reb'\p |
reb'\f |
do'\p |
fa8( la) la( sib) sib( si) si( do') |
do'( reb') reb'( fa') fa'4 reb' |
do8-!\f do'( reb' do') do-! do'( reb' do') |
do4 r r2 |
do8-!\f do'( reb' do') do-! do'( reb' do') |
do4 r r2 |
do8-!\p do'( reb' do') mi'2 |
fa' mi' |
fa'8 fa' fa' fa' fa' fa' fa' fa' |
reb' reb' reb' reb' reb' reb' reb' reb' |
mib' mib' mib' mib' mib' mib' mib' mib' |
do' do' do' do' do' do' do' do' |
reb' reb' reb' reb' reb' reb' reb' reb' |
sib sib sib sib sib sib sib sib |
do'8-!-\sug\f do''( reb'' do'') do'-! do''( reb'' do'') |
mi'4-\sug\p mi' mi' mi' |
do'8-!-\sug\f do''( reb'' do'') do'-! do''( reb'' do'') |
mi'4-\sug\p mi' mi' mi' |
fa' fa' r2 |
r4 fa'2 mi'4 |
fa' do'\pocof r fa' |
fa'2 mi'\trill |
fa'\f do' |
reb' mi' |
fa'1 |
mi'8 do'( reb' do') do-! do'( reb' do') |
do'4 r r2 |
do'8-! do''( reb'' do'') do'-! do''( reb'' do'') |
do'4\p r r2 |
do'8-! do''( reb'' do'') do'-! do''( reb'' do'') |
do'2\f do' |
do'4 sib r2 |
r4 fa'2 mi'4 |
fa'4 do' r fa' |
fa'2 mi'\trill |
fa'4 do' fa2\fermata |
%%%
r8 lab-\sug\p do' mib' lab' lab |
r mib sol sib mib' mib |
r mib\pocof sol sib mib' mib |
r8 lab do' mib' lab' lab |
reb'2.\p |
do'4 sib lab |
r8 mib sol sib mib' sol |
lab2. |
lab |
r8 reb fa lab reb' fa' |
sib2. |
sib |
r8 mib sol sib mib' reb' |
do'4 sol lab |
reb' mib' mib |
fa sol lab |
r reb'\pocof do' |
fa'2\p sol'4 |
lab'8 do''( sib' lab' sol' fa') |
mib' lab'(\pocof sol' fa' mib' re'!) |
mib'2 r4 |
R2. |
fa'8 fa' fa' fa' fa' fa' fa' fa' |
re' re' re' re' re' re' re' re' |
do' do' do' do' do' do' do' do' |
do'4 sib r2 |
r4 fa'2 mi'4\trill |
fa'4 do' r fa' |
fa'2 mi'\trill |
fa'4 do' fa2 |



