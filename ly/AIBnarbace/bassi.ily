\clef "bass" fa8\p fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa |
mi mi mi mi mi mi mi mi |
fa fa fa fa sib, sib, sib, sib, |
do1 |
si, |
do8-!\f do'( reb' do') do-! do'( reb' do') |
do1\p |
reb8\f reb reb reb reb reb reb reb |
re!1\p |
mib8\f mib mib mib mib mib mib mib |
mi1\p |
fa8 fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa |
sol,8-!\f sol( lab sol) sol,-! sol( lab sol) |
sol,4 r r2 |
sol,8-!\f sol( lab sol) sol,-! sol( lab sol) |
sol,4 r r2 |
sol,8-!\f sol( lab sol) sol,-! sol( lab sol) |
mib8 mib mib mib mib mib mib mib |
fa fa fa fa fa fa fa fa |
re re re re re re re re |
mib mib mib mib mib mib mib mib |
do do do do do do do do |
re re re re re re re re |
si, si, si, si, si, si, si, si, |
do do do do re re re re |
mib mib mib mib fa fa fa fa |
sol,8-!\f sol( lab sol) sol,-! sol( lab sol) |
sol,4\p sol, sol, sol, |
sol,8-!\f sol( lab sol) sol,-! sol( lab sol) |
si,4\p si, si, si, |
do fa r2 |
r4 mib fa sol |
lab fa\pocof r2 |
r4 mib fa sol |
do8-!\f do'( reb' do') do-! do'( reb' do') |
do4 r r2 |
do8-! do'( reb' do') do-! do'( reb' do') |
do4\p r r2 |
do8-! do'( reb' do') sib,-! sib( do' sib) |
lab8 fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa |
mi mi mi mi mi mi mi mi |
fa fa fa fa sib, sib, sib, sib, |
do1 |
si, |
do8-!\f do'( reb' do') sib,-! sib( do' sib) |
la1\p |
sib8\f sib sib sib sib sib sib sib |
sol1\p |
lab8\f lab lab lab lab lab lab lab |
fa1\p |
sol8\f sol sol sol sol sol sol sol |
mi1\p |
fa8 la la sib sib si si do' |
do' reb' reb' sib, sib, sib, sib, sib, |
do8-!\f do'( reb' do') do-! do'( reb' do') |
do4 r r2 |
do8-!\f do'( reb' do') do-! do'( reb' do') |
do4 r r2 |
do8-!\p do'( reb' do') sib,-! sib( do' sib) |
lab,-! lab( sib lab) sol,-! sol( lab sol) |
fa,8 fa fa fa fa fa fa fa |
reb reb reb reb reb reb reb reb |
mib mib mib mib mib mib mib mib |
do do do do do do do do |
reb reb reb reb reb reb reb reb |
sib, sib, sib, sib, sib, sib, sib, sib, |
do8-!-\sug\f do'( reb' do') do-! do'( reb' do') |
do4-\sug\p do do do |
do8-!-\sug\f do'( reb' do') do-! do'( reb' do') |
do4-\sug\p do do do |
fa4 sib, r2 |
r4 lab, sib, do |
reb sib,\pocof r sib, |
do2 do, |
fa,\f la, |
sib, do |
reb1 |
do8-! do'( reb' do') do-! do'( reb' do') |
do4 r r2 |
do8-! do'( reb' do') do-! do'( reb' do') |
do4\p r r2 |
do8-! do'( reb' do') do-! do'( reb' do') |
mi2\f do |
fa4 sib, r2 |
r4 lab, sib, do |
reb sib, r sib, |
do2 do, |
fa,\fermata r |
%%%
r8 lab,-\sug\p do mib lab lab, |
r mib, sol, sib, mib mib, |
r mib,\pocof sol, sib, mib mib, |
r8 lab, do mib lab lab, |
reb2.\p |
do4 sib, lab, |
r8 mib, sol, sib, mib reb |
do2. |
do |
r8 reb, fa, lab, reb fa |
re!2. |
re |
r8 mib, sol, sib, mib reb |
do4 sol, lab, |
reb mib mib, |
fa, sol, lab, |
r reb\pocof do |
fa2\p sol4 |
lab8 do'( sib lab sol fa) |
mib lab(\pocof sol fa mib re!) |
mib2. |
lab, |
%%%
fa8 fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa |
mi mi mi mi mi mi mi mi |
fa4 sib, r2 |
r4 lab, sib, do |
reb sib, r sib, |
do2 do, |
fa, r |


