\clef "alto" \override Voice.Script.avoid-slur = #'inside
mi'4 r16. sol'32 si'16. red'32 mi'4-! r16. mi'32 sol'16. si32 |
do'16. sol'32 sol'16. sol'32 sol'4~ sol'2-\sug\p~ |
sol' si'~ |
si'~ si'~ |
si'2. r4 |
do'4-\sug\f r16. mi'32 sol'16. si32 do'16. sol'32 sol'16. sol'32 sol'4~ |
sol'1-\sug\p |
la'~ |
la'2 fa'~ |
fa'4 r r r8 sol'-\sug\f |
mi'4 r16. dod'32 mi'16. sold32 la16. mi'32 mi'16. mi'32 mi'4~ |
mi'1~ |
mi'2. r4 |
re'4 r16. fa'32 la'16. dod'32 re'4 r |
r16. fa'32 la'16. dod'32 re'4 r2 |
r la'-\sug\p~ |
la'2. r4 |
sol'4 sib do' r |
R1 |
r4 r8 la' sol'16. sol'32 sib'16. re'32 mib'16. mib'32 sol'16. sol32 |
lab16. mib'32 mib'16. mib'32 mib'4-! r2 |
r8. sol'16 sol'4 |
r2 r8. fa'16 fa'4-! |
r8. si'!16 si'4-! r2 |
r4 r8 re'' do''4 r16. mib'32 sol'16. si!32 |
do'4 r r8. sol'16 sol'4-! |
r8. mib'16 mib'4-! r8. mib'16 mib'4-! |
r2 r4 r8 re'' |
mib''4 r16. sol'32 sib'16. re'32 mib'4 r |
r16 sol'-.\p( sol'-. sol'-.) sol'-.( sol'-. sol'-. sol'-.) sol'2 |
r16 mib'-.( mib'-. mib'-.) mib'-.( mib'-. sib-. sib-.) do'4 r16. la'!32 do''16. mi'!32 |
fa'4 r r16 do'-\sug\p-.( do'-. do'-.) do'-.( do'-. do'-. do'-.) |
do'1~ |
do'4 r r16 reb'-.( reb'-. reb'-.) reb'-.( reb'-. reb'-. reb'-.) |
fa'1~ |
fa'4 r r8. mib'16-\sug\f mib'4-! |
r2 r8. mib'16 mib'4-! |
r8. re''!16 re''4-! r4 r8 re'' |
re''4 r r2 |
R1*7 |
r2
