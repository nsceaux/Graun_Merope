\key do \major \time 4/4 \midiTempo#80
s1*21
\set Score.measureLength = #(ly:make-moment 2/4) s2
\set Score.measureLength = #(ly:make-moment 4/4) s1*24 s2 \bar "|."
