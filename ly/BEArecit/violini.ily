\clef "treble" \override Voice.Script.avoid-slur = #'inside
r16. sol''32 si''16.-! red''32 mi''16.-! sol'32 si'16. red'32 mi'16. mi''32 sol''16.-! si'32 do''16.-! mi'32 sol'16. si32 |
do'16. <<
  \tag #'violino1 {
    mi''32 mi''16. mi''32 mi''4~ mi''2\p~ |
    mi'' fa''~ |
    fa''~ fa''~ |
    fa''2.
  }
  \tag #'violino2 {
    do''32 do''16. do''32 do''4~ do''2\p~ |
    do'' re''~ |
    re''~ re''~ |
    re''2.
  }
>> r4 |
r16. mi''32\f sol''16.-! si'32 do''16.-! mi'32 sol'16. si32 do'16. <<
  \tag #'violino1 {
    mi''32 mi''16. mi''32 mi''4~ |
    mi''1\p |
    fa''~ |
    fa''2 mib'' |
    re''4
  }
  \tag #'violino2 {
    do''32 do''16. do''32 do''4~ |
    do''1\p~ |
    do''~ |
    do'' |
    sib'4
  }
>> r4 r r8 <>\f <<
  \tag #'violino1 { mi''!8 | la''16. }
  \tag #'violino2 { sib'8 | la'16. }
>> dod''32 mi''16. sold'32 la'16. dod'32 mi'16. sold32 la16. <<
  \tag #'violino1 {
    dod''32 dod''16. dod''32 dod''4~ |
    dod''1~ |
    dod''2.
  }
  \tag #'violino2 {
    sol'!32 sol'16. sol'32 sol'4~ |
    sol'1~ |
    sol'2.
  }
>> r4 |
r16. fa''32\f la''16. dod''32 re''16. fa'32 la'16. dod'32 re'4 r |
r16. fa'32 la'16. dod'32 re'4 r2 |
r <>\p <<
  \tag #'violino1 { fad''2~ | fad''2. }
  \tag #'violino2 { do''2~ | do''2. }
>> r4 |
r16. <>\f sib'32 re''16. fad'32 sol'16. sol'32 sib'16. re'32 mib'4 r |
R1 |
r4 r8 <<
  \tag #'violino1 { fad''8 sol''16. }
  \tag #'violino2 { do''8 sib'16. }
>> sol'32 sib'16. re'32 mib'16. mib'32 sol'16. sol32 |
lab16. do''32 do''16. do''32 do''4-! r2 |
r8. <<
  \tag #'violino1 { reb''16 reb''4-! }
  \tag #'violino2 { si'16 si'4-! }
>>
r2 r8. <<
  \tag #'violino1 { do''16 do''4-! | }
  \tag #'violino2 { lab'16 lab'4-! | }
>>
r8. <<
  \tag #'violino1 { lab''16 lab''4-! }
  \tag #'violino2 { re''16 re''4-! }
>> r2 |
r4 r8 <<
  \tag #'violino1 { si''!8 do'''16. }
  \tag #'violino2 { fa''8 mib''16. }
>> mib''32 sol''16. si'!32 do''16. mib'32 sol'16. si32 |
do'4 r r8. <<
  \tag #'violino1 { mib''16 mib''4-! | }
  \tag #'violino2 { do''16 do''4-! | }
>>
r8. <<
  \tag #'violino1 { reb''16 reb''4-! }
  \tag #'violino2 { sib'16 sib'4-! }
>> r8. <<
  \tag #'violino1 { do''16 do''4-! }
  \tag #'violino2 { lab'16 lab'4-! }
>>
r2 r4 r8 <<
  \tag #'violino1 { lab''8 | sol''16. }
  \tag #'violino2 { fa''8 | mib''16. }
>> sol''32 sib''16. re''32 mib''16. sol'32 sib'16. re'32 mib'4 r4 |
r16 <>\p <<
  \tag #'violino1 {
    reb''(-. reb''-. reb''-.) reb''-.( reb''-. reb''-. reb''-.) reb''2 |
  }
  \tag #'violino2 {
    sib'16-.( sib'-. sib'-.) sib'-.( sib'-. sib'-. sib'-.) sib'2 |
  }
>>
r16 <<
  \tag #'violino1 {
    dob''-.( dob''-. dob''-.) dob''-.( dob''-. sib'-. sib'-.) la'!16.
  }
  \tag #'violino2 {
    lab'16-.( lab'-. lab'-.) lab'-.( lab'-. mib'-. mib'-.) fa'16.
  }
>> la''!32\f do'''16. mi''32 fa''16. la'32 do''16. mi'32 |
fa'4 r r16 <>\p <<
  \tag #'violino1 {
    la'16-.( la'-. la'-.) la'-.( la'-. la'-. la'-.) |
    la'1~ |
    la'4
  }
  \tag #'violino2 {
    mib'16-.( mib'-. mib'-.) mib'(-. mib'-. mib'-. mib'-.) |
    mib'1~ |
    mib'4
  }
>> r4 r16 <<
  \tag #'violino1 {
    sib'16-.( sib'-. sib'-.) sib'-.( sib'-. sib'-. sib'-.) |
    dob''1~ |
    dob''4
  }
  \tag #'violino2 {
    fa'16-.( fa'-. fa'-.) fa'-.( fa'-. fa'-. fa'-.) |
    lab'1~ |
    lab'4
  }
>> r4 r8. <>-\sug\f <<
  \tag #'violino1 { sib'16 sib'4-! | }
  \tag #'violino2 { solb'16 solb'4-! | }
>>
r2 r8. <<
  \tag #'violino1 { dob''16 dob''4-! | }
  \tag #'violino2 { lab'16 lab'4-! | }
>>
r8. <<
  \tag #'violino1 { sib''16 sib''4-! }
  \tag #'violino2 { fa''16 fa''4-! }
>> r4 r8 <<
  \tag #'violino1 { lab''8 }
  \tag #'violino2 { fa''8 }
>>
sol''!4 r r2 |
R1*7 |
r2
