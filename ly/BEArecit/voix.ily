\clef "soprano/treble" R1 |
r4 r8 sol' do''4 r |
do'' r8 do''16 re'' si'8 si' si'8. si'16 |
si'4 si'8. do''16 re''8 re'' r4 |
re''8 re'' re'' mi''16 fa'' fa''8 si' r16 si' si'16. do''32 |
do''8 sol' r4 r r8 sol' |
do'' do'' do'' do''16 re'' mi''8 mi'' re'' do'' |
fa'' fa'' r do'' la'4 do''8 do''16 la' |
fa'8 fa' r fa' mib''8. mib''16 re''8 do'' |
re'' re'' r16 re'' mi'' fa'' fa''8 do'' r4 |
r2 r4 r8 la'16 si' |
dod''8 dod'' r dod''16 re'' mi''8 mi'' r mi'' |
dod''8 dod'' dod''8. mi''16 mi''8 sol'16 sol' sol'8 la' |
fa' fa' r4 la'8 la'16 la' la'8. re''16 |
re''8 la' r4 r r8 la' |
re'' re''16 re'' re''8 mib'' do''4 r |
la'8 la'16 la' la'8 sib' do'' do'' do'' sib' |
sol' sol' r4 r sol'8 sol'16 sol' |
do''4 r8 mib'' do''8 do'' do'' sib' |
sol' sol' r4 r2 |
r do''8 do''16 do'' do''8 reb'' |
sib' sib' r4 |
reb''8 reb''16 reb'' reb''8. do''16 lab'8 lab' r16 lab' sol' fa' |
si'!8 si' r4 si'8 si'16 si' si'8 do'' |
do'' sol' r4 r2 |
sol'4 r8. sol'16 do''8 do'' r16 mib'' mib'' fa'' |
reb''8 reb'' r16 reb'' reb'' mib'' do''8 do'' r lab' |
re''!8 re'' re'' mib'' mib'' sib' r4 |
r2 sib'4 r8 mib'' |
mib'' sib' r4 reb''8 reb''16 reb'' reb''8 mib'' |
dob'' dob'' r4 r2 |
r4 r8 do''16. fa''32 fa''8 do'' r4 |
r8 la'! la' sib' do'' do'' r4 |
mib''8 mib''16 mib'' mib''8 reb'' sib' sib' r4 |
r8 lab' lab' dob'' lab' lab' r fa'' |
fa''8. lab'16 lab'8. sib'16 solb'8 solb' r4 |
sib'8 sib'16 sib' mib''8 sib' dob''4 r |
re''!8 re'' r mib'' mib'' sib' r4 |
re''!8. re''16 si'!8 si'16 re'' re''8 sol' r8. sol'16 |
si'8 si' si'8. do''16 re''4 re''8 re''16. sol''32 |
sol''8 re'' r16 re'' re'' mib'' fa''4 r8 fa'' |
re''8 re''16 re'' re''8 mib'' do'' do'' r sol'16. sol'32 |
do''8 do'' r do''16. reb''32 sib'4 r8 sol'' |
sol'' sib' r do'' lab' lab' r lab' |
reb'' reb'' reb''8. mib''16 do''8 do'' r4 |
do''8 do''16 do'' do''8. mib''16 \appoggiatura re''!8 do''8. do''16 do''8. si'!16 |
re''8 re'' r4
