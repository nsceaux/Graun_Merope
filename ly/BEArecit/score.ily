\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = \markup\character Merope }
    \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Cembalo" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*3\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break s1*2 s2 \bar "" \break s2 s1*2 s2\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3\break s1*2\pageBreak
        s1*3\break s1*2\break s1*3\break
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}