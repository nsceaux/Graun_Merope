\clef "bass" \override Voice.Script.avoid-slur = #'inside
mi4 r16. sol32 si16. red32 mi4-! r16. mi32 sol16. si,32 |
do16. do32 do16. do32 do4~ do2\p~ |
do1~ |
do2~ do~ |
do2. r4 |
do4\f r16. mi32 sol16. si,32 do16. sib,32 sib,16. sib,32 sib,4~ |
sib,1 |
la,~ |
la, |
sib,4 r r do\f |
dod r16. dod32 mi16. sold,32 la,16. la,32 la,16. la,32 la,4~ |
la,1\p~ |
la,2. r4 |
re4 r16. fa32 la16. dod32 re4 r |
r16. fa32 la16. dod32 re4 r2 |
r re\p~ |
re1 |
sol4 sib, do r |
R1 |
r4 r8 re mib16. sol32 sib16. re32 mib16. mib32 sol16. sol,32 |
lab,16. lab,32 lab,16. lab,32 lab,4-! r2 |
r8. mi!16 mi4 |
r2 r8. fa16 fa4 |
r8. fa16 fa4 r2 |
r4 r8 sol do4 r16. mib32 sol16. si,32 |
do4 r r8. do16 do4 |
r8. sol16 sol4 r8. lab16 lab4 |
r2 r4 r8 sib |
mib4 r16. sol32 sib16. re32 mib4 r |
r16 mib\p(-. mib-. mib-.) mib(-. mib-. mib-. mib-.) mib2 |
r16 lab( lab lab) lab( lab solb solb) fa4 r16. la!32 do'16. mi!32 |
fa4 r r16 fa\p(-. fa-. fa-.) fa-.( fa-. fa-. fa-.) |
fa1~ |
fa4 r r16 reb(-. reb-. reb)-. reb(-. reb-. reb-. reb)-. |
re!1~ |
re4 r r8. mib16\f mib4 |
r2 r8. lab16 lab4-! |
r8. lab16 lab4-! r4 r8 sib |
si!1~ |
si~ |
si~ |
si2 mib~ |
mib2 mi~ |
mi fa |
sol lab~ |
lab1 |
sol2
