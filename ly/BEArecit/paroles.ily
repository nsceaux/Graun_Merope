Eb -- ben: sì da -- gli stes -- si
miei pen -- sier dis -- pe -- ra -- ti
ren -- der tut -- to mi sen -- to il mio cor -- rag -- gio.
Nel tem -- pio, o -- ve m’at -- ten -- de il grand ol -- trag -- gio,
o -- mai tut -- ti cor -- ria -- mo,
e al Po -- po -- lo mo -- stria -- mo il Fi -- glio mi -- o.
Fra la Ma -- dre, e l’Al -- ta -- re
a -- gli oc -- chi suoi s’e -- spon -- ga in guar -- dia â Nu -- mi.
Na -- to è del san -- gue lo -- ro:
la sua in -- no -- cen -- za as -- sai
de -- vo -- no a -- ver tra -- di -- ta in fin ad o -- ra:
sì, pren -- de -- ran la sua di -- fe -- sa an -- co -- ra.
Del tra -- di -- to -- re in -- fa -- me
di -- pin -- ge -- rò il fu -- ro -- re,
e al cor d’o -- gnu -- no in -- spi -- re -- rò ven -- det -- ta.
Sì, Ti -- ran -- ni, te -- me -- te i gri -- di, te -- me -- te i pian -- ti
di Ma -- dre di -- spe -- ra -- ta… Ma… chi vie -- ne?

Ah mi s’ag -- ghiac -- cia il san -- gue!.. chi m’ap -- pel -- la?
Già il Fi -- glio mi -- o sov -- ra la tom -- ba il pie -- de,
E a un bat -- ter d’oc -- chio il bar -- ba -- ro Ti -- ran -- no,
pre -- ci -- pi -- tar vel può. Stel -- le! che af -- fan -- no!

Ah mi -- ni -- stri cru -- de -- li
di mo -- stro più cru -- del, sì, voi ve -- ni -- te
a stra -- sci -- nar la vit -- ti -- ma all’ Al -- ta -- re.
O Na -- tu -- ra! o do -- ver! Ven -- det -- ta! A -- mo -- re!
Che tut -- ti m’as -- sal -- ta -- te,
da un di -- spe -- ra -- to cor che più bra -- ma -- te?
