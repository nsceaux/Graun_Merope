\clef "bass" re fad8 la re' la fad re |
sol4-! re-! r8 fad fad fad |
sol sol sol sol la la la la |
re re re re fad fad fad fad |
sol sol sol sol la la la la |
re re re mi fad fad fad fad |
sol sol sol sol sold sold sold sold |
la la la la la la la la |
la la la la la la la la |
si4-! fad-! r8 fad sol la |
si4-! fad-! r8 fad sol la |
re re re re re4 r |
re4\p fad8 la re' la fad re |
sol4-! re-! r8 fad fad fad |
sol sol sol sol la la la la |
re re re re fad fad fad fad |
sol sol sol sol la la la la |
re\noBeam re-\sug\f fad la re' re' la fad |
re\noBeam re-\sug\p re re re re re re |
dod dod dod dod dod la, dod re |
mi mi, mi fad sold mi fad sold |
la la, la sold fad fad, fad mi |
re1 |
r8 mi\f sold si mi' mi' si sold |
mi\noBeam mi\p mi mi mi mi mi mi |
mi mi mi mi mi mi mi mi |
mi mi mi mi mi mi mi mi |
la, la, la, la, dod dod dod dod |
re re re re re re re re |
re re re re re re re re |
mi mi mi mi mi mi mi mi |
mi mi mi mi mi mi mi mi |
fad fad fad fad fad fad fad fad |
fad fad fad fad fad fad fad fad |
sold1 |
la4 r r8 <>_\markup { \italic poco \dynamic f } dod re mi |
fad4 dod\f r8 dod re mi |
la,4\ff dod8 mi la mi dod la, |
re4-! la-! r8 dod dod dod |
re re re re mi mi mi mi |
la, la, la, la, dod dod dod dod |
re re re re mi mi mi mi |
la,4. dod16 mi la8 sol! fad mi |
re4\p fad8 la re' la fad re |
sol4-!\f re-! r8 fad\p fad fad |
sol sol sol sol la la la la |
re re re re fad fad fad fad |
sol sol sol sol la la la la |
re4 r8 re'\f \appoggiatura re'16 dod'8( si) \appoggiatura si16 la8( sol) |
fad8\p fad fad fad fad fad fad fad |
<>_\markup { \italic poco \dynamic f } sol sol sol sol sol sol sol sol |
sold\p sold sold sold sold sold sold sold |
<>_\markup { \italic poco \dynamic f } la la la la la la la la |
la\p la la la la la la la |
<>_\markup { \italic poco \dynamic f } la la la la la la la la |
sold1\p |
la8\noBeam la,\f dod mi la la mi dod |
la,\noBeam la,\p dod mi la la mi dod |
la, la, la, la, la, la, la, la, |
re re re re re re re re |
sol sol sol sol sol sol sol sol |
dod dod dod dod dod dod dod dod |
fad fad fad fad fad fad fad fad |
si, si, si, si, si, si, si, si, |
mi mi la, la, la, la, la, la, |
re re sol, sol, sol, sol, sold, sold, |
la,1~ |
la,2. <>_\markup { \italic poco \dynamic f } la,4 |
si, dod re sol |
la\p la la, la, |
re4 r r8 <>_\markup { \italic poco \dynamic f } fad8 sol la |
si4-! fad-! r8 fad\f sol la |
re4 r fad8\ff fad fad fad |
sol sol sol sol sold sold sold sold |
la la la la la la la la |
la la la la la la la la |
si4-! fad-! r8 fad sol la |
si4-! fad-! r8 fad sol la |
re re re re re4 r |
