\clef "alto" re'4 fad'8 la' re'' la' fad' re' |
re'4-! re'-! r8 re' re' re' |
re' re' re' re' la' la' la' la' |
la' la' la' la' re' re' re' re' |
re' re' re' re' la' la' la' la' |
la'8.\trill sol'16 fad'8 mi' re' re' re' re' |
re' si' si' si' mi' mi' mi' mi' |
mi' dod'' dod'' dod'' dod' dod' dod' dod' |
re' re' re' re' sol' sol' sol' sol' |
fad'4-! la'-! r8 fad' sol' la' |
si'4-! la'-! r8 fad' sol' la' |
re' fad fad fad fad4 r |
re'4\p fad'8 la' re'' la' fad' re' |
re'4-! re'-! r8 fad fad fad |
sol sol sol sol la la la la |
re' re' re' re' fad fad fad fad |
sol sol sol sol la la la la |
re'\noBeam re'-\sug\f fad' la' re'' re'' la' fad' |
re'\noBeam fad'-\sug\p fad' fad' fad' fad' fad' fad' |
mi' mi' mi' mi' mi' la dod' re' |
mi' mi mi' fad' sold' mi' fad' sold' |
la' la la' sold' fad' fad fad' mi' |
re'1 |
r8 mi'\f sold' si' mi'' mi'' si' sold' |
mi'\noBeam sold'8\p sold' sold' sold' sold' sold' sold' |
r8 fad' fad' fad' fad' fad' fad' fad' |
r8 sold' sold' sold' sold' sold' sold' sold' |
la' mi' mi' mi' mi' mi' mi' mi' |
re' re' re' re' re' re' re' re' |
re' re' re' re' re' re' re' re' |
mi' mi' mi' mi' mi' mi' mi' mi' |
mi' mi' mi' mi' mi' mi' mi' mi' |
fad' fad' fad' fad' fad' fad' fad' fad' |
fad' fad' fad' fad' fad' fad' fad' fad' |
mi'1 |
mi'4 r r8 <>_\markup { \italic poco \dynamic f } dod'-! re'-! mi'-!
fad'4-! mi'-!\f r8 dod'-! re'-! mi'-!
la4\ff dod'8 mi' la' mi' dod' la |
la'4-! la'-! r8 la la la |
la la la la mi' mi' mi' mi' |
mi' mi' mi' mi' la la la la |
la la la la mi' mi' mi' mi' |
mi'8.\trill re'16 dod'4 r2 |
re'4\p fad'8 la' re'' la' fad' re' |
re'4-!\f re'-! r8 fad\p fad fad |
sol sol sol sol la la la la |
re' re' re' re' fad fad fad fad |
sol sol sol sol la la la la |
re'4 r8 re''\f \appoggiatura re''16 dod''8( si') \appoggiatura si'16 la'8( sol') |
fad'8\p re' re' re' re' re' re' re' |
<>_\markup { \italic poco \dynamic f } re' re' re' re' re' re' re' re' |
mi'\p mi' mi' mi' mi' mi' mi' mi' |
<>_\markup { \italic poco \dynamic f } mi' mi' mi' mi' mi' mi' mi' mi' |
dod'\p dod' dod' dod' dod' dod' dod' dod' |
<>_\markup { \italic poco \dynamic f } re' re' re' re' re' re' re' re' |
re'1\p |
la'8\noBeam la\f dod' mi' la' la' mi' dod' |
la\noBeam la\p dod' mi' la' la' mi' dod' |
la mi' mi' mi' mi' mi' mi' mi' |
re' re' re' re' re' re' re' re' |
sol' sol' sol' sol' sol' sol' sol' sol' |
dod' dod' dod' dod' dod' dod' dod' dod' |
fad' fad' fad' fad' fad' fad' fad' fad' |
si si si si si si si si |
si si la la la la la la |
la la sol sol sol sol sold sold |
la la la la la la la la |
la la la la la la la la |
<>_\markup { \italic poco \dynamic f } si4 dod' re' si' |
la'8 la'-\sug\p la' la' sol' sol' sol' sol' |
fad'4 r r8 <>_\markup { \italic poco \dynamic f } fad' sol' la' |
si'4-! la'-! r8 fad'\f sol' la' |
re'4 r re'8-\sug\ff re' re' re' |
re' si' si' si' mi' mi' mi' mi' |
mi' dod'' dod'' dod'' dod' dod' dod' dod' |
re' re' re' re' sol' sol' sol' sol' |
fad'4-! la'-! r8 fad' sol' la' |
si'4-! la'-! r8 fad' sol' la' |
re' fad fad fad fad4 r |

