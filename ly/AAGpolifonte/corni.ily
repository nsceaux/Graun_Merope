\clef "treble" \transposition re
do''4 mi''8 sol'' do''' sol'' mi'' do'' |
<<
  \tag #'(corno1 corni) { la''4 sol'' }
  \tag #'(corno2 corni) { fa''4 mi'' }
  { s4-! s-! }
>> r8 do'' do'' do'' |
do''4 r8 <<
  \tag #'(corno1 corni) { la''8 sol'' fa'' mi'' re'' | mi''4 }
  \tag #'(corno2 corni) { fa''8 mi'' re'' do'' sol' | do''4 }
>> r4 r8 do'' do'' do'' |
do''4 r8 <<
  \tag #'(corno1 corni) { do'''8 si'' la'' sol'' fa'' | mi''4 }
  \tag #'(corno2 corni) { la''8 sol'' fa'' mi'' re'' | do''4 }
>> r4 r8 do'' do'' do'' |
do''4 r r8 re'' re'' re'' |
re''4 r <<
  \tag #'(corno1 corni) { fa''2 | mi'' re'' | }
  \tag #'(corno2 corni) { re''2 | do'' sol' | }
>>
do''4 r r8 mi'' fa'' sol'' |
la''4 r r8 mi'' fa'' sol'' |
do''8 <<
  \tag #'(corno1 corni) { sol'8 sol' sol' sol'4 }
  \tag #'(corno2 corni) { mi'8 mi' mi' mi'4 }
>> r4 |
R1*5 |
r8 <<
  \tag #'(corno1 corni) {
    do''8 mi'' sol'' do''' do''' sol'' mi'' | do'' }
  \tag #'(corno2 corni) {
    do' mi' sol' do'' do'' sol' mi' | do' }
>> r8 r4 r2 |
R1 |
re''1\p |
re''4 r r2 |
<>\p <<
  \tag #'(corno1 corni) { mi''1 }
  \tag #'(corno2 corni) { do''1 }
>>
r8 re''\f re'' re'' re'' re'' re'' re'' |
re'' r r4 r2 |
R1*2 |
sol'1\p |
do''4 r r2 |
do''1 |
re''4 r r2 |
re''1 |
mi''4 r r2 |
mi''1 |
re''4 r r2 |
R1*2 |
<>\ff <<
  \tag #'(corno1 corni) {
    re''4 re''8 re'' re'' re'' re'' re'' |
    mi''4 re'' s8 re'' re'' re'' |
    mi''4 s8 mi'' s2 |
    re''4 s s s8 re'' |
    mi''4 s8 mi'' s2 |
    re''4
  }
  \tag #'(corno2 corni) {
    sol'4 sol'8 sol' sol' sol' sol' sol' |
    do''4 sol' s8 sol' sol' sol' |
    do''4 s8 do'' s2 |
    sol'4 s s s8 sol' |
    do''4 s8 do'' s2 |
    sol'4
  }
  { s1 |
    s4-! s-! r8 s4. |
    s4 r8 s re''4 re'' |
    s4 r r r8 s |
    s4 r8 s  re''4 re'' }
>> r4 r2 |
R1*6 |
do''1\p |
r8 <>_\markup { \italic poco \dynamic f } do'' do'' do'' do'' do'' do'' do'' |
re''1\p |
r8 re'' re'' re'' re'' re'' re'' re'' |
<<
  \tag #'(corno1 corni) {
    re''1 |
    mi''1 |
    mi'' |
    s8 re'' re'' re'' re'' re'' re'' re'' |
    re'' }
  \tag #'(corno2 corni) {
    sol'1 |
    do'' |
    do'' |
    s8 sol' sol' sol' sol' sol' sol' sol' |
    sol' }
  { s1\p |
    s1_\markup { \italic poco \dynamic f } |
    s1 |
    r8 <>\f }
>> r8 r4 r2 |
R1 |
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { mi''1 }
  { do'' }
>>
R1*6 |
<>\pp sol'1~ |
sol'2~ sol'4 r |
R1*2 |
do''4 r r8 <>_\markup { \italic poco \dynamic f } mi''8 fa'' sol'' |
la''4 r r8 <>\f mi'' fa'' sol'' |
do''4 r r8 do'' do'' do'' |
do''4 r r8 re'' re'' re'' |
re''4 r <<
  \tag #'(corno1 corni) { fa''2 | mi'' re'' | }
  \tag #'(corno2 corni) { re''2 | do'' sol' | }
>>
do''4 r r8 mi'' fa'' sol'' |
la''4 r r8 mi'' fa'' sol'' |
do''8 <<
  \tag #'(corno1 corni) { sol' sol' sol' }
  \tag #'(corno2 corni) { mi' mi' mi' }
>> sol'4 r4
