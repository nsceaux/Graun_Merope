\clef "soprano/treble" R1*12 |
re'4 fad'8 la' re''[ la'] fad'[ re'] |
si'4 la' la2 |
si4. si'8 \appoggiatura si'16 la'8[ sol'] \appoggiatura sol'16 fad'8[ mi'] |
fad'8.\trill[ sol'16] la'4 la2 |
si4. re''8 \appoggiatura re''16 dod''8([ si']) \appoggiatura si'16 la'8([ sol']) |
fad'8.\trill[ mi'16] re'4 r2 |
re'4 fad'8 la' re''[ la'] fad'[ re'] |
la'4 la la'2~ |
la'4 sold'8 la' si'8*2/3[ la' sold'] fad'[ mi' re'] |
dod'4 r la'2~ |
la'8[ sold'] fad' mi' re'[ dod'] si[ la] |
mi'4 r r2 |
si'4 si'8 si' si'[ sold'] fad'[ mi'] |
mi'[ red'] red'2. |
si'4 si'8 si' si'4 re'!4 |
dod' r la'2~ |
la'8.[\melisma sol'16 fad'8.\trill mi'16] fad'8.[\trill mi'16 re'8.\trill dod'16] |
si8[ dod'16 re'] mi'[ fad' sold' la'] si'2~ |
si'8.[ la'16 sold'8.\trill fad'16] sold'8.[ fad'16 mi'8.\trill re'16] |
dod'8[ re'16 mi'] fad'[ sold' la' si'] dod''2~ |
dod''8.[ si'16 la'8.\trill sold'16] la'8.[ sold'16 fad'8.\trill mi'16] |
re'8[ mi'16 fad'] sold'[ la' si' dod''] re''2~ |
re''8[ dod'']\melismaEnd si' la' \appoggiatura la'16 sold'8[ fad'] \appoggiatura fad'16 mi'8[ re'] |
dod'4 r r8 dod' re' mi' |
fad'4 r r8 dod' re' mi' |
la2 r |
R1*5 |
re'4 fad'8 la' re''[ la'] fad'[ re'] |
si'4 la' la2 |
si4. si'8 \appoggiatura si'16 la'8[ sol'] \appoggiatura sol'16 fad'8[ mi'] |
fad'8.[ sol'16] la'4 la2 |
si4. re''8 \appoggiatura re''16 dod''8[ si'] \appoggiatura si'16 la'8[ sol'] |
fad'8.\trill[ mi'16] re'4 r2 |
la'4 la'8 la' la'4 do' |
si8.[ la16] sol4 r2 |
si'4 si'8 si' si'4 re' |
dod'8.[\trill si16] la4 r2 |
sol'4 la8 la la4 sol' |
fad' r r2 |
re''4 fad'8 fad' fad'4 mi'8.[\trill re'16] |
la'4 r r2 |
la4 dod'8 mi' la'4 mi'8[ dod'] |
la[ sol'] sol'2. |
fad'4.\trill\melisma sol'8 la'4.\trill sol'16[ fad'] |
si'4.\trill re''16[ dod''] si'8.[\trill la'16] sol'8.\trill[ fad'16] |
mi'4. fad'8 sol'4.\trill fad'16[ mi'] |
la'4. dod''16[ si'] la'8.\trill[ sol'16] fad'8.[\trill mi'16] |
re'4 re''~ re''8[ dod''16 si'] \appoggiatura si'16 la'8[ sol'16 fad'] |
sol'4 dod''~ dod''8[ si'16 la'] \appoggiatura la'16 sol'8[ fad'16 mi'] |
fad'4 si'~ si'8[ la'16 sol'] \appoggiatura sol'16 fad'8[ mi'16 re'] |
dod'4\trill re'\trill mi'\trill fad'\trill |
sol'\trill la'\trill si'\trill dod''\trill |
re''4 dod''8.[\trill si'16]\melismaEnd la'8.\trill[ sol'16] fad'8.\trill[ mi'16] |
fad'16([ mi'] re'4.) mi'2\trill |
re'4 r r8 fad' sol' la' |
si'4 r r8 fad' sol' la' |
re'4 r4 r2 |
R1*6 |
