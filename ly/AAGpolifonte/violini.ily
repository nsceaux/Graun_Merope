\clef "treble" re'' fad''8 la'' re''' la'' fad'' re'' |
<<
  \tag #'(violino1 violini) { si''4 la'' }
  \tag #'(violino2 violini) { sol''4 fad'' }
  { s4-! s-! }
>> la'4.(\trill sol'16 la') |
si'4 r8 <<
  \tag #'(violino1 violini) {
    si''8 \appoggiatura si''16 la''8( sol'') \appoggiatura sol''16 fad''8( mi'') |
    fad''8.\trill sol''16 la''4 }
  \tag #'(violino2 violini) {
    sol''8 \appoggiatura sol''16 fad''8( mi'') \appoggiatura mi''16 re''8( dod'') |
    re''8.\trill mi''16 fad''4 }
>> la'4.\trill sol'16 la' |
si'4 r8 <<
  \tag #'(violino1 violini) {
    re'''8 \appoggiatura re'''16 dod'''8( si'') \appoggiatura si''16 la''8( sol'') |
    fad''8.\trill mi''16 }
  \tag #'(violino2 violini) {
    si''8 \appoggiatura si''16 la''8( sol'') \appoggiatura sol''16 fad''8( mi'') |
    re''8.\trill dod''16 }
>> re''4 la''16 do'' si' do'' re'' do'' si' do'' |
si' sol'' fad'' sol'' la'' sol'' fad'' sol'' re''' re'' dod''! re'' re''' re'' dod'' re'' |
dod'' la'' sold'' la'' si'' la'' sold'' la'' la' sol'' fad'' sol'' la'' sol'' fad'' sol'' |
la' fad'' mi'' fad'' sol'' fad'' mi'' fad'' la' mi'' re'' mi'' fad'' mi'' re'' mi'' |
re''4-! <<
  \tag #'(violino1 violini) {
    re'''8.\trill( dod'''32 si'') la''8 }
  \tag #'(violino2 violini) {
    re''8.\trill( dod''32 si') la'8 }
>> fad'8[ sol' la'] |
si'4-! <<
  \tag #'(violino1 violini) {
    re'''8.\trill( dod'''32 si'') la''8 }
  \tag #'(violino2 violini) {
    re''8.\trill( dod''32 si') la'8 }
>> fad'8[ sol' la'] |
re'8 re'16 la re'8 re'16 la re'4 r |
re''4\p fad''8 la'' re''' la'' fad'' re'' |
<<
  \tag #'(violino1 violini) { si''4 la'' }
  \tag #'(violino2 violini) { sol''4 fad'' }
  { s4-! s-! }
>> la4.(\trill\p sol16 la) |
si4 r8 <<
  \tag #'(violino1 violini) {
    si''8 \appoggiatura si''16 la''8( sol'') \appoggiatura sol''16 fad''8( mi'') |
    fad''8.\trill sol''16 la''4 }
  \tag #'(violino2 violini) { sol''8 \appoggiatura sol''16 fad''8( mi'') \appoggiatura mi''16 re''8( dod'') |
    re''8.\trill mi''16 fad''4 }
>> la4.(\trill sol16 la) |
si4 r8 <<
  \tag #'(violino1 violini) {
    re'''8 \appoggiatura re'''16 dod'''8( si'') \appoggiatura si''16 la''8( sol'') |
    fad''8\noBeam }
  \tag #'(violino2 violini) {
    si''8 \appoggiatura si''16 la''8( sol'') \appoggiatura sol''16 fad''8( mi'') |
    re''8\noBeam }
>> re''\f fad'' la'' re''' re''' la'' fad'' |
re''\noBeam re''\p fad'' la'' re''' la'' fad'' re'' |
la''8.(\trill sold''32 la'') la'4 \twoVoices #'(violino1 violino2 violini) <<
  { la''2~ |
    la''4 sold''8 la'' si''8*2/3[ la'' sold''] fad''[ mi'' re''] |
    dod''4 r la''2~ |
    la''8 sold'' fad'' mi'' re'' dod'' si' la' |
    sold' }
  { r8 dod'' mi'' fad'' |
    si'4 mi'8 mi' mi' sold' la' si' |
    mi' mi' dod'' si' la' dod'' la'' sold'' |
    fad'' mi'' re'' dod'' si' la' sold' fad' |
    mi'8 }
>> mi'8\f sold' si' mi'' mi'' si' sold' |
mi'\noBeam <<
  \tag #'(violino1 violini) {
    mi''8 mi'' mi'' mi'' mi'' mi'' mi'' |
    s red'' red'' red'' red'' red'' red'' red'' |
    s re''! re'' re'' re'' re'' re'' re'' |
    dod'' }
  \tag #'(violino2 violini) {
    si'8 si' si' si' si' si' si' |
    s8 la' la' la' la' la' la' la' |
    s si' si' si' si' si' si' si' |
    la'8 }
  { s4.\p s2 | r8 s4. s2 | r8 s4. s2 | }
>> la'8 dod'' mi'' la'' mi'' dod'' la' |
fad''8 <<
  \tag #'(violino1 violini) {
    fad''8 fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sold'' sold'' sold'' sold'' sold'' sold'' sold'' sold'' |
    sold'' sold'' sold'' sold'' sold'' sold'' sold'' sold'' |
    la'' la'' la'' la'' la'' la'' la'' la'' |
    la'' la'' la'' la'' la'' la'' la'' la'' |
    si''1 |
    mi''4 la''8.(\trill sold''32 fad'') mi''8 }
  \tag #'(violino2 violini) {
    la'8 la' la' la' la' la' la' |
    si' si' si' si' si' si' si' si' |
    si' si' si' si' si' si' si' si' |
    dod'' dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    dod'' dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    re''1 |
    dod''4 la'8.(\trill sold'32 fad') mi'8 }
  { s2.. s1*6 |
    s4-! <>_\markup { \italic poco \dynamic f } }
>> dod'8[-! re'-! mi']-! |
fad'4-! <>\f <<
  \tag #'(violino1 violini) {
    la''8.(\trill sold''32 fad'') mi''8 }
  \tag #'(violino2 violini) {
    la'8.(\trill sold'32 fad') mi'8 }
>> dod'8[-! re'-! mi']-! |
la'4\ff dod''8 mi'' la'' mi'' dod'' la' |
<<
  \tag #'(violino1 violini) { fad''4 mi'' }
  \tag #'(violino2 violini) { re'' dod'' }
  { s4-! s-! }
>> mi'4.(\trill re'16 mi') |
fad'4 r8 <<
  \tag #'(violino1 violini) {
    fad''8 \appoggiatura fad''16 mi''8( re'') \appoggiatura re''16 dod''8( si') |
    dod''8.\trill re''16 mi''4 }
  \tag #'(violino2 violini) {
    re''8 \appoggiatura re''16 dod''8( si') \appoggiatura si'16 la'8( sold') |
    la'8.\trill si'16 dod''4 }
>> mi'4.(\trill re'16 mi') |
fad'4 r8 <<
  \tag #'(violino1 violini) {
    la''8 \appoggiatura la''16 sold''8( fad'') \appoggiatura fad''16 mi''8( re'') |
    dod''8.\trill si'16 }
  \tag #'(violino2 violini) {
    fad''8 \appoggiatura fad''16 mi''8( re'') \appoggiatura re''16 dod''8( si') |
    la'8.\trill sold'16 }
>> la'4 r2 |
re''4\p fad''8 la'' re''' la'' fad'' re'' |
<<
  \tag #'(violino1 violini) { si''4 la'' }
  \tag #'(violino2 violini) { sol''4 fad'' }
  { s4-!\f s-! }
>> la4.(\trill\p sol16 la) |
si4 r8 <<
  \tag #'(violino1 violini) {
    si''8 \appoggiatura si''16 la''8( sol'') \appoggiatura sol''16 fad''8( mi'') |
    fad''8.\trill sol''16 la''4 }
  \tag #'(violino2 violini) {
    sol''8 \appoggiatura sol''16 fad''8( mi'') \appoggiatura mi''16 re''8( dod'') |
    re''8.\trill mi''16 fad''4 }
>> la4.(\trill sol16 la) |
si4 r8 <<
  \tag #'(violino1 violini) {
    re'''8 \appoggiatura re'''16 dod'''8( si'') \appoggiatura si''16 la''8( sol'') |
    fad''8.\trill mi''16 }
  \tag #'(violino2 violini) {
    si''8 \appoggiatura si''16 la''8( sol'') \appoggiatura sol''16 fad''8( mi'') |
    re''8.\trill dod''16 }
>> re''8\noBeam re''\f \appoggiatura re''16 dod''8 si' \appoggiatura si'16 la'8 sol' |
la''16\p do'' si' do'' re'' do'' si' do'' la'' do'' si' do'' re'' do'' si' do'' |
<>_\markup { \italic poco \dynamic f } si'16 sol'' fad'' sol'' la'' sol'' fad'' sol'' si' sol'' fad'' sol'' la'' sol'' fad'' sol'' |
si''\p re'' dod''! re'' mi'' re'' dod'' re'' si'' re'' dod'' re'' mi'' re'' dod'' re'' |
<>_\markup { \italic poco \dynamic f } dod'' la'' sold'' la'' si'' la'' sold'' la'' dod'' la'' sold'' la'' si'' la'' sold'' la'' |
la'\p sol''! fad'' sol'' la'' sol'' fad'' sol'' la' sol'' fad'' sol'' la'' sol'' fad'' sol'' |
<>_\markup { \italic poco \dynamic f } la' fad'' mi'' fad'' sol'' fad'' mi'' fad'' la' fad'' mi'' fad'' sol'' fad'' mi'' fad'' |
sold'8\noBeam <>\p <<
  \tag #'(violino1 violini) {
    fad''8 fad'' fad'' fad'' fad'' fad'' fad'' | mi''\noBeam }
  \tag #'(violino2 violini) {
    re''8 re'' re'' re'' re'' re'' re'' | dod''\noBeam }
>> la'8\f dod'' mi'' la'' la'' mi'' dod'' |
la'\noBeam la\p dod' mi' la' la' mi' dod' |
la <<
  \tag #'(violino1 violini) {
    dod''8 dod'' dod'' dod'' dod'' dod'' dod'' |
    re'' la'' la'' la'' la'' la'' la'' la'' |
    si'' si'' si'' si'' si'' si'' si'' si'' |
    si'' si'' si'' si'' si'' si'' si'' si'' |
    la'' la'' la'' la'' la'' la'' la'' la'' |
    la'' la'' la'' la'' la'' la'' la'' la'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    fad'' fad'' si'' si'' si'' si'' re'' re'' |
    dod'' dod'' re'' re'' mi'' mi'' fad'' fad'' |
    sol'' sol'' la'' la'' si'' si'' dod''' dod''' }
  \tag #'(violino2 violini) {
    sol'8 sol' sol' sol' sol' sol' sol' |
    fad' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    re'' re'' dod'' dod'' dod'' dod'' dod'' dod'' |
    re'' re'' re'' re'' re'' re'' si' si' |
    la' la' si' si' dod'' dod'' re'' re'' |
    mi'' mi'' fad'' fad'' sol'' sol'' mi'' mi'' | }
>>
\twoVoices #'(violino1 violino2 violini) <<
  { re'''8. re'''16 dod'''8.\trill si''16 la''8.\trill sol''16 fad''8.\trill mi''16 |
    fad''8 fad'' fad'' fad'' mi'' mi'' mi'' mi'' | }
  { re''8. si''16 la''8. sol''16 fad''8.\trill mi''16 re''4~ |
    re''8 re'' re'' re'' dod'' dod'' dod'' dod'' | }
  { s1_\markup { \italic poco \dynamic f }
    s8 s4.\p s2 }
>>
re''4 <<
  \tag #'(violino1 violini) {
    re'''8.\trill( dod'''32 si'') la''8
  }
  \tag #'(violino2 violini) {
    re''8.\trill( dod''32 si') la'8
  }
  { s4_\markup { \italic poco \dynamic f } }
>> fad'8[ sol' la'] |
si'4-! <>\f <<
  \tag #'(violino1 violini) {
    re'''8.\trill( dod'''32 si'') la''8 }
  \tag #'(violino2 violini) {
    re''8.\trill( dod''32 si') la'8 }
>> fad'8[ sol' la'] |
re'16\ff la' si' dod'' re'' mi'' fad'' sol'' la'' do'' si' do'' re'' do'' si' do'' |
si' sol'' fad'' sol'' la'' sol'' fad'' sol'' si'' re'' dod''! re'' mi'' re'' dod'' re'' |
dod'' la'' sold'' la'' si'' la'' sold'' la'' la' sol''! fad'' sol'' la'' sol'' fad'' sol'' |
la' fad'' mi'' fad'' sol'' fad'' mi'' fad'' la' mi'' re'' mi'' fad'' mi'' re'' mi'' |
re''4-! <<
  \tag #'(violino1 violini) {
    re'''8.\trill( dod'''32 si'') la''8 }
  \tag #'(violino2 violini) {
    re''8.\trill( dod''32 si') la'8 }
>> fad'8[ sol' la'] |
si'4-! <>\f <<
  \tag #'(violino1 violini) {
    re'''8.\trill( dod'''32 si'') la''8 }
  \tag #'(violino2 violini) {
    re''8.\trill( dod''32 si') la'8 }
>> fad'8[ sol' la'] |
re'8 re'16 la re'8 re'16 la re'4 r |
