\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { instrumentName = "Corni" } <<
        \keepWithTag #'corni \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with { instrumentName = "Canto" } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { instrumentName = "Basso" } <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*5\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*6\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}